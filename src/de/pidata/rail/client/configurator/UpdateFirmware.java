package de.pidata.rail.client.configurator;

import de.pidata.gui.controller.base.Controller;
import de.pidata.gui.controller.base.GuiOperation;
import de.pidata.models.tree.Model;
import de.pidata.qnames.QName;
import de.pidata.service.base.ServiceException;

public class UpdateFirmware extends GuiOperation {

  /**
   * Called to execute this operation.
   * The eventID is <UL>
   * <LI>the actionDef's ID if source is a ButtonController</LI>
   * <LI>the command if source is a short cut</LI>
   * <LI>the selected column if source is a TableController's rowAction</LI>
   * </UL>
   *
   * @param eventID     the id of the event, see method description
   * @param sourceCtrl  original source of the event, may differ from eventGroup, e.g. when closing application
   * @param dataContext
   * @throws ServiceException if operation fails
   */
  @Override
  public void execute( QName eventID, Controller sourceCtrl, Model dataContext ) throws ServiceException {
    showMessage( sourceCtrl, "Firmware-Update", "Firmware-Update wird von der CTC-App für iOS nicht unterstützt.\nBitte verwenden Sie die Desktop-Version der CTC-App." );
  }
}