package de.pidata.rail.ios;

import de.pidata.gui.ios.UIFactoryIOS;
import de.pidata.gui.ios.controller.UIViewControllerPI;

public class UIFactoryRailIOS extends UIFactoryIOS {

  @Override
  public UIViewControllerPI getViewController( String nibname ) {
    if (nibname.equals( "" )) {
      return null;
    }
    else {
      return super.getViewController( nibname );
    }
  }
}
