//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../z21-drive/src/main/java/z21Drive/Z21.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "IOSPrimitiveArray.h"
#include "J2ObjC_source.h"
#include "java/io/IOException.h"
#include "java/io/PrintStream.h"
#include "java/lang/Byte.h"
#include "java/lang/Runnable.h"
#include "java/lang/Runtime.h"
#include "java/lang/System.h"
#include "java/lang/Thread.h"
#include "java/net/DatagramPacket.h"
#include "java/net/DatagramSocket.h"
#include "java/net/InetAddress.h"
#include "java/net/SocketException.h"
#include "java/util/ArrayList.h"
#include "java/util/List.h"
#include "java/util/Timer.h"
#include "java/util/TimerTask.h"
#include "java/util/logging/Logger.h"
#include "z21Drive/Z21.h"
#include "z21Drive/actions/Z21Action.h"
#include "z21Drive/actions/Z21ActionGetSerialNumber.h"
#include "z21Drive/actions/Z21ActionLanLogoff.h"
#include "z21Drive/broadcasts/BroadcastTypes.h"
#include "z21Drive/broadcasts/Z21Broadcast.h"
#include "z21Drive/broadcasts/Z21BroadcastLanXLocoInfo.h"
#include "z21Drive/broadcasts/Z21BroadcastLanXProgrammingMode.h"
#include "z21Drive/broadcasts/Z21BroadcastLanXShortCircuit.h"
#include "z21Drive/broadcasts/Z21BroadcastLanXTrackPowerOff.h"
#include "z21Drive/broadcasts/Z21BroadcastLanXTrackPowerOn.h"
#include "z21Drive/broadcasts/Z21BroadcastLanXUnknownCommand.h"
#include "z21Drive/broadcasts/Z21BroadcastListener.h"
#include "z21Drive/responses/ResponseTypes.h"
#include "z21Drive/responses/Z21Response.h"
#include "z21Drive/responses/Z21ResponseGetSerialNumber.h"
#include "z21Drive/responses/Z21ResponseLanXCVNACK.h"
#include "z21Drive/responses/Z21ResponseLanXCVResult.h"
#include "z21Drive/responses/Z21ResponseLanXGetFirmwareVersion.h"
#include "z21Drive/responses/Z21ResponseListener.h"
#include "z21Drive/responses/Z21ResponseRailcomDatachanged.h"

#if !__has_feature(objc_arc)
#error "z21Drive/Z21 must be compiled with ARC (-fobjc-arc)"
#endif

#pragma clang diagnostic ignored "-Wprotocol"

@interface Z21DriveZ21 () {
 @public
  jboolean exit_;
  id<JavaUtilList> responseListeners_;
  id<JavaUtilList> broadcastListeners_;
  JavaUtilTimer *keepAliveTimer_;
  JavaNetInetAddress *z21Address_;
}

@end

J2OBJC_FIELD_SETTER(Z21DriveZ21, responseListeners_, id<JavaUtilList>)
J2OBJC_FIELD_SETTER(Z21DriveZ21, broadcastListeners_, id<JavaUtilList>)
J2OBJC_FIELD_SETTER(Z21DriveZ21, keepAliveTimer_, JavaUtilTimer *)
J2OBJC_FIELD_SETTER(Z21DriveZ21, z21Address_, JavaNetInetAddress *)

inline NSString *Z21DriveZ21_get_staticSync(void);
static NSString *Z21DriveZ21_staticSync = @"Sync";
J2OBJC_STATIC_FIELD_OBJ_FINAL(Z21DriveZ21, staticSync, NSString *)

inline jint Z21DriveZ21_get_port(void);
#define Z21DriveZ21_port 21105
J2OBJC_STATIC_FIELD_CONSTANT(Z21DriveZ21, port, jint)

inline JavaNetDatagramSocket *Z21DriveZ21_get_socket(void);
inline JavaNetDatagramSocket *Z21DriveZ21_set_socket(JavaNetDatagramSocket *value);
static JavaNetDatagramSocket *Z21DriveZ21_socket;
J2OBJC_STATIC_FIELD_OBJ(Z21DriveZ21, socket, JavaNetDatagramSocket *)

@interface Z21DrivePacketConverter ()

+ (IOSByteArray *)toPrimitiveWithJavaLangByteArray:(IOSObjectArray *)inArg;

@end

__attribute__((unused)) static IOSByteArray *Z21DrivePacketConverter_toPrimitiveWithJavaLangByteArray_(IOSObjectArray *inArg);

@interface Z21DriveZ21_1 : NSObject < Z21DriveBroadcastsZ21BroadcastListener >

- (instancetype)init;

- (void)onBroadCastWithZ21DriveBroadcastsBroadcastTypes:(Z21DriveBroadcastsBroadcastTypes *)type
                     withZ21DriveBroadcastsZ21Broadcast:(Z21DriveBroadcastsZ21Broadcast *)broadcast;

- (IOSObjectArray *)getListenerTypes;

@end

J2OBJC_EMPTY_STATIC_INIT(Z21DriveZ21_1)

__attribute__((unused)) static void Z21DriveZ21_1_init(Z21DriveZ21_1 *self);

__attribute__((unused)) static Z21DriveZ21_1 *new_Z21DriveZ21_1_init(void) NS_RETURNS_RETAINED;

__attribute__((unused)) static Z21DriveZ21_1 *create_Z21DriveZ21_1_init(void);

@interface Z21DriveZ21_2 : JavaUtilTimerTask {
 @public
  Z21DriveZ21 *this$0_;
}

- (instancetype)initWithZ21DriveZ21:(Z21DriveZ21 *)outer$;

- (void)run;

@end

J2OBJC_EMPTY_STATIC_INIT(Z21DriveZ21_2)

__attribute__((unused)) static void Z21DriveZ21_2_initWithZ21DriveZ21_(Z21DriveZ21_2 *self, Z21DriveZ21 *outer$);

__attribute__((unused)) static Z21DriveZ21_2 *new_Z21DriveZ21_2_initWithZ21DriveZ21_(Z21DriveZ21 *outer$) NS_RETURNS_RETAINED;

__attribute__((unused)) static Z21DriveZ21_2 *create_Z21DriveZ21_2_initWithZ21DriveZ21_(Z21DriveZ21 *outer$);

@interface Z21DriveZ21_$Lambda$1 : NSObject < JavaLangRunnable > {
 @public
  Z21DriveZ21 *target$_;
}

- (void)run;

@end

J2OBJC_EMPTY_STATIC_INIT(Z21DriveZ21_$Lambda$1)

__attribute__((unused)) static void Z21DriveZ21_$Lambda$1_initWithZ21DriveZ21_(Z21DriveZ21_$Lambda$1 *self, Z21DriveZ21 *outer$);

__attribute__((unused)) static Z21DriveZ21_$Lambda$1 *new_Z21DriveZ21_$Lambda$1_initWithZ21DriveZ21_(Z21DriveZ21 *outer$) NS_RETURNS_RETAINED;

__attribute__((unused)) static Z21DriveZ21_$Lambda$1 *create_Z21DriveZ21_$Lambda$1_initWithZ21DriveZ21_(Z21DriveZ21 *outer$);

@implementation Z21DriveZ21

+ (jboolean)isActive {
  return Z21DriveZ21_isActive();
}

- (instancetype)initWithJavaNetInetAddress:(JavaNetInetAddress *)z21Address {
  Z21DriveZ21_initWithJavaNetInetAddress_(self, z21Address);
  return self;
}

- (JavaNetInetAddress *)getZ21Address {
  return z21Address_;
}

- (jboolean)sendActionToZ21WithZ21DriveActionsZ21Action:(Z21DriveActionsZ21Action *)action {
  @synchronized(self) {
    JavaNetDatagramPacket *packet = Z21DrivePacketConverter_convertWithZ21DriveActionsZ21Action_(action);
    @try {
      [((JavaNetDatagramPacket *) nil_chk(packet)) setAddressWithJavaNetInetAddress:z21Address_];
      [packet setPortWithInt:Z21DriveZ21_port];
      [((JavaNetDatagramSocket *) nil_chk(Z21DriveZ21_socket)) sendWithJavaNetDatagramPacket:packet];
    }
    @catch (JavaIoIOException *e) {
      [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 sender"))) warningWithNSString:JreStrcat("$@", @"Failed to send message to z21... ", e)];
      return false;
    }
    return true;
  }
}

- (void)run {
  while (!exit_) {
    @try {
      JavaNetDatagramPacket *packet = new_JavaNetDatagramPacket_initWithByteArray_withInt_([IOSByteArray newArrayWithLength:510], 510);
      [((JavaNetDatagramSocket *) nil_chk(Z21DriveZ21_socket)) receiveWithJavaNetDatagramPacket:packet];
      if (Z21DrivePacketConverter_responseFromPacketWithJavaNetDatagramPacket_(packet) != nil) {
        Z21DriveResponsesZ21Response *response = Z21DrivePacketConverter_responseFromPacketWithJavaNetDatagramPacket_(packet);
        for (id<Z21DriveResponsesZ21ResponseListener> __strong listener in nil_chk(responseListeners_)) {
          {
            IOSObjectArray *a__ = [((id<Z21DriveResponsesZ21ResponseListener>) nil_chk(listener)) getListenerTypes];
            Z21DriveResponsesResponseTypes * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
            Z21DriveResponsesResponseTypes * const *e__ = b__ + a__->size_;
            while (b__ < e__) {
              Z21DriveResponsesResponseTypes *type = *b__++;
              if (type == ((Z21DriveResponsesZ21Response *) nil_chk(response))->boundType_) {
                [listener responseReceivedWithZ21DriveResponsesResponseTypes:type withZ21DriveResponsesZ21Response:response];
              }
            }
          }
        }
      }
      else {
        Z21DriveBroadcastsZ21Broadcast *broadcast = Z21DrivePacketConverter_broadcastFromPacketWithJavaNetDatagramPacket_(packet);
        if (broadcast != nil) {
          for (id<Z21DriveBroadcastsZ21BroadcastListener> __strong listener in nil_chk(broadcastListeners_)) {
            {
              IOSObjectArray *a__ = [((id<Z21DriveBroadcastsZ21BroadcastListener>) nil_chk(listener)) getListenerTypes];
              Z21DriveBroadcastsBroadcastTypes * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
              Z21DriveBroadcastsBroadcastTypes * const *e__ = b__ + a__->size_;
              while (b__ < e__) {
                Z21DriveBroadcastsBroadcastTypes *type = *b__++;
                if (type == broadcast->boundType_) {
                  [listener onBroadCastWithZ21DriveBroadcastsBroadcastTypes:type withZ21DriveBroadcastsZ21Broadcast:broadcast];
                }
              }
            }
          }
        }
      }
    }
    @catch (JavaIoIOException *e) {
      if (!exit_) [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 Receiver"))) warningWithNSString:JreStrcat("$@", @"Failed to get a message from z21... ", e)];
    }
  }
}

- (void)addResponseListenerWithZ21DriveResponsesZ21ResponseListener:(id<Z21DriveResponsesZ21ResponseListener>)listener {
  [((id<JavaUtilList>) nil_chk(responseListeners_)) addWithId:listener];
}

- (void)removeResponseListenerWithZ21DriveResponsesZ21ResponseListener:(id<Z21DriveResponsesZ21ResponseListener>)listener {
  if ([((id<JavaUtilList>) nil_chk(responseListeners_)) containsWithId:listener]) [((id<JavaUtilList>) nil_chk(responseListeners_)) removeWithId:listener];
}

- (void)addBroadcastListenerWithZ21DriveBroadcastsZ21BroadcastListener:(id<Z21DriveBroadcastsZ21BroadcastListener>)listener {
  [((id<JavaUtilList>) nil_chk(broadcastListeners_)) addWithId:listener];
}

- (void)removeBroadcastListenerWithZ21DriveBroadcastsZ21BroadcastListener:(id<Z21DriveBroadcastsZ21BroadcastListener>)listener {
  if ([((id<JavaUtilList>) nil_chk(broadcastListeners_)) containsWithId:listener]) [((id<JavaUtilList>) nil_chk(broadcastListeners_)) removeWithId:listener];
}

- (void)shutdown {
  [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21"))) infoWithNSString:@"Shutting down all communication."];
  [self sendActionToZ21WithZ21DriveActionsZ21Action:new_Z21DriveActionsZ21ActionLanLogoff_initWithZ21DriveZ21_(self)];
  [((JavaUtilTimer *) nil_chk(keepAliveTimer_)) cancel];
  exit_ = true;
  [((JavaNetDatagramSocket *) nil_chk(Z21DriveZ21_socket)) close];
}

- (void)java_finalize {
  [self shutdown];
  [super java_finalize];
}

- (void)dealloc {
  JreCheckFinalize(self, [Z21DriveZ21 class]);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "Z", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LJavaNetInetAddress;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x21, 1, 2, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 9, -1, 10, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(isActive);
  methods[1].selector = @selector(initWithJavaNetInetAddress:);
  methods[2].selector = @selector(getZ21Address);
  methods[3].selector = @selector(sendActionToZ21WithZ21DriveActionsZ21Action:);
  methods[4].selector = @selector(run);
  methods[5].selector = @selector(addResponseListenerWithZ21DriveResponsesZ21ResponseListener:);
  methods[6].selector = @selector(removeResponseListenerWithZ21DriveResponsesZ21ResponseListener:);
  methods[7].selector = @selector(addBroadcastListenerWithZ21DriveBroadcastsZ21BroadcastListener:);
  methods[8].selector = @selector(removeBroadcastListenerWithZ21DriveBroadcastsZ21BroadcastListener:);
  methods[9].selector = @selector(shutdown);
  methods[10].selector = @selector(java_finalize);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "staticSync", "LNSString;", .constantValue.asLong = 0, 0x1a, -1, 11, -1, -1 },
    { "port", "I", .constantValue.asInt = Z21DriveZ21_port, 0x1a, -1, -1, -1, -1 },
    { "exit_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "responseListeners_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 12, -1 },
    { "broadcastListeners_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 13, -1 },
    { "socket", "LJavaNetDatagramSocket;", .constantValue.asLong = 0, 0xa, -1, 14, -1, -1 },
    { "keepAliveTimer_", "LJavaUtilTimer;", .constantValue.asLong = 0, 0x12, -1, -1, -1, -1 },
    { "z21Address_", "LJavaNetInetAddress;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LJavaNetInetAddress;", "sendActionToZ21", "LZ21DriveActionsZ21Action;", "addResponseListener", "LZ21DriveResponsesZ21ResponseListener;", "removeResponseListener", "addBroadcastListener", "LZ21DriveBroadcastsZ21BroadcastListener;", "removeBroadcastListener", "finalize", "LJavaLangThrowable;", &Z21DriveZ21_staticSync, "Ljava/util/List<Lz21Drive/responses/Z21ResponseListener;>;", "Ljava/util/List<Lz21Drive/broadcasts/Z21BroadcastListener;>;", &Z21DriveZ21_socket };
  static const J2ObjcClassInfo _Z21DriveZ21 = { "Z21", "z21Drive", ptrTable, methods, fields, 7, 0x1, 11, 8, -1, -1, -1, -1, -1 };
  return &_Z21DriveZ21;
}

@end

jboolean Z21DriveZ21_isActive() {
  Z21DriveZ21_initialize();
  @synchronized(Z21DriveZ21_staticSync) {
    return (Z21DriveZ21_socket != nil);
  }
}

void Z21DriveZ21_initWithJavaNetInetAddress_(Z21DriveZ21 *self, JavaNetInetAddress *z21Address) {
  NSObject_init(self);
  self->exit_ = false;
  self->responseListeners_ = new_JavaUtilArrayList_init();
  self->broadcastListeners_ = new_JavaUtilArrayList_init();
  self->z21Address_ = z21Address;
  @synchronized(Z21DriveZ21_staticSync) {
    if (Z21DriveZ21_socket == nil) {
      [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 init"))) infoWithNSString:@"Z21 initializing socket ..."];
      JavaLangThread *listenerThread = new_JavaLangThread_initWithJavaLangRunnable_(self);
      @try {
        Z21DriveZ21_socket = new_JavaNetDatagramSocket_initWithInt_(Z21DriveZ21_port);
      }
      @catch (JavaNetSocketException *e) {
        [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 init"))) warningWithNSString:JreStrcat("$@", @"Failed to open socket to Z21...", e)];
      }
      [listenerThread setDaemonWithBoolean:true];
      [listenerThread start];
      [self addBroadcastListenerWithZ21DriveBroadcastsZ21BroadcastListener:new_Z21DriveZ21_1_init()];
    }
  }
  [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 init"))) infoWithNSString:JreStrcat("$@", @"Z21 init, addr=", z21Address)];
  JavaUtilTimerTask *keepAliveTask = new_Z21DriveZ21_2_initWithZ21DriveZ21_(self);
  self->keepAliveTimer_ = new_JavaUtilTimer_initWithNSString_withBoolean_(@"Keep Alive timer", true);
  [self->keepAliveTimer_ scheduleAtFixedRateWithJavaUtilTimerTask:keepAliveTask withLong:100 withLong:30000];
  [((JavaLangRuntime *) nil_chk(JavaLangRuntime_getRuntime())) addShutdownHookWithJavaLangThread:new_JavaLangThread_initWithJavaLangRunnable_(new_Z21DriveZ21_$Lambda$1_initWithZ21DriveZ21_(self))];
  [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 init"))) infoWithNSString:@"Z21 initialization done."];
}

Z21DriveZ21 *new_Z21DriveZ21_initWithJavaNetInetAddress_(JavaNetInetAddress *z21Address) {
  J2OBJC_NEW_IMPL(Z21DriveZ21, initWithJavaNetInetAddress_, z21Address)
}

Z21DriveZ21 *create_Z21DriveZ21_initWithJavaNetInetAddress_(JavaNetInetAddress *z21Address) {
  J2OBJC_CREATE_IMPL(Z21DriveZ21, initWithJavaNetInetAddress_, z21Address)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(Z21DriveZ21)

@implementation Z21DrivePacketConverter

- (instancetype)initPackagePrivate {
  Z21DrivePacketConverter_initPackagePrivate(self);
  return self;
}

+ (JavaNetDatagramPacket *)convertWithZ21DriveActionsZ21Action:(Z21DriveActionsZ21Action *)action {
  return Z21DrivePacketConverter_convertWithZ21DriveActionsZ21Action_(action);
}

+ (Z21DriveResponsesZ21Response *)responseFromPacketWithJavaNetDatagramPacket:(JavaNetDatagramPacket *)packet {
  return Z21DrivePacketConverter_responseFromPacketWithJavaNetDatagramPacket_(packet);
}

+ (Z21DriveBroadcastsZ21Broadcast *)broadcastFromPacketWithJavaNetDatagramPacket:(JavaNetDatagramPacket *)packet {
  return Z21DrivePacketConverter_broadcastFromPacketWithJavaNetDatagramPacket_(packet);
}

+ (IOSByteArray *)toPrimitiveWithJavaLangByteArray:(IOSObjectArray *)inArg {
  return Z21DrivePacketConverter_toPrimitiveWithJavaLangByteArray_(inArg);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaNetDatagramPacket;", 0x8, 0, 1, -1, -1, -1, -1 },
    { NULL, "LZ21DriveResponsesZ21Response;", 0x8, 2, 3, -1, -1, -1, -1 },
    { NULL, "LZ21DriveBroadcastsZ21Broadcast;", 0x8, 4, 3, -1, -1, -1, -1 },
    { NULL, "[B", 0xa, 5, 6, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initPackagePrivate);
  methods[1].selector = @selector(convertWithZ21DriveActionsZ21Action:);
  methods[2].selector = @selector(responseFromPacketWithJavaNetDatagramPacket:);
  methods[3].selector = @selector(broadcastFromPacketWithJavaNetDatagramPacket:);
  methods[4].selector = @selector(toPrimitiveWithJavaLangByteArray:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "convert", "LZ21DriveActionsZ21Action;", "responseFromPacket", "LJavaNetDatagramPacket;", "broadcastFromPacket", "toPrimitive", "[LJavaLangByte;" };
  static const J2ObjcClassInfo _Z21DrivePacketConverter = { "PacketConverter", "z21Drive", ptrTable, methods, NULL, 7, 0x0, 5, 0, -1, -1, -1, -1, -1 };
  return &_Z21DrivePacketConverter;
}

@end

void Z21DrivePacketConverter_initPackagePrivate(Z21DrivePacketConverter *self) {
  NSObject_init(self);
}

Z21DrivePacketConverter *new_Z21DrivePacketConverter_initPackagePrivate() {
  J2OBJC_NEW_IMPL(Z21DrivePacketConverter, initPackagePrivate)
}

Z21DrivePacketConverter *create_Z21DrivePacketConverter_initPackagePrivate() {
  J2OBJC_CREATE_IMPL(Z21DrivePacketConverter, initPackagePrivate)
}

JavaNetDatagramPacket *Z21DrivePacketConverter_convertWithZ21DriveActionsZ21Action_(Z21DriveActionsZ21Action *action) {
  Z21DrivePacketConverter_initialize();
  IOSByteArray *packetContent = Z21DrivePacketConverter_toPrimitiveWithJavaLangByteArray_([((id<JavaUtilList>) nil_chk([((Z21DriveActionsZ21Action *) nil_chk(action)) getByteRepresentation])) toArrayWithNSObjectArray:[IOSObjectArray newArrayWithLength:0 type:JavaLangByte_class_()]]);
  return new_JavaNetDatagramPacket_initWithByteArray_withInt_(packetContent, [((id<JavaUtilList>) nil_chk([action getByteRepresentation])) size]);
}

Z21DriveResponsesZ21Response *Z21DrivePacketConverter_responseFromPacketWithJavaNetDatagramPacket_(JavaNetDatagramPacket *packet) {
  Z21DrivePacketConverter_initialize();
  IOSByteArray *array = [((JavaNetDatagramPacket *) nil_chk(packet)) getData];
  jbyte header1 = IOSByteArray_Get(nil_chk(array), 2);
  jbyte header2 = IOSByteArray_Get(array, 3);
  jint xHeader = IOSByteArray_Get(array, 4) & 255;
  if (header1 == (jint) 0x10 && header2 == (jint) 0x00) return new_Z21DriveResponsesZ21ResponseGetSerialNumber_initWithByteArray_(array);
  else if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == (jint) 0xF1) return new_Z21DriveResponsesZ21ResponseLanXGetFirmwareVersion_initWithByteArray_(array);
  else if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == (jint) 0x64 && (IOSByteArray_Get(array, 5) & 255) == (jint) 0x14) return new_Z21DriveResponsesZ21ResponseLanXCVResult_initWithByteArray_(array);
  else if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == (jint) 0x61 && (IOSByteArray_Get(array, 5) & 255) == (jint) 0x13) return new_Z21DriveResponsesZ21ResponseLanXCVNACK_initWithByteArray_(array);
  else if ((header1 & (jint) 0xFF) == (jint) 0x88 && header2 == (jint) 0x00) return new_Z21DriveResponsesZ21ResponseRailcomDatachanged_initWithByteArray_(array);
  return nil;
}

Z21DriveBroadcastsZ21Broadcast *Z21DrivePacketConverter_broadcastFromPacketWithJavaNetDatagramPacket_(JavaNetDatagramPacket *packet) {
  Z21DrivePacketConverter_initialize();
  IOSByteArray *data = [((JavaNetDatagramPacket *) nil_chk(packet)) getData];
  jbyte header1 = IOSByteArray_Get(nil_chk(data), 2);
  jbyte header2 = IOSByteArray_Get(data, 3);
  jint xHeader = IOSByteArray_Get(data, 4) & 255;
  IOSByteArray *newArray = [IOSByteArray newArrayWithLength:IOSByteArray_Get(data, 0)];
  JavaLangSystem_arraycopyWithId_withInt_withId_withInt_withInt_(data, 0, newArray, 0, newArray->size_);
  if (IOSByteArray_Get(data, IOSByteArray_Get(data, 0) + 1) != 0) {
    [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 Receiver"))) infoWithNSString:@"Received two messages in one packet. Multiple messages not supported yet. Please report to github."];
  }
  if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == 239) return new_Z21DriveBroadcastsZ21BroadcastLanXLocoInfo_initWithByteArray_(newArray);
  else if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == (jint) 0x61 && (IOSByteArray_Get(data, 5) & 255) == (jint) 0x82) return new_Z21DriveBroadcastsZ21BroadcastLanXUnknownCommand_initWithByteArray_(newArray);
  else if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == (jint) 0x61 && (IOSByteArray_Get(data, 5) & 255) == (jint) 0x00) return new_Z21DriveBroadcastsZ21BroadcastLanXTrackPowerOff_initWithByteArray_(newArray);
  else if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == (jint) 0x61 && (IOSByteArray_Get(data, 5) & 255) == (jint) 0x01) return new_Z21DriveBroadcastsZ21BroadcastLanXTrackPowerOn_initWithByteArray_(newArray);
  else if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == (jint) 0x61 && (IOSByteArray_Get(data, 5) & 255) == (jint) 0x02) return new_Z21DriveBroadcastsZ21BroadcastLanXProgrammingMode_initWithByteArray_(newArray);
  else if (header1 == (jint) 0x40 && header2 == (jint) 0x00 && xHeader == (jint) 0x61 && (IOSByteArray_Get(data, 5) & 255) == (jint) 0x08) return new_Z21DriveBroadcastsZ21BroadcastLanXShortCircuit_initWithByteArray_(newArray);
  else {
    [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 Receiver"))) warningWithNSString:@"Received unknown message. Array:"];
    {
      IOSByteArray *a__ = newArray;
      jbyte const *b__ = a__->buffer_;
      jbyte const *e__ = b__ + a__->size_;
      while (b__ < e__) {
        jbyte b = *b__++;
        [((JavaIoPrintStream *) nil_chk(JreLoadStatic(JavaLangSystem, out))) printWithNSString:JreStrcat("$$", @"0x", NSString_java_formatWithNSString_withNSObjectArray_(@"%02X ", [IOSObjectArray newArrayWithObjects:(id[]){ JavaLangByte_valueOfWithByte_(b) } count:1 type:NSObject_class_()]))];
      }
    }
    [((JavaIoPrintStream *) nil_chk(JreLoadStatic(JavaLangSystem, out))) println];
  }
  return nil;
}

IOSByteArray *Z21DrivePacketConverter_toPrimitiveWithJavaLangByteArray_(IOSObjectArray *inArg) {
  Z21DrivePacketConverter_initialize();
  IOSByteArray *out = [IOSByteArray newArrayWithLength:((IOSObjectArray *) nil_chk(inArg))->size_];
  jint i = 0;
  {
    IOSObjectArray *a__ = inArg;
    JavaLangByte * const *b__ = a__->buffer_;
    JavaLangByte * const *e__ = b__ + a__->size_;
    while (b__ < e__) {
      JavaLangByte *b = *b__++;
      *IOSByteArray_GetRef(out, i++) = [((JavaLangByte *) nil_chk(b)) charValue];
    }
  }
  return out;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(Z21DrivePacketConverter)

@implementation Z21DriveZ21_1

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  Z21DriveZ21_1_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)onBroadCastWithZ21DriveBroadcastsBroadcastTypes:(Z21DriveBroadcastsBroadcastTypes *)type
                     withZ21DriveBroadcastsZ21Broadcast:(Z21DriveBroadcastsZ21Broadcast *)broadcast {
  if (type == JreLoadEnum(Z21DriveBroadcastsBroadcastTypes, LAN_X_UNKNOWN_COMMAND)) [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 monitor"))) warningWithNSString:@"Z21 reported receiving an unknown command."];
  else [((JavaUtilLoggingLogger *) nil_chk(JavaUtilLoggingLogger_getLoggerWithNSString_(@"Z21 monitor"))) severeWithNSString:@"Broadcast delivery messed up. Please report immediately to GitHub issues what have you done."];
}

- (IOSObjectArray *)getListenerTypes {
  return JreRetainedLocalValue([IOSObjectArray newArrayWithObjects:(id[]){ JreLoadEnum(Z21DriveBroadcastsBroadcastTypes, LAN_X_UNKNOWN_COMMAND) } count:1 type:Z21DriveBroadcastsBroadcastTypes_class_()]);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "[LZ21DriveBroadcastsBroadcastTypes;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(onBroadCastWithZ21DriveBroadcastsBroadcastTypes:withZ21DriveBroadcastsZ21Broadcast:);
  methods[2].selector = @selector(getListenerTypes);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "onBroadCast", "LZ21DriveBroadcastsBroadcastTypes;LZ21DriveBroadcastsZ21Broadcast;", "LZ21DriveZ21;", "initWithJavaNetInetAddress:" };
  static const J2ObjcClassInfo _Z21DriveZ21_1 = { "", "z21Drive", ptrTable, methods, NULL, 7, 0x8010, 3, 0, 2, -1, 3, -1, -1 };
  return &_Z21DriveZ21_1;
}

@end

void Z21DriveZ21_1_init(Z21DriveZ21_1 *self) {
  NSObject_init(self);
}

Z21DriveZ21_1 *new_Z21DriveZ21_1_init() {
  J2OBJC_NEW_IMPL(Z21DriveZ21_1, init)
}

Z21DriveZ21_1 *create_Z21DriveZ21_1_init() {
  J2OBJC_CREATE_IMPL(Z21DriveZ21_1, init)
}

@implementation Z21DriveZ21_2

- (instancetype)initWithZ21DriveZ21:(Z21DriveZ21 *)outer$ {
  Z21DriveZ21_2_initWithZ21DriveZ21_(self, outer$);
  return self;
}

- (void)run {
  [this$0_ sendActionToZ21WithZ21DriveActionsZ21Action:new_Z21DriveActionsZ21ActionGetSerialNumber_initWithZ21DriveZ21_(this$0_)];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithZ21DriveZ21:);
  methods[1].selector = @selector(run);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "this$0_", "LZ21DriveZ21;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LZ21DriveZ21;", "initWithJavaNetInetAddress:" };
  static const J2ObjcClassInfo _Z21DriveZ21_2 = { "", "z21Drive", ptrTable, methods, fields, 7, 0x8010, 2, 1, 0, -1, 1, -1, -1 };
  return &_Z21DriveZ21_2;
}

@end

void Z21DriveZ21_2_initWithZ21DriveZ21_(Z21DriveZ21_2 *self, Z21DriveZ21 *outer$) {
  self->this$0_ = outer$;
  JavaUtilTimerTask_init(self);
}

Z21DriveZ21_2 *new_Z21DriveZ21_2_initWithZ21DriveZ21_(Z21DriveZ21 *outer$) {
  J2OBJC_NEW_IMPL(Z21DriveZ21_2, initWithZ21DriveZ21_, outer$)
}

Z21DriveZ21_2 *create_Z21DriveZ21_2_initWithZ21DriveZ21_(Z21DriveZ21 *outer$) {
  J2OBJC_CREATE_IMPL(Z21DriveZ21_2, initWithZ21DriveZ21_, outer$)
}

@implementation Z21DriveZ21_$Lambda$1

- (void)run {
  [target$_ shutdown];
}

@end

void Z21DriveZ21_$Lambda$1_initWithZ21DriveZ21_(Z21DriveZ21_$Lambda$1 *self, Z21DriveZ21 *outer$) {
  self->target$_ = outer$;
  NSObject_init(self);
}

Z21DriveZ21_$Lambda$1 *new_Z21DriveZ21_$Lambda$1_initWithZ21DriveZ21_(Z21DriveZ21 *outer$) {
  J2OBJC_NEW_IMPL(Z21DriveZ21_$Lambda$1, initWithZ21DriveZ21_, outer$)
}

Z21DriveZ21_$Lambda$1 *create_Z21DriveZ21_$Lambda$1_initWithZ21DriveZ21_(Z21DriveZ21 *outer$) {
  J2OBJC_CREATE_IMPL(Z21DriveZ21_$Lambda$1, initWithZ21DriveZ21_, outer$)
}
