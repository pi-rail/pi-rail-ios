//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../z21-drive/src/main/java/z21Drive/actions/Z21ActionLanXCVPomReadByte.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_Z21DriveActionsZ21ActionLanXCVPomReadByte")
#ifdef RESTRICT_Z21DriveActionsZ21ActionLanXCVPomReadByte
#define INCLUDE_ALL_Z21DriveActionsZ21ActionLanXCVPomReadByte 0
#else
#define INCLUDE_ALL_Z21DriveActionsZ21ActionLanXCVPomReadByte 1
#endif
#undef RESTRICT_Z21DriveActionsZ21ActionLanXCVPomReadByte

#if !defined (Z21DriveActionsZ21ActionLanXCVPomReadByte_) && (INCLUDE_ALL_Z21DriveActionsZ21ActionLanXCVPomReadByte || defined(INCLUDE_Z21DriveActionsZ21ActionLanXCVPomReadByte))
#define Z21DriveActionsZ21ActionLanXCVPomReadByte_

#define RESTRICT_Z21DriveActionsZ21Action 1
#define INCLUDE_Z21DriveActionsZ21Action 1
#include "z21Drive/actions/Z21Action.h"

@class IOSObjectArray;
@class Z21DriveZ21;

@interface Z21DriveActionsZ21ActionLanXCVPomReadByte : Z21DriveActionsZ21Action

#pragma mark Public

- (instancetype)initWithZ21DriveZ21:(Z21DriveZ21 *)z21
                            withInt:(jint)locoAddress
                            withInt:(jint)cv;

- (void)addDataToByteRepresentationWithNSObjectArray:(IOSObjectArray *)objs;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithZ21DriveZ21:(Z21DriveZ21 *)arg0 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(Z21DriveActionsZ21ActionLanXCVPomReadByte)

FOUNDATION_EXPORT void Z21DriveActionsZ21ActionLanXCVPomReadByte_initWithZ21DriveZ21_withInt_withInt_(Z21DriveActionsZ21ActionLanXCVPomReadByte *self, Z21DriveZ21 *z21, jint locoAddress, jint cv);

FOUNDATION_EXPORT Z21DriveActionsZ21ActionLanXCVPomReadByte *new_Z21DriveActionsZ21ActionLanXCVPomReadByte_initWithZ21DriveZ21_withInt_withInt_(Z21DriveZ21 *z21, jint locoAddress, jint cv) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT Z21DriveActionsZ21ActionLanXCVPomReadByte *create_Z21DriveActionsZ21ActionLanXCVPomReadByte_initWithZ21DriveZ21_withInt_withInt_(Z21DriveZ21 *z21, jint locoAddress, jint cv);

J2OBJC_TYPE_LITERAL_HEADER(Z21DriveActionsZ21ActionLanXCVPomReadByte)

#endif

#pragma pop_macro("INCLUDE_ALL_Z21DriveActionsZ21ActionLanXCVPomReadByte")
