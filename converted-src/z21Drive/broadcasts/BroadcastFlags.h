//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../z21-drive/src/main/java/z21Drive/broadcasts/BroadcastFlags.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_Z21DriveBroadcastsBroadcastFlags")
#ifdef RESTRICT_Z21DriveBroadcastsBroadcastFlags
#define INCLUDE_ALL_Z21DriveBroadcastsBroadcastFlags 0
#else
#define INCLUDE_ALL_Z21DriveBroadcastsBroadcastFlags 1
#endif
#undef RESTRICT_Z21DriveBroadcastsBroadcastFlags

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability"
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (Z21DriveBroadcastsBroadcastFlags_) && (INCLUDE_ALL_Z21DriveBroadcastsBroadcastFlags || defined(INCLUDE_Z21DriveBroadcastsBroadcastFlags))
#define Z21DriveBroadcastsBroadcastFlags_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;

typedef NS_ENUM(NSUInteger, Z21DriveBroadcastsBroadcastFlags_Enum) {
  Z21DriveBroadcastsBroadcastFlags_Enum_RECEIVE_ALL_LOCOS = 0,
  Z21DriveBroadcastsBroadcastFlags_Enum_GLOBAL_BROADCASTS = 1,
  Z21DriveBroadcastsBroadcastFlags_Enum_CENTRE_STATUS = 2,
};

@interface Z21DriveBroadcastsBroadcastFlags : JavaLangEnum

#pragma mark Public

+ (Z21DriveBroadcastsBroadcastFlags *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (Z21DriveBroadcastsBroadcastFlags_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(Z21DriveBroadcastsBroadcastFlags)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT Z21DriveBroadcastsBroadcastFlags *Z21DriveBroadcastsBroadcastFlags_values_[];

inline Z21DriveBroadcastsBroadcastFlags *Z21DriveBroadcastsBroadcastFlags_get_RECEIVE_ALL_LOCOS(void);
J2OBJC_ENUM_CONSTANT(Z21DriveBroadcastsBroadcastFlags, RECEIVE_ALL_LOCOS)

inline Z21DriveBroadcastsBroadcastFlags *Z21DriveBroadcastsBroadcastFlags_get_GLOBAL_BROADCASTS(void);
J2OBJC_ENUM_CONSTANT(Z21DriveBroadcastsBroadcastFlags, GLOBAL_BROADCASTS)

inline Z21DriveBroadcastsBroadcastFlags *Z21DriveBroadcastsBroadcastFlags_get_CENTRE_STATUS(void);
J2OBJC_ENUM_CONSTANT(Z21DriveBroadcastsBroadcastFlags, CENTRE_STATUS)

FOUNDATION_EXPORT IOSObjectArray *Z21DriveBroadcastsBroadcastFlags_values(void);

FOUNDATION_EXPORT Z21DriveBroadcastsBroadcastFlags *Z21DriveBroadcastsBroadcastFlags_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT Z21DriveBroadcastsBroadcastFlags *Z21DriveBroadcastsBroadcastFlags_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(Z21DriveBroadcastsBroadcastFlags)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_Z21DriveBroadcastsBroadcastFlags")
