//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../z21-drive/src/main/java/z21Drive/testing/TestCV.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_Z21DriveTestingTestCV")
#ifdef RESTRICT_Z21DriveTestingTestCV
#define INCLUDE_ALL_Z21DriveTestingTestCV 0
#else
#define INCLUDE_ALL_Z21DriveTestingTestCV 1
#endif
#undef RESTRICT_Z21DriveTestingTestCV

#if !defined (Z21DriveTestingTestCV_) && (INCLUDE_ALL_Z21DriveTestingTestCV || defined(INCLUDE_Z21DriveTestingTestCV))
#define Z21DriveTestingTestCV_

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

@class IOSObjectArray;
@class JavaUtilArrayList;

@interface Z21DriveTestingTestCV : NSObject < JavaLangRunnable > {
 @public
  JavaUtilArrayList *cvs_;
}

#pragma mark Public

- (instancetype)init;

+ (void)mainWithNSStringArray:(IOSObjectArray *)args;

- (void)run;

@end

J2OBJC_EMPTY_STATIC_INIT(Z21DriveTestingTestCV)

J2OBJC_FIELD_SETTER(Z21DriveTestingTestCV, cvs_, JavaUtilArrayList *)

FOUNDATION_EXPORT void Z21DriveTestingTestCV_init(Z21DriveTestingTestCV *self);

FOUNDATION_EXPORT Z21DriveTestingTestCV *new_Z21DriveTestingTestCV_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT Z21DriveTestingTestCV *create_Z21DriveTestingTestCV_init(void);

FOUNDATION_EXPORT void Z21DriveTestingTestCV_mainWithNSStringArray_(IOSObjectArray *args);

J2OBJC_TYPE_LITERAL_HEADER(Z21DriveTestingTestCV)

#endif

#pragma pop_macro("INCLUDE_ALL_Z21DriveTestingTestCV")
