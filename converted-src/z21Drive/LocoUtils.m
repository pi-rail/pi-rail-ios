//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../z21-drive/src/main/java/z21Drive/LocoUtils.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "java/io/PrintStream.h"
#include "java/lang/Exception.h"
#include "java/lang/InterruptedException.h"
#include "java/lang/System.h"
#include "java/lang/Thread.h"
#include "z21Drive/LocoAddressOutOfRangeException.h"
#include "z21Drive/LocoUtils.h"
#include "z21Drive/Z21.h"
#include "z21Drive/actions/Z21ActionGetLocoInfo.h"
#include "z21Drive/actions/Z21ActionSetLocoDrive.h"
#include "z21Drive/actions/Z21ActionSetLocoFunction.h"
#include "z21Drive/broadcasts/BroadcastTypes.h"
#include "z21Drive/broadcasts/Z21Broadcast.h"
#include "z21Drive/broadcasts/Z21BroadcastLanXLocoInfo.h"
#include "z21Drive/broadcasts/Z21BroadcastListener.h"

#if !__has_feature(objc_arc)
#error "z21Drive/LocoUtils must be compiled with ARC (-fobjc-arc)"
#endif

@interface Z21DriveLocoUtils_1 : NSObject < Z21DriveBroadcastsZ21BroadcastListener > {
 @public
  Z21DriveZ21 *val$z21_;
  jint val$locoAddress_;
}

- (instancetype)initWithZ21DriveZ21:(Z21DriveZ21 *)capture$0
                            withInt:(jint)capture$1;

- (void)onBroadCastWithZ21DriveBroadcastsBroadcastTypes:(Z21DriveBroadcastsBroadcastTypes *)type
                     withZ21DriveBroadcastsZ21Broadcast:(Z21DriveBroadcastsZ21Broadcast *)broadcast;

- (IOSObjectArray *)getListenerTypes;

@end

J2OBJC_EMPTY_STATIC_INIT(Z21DriveLocoUtils_1)

__attribute__((unused)) static void Z21DriveLocoUtils_1_initWithZ21DriveZ21_withInt_(Z21DriveLocoUtils_1 *self, Z21DriveZ21 *capture$0, jint capture$1);

__attribute__((unused)) static Z21DriveLocoUtils_1 *new_Z21DriveLocoUtils_1_initWithZ21DriveZ21_withInt_(Z21DriveZ21 *capture$0, jint capture$1) NS_RETURNS_RETAINED;

__attribute__((unused)) static Z21DriveLocoUtils_1 *create_Z21DriveLocoUtils_1_initWithZ21DriveZ21_withInt_(Z21DriveZ21 *capture$0, jint capture$1);

@implementation Z21DriveLocoUtils

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  Z21DriveLocoUtils_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (void)stopLocoWithZ21DriveZ21:(Z21DriveZ21 *)z21
                        withInt:(jint)locoAddress {
  Z21DriveLocoUtils_stopLocoWithZ21DriveZ21_withInt_(z21, locoAddress);
}

+ (void)activateFunctionWithZ21DriveZ21:(Z21DriveZ21 *)z21
                                withInt:(jint)locoAddress
                                withInt:(jint)function {
  Z21DriveLocoUtils_activateFunctionWithZ21DriveZ21_withInt_withInt_(z21, locoAddress, function);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x9, 2, 3, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(stopLocoWithZ21DriveZ21:withInt:);
  methods[2].selector = @selector(activateFunctionWithZ21DriveZ21:withInt:withInt:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "stopLoco", "LZ21DriveZ21;I", "activateFunction", "LZ21DriveZ21;II" };
  static const J2ObjcClassInfo _Z21DriveLocoUtils = { "LocoUtils", "z21Drive", ptrTable, methods, NULL, 7, 0x1, 3, 0, -1, -1, -1, -1, -1 };
  return &_Z21DriveLocoUtils;
}

@end

void Z21DriveLocoUtils_init(Z21DriveLocoUtils *self) {
  NSObject_init(self);
}

Z21DriveLocoUtils *new_Z21DriveLocoUtils_init() {
  J2OBJC_NEW_IMPL(Z21DriveLocoUtils, init)
}

Z21DriveLocoUtils *create_Z21DriveLocoUtils_init() {
  J2OBJC_CREATE_IMPL(Z21DriveLocoUtils, init)
}

void Z21DriveLocoUtils_stopLocoWithZ21DriveZ21_withInt_(Z21DriveZ21 *z21, jint locoAddress) {
  Z21DriveLocoUtils_initialize();
  [((Z21DriveZ21 *) nil_chk(z21)) addBroadcastListenerWithZ21DriveBroadcastsZ21BroadcastListener:new_Z21DriveLocoUtils_1_initWithZ21DriveZ21_withInt_(z21, locoAddress)];
  @try {
    [z21 sendActionToZ21WithZ21DriveActionsZ21Action:new_Z21DriveActionsZ21ActionGetLocoInfo_initWithZ21DriveZ21_withInt_(z21, locoAddress)];
  }
  @catch (Z21DriveLocoAddressOutOfRangeException *e) {
    [((JavaIoPrintStream *) nil_chk(JreLoadStatic(JavaLangSystem, err))) printlnWithNSString:@"Idiot detected."];
    [e printStackTrace];
  }
}

void Z21DriveLocoUtils_activateFunctionWithZ21DriveZ21_withInt_withInt_(Z21DriveZ21 *z21, jint locoAddress, jint function) {
  Z21DriveLocoUtils_initialize();
  @try {
    Z21DriveActionsZ21ActionSetLocoFunction *action = new_Z21DriveActionsZ21ActionSetLocoFunction_initWithZ21DriveZ21_withInt_withInt_withBoolean_(z21, locoAddress, function, true);
    [((Z21DriveZ21 *) nil_chk(z21)) sendActionToZ21WithZ21DriveActionsZ21Action:action];
    JavaLangThread_sleepWithLong_(500);
    action = new_Z21DriveActionsZ21ActionSetLocoFunction_initWithZ21DriveZ21_withInt_withInt_withBoolean_(z21, locoAddress, function, false);
    [z21 sendActionToZ21WithZ21DriveActionsZ21Action:action];
  }
  @catch (Z21DriveLocoAddressOutOfRangeException *e) {
    [e printStackTrace];
  }
  @catch (JavaLangInterruptedException *e) {
    [e printStackTrace];
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(Z21DriveLocoUtils)

@implementation Z21DriveLocoUtils_1

- (instancetype)initWithZ21DriveZ21:(Z21DriveZ21 *)capture$0
                            withInt:(jint)capture$1 {
  Z21DriveLocoUtils_1_initWithZ21DriveZ21_withInt_(self, capture$0, capture$1);
  return self;
}

- (void)onBroadCastWithZ21DriveBroadcastsBroadcastTypes:(Z21DriveBroadcastsBroadcastTypes *)type
                     withZ21DriveBroadcastsZ21Broadcast:(Z21DriveBroadcastsZ21Broadcast *)broadcast {
  Z21DriveBroadcastsZ21BroadcastLanXLocoInfo *bcast = (Z21DriveBroadcastsZ21BroadcastLanXLocoInfo *) cast_chk(broadcast, [Z21DriveBroadcastsZ21BroadcastLanXLocoInfo class]);
  jbyte speedStepsId = (jbyte) [((Z21DriveBroadcastsZ21BroadcastLanXLocoInfo *) nil_chk(bcast)) getSpeedSteps];
  jboolean direction = [bcast getDirection];
  Z21DriveActionsZ21ActionSetLocoDrive *action;
  @try {
    action = new_Z21DriveActionsZ21ActionSetLocoDrive_initWithZ21DriveZ21_withInt_withInt_withInt_withBoolean_(val$z21_, val$locoAddress_, 0, speedStepsId, direction);
  }
  @catch (Z21DriveLocoAddressOutOfRangeException *e) {
    [((JavaIoPrintStream *) nil_chk(JreLoadStatic(JavaLangSystem, err))) printlnWithNSString:@"Idiot detected."];
    [e printStackTrace];
    return;
  }
  [((Z21DriveZ21 *) nil_chk(val$z21_)) sendActionToZ21WithZ21DriveActionsZ21Action:action];
}

- (IOSObjectArray *)getListenerTypes {
  return JreRetainedLocalValue([IOSObjectArray newArrayWithObjects:(id[]){ JreLoadEnum(Z21DriveBroadcastsBroadcastTypes, LAN_X_LOCO_INFO) } count:1 type:Z21DriveBroadcastsBroadcastTypes_class_()]);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "[LZ21DriveBroadcastsBroadcastTypes;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithZ21DriveZ21:withInt:);
  methods[1].selector = @selector(onBroadCastWithZ21DriveBroadcastsBroadcastTypes:withZ21DriveBroadcastsZ21Broadcast:);
  methods[2].selector = @selector(getListenerTypes);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "val$z21_", "LZ21DriveZ21;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
    { "val$locoAddress_", "I", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LZ21DriveZ21;I", "onBroadCast", "LZ21DriveBroadcastsBroadcastTypes;LZ21DriveBroadcastsZ21Broadcast;", "LZ21DriveLocoUtils;", "stopLocoWithZ21DriveZ21:withInt:" };
  static const J2ObjcClassInfo _Z21DriveLocoUtils_1 = { "", "z21Drive", ptrTable, methods, fields, 7, 0x8018, 3, 2, 3, -1, 4, -1, -1 };
  return &_Z21DriveLocoUtils_1;
}

@end

void Z21DriveLocoUtils_1_initWithZ21DriveZ21_withInt_(Z21DriveLocoUtils_1 *self, Z21DriveZ21 *capture$0, jint capture$1) {
  self->val$z21_ = capture$0;
  self->val$locoAddress_ = capture$1;
  NSObject_init(self);
}

Z21DriveLocoUtils_1 *new_Z21DriveLocoUtils_1_initWithZ21DriveZ21_withInt_(Z21DriveZ21 *capture$0, jint capture$1) {
  J2OBJC_NEW_IMPL(Z21DriveLocoUtils_1, initWithZ21DriveZ21_withInt_, capture$0, capture$1)
}

Z21DriveLocoUtils_1 *create_Z21DriveLocoUtils_1_initWithZ21DriveZ21_withInt_(Z21DriveZ21 *capture$0, jint capture$1) {
  J2OBJC_CREATE_IMPL(Z21DriveLocoUtils_1, initWithZ21DriveZ21_withInt_, capture$0, capture$1)
}
