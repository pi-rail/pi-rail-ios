//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/z21/StartZ21Central.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientZ21StartZ21Central")
#ifdef RESTRICT_DePidataRailClientZ21StartZ21Central
#define INCLUDE_ALL_DePidataRailClientZ21StartZ21Central 0
#else
#define INCLUDE_ALL_DePidataRailClientZ21StartZ21Central 1
#endif
#undef RESTRICT_DePidataRailClientZ21StartZ21Central

#if !defined (DePidataRailClientZ21StartZ21Central_) && (INCLUDE_ALL_DePidataRailClientZ21StartZ21Central || defined(INCLUDE_DePidataRailClientZ21StartZ21Central))
#define DePidataRailClientZ21StartZ21Central_

#define RESTRICT_DePidataGuiControllerBaseGuiOperation 1
#define INCLUDE_PIGC_GuiOperation 1
#include "de/pidata/gui/controller/base/GuiOperation.h"

@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIMR_Model;

@interface DePidataRailClientZ21StartZ21Central : PIGC_GuiOperation

#pragma mark Public

- (instancetype)init;

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(DePidataRailClientZ21StartZ21Central)

FOUNDATION_EXPORT void DePidataRailClientZ21StartZ21Central_init(DePidataRailClientZ21StartZ21Central *self);

FOUNDATION_EXPORT DePidataRailClientZ21StartZ21Central *new_DePidataRailClientZ21StartZ21Central_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailClientZ21StartZ21Central *create_DePidataRailClientZ21StartZ21Central_init(void);

J2OBJC_TYPE_LITERAL_HEADER(DePidataRailClientZ21StartZ21Central)

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientZ21StartZ21Central")
