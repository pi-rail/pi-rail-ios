//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editTimetable/EditSchedule.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditTimetableEditSchedule")
#ifdef RESTRICT_DePidataRailClientEditTimetableEditSchedule
#define INCLUDE_ALL_DePidataRailClientEditTimetableEditSchedule 0
#else
#define INCLUDE_ALL_DePidataRailClientEditTimetableEditSchedule 1
#endif
#undef RESTRICT_DePidataRailClientEditTimetableEditSchedule

#if !defined (DePidataRailClientEditTimetableEditSchedule_) && (INCLUDE_ALL_DePidataRailClientEditTimetableEditSchedule || defined(INCLUDE_DePidataRailClientEditTimetableEditSchedule))
#define DePidataRailClientEditTimetableEditSchedule_

#define RESTRICT_DePidataGuiControllerBaseGuiDelegateOperation 1
#define INCLUDE_PIGC_GuiDelegateOperation 1
#include "de/pidata/gui/controller/base/GuiDelegateOperation.h"

@class DePidataRailClientEditTimetableEditTimetableDelegate;
@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface DePidataRailClientEditTimetableEditSchedule : PIGC_GuiDelegateOperation

#pragma mark Public

- (instancetype)init;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)parentDlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

#pragma mark Protected

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(DePidataRailClientEditTimetableEditTimetableDelegate *)delegate
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(DePidataRailClientEditTimetableEditSchedule)

FOUNDATION_EXPORT void DePidataRailClientEditTimetableEditSchedule_init(DePidataRailClientEditTimetableEditSchedule *self);

FOUNDATION_EXPORT DePidataRailClientEditTimetableEditSchedule *new_DePidataRailClientEditTimetableEditSchedule_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailClientEditTimetableEditSchedule *create_DePidataRailClientEditTimetableEditSchedule_init(void);

J2OBJC_TYPE_LITERAL_HEADER(DePidataRailClientEditTimetableEditSchedule)

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditTimetableEditSchedule")
