//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/swgrid/ToolRotation.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/view/figure/BitmapPI.h"
#include "de/pidata/gui/view/figure/Figure.h"
#include "de/pidata/gui/view/figure/ShapeStyle.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/swgrid/ToolRotation.h"
#include "de/pidata/rect/Rect.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/swgrid/ToolRotation must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRCS_ToolRotation () {
 @public
  jint angle_;
}

@end

@implementation PIRCS_ToolRotation

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)figure
                      withPICR_Rect:(id<PICR_Rect>)bounds
                      withPIQ_QName:(PIQ_QName *)bitmapID
                withPIGF_ShapeStyle:(PIGF_ShapeStyle *)style
                            withInt:(jint)angle {
  PIRCS_ToolRotation_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_(self, figure, bounds, bitmapID, style, angle);
  return self;
}

- (jint)getAngle {
  return angle_;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIGF_Figure:withPICR_Rect:withPIQ_QName:withPIGF_ShapeStyle:withInt:);
  methods[1].selector = @selector(getAngle);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "angle_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIGF_Figure;LPICR_Rect;LPIQ_QName;LPIGF_ShapeStyle;I" };
  static const J2ObjcClassInfo _PIRCS_ToolRotation = { "ToolRotation", "de.pidata.rail.client.swgrid", ptrTable, methods, fields, 7, 0x1, 2, 1, -1, -1, -1, -1, -1 };
  return &_PIRCS_ToolRotation;
}

@end

void PIRCS_ToolRotation_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_(PIRCS_ToolRotation *self, id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIQ_QName *bitmapID, PIGF_ShapeStyle *style, jint angle) {
  PIGF_BitmapPI_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_(self, figure, bounds, bitmapID, style);
  self->angle_ = angle;
}

PIRCS_ToolRotation *new_PIRCS_ToolRotation_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_(id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIQ_QName *bitmapID, PIGF_ShapeStyle *style, jint angle) {
  J2OBJC_NEW_IMPL(PIRCS_ToolRotation, initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_, figure, bounds, bitmapID, style, angle)
}

PIRCS_ToolRotation *create_PIRCS_ToolRotation_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_(id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIQ_QName *bitmapID, PIGF_ShapeStyle *style, jint angle) {
  J2OBJC_CREATE_IMPL(PIRCS_ToolRotation, initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_, figure, bounds, bitmapID, style, angle)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCS_ToolRotation)

J2OBJC_NAME_MAPPING(PIRCS_ToolRotation, "de.pidata.rail.client.swgrid", "PIRCS_")
