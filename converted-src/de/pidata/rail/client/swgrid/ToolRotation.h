//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/swgrid/ToolRotation.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientSwgridToolRotation")
#ifdef RESTRICT_DePidataRailClientSwgridToolRotation
#define INCLUDE_ALL_DePidataRailClientSwgridToolRotation 0
#else
#define INCLUDE_ALL_DePidataRailClientSwgridToolRotation 1
#endif
#undef RESTRICT_DePidataRailClientSwgridToolRotation

#if !defined (PIRCS_ToolRotation_) && (INCLUDE_ALL_DePidataRailClientSwgridToolRotation || defined(INCLUDE_PIRCS_ToolRotation))
#define PIRCS_ToolRotation_

#define RESTRICT_DePidataGuiViewFigureBitmapPI 1
#define INCLUDE_PIGF_BitmapPI 1
#include "de/pidata/gui/view/figure/BitmapPI.h"

@class PIGF_AnimationType;
@class PIGF_BitmapAdjust;
@class PIGF_ShapeStyle;
@class PIQ_QName;
@protocol PICR_Rect;
@protocol PIGB_ComponentBitmap;
@protocol PIGF_Figure;

@interface PIRCS_ToolRotation : PIGF_BitmapPI

#pragma mark Public

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)figure
                      withPICR_Rect:(id<PICR_Rect>)bounds
                      withPIQ_QName:(PIQ_QName *)bitmapID
                withPIGF_ShapeStyle:(PIGF_ShapeStyle *)style
                            withInt:(jint)angle;

- (jint)getAngle;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)arg0
                      withPICR_Rect:(id<PICR_Rect>)arg1
           withPIGB_ComponentBitmap:(id<PIGB_ComponentBitmap>)arg2
                withPIGF_ShapeStyle:(PIGF_ShapeStyle *)arg3 NS_UNAVAILABLE;

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)arg0
                      withPICR_Rect:(id<PICR_Rect>)arg1
                      withPIQ_QName:(PIQ_QName *)arg2
             withPIGF_AnimationType:(PIGF_AnimationType *)arg3
                withPIGF_ShapeStyle:(PIGF_ShapeStyle *)arg4 NS_UNAVAILABLE;

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)arg0
                      withPICR_Rect:(id<PICR_Rect>)arg1
                      withPIQ_QName:(PIQ_QName *)arg2
              withPIGF_BitmapAdjust:(PIGF_BitmapAdjust *)arg3
                withPIGF_ShapeStyle:(PIGF_ShapeStyle *)arg4 NS_UNAVAILABLE;

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)arg0
                      withPICR_Rect:(id<PICR_Rect>)arg1
                      withPIQ_QName:(PIQ_QName *)arg2
                withPIGF_ShapeStyle:(PIGF_ShapeStyle *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCS_ToolRotation)

FOUNDATION_EXPORT void PIRCS_ToolRotation_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_(PIRCS_ToolRotation *self, id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIQ_QName *bitmapID, PIGF_ShapeStyle *style, jint angle);

FOUNDATION_EXPORT PIRCS_ToolRotation *new_PIRCS_ToolRotation_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_(id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIQ_QName *bitmapID, PIGF_ShapeStyle *style, jint angle) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCS_ToolRotation *create_PIRCS_ToolRotation_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_withInt_(id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIQ_QName *bitmapID, PIGF_ShapeStyle *style, jint angle);

J2OBJC_TYPE_LITERAL_HEADER(PIRCS_ToolRotation)

@compatibility_alias DePidataRailClientSwgridToolRotation PIRCS_ToolRotation;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientSwgridToolRotation")
