//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/swgrid/TrackShape.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/guidef/ControllerBuilder.h"
#include "de/pidata/gui/view/figure/BitmapPI.h"
#include "de/pidata/gui/view/figure/Figure.h"
#include "de/pidata/gui/view/figure/ShapeStyle.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/swgrid/TrackShape.h"
#include "de/pidata/rail/model/TrackPos.h"
#include "de/pidata/rail/railway/RailAction.h"
#include "de/pidata/rail/railway/TrackPart.h"
#include "de/pidata/rect/Pos.h"
#include "de/pidata/rect/Rect.h"
#include "de/pidata/rect/RectRelatedRect.h"
#include "de/pidata/rect/Rotation.h"
#include "java/lang/Integer.h"
#include "java/lang/StringBuilder.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/swgrid/TrackShape must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRCS_TrackShape () {
 @public
  PIRR_RailAction *railAction_;
  jboolean modified_;
  PIRR_TrackPart *trackPart_;
  jint angle_;
}

@end

J2OBJC_FIELD_SETTER(PIRCS_TrackShape, railAction_, PIRR_RailAction *)
J2OBJC_FIELD_SETTER(PIRCS_TrackShape, trackPart_, PIRR_TrackPart *)

@implementation PIRCS_TrackShape

- (instancetype)initWithPIGF_Figure:(id<PIGF_Figure>)figure
                      withPICR_Rect:(id<PICR_Rect>)bounds
                 withPIRR_TrackPart:(PIRR_TrackPart *)trackPart
                            withInt:(jint)angle
                withPIGF_ShapeStyle:(PIGF_ShapeStyle *)style
                withPIRR_RailAction:(PIRR_RailAction *)railAction {
  PIRCS_TrackShape_initWithPIGF_Figure_withPICR_Rect_withPIRR_TrackPart_withInt_withPIGF_ShapeStyle_withPIRR_RailAction_(self, figure, bounds, trackPart, angle, style, railAction);
  return self;
}

- (PIRR_TrackPart *)getTrackPart {
  return trackPart_;
}

- (void)setTrackPartWithPIRR_TrackPart:(PIRR_TrackPart *)trackPart
                               withInt:(jint)angle {
  self->trackPart_ = trackPart;
  self->angle_ = angle;
  jint index = JreIntDiv(angle, 45);
  jint rot = (JreIntDiv(index, 2)) * 90;
  PICR_RectRelatedRect *bounds = (PICR_RectRelatedRect *) cast_chk([self getBounds], [PICR_RectRelatedRect class]);
  PICR_Rotation *oldRotation = [((PICR_RectRelatedRect *) nil_chk(bounds)) getRotation];
  PICR_Rotation *newRotation = new_PICR_Rotation_initWithId_withDouble_withPICR_Pos_([((PICR_Rotation *) nil_chk(oldRotation)) getOwner], rot, [oldRotation getCenter]);
  [bounds setRotationWithPICR_Rotation:newRotation];
  [self applyBitmap];
}

- (PIRR_RailAction *)getRailAction {
  return railAction_;
}

- (jint)getAngle {
  return angle_;
}

- (void)setRailActionWithPIRR_RailAction:(PIRR_RailAction *)railAction {
  self->railAction_ = railAction;
  [self applyBitmap];
}

- (void)applyBitmap {
  PIQ_QName *imageID;
  jint index = JreIntDiv([self getAngle], 45);
  jboolean diag = ((JreIntMod(index, 2)) != 0);
  if (railAction_ == nil) {
    if (diag) {
      imageID = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE))) getQNameWithNSString:JreStrcat("$$", @"icons/track/", [((PIRR_TrackPart *) nil_chk(trackPart_)) getIconDiag])];
    }
    else {
      imageID = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE))) getQNameWithNSString:JreStrcat("$$", @"icons/track/", [((PIRR_TrackPart *) nil_chk(trackPart_)) getIconStraight])];
    }
  }
  else {
    imageID = [railAction_ getCurrentImageIDWithBoolean:diag];
  }
  [self setBitmapIDWithPIQ_QName:imageID];
  [self appearanceChanged];
}

- (void)setModifiedWithBoolean:(jboolean)modified {
  self->modified_ = modified;
}

- (jboolean)isModified {
  return modified_;
}

- (NSString *)description {
  JavaLangStringBuilder *buf = new_JavaLangStringBuilder_initWithNSString_([((PIQ_QName *) nil_chk([((PIRR_TrackPart *) nil_chk(trackPart_)) getName])) getName]);
  if (railAction_ != nil) {
    (void) [((JavaLangStringBuilder *) nil_chk([buf appendWithNSString:@" "])) appendWithNSString:[((PIQ_QName *) nil_chk([((PIRR_RailAction *) nil_chk(railAction_)) getId])) getName]];
    PIRM_TrackPos *pos = [((PIRR_RailAction *) nil_chk(railAction_)) getTrackPos];
    if (pos != nil) {
      (void) [((JavaLangStringBuilder *) nil_chk([((JavaLangStringBuilder *) nil_chk([((JavaLangStringBuilder *) nil_chk([((JavaLangStringBuilder *) nil_chk([buf appendWithNSString:@"("])) appendWithId:[pos getX]])) appendWithNSString:@","])) appendWithId:[pos getY]])) appendWithNSString:@")"];
    }
  }
  return [buf description];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, "LPIRR_TrackPart;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 1, 2, -1, -1, -1, -1 },
    { NULL, "LPIRR_RailAction;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 7, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIGF_Figure:withPICR_Rect:withPIRR_TrackPart:withInt:withPIGF_ShapeStyle:withPIRR_RailAction:);
  methods[1].selector = @selector(getTrackPart);
  methods[2].selector = @selector(setTrackPartWithPIRR_TrackPart:withInt:);
  methods[3].selector = @selector(getRailAction);
  methods[4].selector = @selector(getAngle);
  methods[5].selector = @selector(setRailActionWithPIRR_RailAction:);
  methods[6].selector = @selector(applyBitmap);
  methods[7].selector = @selector(setModifiedWithBoolean:);
  methods[8].selector = @selector(isModified);
  methods[9].selector = @selector(description);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "railAction_", "LPIRR_RailAction;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "modified_", "Z", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "trackPart_", "LPIRR_TrackPart;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "angle_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIGF_Figure;LPICR_Rect;LPIRR_TrackPart;ILPIGF_ShapeStyle;LPIRR_RailAction;", "setTrackPart", "LPIRR_TrackPart;I", "setRailAction", "LPIRR_RailAction;", "setModified", "Z", "toString" };
  static const J2ObjcClassInfo _PIRCS_TrackShape = { "TrackShape", "de.pidata.rail.client.swgrid", ptrTable, methods, fields, 7, 0x1, 10, 4, -1, -1, -1, -1, -1 };
  return &_PIRCS_TrackShape;
}

@end

void PIRCS_TrackShape_initWithPIGF_Figure_withPICR_Rect_withPIRR_TrackPart_withInt_withPIGF_ShapeStyle_withPIRR_RailAction_(PIRCS_TrackShape *self, id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIRR_TrackPart *trackPart, jint angle, PIGF_ShapeStyle *style, PIRR_RailAction *railAction) {
  PIGF_BitmapPI_initWithPIGF_Figure_withPICR_Rect_withPIQ_QName_withPIGF_ShapeStyle_(self, figure, bounds, [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE))) getQNameWithNSString:JreStrcat("$$", @"icons/track/", [((PIRR_TrackPart *) nil_chk(trackPart)) getIconStraight])], style);
  self->modified_ = false;
  self->angle_ = 0;
  self->trackPart_ = trackPart;
  self->angle_ = angle;
  [self setRailActionWithPIRR_RailAction:railAction];
}

PIRCS_TrackShape *new_PIRCS_TrackShape_initWithPIGF_Figure_withPICR_Rect_withPIRR_TrackPart_withInt_withPIGF_ShapeStyle_withPIRR_RailAction_(id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIRR_TrackPart *trackPart, jint angle, PIGF_ShapeStyle *style, PIRR_RailAction *railAction) {
  J2OBJC_NEW_IMPL(PIRCS_TrackShape, initWithPIGF_Figure_withPICR_Rect_withPIRR_TrackPart_withInt_withPIGF_ShapeStyle_withPIRR_RailAction_, figure, bounds, trackPart, angle, style, railAction)
}

PIRCS_TrackShape *create_PIRCS_TrackShape_initWithPIGF_Figure_withPICR_Rect_withPIRR_TrackPart_withInt_withPIGF_ShapeStyle_withPIRR_RailAction_(id<PIGF_Figure> figure, id<PICR_Rect> bounds, PIRR_TrackPart *trackPart, jint angle, PIGF_ShapeStyle *style, PIRR_RailAction *railAction) {
  J2OBJC_CREATE_IMPL(PIRCS_TrackShape, initWithPIGF_Figure_withPICR_Rect_withPIRR_TrackPart_withInt_withPIGF_ShapeStyle_withPIRR_RailAction_, figure, bounds, trackPart, angle, style, railAction)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCS_TrackShape)

J2OBJC_NAME_MAPPING(PIRCS_TrackShape, "de.pidata.rail.client.swgrid", "PIRCS_")
