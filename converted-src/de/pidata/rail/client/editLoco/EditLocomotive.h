//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editLoco/EditLocomotive.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditLocoEditLocomotive")
#ifdef RESTRICT_DePidataRailClientEditLocoEditLocomotive
#define INCLUDE_ALL_DePidataRailClientEditLocoEditLocomotive 0
#else
#define INCLUDE_ALL_DePidataRailClientEditLocoEditLocomotive 1
#endif
#undef RESTRICT_DePidataRailClientEditLocoEditLocomotive

#if !defined (PIRCL_EditLocomotive_) && (INCLUDE_ALL_DePidataRailClientEditLocoEditLocomotive || defined(INCLUDE_PIRCL_EditLocomotive))
#define PIRCL_EditLocomotive_

#define RESTRICT_DePidataGuiControllerBaseGuiOperation 1
#define INCLUDE_PIGC_GuiOperation 1
#include "de/pidata/gui/controller/base/GuiOperation.h"

@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIMR_Model;

@interface PIRCL_EditLocomotive : PIGC_GuiOperation

#pragma mark Public

- (instancetype)init;

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCL_EditLocomotive)

FOUNDATION_EXPORT void PIRCL_EditLocomotive_init(PIRCL_EditLocomotive *self);

FOUNDATION_EXPORT PIRCL_EditLocomotive *new_PIRCL_EditLocomotive_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCL_EditLocomotive *create_PIRCL_EditLocomotive_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCL_EditLocomotive)

@compatibility_alias DePidataRailClientEditLocoEditLocomotive PIRCL_EditLocomotive;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditLocoEditLocomotive")
