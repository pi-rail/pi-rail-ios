//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editLoco/EditLoco.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/GuiOperation.h"
#include "de/pidata/gui/guidef/ControllerBuilder.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/editLoco/EditLoco.h"
#include "de/pidata/rail/client/editcfg/EditCfgParamList.h"
#include "de/pidata/rail/railway/Locomotive.h"
#include "de/pidata/rail/railway/RailDeviceAddress.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/net/InetAddress.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/editLoco/EditLoco must be compiled with ARC (-fobjc-arc)"
#endif

@implementation PIRCL_EditLoco

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCL_EditLoco_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  if ([dataContext isKindOfClass:[PIRR_Locomotive class]]) {
    PIRR_Locomotive *selectedLoco = (PIRR_Locomotive *) dataContext;
    JavaNetInetAddress *ipAddress = [((PIRR_RailDeviceAddress *) nil_chk([((PIRR_Locomotive *) nil_chk(selectedLoco)) getAddress])) getInetAddress];
    PIRCE_EditCfgParamList *parameterList = new_PIRCE_EditCfgParamList_initWithPIQ_QName_withNSString_withJavaNetInetAddress_withNSString_([selectedLoco getId], [selectedLoco getDisplayName], ipAddress, [selectedLoco getDeviceType]);
    NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"editLocoEdit_H" withNSString:nil withJavaUtilProperties:nil];
    PIGC_GuiOperation_openChildDialogWithPIGC_Controller_withPIQ_QName_withNSString_withPISB_ParameterList_(sourceCtrl, [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE))) getQNameWithNSString:@"edit_loco"], title, parameterList);
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(executeWithPIQ_QName:withPIGC_Controller:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "execute", "LPIQ_QName;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;" };
  static const J2ObjcClassInfo _PIRCL_EditLoco = { "EditLoco", "de.pidata.rail.client.editLoco", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_PIRCL_EditLoco;
}

@end

void PIRCL_EditLoco_init(PIRCL_EditLoco *self) {
  PIGC_GuiOperation_init(self);
}

PIRCL_EditLoco *new_PIRCL_EditLoco_init() {
  J2OBJC_NEW_IMPL(PIRCL_EditLoco, init)
}

PIRCL_EditLoco *create_PIRCL_EditLoco_init() {
  J2OBJC_CREATE_IMPL(PIRCL_EditLoco, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCL_EditLoco)

J2OBJC_NAME_MAPPING(PIRCL_EditLoco, "de.pidata.rail.client.editLoco", "PIRCL_")
