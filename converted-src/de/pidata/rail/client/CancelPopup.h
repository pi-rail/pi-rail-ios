//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/CancelPopup.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientCancelPopup")
#ifdef RESTRICT_DePidataRailClientCancelPopup
#define INCLUDE_ALL_DePidataRailClientCancelPopup 0
#else
#define INCLUDE_ALL_DePidataRailClientCancelPopup 1
#endif
#undef RESTRICT_DePidataRailClientCancelPopup

#if !defined (PIRC_CancelPopup_) && (INCLUDE_ALL_DePidataRailClientCancelPopup || defined(INCLUDE_PIRC_CancelPopup))
#define PIRC_CancelPopup_

#define RESTRICT_DePidataGuiControllerBaseGuiOperation 1
#define INCLUDE_PIGC_GuiOperation 1
#include "de/pidata/gui/controller/base/GuiOperation.h"

@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIMR_Model;

@interface PIRC_CancelPopup : PIGC_GuiOperation

#pragma mark Public

- (instancetype)init;

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRC_CancelPopup)

FOUNDATION_EXPORT void PIRC_CancelPopup_init(PIRC_CancelPopup *self);

FOUNDATION_EXPORT PIRC_CancelPopup *new_PIRC_CancelPopup_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRC_CancelPopup *create_PIRC_CancelPopup_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRC_CancelPopup)

@compatibility_alias DePidataRailClientCancelPopup PIRC_CancelPopup;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientCancelPopup")
