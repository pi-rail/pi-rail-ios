//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/uiModels/EditCfgUI.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/uiModels/EditCfgUI.h"
#include "de/pidata/rail/client/uiModels/RailUiFactory.h"
#include "de/pidata/rail/model/Cfg.h"
#include "de/pidata/rail/model/ExtCfg.h"
#include "de/pidata/rail/model/InCfg.h"
#include "de/pidata/rail/model/IoCfg.h"
#include "de/pidata/rail/model/NetCfg.h"
#include "de/pidata/rail/model/OutCfg.h"
#include "de/pidata/rail/model/PortCfg.h"
#include "de/pidata/rail/track/TrackCfg.h"
#include "java/net/InetAddress.h"
#include "java/net/UnknownHostException.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/uiModels/EditCfgUI must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRCU_EditCfgUI () {
 @public
  PIQ_QName *deviceId_;
  JavaNetInetAddress *inetAddress_;
}

@end

J2OBJC_FIELD_SETTER(PIRCU_EditCfgUI, deviceId_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIRCU_EditCfgUI, inetAddress_, JavaNetInetAddress *)

J2OBJC_INITIALIZED_DEFN(PIRCU_EditCfgUI)

PIQ_Namespace *PIRCU_EditCfgUI_NAMESPACE;
PIQ_QName *PIRCU_EditCfgUI_ID_CFG;
PIQ_QName *PIRCU_EditCfgUI_ID_DEVICEADDRESS;
PIQ_QName *PIRCU_EditCfgUI_ID_DEVICENAME;
PIQ_QName *PIRCU_EditCfgUI_ID_IOCFG;
PIQ_QName *PIRCU_EditCfgUI_ID_NETCFG;
PIQ_QName *PIRCU_EditCfgUI_ID_TRACKCFG;

@implementation PIRCU_EditCfgUI

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCU_EditCfgUI_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRCU_EditCfgUI_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRCU_EditCfgUI_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (NSString *)getDeviceName {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIRCU_EditCfgUI_ID_DEVICENAME], [NSString class]);
}

- (void)setDeviceNameWithNSString:(NSString *)deviceName {
  [self setWithPIQ_QName:PIRCU_EditCfgUI_ID_DEVICENAME withId:deviceName];
}

- (NSString *)getDeviceAddress {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIRCU_EditCfgUI_ID_DEVICEADDRESS], [NSString class]);
}

- (void)setDeviceAddressWithNSString:(NSString *)deviceAddress {
  [self setWithPIQ_QName:PIRCU_EditCfgUI_ID_DEVICEADDRESS withId:deviceAddress];
}

- (PIRM_Cfg *)getCfg {
  return (PIRM_Cfg *) cast_chk([self getWithPIQ_QName:PIRCU_EditCfgUI_ID_CFG withPIQ_Key:nil], [PIRM_Cfg class]);
}

- (void)setCfgWithPIRM_Cfg:(PIRM_Cfg *)cfg {
  [self setChildWithPIQ_QName:PIRCU_EditCfgUI_ID_CFG withPIMR_Model:cfg];
}

- (PIRM_IoCfg *)getIoCfg {
  return (PIRM_IoCfg *) cast_chk([self getWithPIQ_QName:PIRCU_EditCfgUI_ID_IOCFG withPIQ_Key:nil], [PIRM_IoCfg class]);
}

- (void)setIoCfgWithPIRM_IoCfg:(PIRM_IoCfg *)ioCfg {
  [self setChildWithPIQ_QName:PIRCU_EditCfgUI_ID_IOCFG withPIMR_Model:ioCfg];
}

- (PIRM_NetCfg *)getNetCfg {
  return (PIRM_NetCfg *) cast_chk([self getWithPIQ_QName:PIRCU_EditCfgUI_ID_NETCFG withPIQ_Key:nil], [PIRM_NetCfg class]);
}

- (void)setNetCfgWithPIRM_NetCfg:(PIRM_NetCfg *)netCfg {
  [self setChildWithPIQ_QName:PIRCU_EditCfgUI_ID_NETCFG withPIMR_Model:netCfg];
}

- (DePidataRailTrackTrackCfg *)getTrackCfg {
  return (DePidataRailTrackTrackCfg *) cast_chk([self getWithPIQ_QName:PIRCU_EditCfgUI_ID_TRACKCFG withPIQ_Key:nil], [DePidataRailTrackTrackCfg class]);
}

- (void)setTrackCfgWithDePidataRailTrackTrackCfg:(DePidataRailTrackTrackCfg *)trackCfg {
  [self setChildWithPIQ_QName:PIRCU_EditCfgUI_ID_TRACKCFG withPIMR_Model:trackCfg];
}

- (instancetype)initWithNSString:(NSString *)name
                   withPIQ_QName:(PIQ_QName *)deviceId
          withJavaNetInetAddress:(JavaNetInetAddress *)inetAddress {
  PIRCU_EditCfgUI_initWithNSString_withPIQ_QName_withJavaNetInetAddress_(self, name, deviceId, inetAddress);
  return self;
}

- (JavaNetInetAddress *)getInetAddress {
  if (inetAddress_ == nil) {
    @try {
      inetAddress_ = JavaNetInetAddress_getByNameWithNSString_([self getDeviceAddress]);
    }
    @catch (JavaNetUnknownHostException *e) {
      PICL_Logger_errorWithNSString_(@"Error getting IP address, e ");
    }
  }
  return inetAddress_;
}

- (PIQ_QName *)getDeviceID {
  return deviceId_;
}

- (PIRM_OutCfg *)findOutCfgWithPIQ_QName:(PIQ_QName *)outPinID {
  if (outPinID == nil) {
    return nil;
  }
  PIRM_IoCfg *ioCfg = [self getIoCfg];
  if (ioCfg != nil) {
    PIRM_OutCfg *outCfg = [ioCfg getOutCfgWithPIQ_Key:outPinID];
    if (outCfg != nil) {
      return outCfg;
    }
    for (PIRM_ExtCfg * __strong extCfg in nil_chk([ioCfg extCfgIter])) {
      outCfg = [((PIRM_ExtCfg *) nil_chk(extCfg)) getOutCfgWithPIQ_Key:outPinID];
      if (outCfg != nil) {
        return outCfg;
      }
    }
  }
  PIRM_Cfg *cfg = [self getCfg];
  if (cfg != nil) {
    for (PIRM_ExtCfg * __strong extCfg in nil_chk([cfg extCfgIter])) {
      PIRM_OutCfg *outCfg = [((PIRM_ExtCfg *) nil_chk(extCfg)) getOutCfgWithPIQ_Key:outPinID];
      if (outCfg != nil) {
        return outCfg;
      }
    }
  }
  return nil;
}

- (PIRM_InCfg *)findInCfgWithPIQ_QName:(PIQ_QName *)inPinID {
  if (inPinID == nil) {
    return nil;
  }
  PIRM_IoCfg *ioCfg = [self getIoCfg];
  if (ioCfg != nil) {
    PIRM_InCfg *inCfg = [ioCfg getInCfgWithPIQ_Key:inPinID];
    if (inCfg != nil) {
      return inCfg;
    }
    for (PIRM_ExtCfg * __strong extCfg in nil_chk([ioCfg extCfgIter])) {
      inCfg = [((PIRM_ExtCfg *) nil_chk(extCfg)) getInCfgWithPIQ_Key:inPinID];
      if (inCfg != nil) {
        return inCfg;
      }
    }
  }
  PIRM_Cfg *cfg = [self getCfg];
  if (cfg != nil) {
    for (PIRM_ExtCfg * __strong extCfg in nil_chk([cfg extCfgIter])) {
      PIRM_InCfg *inCfg = [((PIRM_ExtCfg *) nil_chk(extCfg)) getInCfgWithPIQ_Key:inPinID];
      if (inCfg != nil) {
        return inCfg;
      }
    }
  }
  return nil;
}

- (PIRM_PortCfg *)findPortCfgWithPIQ_QName:(PIQ_QName *)portID {
  if (portID == nil) {
    return nil;
  }
  PIRM_IoCfg *ioCfg = [self getIoCfg];
  if (ioCfg != nil) {
    PIRM_PortCfg *portCfg = [ioCfg getPortCfgWithPIQ_Key:portID];
    if (portCfg != nil) {
      return portCfg;
    }
    for (PIRM_ExtCfg * __strong extCfg in nil_chk([ioCfg extCfgIter])) {
      portCfg = [((PIRM_ExtCfg *) nil_chk(extCfg)) getPortCfgWithPIQ_Key:portID];
      if (portCfg != nil) {
        return portCfg;
      }
    }
  }
  PIRM_Cfg *cfg = [self getCfg];
  if (cfg != nil) {
    for (PIRM_ExtCfg * __strong extCfg in nil_chk([cfg extCfgIter])) {
      PIRM_PortCfg *portCfg = [((PIRM_ExtCfg *) nil_chk(extCfg)) getPortCfgWithPIQ_Key:portID];
      if (portCfg != nil) {
        return portCfg;
      }
    }
  }
  return nil;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, 3, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 5, -1, -1, -1, -1 },
    { NULL, "LPIRM_Cfg;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "LPIRM_IoCfg;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "LPIRM_NetCfg;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 12, -1, -1, -1, -1 },
    { NULL, "LDePidataRailTrackTrackCfg;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 13, 14, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 15, -1, -1, -1, -1 },
    { NULL, "LJavaNetInetAddress;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIRM_OutCfg;", 0x1, 16, 17, -1, -1, -1, -1 },
    { NULL, "LPIRM_InCfg;", 0x1, 18, 17, -1, -1, -1, -1 },
    { NULL, "LPIRM_PortCfg;", 0x1, 19, 17, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getDeviceName);
  methods[4].selector = @selector(setDeviceNameWithNSString:);
  methods[5].selector = @selector(getDeviceAddress);
  methods[6].selector = @selector(setDeviceAddressWithNSString:);
  methods[7].selector = @selector(getCfg);
  methods[8].selector = @selector(setCfgWithPIRM_Cfg:);
  methods[9].selector = @selector(getIoCfg);
  methods[10].selector = @selector(setIoCfgWithPIRM_IoCfg:);
  methods[11].selector = @selector(getNetCfg);
  methods[12].selector = @selector(setNetCfgWithPIRM_NetCfg:);
  methods[13].selector = @selector(getTrackCfg);
  methods[14].selector = @selector(setTrackCfgWithDePidataRailTrackTrackCfg:);
  methods[15].selector = @selector(initWithNSString:withPIQ_QName:withJavaNetInetAddress:);
  methods[16].selector = @selector(getInetAddress);
  methods[17].selector = @selector(getDeviceID);
  methods[18].selector = @selector(findOutCfgWithPIQ_QName:);
  methods[19].selector = @selector(findInCfgWithPIQ_QName:);
  methods[20].selector = @selector(findPortCfgWithPIQ_QName:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 20, -1, -1 },
    { "ID_CFG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 21, -1, -1 },
    { "ID_DEVICEADDRESS", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 22, -1, -1 },
    { "ID_DEVICENAME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 23, -1, -1 },
    { "ID_IOCFG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 24, -1, -1 },
    { "ID_NETCFG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 25, -1, -1 },
    { "ID_TRACKCFG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 26, -1, -1 },
    { "deviceId_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "inetAddress_", "LJavaNetInetAddress;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setDeviceName", "LNSString;", "setDeviceAddress", "setCfg", "LPIRM_Cfg;", "setIoCfg", "LPIRM_IoCfg;", "setNetCfg", "LPIRM_NetCfg;", "setTrackCfg", "LDePidataRailTrackTrackCfg;", "LNSString;LPIQ_QName;LJavaNetInetAddress;", "findOutCfg", "LPIQ_QName;", "findInCfg", "findPortCfg", &PIRCU_EditCfgUI_NAMESPACE, &PIRCU_EditCfgUI_ID_CFG, &PIRCU_EditCfgUI_ID_DEVICEADDRESS, &PIRCU_EditCfgUI_ID_DEVICENAME, &PIRCU_EditCfgUI_ID_IOCFG, &PIRCU_EditCfgUI_ID_NETCFG, &PIRCU_EditCfgUI_ID_TRACKCFG };
  static const J2ObjcClassInfo _PIRCU_EditCfgUI = { "EditCfgUI", "de.pidata.rail.client.uiModels", ptrTable, methods, fields, 7, 0x1, 21, 9, -1, -1, -1, -1, -1 };
  return &_PIRCU_EditCfgUI;
}

+ (void)initialize {
  if (self == [PIRCU_EditCfgUI class]) {
    PIRCU_EditCfgUI_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/pi-rail-ui.xsd");
    PIRCU_EditCfgUI_ID_CFG = [((PIQ_Namespace *) nil_chk(PIRCU_EditCfgUI_NAMESPACE)) getQNameWithNSString:@"cfg"];
    PIRCU_EditCfgUI_ID_DEVICEADDRESS = [PIRCU_EditCfgUI_NAMESPACE getQNameWithNSString:@"deviceAddress"];
    PIRCU_EditCfgUI_ID_DEVICENAME = [PIRCU_EditCfgUI_NAMESPACE getQNameWithNSString:@"deviceName"];
    PIRCU_EditCfgUI_ID_IOCFG = [PIRCU_EditCfgUI_NAMESPACE getQNameWithNSString:@"ioCfg"];
    PIRCU_EditCfgUI_ID_NETCFG = [PIRCU_EditCfgUI_NAMESPACE getQNameWithNSString:@"netCfg"];
    PIRCU_EditCfgUI_ID_TRACKCFG = [PIRCU_EditCfgUI_NAMESPACE getQNameWithNSString:@"trackCfg"];
    J2OBJC_SET_INITIALIZED(PIRCU_EditCfgUI)
  }
}

@end

void PIRCU_EditCfgUI_init(PIRCU_EditCfgUI *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(PIRCU_RailUiFactory, EDITCFGUI_TYPE), nil, nil, nil);
}

PIRCU_EditCfgUI *new_PIRCU_EditCfgUI_init() {
  J2OBJC_NEW_IMPL(PIRCU_EditCfgUI, init)
}

PIRCU_EditCfgUI *create_PIRCU_EditCfgUI_init() {
  J2OBJC_CREATE_IMPL(PIRCU_EditCfgUI, init)
}

void PIRCU_EditCfgUI_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRCU_EditCfgUI *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRCU_RailUiFactory, EDITCFGUI_TYPE), attributeNames, anyAttribs, childNames);
}

PIRCU_EditCfgUI *new_PIRCU_EditCfgUI_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRCU_EditCfgUI, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIRCU_EditCfgUI *create_PIRCU_EditCfgUI_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRCU_EditCfgUI, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIRCU_EditCfgUI_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRCU_EditCfgUI *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIRCU_EditCfgUI *new_PIRCU_EditCfgUI_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRCU_EditCfgUI, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIRCU_EditCfgUI *create_PIRCU_EditCfgUI_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRCU_EditCfgUI, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

void PIRCU_EditCfgUI_initWithNSString_withPIQ_QName_withJavaNetInetAddress_(PIRCU_EditCfgUI *self, NSString *name, PIQ_QName *deviceId, JavaNetInetAddress *inetAddress) {
  PIRCU_EditCfgUI_init(self);
  [self setDeviceNameWithNSString:name];
  self->deviceId_ = deviceId;
  self->inetAddress_ = inetAddress;
  [self setDeviceAddressWithNSString:[((JavaNetInetAddress *) nil_chk(inetAddress)) getHostAddress]];
}

PIRCU_EditCfgUI *new_PIRCU_EditCfgUI_initWithNSString_withPIQ_QName_withJavaNetInetAddress_(NSString *name, PIQ_QName *deviceId, JavaNetInetAddress *inetAddress) {
  J2OBJC_NEW_IMPL(PIRCU_EditCfgUI, initWithNSString_withPIQ_QName_withJavaNetInetAddress_, name, deviceId, inetAddress)
}

PIRCU_EditCfgUI *create_PIRCU_EditCfgUI_initWithNSString_withPIQ_QName_withJavaNetInetAddress_(NSString *name, PIQ_QName *deviceId, JavaNetInetAddress *inetAddress) {
  J2OBJC_CREATE_IMPL(PIRCU_EditCfgUI, initWithNSString_withPIQ_QName_withJavaNetInetAddress_, name, deviceId, inetAddress)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCU_EditCfgUI)

J2OBJC_NAME_MAPPING(PIRCU_EditCfgUI, "de.pidata.rail.client.uiModels", "PIRCU_")
