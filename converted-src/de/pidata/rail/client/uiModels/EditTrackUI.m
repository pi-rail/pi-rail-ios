//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/uiModels/EditTrackUI.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/guidef/StringTable.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Filter.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/tree/ModelIteratorCollection.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/complex/DefaultComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/uiModels/EditTrackUI.h"
#include "de/pidata/rail/client/uiModels/RailUiFactory.h"
#include "de/pidata/rail/comm/PiRail.h"
#include "de/pidata/rail/model/TrackPos.h"
#include "de/pidata/rail/railway/ModelRailway.h"
#include "de/pidata/rail/railway/RailwayFactory.h"
#include "de/pidata/rail/track/PiRailTrackFactory.h"
#include "de/pidata/rail/track/TrackCfg.h"
#include "java/lang/Integer.h"
#include "java/net/InetAddress.h"
#include "java/net/UnknownHostException.h"
#include "java/util/Hashtable.h"
#include "java/util/LinkedList.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/uiModels/EditTrackUI must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRCU_EditTrackUI () {
 @public
  JavaNetInetAddress *inetAddress_;
}

@end

J2OBJC_FIELD_SETTER(PIRCU_EditTrackUI, inetAddress_, JavaNetInetAddress *)

J2OBJC_INITIALIZED_DEFN(PIRCU_EditTrackUI)

PIQ_Namespace *PIRCU_EditTrackUI_NAMESPACE;
PIQ_QName *PIRCU_EditTrackUI_ID_DEVICEADDRESS;
PIQ_QName *PIRCU_EditTrackUI_ID_DEVICENAME;
PIQ_QName *PIRCU_EditTrackUI_ID_EDITACTIONID;
PIQ_QName *PIRCU_EditTrackUI_ID_EDITTRACKPOS;
PIQ_QName *PIRCU_EditTrackUI_ID_EVFROMIDLIST;
PIQ_QName *PIRCU_EditTrackUI_ID_EVIDLIST;
PIQ_QName *PIRCU_EditTrackUI_ID_EVSRCIDLIST;
PIQ_QName *PIRCU_EditTrackUI_ID_TRACKCFG;
PIQ_QName *PIRCU_EditTrackUI_ID_ACTION_GROUP;
PIQ_QName *PIRCU_EditTrackUI_ID_ALL_PANEL_CFGS;
id<PIMT_ComplexType> PIRCU_EditTrackUI_TRANSIENT_TYPE;

@implementation PIRCU_EditTrackUI

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCU_EditTrackUI_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRCU_EditTrackUI_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRCU_EditTrackUI_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (NSString *)getDeviceName {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIRCU_EditTrackUI_ID_DEVICENAME], [NSString class]);
}

- (void)setDeviceNameWithNSString:(NSString *)deviceName {
  [self setWithPIQ_QName:PIRCU_EditTrackUI_ID_DEVICENAME withId:deviceName];
}

- (NSString *)getDeviceAddress {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIRCU_EditTrackUI_ID_DEVICEADDRESS], [NSString class]);
}

- (void)setDeviceAddressWithNSString:(NSString *)deviceAddress {
  [self setWithPIQ_QName:PIRCU_EditTrackUI_ID_DEVICEADDRESS withId:deviceAddress];
}

- (PIQ_QName *)getEditActionID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRCU_EditTrackUI_ID_EDITACTIONID], [PIQ_QName class]);
}

- (void)setEditActionIDWithPIQ_QName:(PIQ_QName *)editActionID {
  [self setWithPIQ_QName:PIRCU_EditTrackUI_ID_EDITACTIONID withId:editActionID];
}

- (DePidataRailTrackTrackCfg *)getTrackCfg {
  return (DePidataRailTrackTrackCfg *) cast_chk([self getWithPIQ_QName:PIRCU_EditTrackUI_ID_TRACKCFG withPIQ_Key:nil], [DePidataRailTrackTrackCfg class]);
}

- (void)setTrackCfgWithDePidataRailTrackTrackCfg:(DePidataRailTrackTrackCfg *)trackCfg {
  [self setChildWithPIQ_QName:PIRCU_EditTrackUI_ID_TRACKCFG withPIMR_Model:trackCfg];
}

- (PIGG_StringTable *)getEvIDList {
  return (PIGG_StringTable *) cast_chk([self getWithPIQ_QName:PIRCU_EditTrackUI_ID_EVIDLIST withPIQ_Key:nil], [PIGG_StringTable class]);
}

- (void)setEvIDListWithPIGG_StringTable:(PIGG_StringTable *)evIDList {
  [self setChildWithPIQ_QName:PIRCU_EditTrackUI_ID_EVIDLIST withPIMR_Model:evIDList];
}

- (PIGG_StringTable *)getEvSrcIDList {
  return (PIGG_StringTable *) cast_chk([self getWithPIQ_QName:PIRCU_EditTrackUI_ID_EVSRCIDLIST withPIQ_Key:nil], [PIGG_StringTable class]);
}

- (void)setEvSrcIDListWithPIGG_StringTable:(PIGG_StringTable *)evSrcIDList {
  [self setChildWithPIQ_QName:PIRCU_EditTrackUI_ID_EVSRCIDLIST withPIMR_Model:evSrcIDList];
}

- (PIGG_StringTable *)getEvFromIDList {
  return (PIGG_StringTable *) cast_chk([self getWithPIQ_QName:PIRCU_EditTrackUI_ID_EVFROMIDLIST withPIQ_Key:nil], [PIGG_StringTable class]);
}

- (void)setEvFromIDListWithPIGG_StringTable:(PIGG_StringTable *)evFromIDList {
  [self setChildWithPIQ_QName:PIRCU_EditTrackUI_ID_EVFROMIDLIST withPIMR_Model:evFromIDList];
}

- (PIRM_TrackPos *)getEditTrackPos {
  return (PIRM_TrackPos *) cast_chk([self getWithPIQ_QName:PIRCU_EditTrackUI_ID_EDITTRACKPOS withPIQ_Key:nil], [PIRM_TrackPos class]);
}

- (void)setEditTrackPosWithPIRM_TrackPos:(PIRM_TrackPos *)editTrackPos {
  [self setChildWithPIQ_QName:PIRCU_EditTrackUI_ID_EDITTRACKPOS withPIMR_Model:editTrackPos];
}

- (instancetype)initWithNSString:(NSString *)name
          withJavaNetInetAddress:(JavaNetInetAddress *)inetAddress {
  PIRCU_EditTrackUI_initWithNSString_withJavaNetInetAddress_(self, name, inetAddress);
  return self;
}

- (JavaNetInetAddress *)getInetAddress {
  if (inetAddress_ == nil) {
    @try {
      inetAddress_ = JavaNetInetAddress_getByNameWithNSString_([self getDeviceAddress]);
    }
    @catch (JavaNetUnknownHostException *e) {
      PICL_Logger_errorWithNSString_(@"Error getting IP address, e ");
    }
  }
  return inetAddress_;
}

- (void)initTransient {
  [super initTransient];
}

- (id<PIMT_ComplexType>)transientType {
  return PIRCU_EditTrackUI_TRANSIENT_TYPE;
}

- (id<PIMR_ModelIterator>)transientChildIterWithPIQ_QName:(PIQ_QName *)relationName
                                          withPIMR_Filter:(id<PIMR_Filter>)filter {
  if (JreObjectEqualsEquals(relationName, PIRCU_EditTrackUI_ID_ACTION_GROUP)) {
    return [((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) actionGroupIter];
  }
  else if (JreObjectEqualsEquals(relationName, PIRCU_EditTrackUI_ID_ALL_PANEL_CFGS)) {
    return [self allPanelCfgIterWithPIMR_Filter:filter];
  }
  else {
    return [super transientChildIterWithPIQ_QName:relationName withPIMR_Filter:filter];
  }
}

- (id<PIMR_ModelIterator>)allPanelCfgIterWithPIMR_Filter:(id<PIMR_Filter>)filter {
  return new_PIMR_ModelIteratorCollection_initWithJavaUtilCollection_withPIMR_Filter_([((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) getPanelCfgsRecursive], filter);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, 3, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 5, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "LDePidataRailTrackTrackCfg;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "LPIGG_StringTable;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 12, -1, -1, -1, -1 },
    { NULL, "LPIGG_StringTable;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 13, 12, -1, -1, -1, -1 },
    { NULL, "LPIGG_StringTable;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 12, -1, -1, -1, -1 },
    { NULL, "LPIRM_TrackPos;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 15, 16, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 17, -1, -1, -1, -1 },
    { NULL, "LJavaNetInetAddress;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMT_ComplexType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, 18, 19, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, 20, 21, -1, 22, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getDeviceName);
  methods[4].selector = @selector(setDeviceNameWithNSString:);
  methods[5].selector = @selector(getDeviceAddress);
  methods[6].selector = @selector(setDeviceAddressWithNSString:);
  methods[7].selector = @selector(getEditActionID);
  methods[8].selector = @selector(setEditActionIDWithPIQ_QName:);
  methods[9].selector = @selector(getTrackCfg);
  methods[10].selector = @selector(setTrackCfgWithDePidataRailTrackTrackCfg:);
  methods[11].selector = @selector(getEvIDList);
  methods[12].selector = @selector(setEvIDListWithPIGG_StringTable:);
  methods[13].selector = @selector(getEvSrcIDList);
  methods[14].selector = @selector(setEvSrcIDListWithPIGG_StringTable:);
  methods[15].selector = @selector(getEvFromIDList);
  methods[16].selector = @selector(setEvFromIDListWithPIGG_StringTable:);
  methods[17].selector = @selector(getEditTrackPos);
  methods[18].selector = @selector(setEditTrackPosWithPIRM_TrackPos:);
  methods[19].selector = @selector(initWithNSString:withJavaNetInetAddress:);
  methods[20].selector = @selector(getInetAddress);
  methods[21].selector = @selector(initTransient);
  methods[22].selector = @selector(transientType);
  methods[23].selector = @selector(transientChildIterWithPIQ_QName:withPIMR_Filter:);
  methods[24].selector = @selector(allPanelCfgIterWithPIMR_Filter:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 23, -1, -1 },
    { "ID_DEVICEADDRESS", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 24, -1, -1 },
    { "ID_DEVICENAME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 25, -1, -1 },
    { "ID_EDITACTIONID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 26, -1, -1 },
    { "ID_EDITTRACKPOS", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 27, -1, -1 },
    { "ID_EVFROMIDLIST", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 28, -1, -1 },
    { "ID_EVIDLIST", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 29, -1, -1 },
    { "ID_EVSRCIDLIST", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 30, -1, -1 },
    { "ID_TRACKCFG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 31, -1, -1 },
    { "ID_ACTION_GROUP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 32, -1, -1 },
    { "ID_ALL_PANEL_CFGS", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 33, -1, -1 },
    { "TRANSIENT_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x19, -1, 34, -1, -1 },
    { "inetAddress_", "LJavaNetInetAddress;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setDeviceName", "LNSString;", "setDeviceAddress", "setEditActionID", "LPIQ_QName;", "setTrackCfg", "LDePidataRailTrackTrackCfg;", "setEvIDList", "LPIGG_StringTable;", "setEvSrcIDList", "setEvFromIDList", "setEditTrackPos", "LPIRM_TrackPos;", "LNSString;LJavaNetInetAddress;", "transientChildIter", "LPIQ_QName;LPIMR_Filter;", "allPanelCfgIter", "LPIMR_Filter;", "(Lde/pidata/models/tree/Filter;)Lde/pidata/models/tree/ModelIterator<Lde/pidata/rail/track/PanelCfg;>;", &PIRCU_EditTrackUI_NAMESPACE, &PIRCU_EditTrackUI_ID_DEVICEADDRESS, &PIRCU_EditTrackUI_ID_DEVICENAME, &PIRCU_EditTrackUI_ID_EDITACTIONID, &PIRCU_EditTrackUI_ID_EDITTRACKPOS, &PIRCU_EditTrackUI_ID_EVFROMIDLIST, &PIRCU_EditTrackUI_ID_EVIDLIST, &PIRCU_EditTrackUI_ID_EVSRCIDLIST, &PIRCU_EditTrackUI_ID_TRACKCFG, &PIRCU_EditTrackUI_ID_ACTION_GROUP, &PIRCU_EditTrackUI_ID_ALL_PANEL_CFGS, &PIRCU_EditTrackUI_TRANSIENT_TYPE };
  static const J2ObjcClassInfo _PIRCU_EditTrackUI = { "EditTrackUI", "de.pidata.rail.client.uiModels", ptrTable, methods, fields, 7, 0x1, 25, 13, -1, -1, -1, -1, -1 };
  return &_PIRCU_EditTrackUI;
}

+ (void)initialize {
  if (self == [PIRCU_EditTrackUI class]) {
    PIRCU_EditTrackUI_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/pi-rail-ui.xsd");
    PIRCU_EditTrackUI_ID_DEVICEADDRESS = [((PIQ_Namespace *) nil_chk(PIRCU_EditTrackUI_NAMESPACE)) getQNameWithNSString:@"deviceAddress"];
    PIRCU_EditTrackUI_ID_DEVICENAME = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"deviceName"];
    PIRCU_EditTrackUI_ID_EDITACTIONID = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"editActionID"];
    PIRCU_EditTrackUI_ID_EDITTRACKPOS = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"editTrackPos"];
    PIRCU_EditTrackUI_ID_EVFROMIDLIST = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"evFromIDList"];
    PIRCU_EditTrackUI_ID_EVIDLIST = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"evIDList"];
    PIRCU_EditTrackUI_ID_EVSRCIDLIST = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"evSrcIDList"];
    PIRCU_EditTrackUI_ID_TRACKCFG = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"trackCfg"];
    PIRCU_EditTrackUI_ID_ACTION_GROUP = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"actionGroup"];
    PIRCU_EditTrackUI_ID_ALL_PANEL_CFGS = [PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"allPanelCfgs"];
    {
      PIMO_DefaultComplexType *type = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([PIRCU_EditTrackUI_NAMESPACE getQNameWithNSString:@"EditTrackUI_Transient"], [PIRCU_EditTrackUI_class_() getName], 0);
      PIRCU_EditTrackUI_TRANSIENT_TYPE = type;
      [type addRelationWithPIQ_QName:PIRCU_EditTrackUI_ID_ACTION_GROUP withPIMT_Type:JreLoadStatic(PIRR_RailwayFactory, ACTIONGROUP_TYPE) withInt:0 withInt:JavaLangInteger_MAX_VALUE];
      [type addRelationWithPIQ_QName:PIRCU_EditTrackUI_ID_ALL_PANEL_CFGS withPIMT_Type:JreLoadStatic(DePidataRailTrackPiRailTrackFactory, PANELCFG_TYPE) withInt:0 withInt:JavaLangInteger_MAX_VALUE];
    }
    J2OBJC_SET_INITIALIZED(PIRCU_EditTrackUI)
  }
}

@end

void PIRCU_EditTrackUI_init(PIRCU_EditTrackUI *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(PIRCU_RailUiFactory, EDITTRACKUI_TYPE), nil, nil, nil);
}

PIRCU_EditTrackUI *new_PIRCU_EditTrackUI_init() {
  J2OBJC_NEW_IMPL(PIRCU_EditTrackUI, init)
}

PIRCU_EditTrackUI *create_PIRCU_EditTrackUI_init() {
  J2OBJC_CREATE_IMPL(PIRCU_EditTrackUI, init)
}

void PIRCU_EditTrackUI_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRCU_EditTrackUI *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRCU_RailUiFactory, EDITTRACKUI_TYPE), attributeNames, anyAttribs, childNames);
}

PIRCU_EditTrackUI *new_PIRCU_EditTrackUI_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRCU_EditTrackUI, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIRCU_EditTrackUI *create_PIRCU_EditTrackUI_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRCU_EditTrackUI, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIRCU_EditTrackUI_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRCU_EditTrackUI *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIRCU_EditTrackUI *new_PIRCU_EditTrackUI_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRCU_EditTrackUI, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIRCU_EditTrackUI *create_PIRCU_EditTrackUI_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRCU_EditTrackUI, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

void PIRCU_EditTrackUI_initWithNSString_withJavaNetInetAddress_(PIRCU_EditTrackUI *self, NSString *name, JavaNetInetAddress *inetAddress) {
  PIRCU_EditTrackUI_init(self);
  [self setDeviceNameWithNSString:name];
  self->inetAddress_ = inetAddress;
  [self setDeviceAddressWithNSString:[((JavaNetInetAddress *) nil_chk(inetAddress)) getHostAddress]];
}

PIRCU_EditTrackUI *new_PIRCU_EditTrackUI_initWithNSString_withJavaNetInetAddress_(NSString *name, JavaNetInetAddress *inetAddress) {
  J2OBJC_NEW_IMPL(PIRCU_EditTrackUI, initWithNSString_withJavaNetInetAddress_, name, inetAddress)
}

PIRCU_EditTrackUI *create_PIRCU_EditTrackUI_initWithNSString_withJavaNetInetAddress_(NSString *name, JavaNetInetAddress *inetAddress) {
  J2OBJC_CREATE_IMPL(PIRCU_EditTrackUI, initWithNSString_withJavaNetInetAddress_, name, inetAddress)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCU_EditTrackUI)

J2OBJC_NAME_MAPPING(PIRCU_EditTrackUI, "de.pidata.rail.client.uiModels", "PIRCU_")
