//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/ioboard/NextIO.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/GuiOperation.h"
#include "de/pidata/gui/controller/base/MutableControllerGroup.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/ioboard/NextIO.h"
#include "de/pidata/rail/comm/PiRail.h"
#include "de/pidata/rail/railway/ModelRailway.h"
#include "de/pidata/rail/railway/SwitchBox.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/ioboard/NextIO must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIRCD_NextIO)

PIQ_QName *PIRCD_NextIO_PREV_IO;
PIQ_QName *PIRCD_NextIO_NEXT_IO;

@implementation PIRCD_NextIO

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCD_NextIO_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  PIRR_ModelRailway *modelRailway = [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway];
  if (JreObjectEqualsEquals(eventID, PIRCD_NextIO_PREV_IO)) {
    PIRR_SwitchBox *tower = (PIRR_SwitchBox *) cast_chk(dataContext, [PIRR_SwitchBox class]);
    PIRR_SwitchBox *prevTower;
    if (tower == nil) {
      prevTower = (PIRR_SwitchBox *) cast_chk([((PIRR_ModelRailway *) nil_chk(modelRailway)) lastChildWithPIQ_QName:JreLoadStatic(PIRR_ModelRailway, ID_SWITCHBOX)], [PIRR_SwitchBox class]);
    }
    else {
      prevTower = (PIRR_SwitchBox *) cast_chk([tower prevSiblingWithPIQ_QName:[tower getParentRelationID]], [PIRR_SwitchBox class]);
      if (prevTower == nil) {
        prevTower = (PIRR_SwitchBox *) cast_chk([((PIRR_ModelRailway *) nil_chk(modelRailway)) lastChildWithPIQ_QName:JreLoadStatic(PIRR_ModelRailway, ID_SWITCHBOX)], [PIRR_SwitchBox class]);
      }
    }
    if (prevTower != nil) {
      [((id<PIGC_MutableControllerGroup>) nil_chk([((id<PIGC_Controller>) nil_chk(sourceCtrl)) getControllerGroup])) setModelWithPIMR_Model:prevTower];
    }
  }
  else if (JreObjectEqualsEquals(eventID, PIRCD_NextIO_NEXT_IO)) {
    PIRR_SwitchBox *tower = (PIRR_SwitchBox *) cast_chk(dataContext, [PIRR_SwitchBox class]);
    PIRR_SwitchBox *nextTower;
    if (tower == nil) {
      nextTower = (PIRR_SwitchBox *) cast_chk([((PIRR_ModelRailway *) nil_chk(modelRailway)) firstChildWithPIQ_QName:JreLoadStatic(PIRR_ModelRailway, ID_SWITCHBOX)], [PIRR_SwitchBox class]);
    }
    else {
      nextTower = (PIRR_SwitchBox *) cast_chk([tower nextSiblingWithPIQ_QName:[tower getParentRelationID]], [PIRR_SwitchBox class]);
      if (nextTower == nil) {
        nextTower = (PIRR_SwitchBox *) cast_chk([((PIRR_ModelRailway *) nil_chk(modelRailway)) firstChildWithPIQ_QName:JreLoadStatic(PIRR_ModelRailway, ID_SWITCHBOX)], [PIRR_SwitchBox class]);
      }
    }
    if (nextTower != nil) {
      [((id<PIGC_MutableControllerGroup>) nil_chk([((id<PIGC_Controller>) nil_chk(sourceCtrl)) getControllerGroup])) setModelWithPIMR_Model:nextTower];
    }
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(executeWithPIQ_QName:withPIGC_Controller:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "PREV_IO", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 3, -1, -1 },
    { "NEXT_IO", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 4, -1, -1 },
  };
  static const void *ptrTable[] = { "execute", "LPIQ_QName;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;", &PIRCD_NextIO_PREV_IO, &PIRCD_NextIO_NEXT_IO };
  static const J2ObjcClassInfo _PIRCD_NextIO = { "NextIO", "de.pidata.rail.client.ioboard", ptrTable, methods, fields, 7, 0x1, 2, 2, -1, -1, -1, -1, -1 };
  return &_PIRCD_NextIO;
}

+ (void)initialize {
  if (self == [PIRCD_NextIO class]) {
    PIRCD_NextIO_PREV_IO = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGC_GuiOperation, NAMESPACE))) getQNameWithNSString:@"PrevIO"];
    PIRCD_NextIO_NEXT_IO = [JreLoadStatic(PIGC_GuiOperation, NAMESPACE) getQNameWithNSString:@"NextIO"];
    J2OBJC_SET_INITIALIZED(PIRCD_NextIO)
  }
}

@end

void PIRCD_NextIO_init(PIRCD_NextIO *self) {
  PIGC_GuiOperation_init(self);
}

PIRCD_NextIO *new_PIRCD_NextIO_init() {
  J2OBJC_NEW_IMPL(PIRCD_NextIO, init)
}

PIRCD_NextIO *create_PIRCD_NextIO_init() {
  J2OBJC_CREATE_IMPL(PIRCD_NextIO, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCD_NextIO)

J2OBJC_NAME_MAPPING(PIRCD_NextIO, "de.pidata.rail.client.ioboard", "PIRCD_")
