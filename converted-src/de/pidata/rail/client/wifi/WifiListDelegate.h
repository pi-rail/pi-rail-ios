//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/wifi/WifiListDelegate.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientWifiWifiListDelegate")
#ifdef RESTRICT_DePidataRailClientWifiWifiListDelegate
#define INCLUDE_ALL_DePidataRailClientWifiWifiListDelegate 0
#else
#define INCLUDE_ALL_DePidataRailClientWifiWifiListDelegate 1
#endif
#undef RESTRICT_DePidataRailClientWifiWifiListDelegate

#if !defined (PIRCW_WifiListDelegate_) && (INCLUDE_ALL_DePidataRailClientWifiWifiListDelegate || defined(INCLUDE_PIRCW_WifiListDelegate))
#define PIRCW_WifiListDelegate_

#define RESTRICT_DePidataGuiControllerBaseDialogControllerDelegate 1
#define INCLUDE_PIGC_DialogControllerDelegate 1
#include "de/pidata/gui/controller/base/DialogControllerDelegate.h"

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

@class PIGC_ModuleGroup;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface PIRCW_WifiListDelegate : NSObject < PIGC_DialogControllerDelegate, JavaLangRunnable >

#pragma mark Public

- (instancetype)init;

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

- (id<PISB_ParameterList>)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                     withBoolean:(jboolean)ok;

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(id<PISB_ParameterList>)parameterList OBJC_METHOD_FAMILY_NONE;

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup;

- (void)run;

- (void)setPausedWithBoolean:(jboolean)paused;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCW_WifiListDelegate)

FOUNDATION_EXPORT void PIRCW_WifiListDelegate_init(PIRCW_WifiListDelegate *self);

FOUNDATION_EXPORT PIRCW_WifiListDelegate *new_PIRCW_WifiListDelegate_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCW_WifiListDelegate *create_PIRCW_WifiListDelegate_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCW_WifiListDelegate)

@compatibility_alias DePidataRailClientWifiWifiListDelegate PIRCW_WifiListDelegate;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientWifiWifiListDelegate")
