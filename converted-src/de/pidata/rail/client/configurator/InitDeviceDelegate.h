//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/configurator/InitDeviceDelegate.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientConfiguratorInitDeviceDelegate")
#ifdef RESTRICT_DePidataRailClientConfiguratorInitDeviceDelegate
#define INCLUDE_ALL_DePidataRailClientConfiguratorInitDeviceDelegate 0
#else
#define INCLUDE_ALL_DePidataRailClientConfiguratorInitDeviceDelegate 1
#endif
#undef RESTRICT_DePidataRailClientConfiguratorInitDeviceDelegate

#if !defined (PIRCC_InitDeviceDelegate_) && (INCLUDE_ALL_DePidataRailClientConfiguratorInitDeviceDelegate || defined(INCLUDE_PIRCC_InitDeviceDelegate))
#define PIRCC_InitDeviceDelegate_

#define RESTRICT_DePidataGuiControllerBaseDialogControllerDelegate 1
#define INCLUDE_PIGC_DialogControllerDelegate 1
#include "de/pidata/gui/controller/base/DialogControllerDelegate.h"

@class PIGC_ModuleGroup;
@class PIRCE_EditCfgParamList;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface PIRCC_InitDeviceDelegate : NSObject < PIGC_DialogControllerDelegate >

#pragma mark Public

- (instancetype)init;

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

- (id<PISB_ParameterList>)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                     withBoolean:(jboolean)ok;

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(PIRCE_EditCfgParamList *)parameterList OBJC_METHOD_FAMILY_NONE;

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCC_InitDeviceDelegate)

FOUNDATION_EXPORT void PIRCC_InitDeviceDelegate_init(PIRCC_InitDeviceDelegate *self);

FOUNDATION_EXPORT PIRCC_InitDeviceDelegate *new_PIRCC_InitDeviceDelegate_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCC_InitDeviceDelegate *create_PIRCC_InitDeviceDelegate_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCC_InitDeviceDelegate)

@compatibility_alias DePidataRailClientConfiguratorInitDeviceDelegate PIRCC_InitDeviceDelegate;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientConfiguratorInitDeviceDelegate")
