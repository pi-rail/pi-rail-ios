//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/SelectionFragment.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/ModuleGroup.h"
#include "de/pidata/gui/ui/base/UIContainer.h"
#include "de/pidata/rail/client/SelectionFragment.h"
#include "de/pidata/rail/comm/PiRail.h"
#include "de/pidata/rail/railway/ModelRailway.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/SelectionFragment must be compiled with ARC (-fobjc-arc)"
#endif

@implementation PIRC_SelectionFragment

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRC_SelectionFragment_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)activateModuleWithPIGU_UIContainer:(id<PIGU_UIContainer>)moduleContainer {
  [self setModelWithPIMR_Model:[((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway]];
  [super activateModuleWithPIGU_UIContainer:moduleContainer];
}

- (void)activateWithPIGU_UIContainer:(id<PIGU_UIContainer>)uiContainer {
  @synchronized(self) {
    [super activateWithPIGU_UIContainer:uiContainer];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, 2, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(activateModuleWithPIGU_UIContainer:);
  methods[2].selector = @selector(activateWithPIGU_UIContainer:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "activateModule", "LPIGU_UIContainer;", "activate" };
  static const J2ObjcClassInfo _PIRC_SelectionFragment = { "SelectionFragment", "de.pidata.rail.client", ptrTable, methods, NULL, 7, 0x1, 3, 0, -1, -1, -1, -1, -1 };
  return &_PIRC_SelectionFragment;
}

@end

void PIRC_SelectionFragment_init(PIRC_SelectionFragment *self) {
  PIGC_ModuleGroup_init(self);
}

PIRC_SelectionFragment *new_PIRC_SelectionFragment_init() {
  J2OBJC_NEW_IMPL(PIRC_SelectionFragment, init)
}

PIRC_SelectionFragment *create_PIRC_SelectionFragment_init() {
  J2OBJC_CREATE_IMPL(PIRC_SelectionFragment, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRC_SelectionFragment)

J2OBJC_NAME_MAPPING(PIRC_SelectionFragment, "de.pidata.rail.client", "PIRC_")
