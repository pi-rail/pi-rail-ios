//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/file/SelectAssetFileParamList.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientFileSelectAssetFileParamList")
#ifdef RESTRICT_DePidataRailClientFileSelectAssetFileParamList
#define INCLUDE_ALL_DePidataRailClientFileSelectAssetFileParamList 0
#else
#define INCLUDE_ALL_DePidataRailClientFileSelectAssetFileParamList 1
#endif
#undef RESTRICT_DePidataRailClientFileSelectAssetFileParamList

#if !defined (DePidataRailClientFileSelectAssetFileParamList_) && (INCLUDE_ALL_DePidataRailClientFileSelectAssetFileParamList || defined(INCLUDE_DePidataRailClientFileSelectAssetFileParamList))
#define DePidataRailClientFileSelectAssetFileParamList_

#define RESTRICT_DePidataServiceBaseAbstractParameterList 1
#define INCLUDE_PISB_AbstractParameterList 1
#include "de/pidata/service/base/AbstractParameterList.h"

@class IOSObjectArray;
@class PIQ_QName;
@class PISB_ParameterType;

@interface DePidataRailClientFileSelectAssetFileParamList : PISB_AbstractParameterList

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithNSString:(NSString *)storageType
                    withNSString:(NSString *)dirPath
                    withNSString:(NSString *)filePattern
                    withNSString:(NSString *)fileName
                    withNSString:(NSString *)infoText;

- (NSString *)getDirPath;

- (NSString *)getFileName;

- (NSString *)getFilePattern;

- (NSString *)getInfoText;

- (NSString *)getStorageType;

- (void)setFileNameWithNSString:(NSString *)fileName;

- (void)setStorageTypeWithNSString:(NSString *)storageType;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithInt:(jint)arg0 NS_UNAVAILABLE;

- (instancetype)initWithNSString:(NSString *)arg0 NS_UNAVAILABLE;

- (instancetype)initWithNSString:(NSString *)arg0
     withPISB_ParameterTypeArray:(IOSObjectArray *)arg1
              withPIQ_QNameArray:(IOSObjectArray *)arg2
               withNSObjectArray:(IOSObjectArray *)arg3 NS_UNAVAILABLE;

- (instancetype)initWithPISB_ParameterType:(PISB_ParameterType *)arg0
                             withPIQ_QName:(PIQ_QName *)arg1 NS_UNAVAILABLE;

- (instancetype)initWithPISB_ParameterType:(PISB_ParameterType *)arg0
                             withPIQ_QName:(PIQ_QName *)arg1
                    withPISB_ParameterType:(PISB_ParameterType *)arg2
                             withPIQ_QName:(PIQ_QName *)arg3 NS_UNAVAILABLE;

- (instancetype)initWithPISB_ParameterType:(PISB_ParameterType *)arg0
                             withPIQ_QName:(PIQ_QName *)arg1
                    withPISB_ParameterType:(PISB_ParameterType *)arg2
                             withPIQ_QName:(PIQ_QName *)arg3
                    withPISB_ParameterType:(PISB_ParameterType *)arg4
                             withPIQ_QName:(PIQ_QName *)arg5 NS_UNAVAILABLE;

- (instancetype)initWithPISB_ParameterType:(PISB_ParameterType *)arg0
                             withPIQ_QName:(PIQ_QName *)arg1
                    withPISB_ParameterType:(PISB_ParameterType *)arg2
                             withPIQ_QName:(PIQ_QName *)arg3
                    withPISB_ParameterType:(PISB_ParameterType *)arg4
                             withPIQ_QName:(PIQ_QName *)arg5
                    withPISB_ParameterType:(PISB_ParameterType *)arg6
                             withPIQ_QName:(PIQ_QName *)arg7 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(DePidataRailClientFileSelectAssetFileParamList)

inline PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_get_PARAM_STORAGE_TYPE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_PARAM_STORAGE_TYPE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailClientFileSelectAssetFileParamList, PARAM_STORAGE_TYPE, PIQ_QName *)

inline PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_get_PARAM_DIR_PATH(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_PARAM_DIR_PATH;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailClientFileSelectAssetFileParamList, PARAM_DIR_PATH, PIQ_QName *)

inline PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_get_PARAM_FILE_PATTERN(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_PARAM_FILE_PATTERN;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailClientFileSelectAssetFileParamList, PARAM_FILE_PATTERN, PIQ_QName *)

inline PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_get_PARAM_FILE_NAME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_PARAM_FILE_NAME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailClientFileSelectAssetFileParamList, PARAM_FILE_NAME, PIQ_QName *)

inline PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_get_PARAM_INFO_TEXT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailClientFileSelectAssetFileParamList_PARAM_INFO_TEXT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailClientFileSelectAssetFileParamList, PARAM_INFO_TEXT, PIQ_QName *)

FOUNDATION_EXPORT void DePidataRailClientFileSelectAssetFileParamList_init(DePidataRailClientFileSelectAssetFileParamList *self);

FOUNDATION_EXPORT DePidataRailClientFileSelectAssetFileParamList *new_DePidataRailClientFileSelectAssetFileParamList_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailClientFileSelectAssetFileParamList *create_DePidataRailClientFileSelectAssetFileParamList_init(void);

FOUNDATION_EXPORT void DePidataRailClientFileSelectAssetFileParamList_initWithNSString_withNSString_withNSString_withNSString_withNSString_(DePidataRailClientFileSelectAssetFileParamList *self, NSString *storageType, NSString *dirPath, NSString *filePattern, NSString *fileName, NSString *infoText);

FOUNDATION_EXPORT DePidataRailClientFileSelectAssetFileParamList *new_DePidataRailClientFileSelectAssetFileParamList_initWithNSString_withNSString_withNSString_withNSString_withNSString_(NSString *storageType, NSString *dirPath, NSString *filePattern, NSString *fileName, NSString *infoText) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailClientFileSelectAssetFileParamList *create_DePidataRailClientFileSelectAssetFileParamList_initWithNSString_withNSString_withNSString_withNSString_withNSString_(NSString *storageType, NSString *dirPath, NSString *filePattern, NSString *fileName, NSString *infoText);

J2OBJC_TYPE_LITERAL_HEADER(DePidataRailClientFileSelectAssetFileParamList)

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientFileSelectAssetFileParamList")
