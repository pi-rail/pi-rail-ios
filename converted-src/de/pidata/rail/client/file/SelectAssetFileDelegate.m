//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/file/SelectAssetFileDelegate.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/GuiBuilder.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/ListController.h"
#include "de/pidata/gui/controller/base/ModuleGroup.h"
#include "de/pidata/gui/controller/base/TextController.h"
#include "de/pidata/gui/guidef/KeyAndValue.h"
#include "de/pidata/gui/guidef/StringTable.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/file/SelectAssetFileDelegate.h"
#include "de/pidata/rail/client/file/SelectAssetFileParamList.h"
#include "de/pidata/service/base/ParameterList.h"
#include "de/pidata/string/Helper.h"
#include "de/pidata/system/base/Storage.h"
#include "de/pidata/system/base/SystemManager.h"
#include "de/pidata/system/filebased/FilebasedStorage.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/file/SelectAssetFileDelegate must be compiled with ARC (-fobjc-arc)"
#endif

@interface DePidataRailClientFileSelectAssetFileDelegate () {
 @public
  DePidataRailClientFileSelectAssetFileParamList *parameterList_;
  NSString *externalFilePath_;
  id<PIGC_DialogController> dlgCtrl_;
}

@end

J2OBJC_FIELD_SETTER(DePidataRailClientFileSelectAssetFileDelegate, parameterList_, DePidataRailClientFileSelectAssetFileParamList *)
J2OBJC_FIELD_SETTER(DePidataRailClientFileSelectAssetFileDelegate, externalFilePath_, NSString *)
J2OBJC_FIELD_SETTER(DePidataRailClientFileSelectAssetFileDelegate, dlgCtrl_, id<PIGC_DialogController>)

@implementation DePidataRailClientFileSelectAssetFileDelegate

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  DePidataRailClientFileSelectAssetFileDelegate_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (DePidataRailClientFileSelectAssetFileParamList *)getParameterList {
  return parameterList_;
}

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(DePidataRailClientFileSelectAssetFileParamList *)parameterList {
  self->parameterList_ = parameterList;
  self->dlgCtrl_ = dlgCtrl;
  self->externalFilePath_ = nil;
  PISYSB_Storage *assetStorage = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getStorageWithNSString:[((DePidataRailClientFileSelectAssetFileParamList *) nil_chk(parameterList)) getStorageType] withNSString:[parameterList getDirPath]];
  IOSObjectArray *files = [((PISYSB_Storage *) nil_chk(assetStorage)) list];
  PIGG_StringTable *fileList = PIGG_StringTable_fromArrayWithPIQ_QName_withNSStringArray_([((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"FileList"], files);
  PIGC_TextController *topLabel = (PIGC_TextController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl)) getControllerWithPIQ_QName:[JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"topLabel"]], [PIGC_TextController class]);
  [((PIGC_TextController *) nil_chk(topLabel)) setValueWithId:[parameterList getInfoText]];
  return fileList;
}

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) closeWithBoolean:false];
}

- (DePidataRailClientFileSelectAssetFileParamList *)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                                               withBoolean:(jboolean)ok {
  NSString *selFileName = nil;
  if (ok) {
    if (externalFilePath_ == nil) {
      id<PIGC_ListController> listController = (id<PIGC_ListController>) cast_check([((id<PIGC_DialogController>) nil_chk(dlgCtrl)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"filesList"]], PIGC_ListController_class_());
      if (listController != nil) {
        PIGG_KeyAndValue *selEntry = (PIGG_KeyAndValue *) cast_chk([listController getSelectedRowWithInt:0], [PIGG_KeyAndValue class]);
        if (selEntry != nil) {
          NSString *path = [((DePidataRailClientFileSelectAssetFileParamList *) nil_chk(parameterList_)) getDirPath];
          if (PICS_Helper_isNullOrEmptyWithId_(path) || [((NSString *) nil_chk(path)) isEqual:@"."]) {
            selFileName = [selEntry getValue];
          }
          else {
            if (!PISYSF_FilebasedStorage_isPathSeparatorWithChar_([((NSString *) nil_chk(path)) charAtWithInt:[path java_length] - 1])) {
              (void) JreStrAppendStrong(&path, "C", '/');
            }
            selFileName = JreStrcat("$$", path, [selEntry getValue]);
          }
        }
      }
    }
    else {
      [((DePidataRailClientFileSelectAssetFileParamList *) nil_chk(parameterList_)) setStorageTypeWithNSString:PISYSB_SystemManager_STORAGE_PRIVATE_DOWNLOADS];
      selFileName = externalFilePath_;
    }
  }
  [((DePidataRailClientFileSelectAssetFileParamList *) nil_chk(parameterList_)) setFileNameWithNSString:selFileName];
  return parameterList_;
}

- (void)externalFileSelectedWithNSString:(NSString *)path {
  self->externalFilePath_ = path;
  [((id<PIGC_DialogController>) nil_chk(self->dlgCtrl_)) closeWithBoolean:true];
}

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList {
}

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup {
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LDePidataRailClientFileSelectAssetFileParamList;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x1, 0, 1, 2, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 4, -1, -1, -1, -1 },
    { NULL, "LDePidataRailClientFileSelectAssetFileParamList;", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getParameterList);
  methods[2].selector = @selector(initializeDialogWithPIGC_DialogController:withPISB_ParameterList:);
  methods[3].selector = @selector(dialogCreatedWithPIGC_DialogController:);
  methods[4].selector = @selector(dialogShowingWithPIGC_DialogController:);
  methods[5].selector = @selector(dialogBindingsInitializedWithPIGC_DialogController:);
  methods[6].selector = @selector(backPressedWithPIGC_DialogController:);
  methods[7].selector = @selector(dialogClosingWithPIGC_DialogController:withBoolean:);
  methods[8].selector = @selector(externalFileSelectedWithNSString:);
  methods[9].selector = @selector(dialogClosedWithPIGC_DialogController:withBoolean:withPISB_ParameterList:);
  methods[10].selector = @selector(popupClosedWithPIGC_ModuleGroup:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "parameterList_", "LDePidataRailClientFileSelectAssetFileParamList;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "externalFilePath_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "dlgCtrl_", "LPIGC_DialogController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "initializeDialog", "LPIGC_DialogController;LDePidataRailClientFileSelectAssetFileParamList;", "LJavaLangException;", "dialogCreated", "LPIGC_DialogController;", "dialogShowing", "dialogBindingsInitialized", "backPressed", "dialogClosing", "LPIGC_DialogController;Z", "externalFileSelected", "LNSString;", "dialogClosed", "LPIGC_DialogController;ZLPISB_ParameterList;", "popupClosed", "LPIGC_ModuleGroup;", "Ljava/lang/Object;Lde/pidata/gui/controller/base/DialogControllerDelegate<Lde/pidata/rail/client/file/SelectAssetFileParamList;Lde/pidata/rail/client/file/SelectAssetFileParamList;>;" };
  static const J2ObjcClassInfo _DePidataRailClientFileSelectAssetFileDelegate = { "SelectAssetFileDelegate", "de.pidata.rail.client.file", ptrTable, methods, fields, 7, 0x1, 11, 3, -1, -1, -1, 16, -1 };
  return &_DePidataRailClientFileSelectAssetFileDelegate;
}

@end

void DePidataRailClientFileSelectAssetFileDelegate_init(DePidataRailClientFileSelectAssetFileDelegate *self) {
  NSObject_init(self);
  self->externalFilePath_ = nil;
}

DePidataRailClientFileSelectAssetFileDelegate *new_DePidataRailClientFileSelectAssetFileDelegate_init() {
  J2OBJC_NEW_IMPL(DePidataRailClientFileSelectAssetFileDelegate, init)
}

DePidataRailClientFileSelectAssetFileDelegate *create_DePidataRailClientFileSelectAssetFileDelegate_init() {
  J2OBJC_CREATE_IMPL(DePidataRailClientFileSelectAssetFileDelegate, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataRailClientFileSelectAssetFileDelegate)
