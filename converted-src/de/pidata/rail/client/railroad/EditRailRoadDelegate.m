//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/railroad/EditRailRoadDelegate.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentColor.h"
#include "de/pidata/gui/component/base/GuiBuilder.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/IntegerController.h"
#include "de/pidata/gui/controller/base/ModuleGroup.h"
#include "de/pidata/gui/controller/base/PaintController.h"
#include "de/pidata/gui/controller/base/SelectionController.h"
#include "de/pidata/gui/controller/base/TableController.h"
#include "de/pidata/gui/event/Dialog.h"
#include "de/pidata/gui/guidef/ControllerBuilder.h"
#include "de/pidata/gui/view/base/PaintViewPI.h"
#include "de/pidata/gui/view/base/ViewPI.h"
#include "de/pidata/gui/view/figure/ShapePI.h"
#include "de/pidata/gui/view/figure/ShapeStyle.h"
#include "de/pidata/models/tree/EventSender.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/editcfg/EditCfgParamList.h"
#include "de/pidata/rail/client/editcfg/SaveCfgOperation.h"
#include "de/pidata/rail/client/railroad/EditRailRoadDelegate.h"
#include "de/pidata/rail/client/swgrid/SwitchGridFigure.h"
#include "de/pidata/rail/client/uiModels/EditTrackUI.h"
#include "de/pidata/rail/comm/ConfigLoader.h"
#include "de/pidata/rail/comm/PiRail.h"
#include "de/pidata/rail/railway/ModelRailway.h"
#include "de/pidata/rail/track/PanelCfg.h"
#include "de/pidata/rail/track/PanelRef.h"
#include "de/pidata/rail/track/RailroadCfg.h"
#include "de/pidata/rail/track/SectionCfg.h"
#include "de/pidata/rail/track/TrackCfg.h"
#include "de/pidata/rect/Rect.h"
#include "de/pidata/rect/SimpleRect.h"
#include "de/pidata/service/base/AbstractParameterList.h"
#include "de/pidata/service/base/ParameterList.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/lang/Integer.h"
#include "java/lang/Runnable.h"
#include "java/lang/Thread.h"
#include "java/net/InetAddress.h"
#include "java/util/Collection.h"
#include "java/util/HashMap.h"
#include "java/util/Map.h"
#include "java/util/Properties.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/railroad/EditRailRoadDelegate must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRCR_EditRailRoadDelegate () {
 @public
  PIRCU_EditTrackUI *uiModel_;
  id<PIGC_DialogController> dlgCtrl_;
  id<JavaUtilMap> switchGridFigureMap_;
  DePidataRailTrackRailroadCfg *railroadCfg_;
  DePidataRailTrackSectionCfg *sectionCfg_;
  DePidataRailTrackPanelRef *selectedPanelRef_;
  PIRCS_SwitchGridFigure *selectedFigure_;
}

- (void)addPanelCfgWithDePidataRailTrackPanelRef:(DePidataRailTrackPanelRef *)panelRef;

@end

J2OBJC_FIELD_SETTER(PIRCR_EditRailRoadDelegate, uiModel_, PIRCU_EditTrackUI *)
J2OBJC_FIELD_SETTER(PIRCR_EditRailRoadDelegate, dlgCtrl_, id<PIGC_DialogController>)
J2OBJC_FIELD_SETTER(PIRCR_EditRailRoadDelegate, switchGridFigureMap_, id<JavaUtilMap>)
J2OBJC_FIELD_SETTER(PIRCR_EditRailRoadDelegate, railroadCfg_, DePidataRailTrackRailroadCfg *)
J2OBJC_FIELD_SETTER(PIRCR_EditRailRoadDelegate, sectionCfg_, DePidataRailTrackSectionCfg *)
J2OBJC_FIELD_SETTER(PIRCR_EditRailRoadDelegate, selectedPanelRef_, DePidataRailTrackPanelRef *)
J2OBJC_FIELD_SETTER(PIRCR_EditRailRoadDelegate, selectedFigure_, PIRCS_SwitchGridFigure *)

__attribute__((unused)) static void PIRCR_EditRailRoadDelegate_addPanelCfgWithDePidataRailTrackPanelRef_(PIRCR_EditRailRoadDelegate *self, DePidataRailTrackPanelRef *panelRef);

@interface PIRCR_EditRailRoadDelegate_1 : NSObject < JavaLangRunnable > {
 @public
  PIRCR_EditRailRoadDelegate *this$0_;
}

- (instancetype)initWithPIRCR_EditRailRoadDelegate:(PIRCR_EditRailRoadDelegate *)outer$;

- (void)run;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCR_EditRailRoadDelegate_1)

__attribute__((unused)) static void PIRCR_EditRailRoadDelegate_1_initWithPIRCR_EditRailRoadDelegate_(PIRCR_EditRailRoadDelegate_1 *self, PIRCR_EditRailRoadDelegate *outer$);

__attribute__((unused)) static PIRCR_EditRailRoadDelegate_1 *new_PIRCR_EditRailRoadDelegate_1_initWithPIRCR_EditRailRoadDelegate_(PIRCR_EditRailRoadDelegate *outer$) NS_RETURNS_RETAINED;

__attribute__((unused)) static PIRCR_EditRailRoadDelegate_1 *create_PIRCR_EditRailRoadDelegate_1_initWithPIRCR_EditRailRoadDelegate_(PIRCR_EditRailRoadDelegate *outer$);

J2OBJC_INITIALIZED_DEFN(PIRCR_EditRailRoadDelegate)

PIQ_QName *PIRCR_EditRailRoadDelegate_ID_PANEL_POS_ROW;
PIQ_QName *PIRCR_EditRailRoadDelegate_ID_PANEL_POS_COL;

@implementation PIRCR_EditRailRoadDelegate

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCR_EditRailRoadDelegate_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (JavaNetInetAddress *)getDeviceAddress {
  return [((PIRCU_EditTrackUI *) nil_chk(uiModel_)) getInetAddress];
}

- (NSString *)getDeviceName {
  return [((PIRCU_EditTrackUI *) nil_chk(uiModel_)) getDeviceName];
}

- (DePidataRailTrackPanelRef *)getSelectedPanelRef {
  return selectedPanelRef_;
}

- (DePidataRailTrackRailroadCfg *)getRailroadCfg {
  return railroadCfg_;
}

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(PIRCE_EditCfgParamList *)parameterList {
  self->dlgCtrl_ = dlgCtrl;
  PIQ_QName *deviceId = [((PIRCE_EditCfgParamList *) nil_chk(parameterList)) getDeviceId];
  JavaNetInetAddress *ipAddress = [parameterList getIPAddress];
  self->uiModel_ = new_PIRCU_EditTrackUI_initWithNSString_withJavaNetInetAddress_([parameterList getDeviceName], ipAddress);
  id<PIGC_SelectionController> sectionCfgCtrl = (id<PIGC_SelectionController>) cast_check([((id<PIGC_DialogController>) nil_chk(dlgCtrl)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE))) getQNameWithNSString:@"sectionCfgName"]], PIGC_SelectionController_class_());
  [((id<PIGC_SelectionController>) nil_chk(sectionCfgCtrl)) addListenerWithPIMR_EventListener:self];
  id<PIGC_SelectionController> panelCfgCtrl = (id<PIGC_SelectionController>) cast_check([dlgCtrl getControllerWithPIQ_QName:[JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE) getQNameWithNSString:@"panelCfgTable"]], PIGC_SelectionController_class_());
  [((id<PIGC_SelectionController>) nil_chk(panelCfgCtrl)) addListenerWithPIMR_EventListener:self];
  (void) PIRO_ConfigLoader_loadConfigWithPIQ_QName_withJavaNetInetAddress_withNSString_withPIRO_ConfigLoaderListener_withBoolean_withBoolean_(deviceId, ipAddress, PIRO_ConfigLoader_TRACK_CFG_XML, self, false, false);
  return uiModel_;
}

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (id<PISB_ParameterList>)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                     withBoolean:(jboolean)ok {
  return JreLoadStatic(PISB_AbstractParameterList, EMPTY);
}

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList {
}

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup {
}

- (void)setSectionCfgWithDePidataRailTrackSectionCfg:(DePidataRailTrackSectionCfg *)sectionCfg {
  PIGC_PaintController *switchGrid = (PIGC_PaintController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"switchGrid"]], [PIGC_PaintController class]);
  PIGV_PaintViewPI *switchGridView = (PIGV_PaintViewPI *) cast_chk([((PIGC_PaintController *) nil_chk(switchGrid)) getView], [PIGV_PaintViewPI class]);
  if (self->sectionCfg_ != nil) {
    for (PIRCS_SwitchGridFigure * __strong switchGridFigure in nil_chk([((id<JavaUtilMap>) nil_chk(switchGridFigureMap_)) values])) {
      [((PIGV_PaintViewPI *) nil_chk(switchGridView)) removeFigureWithPIGF_Figure:switchGridFigure];
    }
    [((id<JavaUtilMap>) nil_chk(switchGridFigureMap_)) clear];
  }
  if (sectionCfg != nil) {
    self->sectionCfg_ = sectionCfg;
    for (DePidataRailTrackPanelRef * __strong panelRef in nil_chk([sectionCfg panelRefIter])) {
      PIRCR_EditRailRoadDelegate_addPanelCfgWithDePidataRailTrackPanelRef_(self, panelRef);
    }
    DePidataRailTrackPanelRef *newSelectedPanelRef = [sectionCfg getPanelRefWithPIQ_Key:nil];
    [self setSelectedPanelRefWithDePidataRailTrackPanelRef:newSelectedPanelRef];
  }
  id<PIGC_SelectionController> sectionCtrl = (id<PIGC_SelectionController>) cast_check([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:[JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"sectionCfgName"]], PIGC_SelectionController_class_());
  [((id<PIGC_SelectionController>) nil_chk(sectionCtrl)) selectRowWithPIMR_Model:self->sectionCfg_];
}

- (void)addPanelCfgWithDePidataRailTrackPanelRef:(DePidataRailTrackPanelRef *)panelRef {
  PIRCR_EditRailRoadDelegate_addPanelCfgWithDePidataRailTrackPanelRef_(self, panelRef);
}

- (void)finishedLoadingWithPIRO_ConfigLoader:(PIRO_ConfigLoader *)configLoader
                                 withBoolean:(jboolean)success {
  if (success) {
    DePidataRailTrackTrackCfg *trackCfg = (DePidataRailTrackTrackCfg *) cast_chk([((PIRO_ConfigLoader *) nil_chk(configLoader)) getConfigModel], [DePidataRailTrackTrackCfg class]);
    [((DePidataRailTrackTrackCfg *) nil_chk(trackCfg)) setDeviceAddressWithJavaNetInetAddress:[((PIRCU_EditTrackUI *) nil_chk(uiModel_)) getInetAddress]];
    [((PIRCU_EditTrackUI *) nil_chk(uiModel_)) setTrackCfgWithDePidataRailTrackTrackCfg:trackCfg];
    railroadCfg_ = [trackCfg getRailroadCfg];
    [((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) activateWithPIGU_UIContainer:[dlgCtrl_ getDialogComp]];
    if (railroadCfg_ != nil) {
      DePidataRailTrackSectionCfg *sectionCfg = [railroadCfg_ getSectionCfgWithPIQ_Key:nil];
      if (sectionCfg != nil) {
        id<PIGC_SelectionController> sectionCfgCtrl = (id<PIGC_SelectionController>) cast_check([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE))) getQNameWithNSString:@"sectionCfgName"]], PIGC_SelectionController_class_());
        [((id<PIGC_SelectionController>) nil_chk(sectionCfgCtrl)) selectRowWithPIMR_Model:sectionCfg];
      }
    }
  }
}

- (void)saveGrid {
  [((id<PIGE_Dialog>) nil_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getDialogComp])) showBusyWithBoolean:true];
  id<JavaLangRunnable> task = new_PIRCR_EditRailRoadDelegate_1_initWithPIRCR_EditRailRoadDelegate_(self);
  [new_JavaLangThread_initWithJavaLangRunnable_(task) start];
}

- (void)eventOccuredWithPIMR_EventSender:(PIMR_EventSender *)eventSender
                                 withInt:(jint)eventID
                                  withId:(id)source
                           withPIQ_QName:(PIQ_QName *)modelID
                                  withId:(id)oldValue
                                  withId:(id)newValue {
  if ([newValue isKindOfClass:[DePidataRailTrackSectionCfg class]]) {
    if (!JreObjectEqualsEquals(newValue, sectionCfg_)) {
      [self setSectionCfgWithDePidataRailTrackSectionCfg:(DePidataRailTrackSectionCfg *) newValue];
    }
  }
  else if ([newValue isKindOfClass:[DePidataRailTrackPanelCfg class]]) {
    DePidataRailTrackPanelCfg *panelCfg = (DePidataRailTrackPanelCfg *) newValue;
    if (sectionCfg_ != nil) {
      DePidataRailTrackPanelRef *newSelectedPanelRef = [sectionCfg_ findPanelRefWithPIQ_QName:[((DePidataRailTrackPanelCfg *) nil_chk(panelCfg)) getId]];
      [self setSelectedPanelRefWithDePidataRailTrackPanelRef:newSelectedPanelRef];
    }
  }
}

- (void)setSelectedPanelRefWithDePidataRailTrackPanelRef:(DePidataRailTrackPanelRef *)newSelectedPanelRef {
  if (selectedFigure_ != nil) {
    id<PIGF_ShapePI> borderShape = [selectedFigure_ getBorderShape];
    [((PIGF_ShapeStyle *) nil_chk([((id<PIGF_ShapePI>) nil_chk(borderShape)) getShapeStyle])) setBorderColorWithPIQ_QName:JreLoadStatic(PIGB_ComponentColor, BLACK)];
    [((PIGF_ShapeStyle *) nil_chk([borderShape getShapeStyle])) setStrokeWidthWithDouble:1];
    self->selectedFigure_ = nil;
    self->selectedPanelRef_ = nil;
  }
  if (newSelectedPanelRef != nil) {
    DePidataRailTrackPanelCfg *panelCfg = [((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) getPanelCfgWithPIQ_QName:[newSelectedPanelRef getRefID]];
    if (panelCfg != nil) {
      PIRCS_SwitchGridFigure *switchGridFigure = [((id<JavaUtilMap>) nil_chk(switchGridFigureMap_)) getWithId:[panelCfg getId]];
      if (switchGridFigure != nil) {
        id<PIGF_ShapePI> borderShape = [switchGridFigure getBorderShape];
        [((PIGF_ShapeStyle *) nil_chk([((id<PIGF_ShapePI>) nil_chk(borderShape)) getShapeStyle])) setBorderColorWithPIQ_QName:JreLoadStatic(PIGB_ComponentColor, ORANGE)];
        [((PIGF_ShapeStyle *) nil_chk([borderShape getShapeStyle])) setStrokeWidthWithDouble:5];
        selectedPanelRef_ = [((DePidataRailTrackSectionCfg *) nil_chk(sectionCfg_)) findPanelRefWithPIQ_QName:[panelCfg getId]];
        PIGC_IntegerController *rowPosCtrl = (PIGC_IntegerController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:PIRCR_EditRailRoadDelegate_ID_PANEL_POS_ROW], [PIGC_IntegerController class]);
        [((PIGC_IntegerController *) nil_chk(rowPosCtrl)) setValueWithId:[((DePidataRailTrackPanelRef *) nil_chk(selectedPanelRef_)) getY]];
        PIGC_IntegerController *colPosCtrl = (PIGC_IntegerController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:PIRCR_EditRailRoadDelegate_ID_PANEL_POS_COL], [PIGC_IntegerController class]);
        [((PIGC_IntegerController *) nil_chk(colPosCtrl)) setValueWithId:[((DePidataRailTrackPanelRef *) nil_chk(selectedPanelRef_)) getX]];
      }
      self->selectedFigure_ = switchGridFigure;
    }
  }
}

- (void)updatePanelPosWithDePidataRailTrackPanelRef:(DePidataRailTrackPanelRef *)panelRef {
  PIRCS_SwitchGridFigure *switchGridFigure = [((id<JavaUtilMap>) nil_chk(switchGridFigureMap_)) getWithId:[((DePidataRailTrackPanelRef *) nil_chk(panelRef)) getRefID]];
  if (switchGridFigure != nil) {
    id<PICR_Rect> bounds = [switchGridFigure getBounds];
    [((id<PICR_Rect>) nil_chk(bounds)) setXWithDouble:[panelRef getXInt] * PIRCR_EditRailRoadDelegate_smallCellSize];
    [bounds setYWithDouble:[panelRef getYInt] * PIRCR_EditRailRoadDelegate_smallCellSize];
  }
}

- (jint)getWidthWithDePidataRailTrackPanelRef:(DePidataRailTrackPanelRef *)panelRef {
  DePidataRailTrackPanelCfg *panelCfg = [((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) getPanelCfgWithPIQ_QName:[((DePidataRailTrackPanelRef *) nil_chk(panelRef)) getRefID]];
  if (panelCfg != nil) {
    return [panelCfg panelColCount];
  }
  else {
    return 0;
  }
}

- (jint)getHeightWithDePidataRailTrackPanelRef:(DePidataRailTrackPanelRef *)panelRef {
  DePidataRailTrackPanelCfg *panelCfg = [((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) getPanelCfgWithPIQ_QName:[((DePidataRailTrackPanelRef *) nil_chk(panelRef)) getRefID]];
  if (panelCfg != nil) {
    return [panelCfg panelRowCount];
  }
  else {
    return 0;
  }
}

- (void)addPanel {
  id<PIGC_TableController> panelCfgTable = (id<PIGC_TableController>) cast_check([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"panelCfgTable"]], PIGC_TableController_class_());
  DePidataRailTrackPanelCfg *panelCfg = (DePidataRailTrackPanelCfg *) cast_chk([((id<PIGC_TableController>) nil_chk(panelCfgTable)) getSelectedRowWithInt:0], [DePidataRailTrackPanelCfg class]);
  if (panelCfg != nil) {
    PIRCS_SwitchGridFigure *panelFigure = [((id<JavaUtilMap>) nil_chk(switchGridFigureMap_)) getWithId:[panelCfg getId]];
    if (panelFigure == nil) {
      if (sectionCfg_ != nil) {
        jint x = 0;
        for (DePidataRailTrackPanelRef * __strong pRef in nil_chk([sectionCfg_ panelRefIter])) {
          jint newX = [((JavaLangInteger *) nil_chk([((DePidataRailTrackPanelRef *) nil_chk(pRef)) getX])) intValue] + [self getWidthWithDePidataRailTrackPanelRef:pRef];
          if (newX > x) {
            x = newX;
          }
        }
        DePidataRailTrackPanelRef *panelRef = new_DePidataRailTrackPanelRef_initWithDePidataRailTrackPanelCfg_withInt_withInt_(panelCfg, x, 0);
        [((DePidataRailTrackSectionCfg *) nil_chk(sectionCfg_)) addPanelRefWithDePidataRailTrackPanelRef:panelRef];
        PIRCR_EditRailRoadDelegate_addPanelCfgWithDePidataRailTrackPanelRef_(self, panelRef);
        [self setSelectedPanelRefWithDePidataRailTrackPanelRef:panelRef];
      }
    }
  }
}

- (void)removePanel {
  id<PIGC_TableController> panelCfgTable = (id<PIGC_TableController>) cast_check([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"panelCfgTable"]], PIGC_TableController_class_());
  DePidataRailTrackPanelCfg *panelCfg = (DePidataRailTrackPanelCfg *) cast_chk([((id<PIGC_TableController>) nil_chk(panelCfgTable)) getSelectedRowWithInt:0], [DePidataRailTrackPanelCfg class]);
  if (panelCfg != nil) {
    DePidataRailTrackPanelRef *panelRef = [((DePidataRailTrackSectionCfg *) nil_chk(sectionCfg_)) getPanelRefWithPIQ_Key:[panelCfg getId]];
    if (panelRef != nil) {
      [((DePidataRailTrackSectionCfg *) nil_chk(sectionCfg_)) removePanelRefWithDePidataRailTrackPanelRef:panelRef];
      PIRCS_SwitchGridFigure *panelFigure = [((id<JavaUtilMap>) nil_chk(switchGridFigureMap_)) getWithId:[panelCfg getId]];
      if (panelFigure != nil) {
        PIGC_PaintController *switchGrid = (PIGC_PaintController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:[JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"switchGrid"]], [PIGC_PaintController class]);
        PIGV_PaintViewPI *switchGridView = (PIGV_PaintViewPI *) cast_chk([((PIGC_PaintController *) nil_chk(switchGrid)) getView], [PIGV_PaintViewPI class]);
        [((PIGV_PaintViewPI *) nil_chk(switchGridView)) removeFigureWithPIGF_Figure:panelFigure];
        (void) [((id<JavaUtilMap>) nil_chk(switchGridFigureMap_)) removeWithId:[panelCfg getId]];
      }
    }
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaNetInetAddress;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LDePidataRailTrackPanelRef;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LDePidataRailTrackRailroadCfg;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x1, 0, 1, 2, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 4, -1, -1, -1, -1 },
    { NULL, "LPISB_ParameterList;", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
    { NULL, "V", 0x2, 16, 17, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 18, 19, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 20, 21, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 22, 17, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 23, 17, -1, -1, -1, -1 },
    { NULL, "I", 0x1, 24, 17, -1, -1, -1, -1 },
    { NULL, "I", 0x1, 25, 17, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(getDeviceAddress);
  methods[2].selector = @selector(getDeviceName);
  methods[3].selector = @selector(getSelectedPanelRef);
  methods[4].selector = @selector(getRailroadCfg);
  methods[5].selector = @selector(initializeDialogWithPIGC_DialogController:withPISB_ParameterList:);
  methods[6].selector = @selector(dialogCreatedWithPIGC_DialogController:);
  methods[7].selector = @selector(dialogShowingWithPIGC_DialogController:);
  methods[8].selector = @selector(dialogBindingsInitializedWithPIGC_DialogController:);
  methods[9].selector = @selector(backPressedWithPIGC_DialogController:);
  methods[10].selector = @selector(dialogClosingWithPIGC_DialogController:withBoolean:);
  methods[11].selector = @selector(dialogClosedWithPIGC_DialogController:withBoolean:withPISB_ParameterList:);
  methods[12].selector = @selector(popupClosedWithPIGC_ModuleGroup:);
  methods[13].selector = @selector(setSectionCfgWithDePidataRailTrackSectionCfg:);
  methods[14].selector = @selector(addPanelCfgWithDePidataRailTrackPanelRef:);
  methods[15].selector = @selector(finishedLoadingWithPIRO_ConfigLoader:withBoolean:);
  methods[16].selector = @selector(saveGrid);
  methods[17].selector = @selector(eventOccuredWithPIMR_EventSender:withInt:withId:withPIQ_QName:withId:withId:);
  methods[18].selector = @selector(setSelectedPanelRefWithDePidataRailTrackPanelRef:);
  methods[19].selector = @selector(updatePanelPosWithDePidataRailTrackPanelRef:);
  methods[20].selector = @selector(getWidthWithDePidataRailTrackPanelRef:);
  methods[21].selector = @selector(getHeightWithDePidataRailTrackPanelRef:);
  methods[22].selector = @selector(addPanel);
  methods[23].selector = @selector(removePanel);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "smallCellSize", "I", .constantValue.asInt = PIRCR_EditRailRoadDelegate_smallCellSize, 0x19, -1, -1, -1, -1 },
    { "ID_PANEL_POS_ROW", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 26, -1, -1 },
    { "ID_PANEL_POS_COL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 27, -1, -1 },
    { "uiModel_", "LPIRCU_EditTrackUI;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "dlgCtrl_", "LPIGC_DialogController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "switchGridFigureMap_", "LJavaUtilMap;", .constantValue.asLong = 0, 0x2, -1, -1, 28, -1 },
    { "railroadCfg_", "LDePidataRailTrackRailroadCfg;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "sectionCfg_", "LDePidataRailTrackSectionCfg;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "selectedPanelRef_", "LDePidataRailTrackPanelRef;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "selectedFigure_", "LPIRCS_SwitchGridFigure;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "initializeDialog", "LPIGC_DialogController;LPIRCE_EditCfgParamList;", "LJavaLangException;", "dialogCreated", "LPIGC_DialogController;", "dialogShowing", "dialogBindingsInitialized", "backPressed", "dialogClosing", "LPIGC_DialogController;Z", "dialogClosed", "LPIGC_DialogController;ZLPISB_ParameterList;", "popupClosed", "LPIGC_ModuleGroup;", "setSectionCfg", "LDePidataRailTrackSectionCfg;", "addPanelCfg", "LDePidataRailTrackPanelRef;", "finishedLoading", "LPIRO_ConfigLoader;Z", "eventOccured", "LPIMR_EventSender;ILNSObject;LPIQ_QName;LNSObject;LNSObject;", "setSelectedPanelRef", "updatePanelPos", "getWidth", "getHeight", &PIRCR_EditRailRoadDelegate_ID_PANEL_POS_ROW, &PIRCR_EditRailRoadDelegate_ID_PANEL_POS_COL, "Ljava/util/Map<Lde/pidata/qnames/QName;Lde/pidata/rail/client/swgrid/SwitchGridFigure;>;", "Ljava/lang/Object;Lde/pidata/gui/controller/base/DialogControllerDelegate<Lde/pidata/rail/client/editcfg/EditCfgParamList;Lde/pidata/service/base/ParameterList;>;Lde/pidata/rail/comm/ConfigLoaderListener;Lde/pidata/models/tree/EventListener;" };
  static const J2ObjcClassInfo _PIRCR_EditRailRoadDelegate = { "EditRailRoadDelegate", "de.pidata.rail.client.railroad", ptrTable, methods, fields, 7, 0x1, 24, 10, -1, -1, -1, 29, -1 };
  return &_PIRCR_EditRailRoadDelegate;
}

+ (void)initialize {
  if (self == [PIRCR_EditRailRoadDelegate class]) {
    PIRCR_EditRailRoadDelegate_ID_PANEL_POS_ROW = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE))) getQNameWithNSString:@"panelPosRow"];
    PIRCR_EditRailRoadDelegate_ID_PANEL_POS_COL = [JreLoadStatic(PIGG_ControllerBuilder, NAMESPACE) getQNameWithNSString:@"panelPosCol"];
    J2OBJC_SET_INITIALIZED(PIRCR_EditRailRoadDelegate)
  }
}

@end

void PIRCR_EditRailRoadDelegate_init(PIRCR_EditRailRoadDelegate *self) {
  NSObject_init(self);
  self->switchGridFigureMap_ = new_JavaUtilHashMap_init();
  self->selectedFigure_ = nil;
}

PIRCR_EditRailRoadDelegate *new_PIRCR_EditRailRoadDelegate_init() {
  J2OBJC_NEW_IMPL(PIRCR_EditRailRoadDelegate, init)
}

PIRCR_EditRailRoadDelegate *create_PIRCR_EditRailRoadDelegate_init() {
  J2OBJC_CREATE_IMPL(PIRCR_EditRailRoadDelegate, init)
}

void PIRCR_EditRailRoadDelegate_addPanelCfgWithDePidataRailTrackPanelRef_(PIRCR_EditRailRoadDelegate *self, DePidataRailTrackPanelRef *panelRef) {
  PIRR_ModelRailway *modelRailway = [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway];
  DePidataRailTrackPanelCfg *panelCfg = [((PIRR_ModelRailway *) nil_chk(modelRailway)) getPanelCfgWithPIQ_QName:[((DePidataRailTrackPanelRef *) nil_chk(panelRef)) getRefID]];
  if (panelCfg != nil) {
    PIGC_PaintController *switchGrid = (PIGC_PaintController *) cast_chk([((id<PIGC_DialogController>) nil_chk(self->dlgCtrl_)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"switchGrid"]], [PIGC_PaintController class]);
    PIGV_PaintViewPI *switchGridView = (PIGV_PaintViewPI *) cast_chk([((PIGC_PaintController *) nil_chk(switchGrid)) getView], [PIGV_PaintViewPI class]);
    jdouble w = PIRCR_EditRailRoadDelegate_smallCellSize;
    jdouble h = PIRCR_EditRailRoadDelegate_smallCellSize;
    PICR_SimpleRect *bounds = new_PICR_SimpleRect_initWithDouble_withDouble_withDouble_withDouble_([panelRef getXInt] * w, [panelRef getYInt] * h, 2 + [panelCfg panelColCount] * w, 2 + [panelCfg panelRowCount] * h);
    PIRCS_SwitchGridFigure *switchGridFigure = new_PIRCS_SwitchGridFigure_initWithPICR_Rect_withDePidataRailTrackPanelCfg_withPIGC_TableController_withInt_(bounds, panelCfg, nil, PIRCR_EditRailRoadDelegate_smallCellSize);
    (void) [((id<JavaUtilMap>) nil_chk(self->switchGridFigureMap_)) putWithId:[panelCfg getId] withId:switchGridFigure];
    [((PIGV_PaintViewPI *) nil_chk(switchGridView)) addFigureWithPIGF_Figure:switchGridFigure];
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCR_EditRailRoadDelegate)

J2OBJC_NAME_MAPPING(PIRCR_EditRailRoadDelegate, "de.pidata.rail.client.railroad", "PIRCR_")

@implementation PIRCR_EditRailRoadDelegate_1

- (instancetype)initWithPIRCR_EditRailRoadDelegate:(PIRCR_EditRailRoadDelegate *)outer$ {
  PIRCR_EditRailRoadDelegate_1_initWithPIRCR_EditRailRoadDelegate_(self, outer$);
  return self;
}

- (void)run {
  DePidataRailTrackTrackCfg *trackCfg = [((PIRCU_EditTrackUI *) nil_chk(this$0_->uiModel_)) getTrackCfg];
  NSString *panelDevice = nil;
  if (trackCfg != nil) {
    if (PIRCE_SaveCfgOperation_uploadTrackCfgWithJavaNetInetAddress_withDePidataRailTrackTrackCfg_withPIGC_DialogController_([((PIRCU_EditTrackUI *) nil_chk(this$0_->uiModel_)) getInetAddress], trackCfg, this$0_->dlgCtrl_)) {
      panelDevice = [((PIRCU_EditTrackUI *) nil_chk(this$0_->uiModel_)) getDeviceName];
    }
  }
  [((id<PIGE_Dialog>) nil_chk([((id<PIGC_DialogController>) nil_chk(this$0_->dlgCtrl_)) getDialogComp])) showBusyWithBoolean:false];
  if (panelDevice == nil) {
    NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"editRailRoadNotUpdated_H" withNSString:nil withJavaUtilProperties:nil];
    NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"editRailRoadNotUpdated_TXT" withNSString:nil withJavaUtilProperties:nil];
    [((id<PIGC_DialogController>) nil_chk(this$0_->dlgCtrl_)) showMessageWithNSString:title withNSString:text];
  }
  else {
    JavaUtilProperties *params = new_JavaUtilProperties_init();
    (void) [params putWithId:@"moduleName" withId:panelDevice];
    NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"editRailRoadUpdated_H" withNSString:nil withJavaUtilProperties:nil];
    NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"editRailRoadUpdated_TXT" withNSString:nil withJavaUtilProperties:params];
    [((id<PIGC_DialogController>) nil_chk(this$0_->dlgCtrl_)) showMessageWithNSString:title withNSString:text];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIRCR_EditRailRoadDelegate:);
  methods[1].selector = @selector(run);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "this$0_", "LPIRCR_EditRailRoadDelegate;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIRCR_EditRailRoadDelegate;", "saveGrid" };
  static const J2ObjcClassInfo _PIRCR_EditRailRoadDelegate_1 = { "", "de.pidata.rail.client.railroad", ptrTable, methods, fields, 7, 0x8010, 2, 1, 0, -1, 1, -1, -1 };
  return &_PIRCR_EditRailRoadDelegate_1;
}

@end

void PIRCR_EditRailRoadDelegate_1_initWithPIRCR_EditRailRoadDelegate_(PIRCR_EditRailRoadDelegate_1 *self, PIRCR_EditRailRoadDelegate *outer$) {
  self->this$0_ = outer$;
  NSObject_init(self);
}

PIRCR_EditRailRoadDelegate_1 *new_PIRCR_EditRailRoadDelegate_1_initWithPIRCR_EditRailRoadDelegate_(PIRCR_EditRailRoadDelegate *outer$) {
  J2OBJC_NEW_IMPL(PIRCR_EditRailRoadDelegate_1, initWithPIRCR_EditRailRoadDelegate_, outer$)
}

PIRCR_EditRailRoadDelegate_1 *create_PIRCR_EditRailRoadDelegate_1_initWithPIRCR_EditRailRoadDelegate_(PIRCR_EditRailRoadDelegate *outer$) {
  J2OBJC_CREATE_IMPL(PIRCR_EditRailRoadDelegate_1, initWithPIRCR_EditRailRoadDelegate_, outer$)
}
