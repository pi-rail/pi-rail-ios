//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/motor/MotorSetupDelegate.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/chart/ChartController.h"
#include "de/pidata/gui/chart/ChartViewPI.h"
#include "de/pidata/gui/component/base/GuiBuilder.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/IntegerController.h"
#include "de/pidata/gui/controller/base/ModuleGroup.h"
#include "de/pidata/gui/controller/base/TextController.h"
#include "de/pidata/gui/view/base/ViewPI.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/editcfg/EditCfgParamList.h"
#include "de/pidata/rail/client/motor/MotorSetupDelegate.h"
#include "de/pidata/rail/comm/PiRail.h"
#include "de/pidata/rail/comm/WifiState.h"
#include "de/pidata/rail/model/Action.h"
#include "de/pidata/rail/model/Cfg.h"
#include "de/pidata/rail/model/Csv.h"
#include "de/pidata/rail/model/CsvType.h"
#include "de/pidata/rail/model/Data.h"
#include "de/pidata/rail/model/ItemConn.h"
#include "de/pidata/rail/model/MotorAction.h"
#include "de/pidata/rail/model/MotorMode.h"
#include "de/pidata/rail/model/State.h"
#include "de/pidata/rail/model/TriggerAction.h"
#include "de/pidata/rail/railway/Locomotive.h"
#include "de/pidata/rail/railway/ModelRailway.h"
#include "de/pidata/rail/railway/RailFunction.h"
#include "de/pidata/rail/railway/RailRange.h"
#include "de/pidata/service/base/AbstractParameterList.h"
#include "de/pidata/service/base/ParameterList.h"
#include "de/pidata/stream/StreamHelper.h"
#include "de/pidata/system/base/Storage.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/io/OutputStream.h"
#include "java/io/PrintWriter.h"
#include "java/lang/Exception.h"
#include "java/lang/Integer.h"
#include "java/lang/Long.h"
#include "java/util/ArrayList.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/motor/MotorSetupDelegate must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRCM_MotorSetupDelegate () {
 @public
  PIGH_ChartController *chartController_;
  PIRR_Locomotive *locomotive_;
  id<PIGC_DialogController> dlgCtrl_;
  jlong startX_;
  NSString *xStr_;
  jint dataCount_;
  id<JavaUtilList> motorIDs_;
  JavaIoOutputStream *csvOut_;
  JavaIoPrintWriter *csvPrint_;
  jchar motorModeValue_;
}

- (jboolean)isSlaveMotorWithPIRM_MotorAction:(PIRM_MotorAction *)motorAction
                               withPIQ_QName:(PIQ_QName *)mainMotorActionID;

@end

J2OBJC_FIELD_SETTER(PIRCM_MotorSetupDelegate, chartController_, PIGH_ChartController *)
J2OBJC_FIELD_SETTER(PIRCM_MotorSetupDelegate, locomotive_, PIRR_Locomotive *)
J2OBJC_FIELD_SETTER(PIRCM_MotorSetupDelegate, dlgCtrl_, id<PIGC_DialogController>)
J2OBJC_FIELD_SETTER(PIRCM_MotorSetupDelegate, xStr_, NSString *)
J2OBJC_FIELD_SETTER(PIRCM_MotorSetupDelegate, motorIDs_, id<JavaUtilList>)
J2OBJC_FIELD_SETTER(PIRCM_MotorSetupDelegate, csvOut_, JavaIoOutputStream *)
J2OBJC_FIELD_SETTER(PIRCM_MotorSetupDelegate, csvPrint_, JavaIoPrintWriter *)

__attribute__((unused)) static jboolean PIRCM_MotorSetupDelegate_isSlaveMotorWithPIRM_MotorAction_withPIQ_QName_(PIRCM_MotorSetupDelegate *self, PIRM_MotorAction *motorAction, PIQ_QName *mainMotorActionID);

J2OBJC_INITIALIZED_DEFN(PIRCM_MotorSetupDelegate)

PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_KP;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_KI;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_KD;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_MIN;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_MAX;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_SLOW;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_DECEL;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_BRK_FAK;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_DELAY;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_SET_ACCEL;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_MOTOR_MODE;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_CURRENT_KP;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_CURRENT_KI;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_CURRENT_KD;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_KP;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_KI;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_KD;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_MIN;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_MAX;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_SLOW;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_DECEL;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_BRK_FAK;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_DELAY;
PIQ_QName *PIRCM_MotorSetupDelegate_ID_EDIT_ACCEL;
NSString *PIRCM_MotorSetupDelegate_SENSOR_PREFIX = @"Sensor_";
NSString *PIRCM_MotorSetupDelegate_SENSOR_RAW_PREFIX = @"Sensor_Raw_";
NSString *PIRCM_MotorSetupDelegate_SOLL = @"Soll";
NSString *PIRCM_MotorSetupDelegate_STELLWERT = @"Stellwert";

@implementation PIRCM_MotorSetupDelegate

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCM_MotorSetupDelegate_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(PIRCE_EditCfgParamList *)parameterList {
  self->dlgCtrl_ = dlgCtrl;
  chartController_ = (PIGH_ChartController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"motorChart"]], [PIGH_ChartController class]);
  locomotive_ = [((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) getLocoWithPIQ_QName:[((PIRCE_EditCfgParamList *) nil_chk(parameterList)) getDeviceId]];
  @try {
    PISYSB_Storage *appDataDir = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getStorageWithNSString:PISYSB_SystemManager_STORAGE_APPDATA withNSString:nil];
    csvOut_ = [((PISYSB_Storage *) nil_chk(appDataDir)) writeWithNSString:@"Motor-Setup.csv" withBoolean:true withBoolean:false];
    csvPrint_ = new_JavaIoPrintWriter_initWithJavaIoOutputStream_(csvOut_);
    [csvPrint_ printlnWithNSString:PIRM_Data_HEADER_MO_DATA];
  }
  @catch (JavaLangException *ex) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Error opening Motor-Setup.csv", ex);
    PICM_StreamHelper_closeWithJavaIoOutputStream_(csvOut_);
    csvOut_ = nil;
    csvPrint_ = nil;
  }
  return locomotive_;
}

- (PIRR_Locomotive *)getLocomotive {
  return locomotive_;
}

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (jboolean)isSlaveMotorWithPIRM_MotorAction:(PIRM_MotorAction *)motorAction
                               withPIQ_QName:(PIQ_QName *)mainMotorActionID {
  return PIRCM_MotorSetupDelegate_isSlaveMotorWithPIRM_MotorAction_withPIQ_QName_(self, motorAction, mainMotorActionID);
}

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  PIGH_ChartViewPI *chartViewPI = (PIGH_ChartViewPI *) cast_chk([((PIGH_ChartController *) nil_chk(chartController_)) getView], [PIGH_ChartViewPI class]);
  [((PIGH_ChartViewPI *) nil_chk(chartViewPI)) addSeriesWithNSString:PIRCM_MotorSetupDelegate_SOLL];
  [chartViewPI addSeriesWithNSString:PIRCM_MotorSetupDelegate_STELLWERT];
  PIRM_Cfg *locoCfg = [((PIRR_Locomotive *) nil_chk(locomotive_)) getConfig];
  for (PIRM_MotorAction * __strong motorAction in nil_chk([((PIRM_Cfg *) nil_chk(locoCfg)) motorIter])) {
    if ([((PIRM_MotorAction *) nil_chk(motorAction)) getGroup] == nil) {
      PIRM_MotorMode *motorMode = [motorAction getModeWithPIQ_Key:JreLoadStatic(PIRM_MotorMode, MOTOR_MODE_PID)];
      if (motorMode == nil) {
        motorMode = new_PIRM_MotorMode_initWithPIQ_Key_(JreLoadStatic(PIRM_MotorMode, MOTOR_MODE_PID));
        [motorAction addModeWithPIRM_MotorMode:motorMode];
      }
    }
  }
  PIGC_TextController *modeLabel = (PIGC_TextController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl)) getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_MOTOR_MODE], [PIGC_TextController class]);
  motorModeValue_ = [((PIRR_RailFunction *) nil_chk([((PIRR_Locomotive *) nil_chk(locomotive_)) getModeFunction])) getStateChar];
  PIRM_MotorAction *mainMotorAction = (PIRM_MotorAction *) cast_chk([((PIRR_RailRange *) nil_chk([((PIRR_Locomotive *) nil_chk(locomotive_)) getMainMotor])) getAction], [PIRM_MotorAction class]);
  PIRM_MotorMode *motorMode = [((PIRM_MotorAction *) nil_chk(mainMotorAction)) getMoModeWithChar:motorModeValue_];
  [((PIGC_TextController *) nil_chk(modeLabel)) setValueWithId:[((PIQ_QName *) nil_chk([((PIRM_MotorMode *) nil_chk(motorMode)) getId])) getName]];
  PIGC_IntegerController *intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_KP], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[motorMode getKP]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_KI], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[motorMode getKI]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_KD], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[motorMode getKD]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_MIN], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[mainMotorAction getMinVal]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_MAX], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[mainMotorAction getMaxVal]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_SLOW], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[motorMode getSlow]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_DECEL], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[motorMode getDecel]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_BRK_FAK], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[motorMode getBrkFak]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_DELAY], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[motorMode getDelay]];
  intCtrl = (PIGC_IntegerController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_EDIT_ACCEL], [PIGC_IntegerController class]);
  [((PIGC_IntegerController *) nil_chk(intCtrl)) setValueWithId:[motorMode getAccel]];
  jint motorIndex = 1;
  [chartViewPI addSeriesWithNSString:JreStrcat("$I", PIRCM_MotorSetupDelegate_SENSOR_PREFIX, motorIndex)];
  [chartViewPI addSeriesWithNSString:JreStrcat("$I", PIRCM_MotorSetupDelegate_SENSOR_RAW_PREFIX, motorIndex)];
  PIQ_QName *mainMotorActionID = [mainMotorAction getId];
  [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) sendSetCmdWithPIRR_RailDevice:locomotive_ withPIQ_QName:mainMotorActionID withPIQ_QName:JreLoadStatic(PIRM_MotorAction, ID_SENDCSV) withJavaLangInteger:JavaLangInteger_valueOfWithInt_(motorIndex)];
  [((id<JavaUtilList>) nil_chk(motorIDs_)) addWithId:mainMotorActionID];
  for (PIRM_MotorAction * __strong motorAction in nil_chk([locoCfg motorIter])) {
    if (PIRCM_MotorSetupDelegate_isSlaveMotorWithPIRM_MotorAction_withPIQ_QName_(self, motorAction, mainMotorActionID)) {
      motorIndex++;
      [chartViewPI addSeriesWithNSString:JreStrcat("$I", PIRCM_MotorSetupDelegate_SENSOR_PREFIX, motorIndex)];
      [chartViewPI addSeriesWithNSString:JreStrcat("$I", PIRCM_MotorSetupDelegate_SENSOR_RAW_PREFIX, motorIndex)];
      [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) sendSetCmdWithPIRR_RailDevice:locomotive_ withPIQ_QName:[((PIRM_MotorAction *) nil_chk(motorAction)) getId] withPIQ_QName:JreLoadStatic(PIRM_MotorAction, ID_SENDCSV) withJavaLangInteger:JavaLangInteger_valueOfWithInt_(motorIndex)];
      [((id<JavaUtilList>) nil_chk(motorIDs_)) addWithId:[motorAction getId]];
    }
  }
  [((PIRR_Locomotive *) nil_chk(locomotive_)) setDataListenerWithPIRR_DataListener:self];
}

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (id<PISB_ParameterList>)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                     withBoolean:(jboolean)ok {
  [((PIRR_Locomotive *) nil_chk(locomotive_)) setDataListenerWithPIRR_DataListener:nil];
  if (csvPrint_ != nil) {
    [csvPrint_ flush];
    [((JavaIoPrintWriter *) nil_chk(csvPrint_)) close];
  }
  PICM_StreamHelper_closeWithJavaIoOutputStream_(csvOut_);
  for (PIQ_QName * __strong motorActionID in nil_chk(motorIDs_)) {
    [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) sendSetCmdWithPIRR_RailDevice:locomotive_ withPIQ_QName:motorActionID withPIQ_QName:JreLoadStatic(PIRM_MotorAction, ID_SENDCSV) withJavaLangInteger:JavaLangInteger_valueOfWithInt_(0)];
  }
  return JreLoadStatic(PISB_AbstractParameterList, EMPTY);
}

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList {
}

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup {
}

- (jchar)getMotorModeValue {
  return motorModeValue_;
}

- (void)setMotorModeValueWithChar:(jchar)motorModeValue {
  self->motorModeValue_ = motorModeValue;
}

- (void)updatePIDWithNSString:(NSString *)kP
                 withNSString:(NSString *)kI
                 withNSString:(NSString *)kD {
  PIGC_TextController *intCtrl = (PIGC_TextController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_CURRENT_KP], [PIGC_TextController class]);
  [((PIGC_TextController *) nil_chk(intCtrl)) setValueWithId:kP];
  intCtrl = (PIGC_TextController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_CURRENT_KI], [PIGC_TextController class]);
  [((PIGC_TextController *) nil_chk(intCtrl)) setValueWithId:kI];
  intCtrl = (PIGC_TextController *) cast_chk([((id<PIGC_DialogController>) nil_chk(dlgCtrl_)) getControllerWithPIQ_QName:PIRCM_MotorSetupDelegate_ID_CURRENT_KD], [PIGC_TextController class]);
  [((PIGC_TextController *) nil_chk(intCtrl)) setValueWithId:kD];
}

- (void)processDataWithPIRM_Csv:(PIRM_Csv *)csvData {
  if ([((PIRM_Csv *) nil_chk(csvData)) getT] == JreLoadEnum(PIRM_CsvType, moData)) {
    jint index = [csvData getIndex];
    if (index == 0) {
      index = 1;
    }
    PIGH_ChartViewPI *chartViewPI = (PIGH_ChartViewPI *) cast_chk([((PIGH_ChartController *) nil_chk(chartController_)) getView], [PIGH_ChartViewPI class]);
    [((PIGH_ChartViewPI *) nil_chk(chartViewPI)) setYAxisWithDouble:0.0 withDouble:2500.0];
    if (csvPrint_ != nil) {
      [csvPrint_ printlnWithNSString:[csvData getDat]];
    }
    IOSObjectArray *values = [((NSString *) nil_chk([csvData getDat])) java_split:@";"];
    jlong time = JavaLangLong_parseLongWithNSString_(IOSObjectArray_Get(nil_chk(values), 0));
    if (startX_ < 0) {
      startX_ = time;
    }
    jlong x = time - startX_ + 1000000;
    jint stellwert = JavaLangInteger_parseIntWithNSString_(IOSObjectArray_Get(values, 1));
    jint sensorWert = JavaLangInteger_parseIntWithNSString_(IOSObjectArray_Get(values, 2));
    jint sollWert = JavaLangInteger_parseIntWithNSString_(IOSObjectArray_Get(values, 3));
    jint sensorRaw = JavaLangInteger_parseIntWithNSString_(IOSObjectArray_Get(values, 11));
    if ((stellwert != 0) || (sensorWert != 0) || (sollWert != 0)) {
      if (index == 1) {
        xStr_ = JavaLangLong_toStringWithLong_(x);
        [chartViewPI addDataWithNSString:PIRCM_MotorSetupDelegate_STELLWERT withNSString:xStr_ withInt:stellwert];
        [chartViewPI addDataWithNSString:PIRCM_MotorSetupDelegate_SOLL withNSString:xStr_ withInt:sollWert];
        dataCount_++;
        if (dataCount_ >= PIRCM_MotorSetupDelegate_MAX_DATA_COUNT) {
          [chartViewPI removeDataWithNSString:nil withInt:1];
        }
      }
      [chartViewPI addDataWithNSString:JreStrcat("$I", PIRCM_MotorSetupDelegate_SENSOR_RAW_PREFIX, index) withNSString:xStr_ withInt:sensorRaw];
      [chartViewPI addDataWithNSString:JreStrcat("$I", PIRCM_MotorSetupDelegate_SENSOR_PREFIX, index) withNSString:xStr_ withInt:sensorWert];
    }
    [self updatePIDWithNSString:IOSObjectArray_Get(values, 4) withNSString:IOSObjectArray_Get(values, 5) withNSString:IOSObjectArray_Get(values, 6)];
  }
}

- (void)processStateWithPIRM_State:(PIRM_State *)state {
}

- (void)processWifiStateWithPIRO_WifiState:(PIRO_WifiState *)newState {
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x1, 0, 1, 2, -1, -1, -1 },
    { NULL, "LPIRR_Locomotive;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "Z", 0x2, 5, 6, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 4, -1, -1, -1, -1 },
    { NULL, "LPISB_ParameterList;", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
    { NULL, "C", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 16, 17, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 18, 19, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 20, 21, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 22, 23, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 24, 25, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initializeDialogWithPIGC_DialogController:withPISB_ParameterList:);
  methods[2].selector = @selector(getLocomotive);
  methods[3].selector = @selector(dialogCreatedWithPIGC_DialogController:);
  methods[4].selector = @selector(isSlaveMotorWithPIRM_MotorAction:withPIQ_QName:);
  methods[5].selector = @selector(dialogShowingWithPIGC_DialogController:);
  methods[6].selector = @selector(dialogBindingsInitializedWithPIGC_DialogController:);
  methods[7].selector = @selector(backPressedWithPIGC_DialogController:);
  methods[8].selector = @selector(dialogClosingWithPIGC_DialogController:withBoolean:);
  methods[9].selector = @selector(dialogClosedWithPIGC_DialogController:withBoolean:withPISB_ParameterList:);
  methods[10].selector = @selector(popupClosedWithPIGC_ModuleGroup:);
  methods[11].selector = @selector(getMotorModeValue);
  methods[12].selector = @selector(setMotorModeValueWithChar:);
  methods[13].selector = @selector(updatePIDWithNSString:withNSString:withNSString:);
  methods[14].selector = @selector(processDataWithPIRM_Csv:);
  methods[15].selector = @selector(processStateWithPIRM_State:);
  methods[16].selector = @selector(processWifiStateWithPIRO_WifiState:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "ID_SET_KP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 26, -1, -1 },
    { "ID_SET_KI", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 27, -1, -1 },
    { "ID_SET_KD", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 28, -1, -1 },
    { "ID_SET_MIN", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 29, -1, -1 },
    { "ID_SET_MAX", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 30, -1, -1 },
    { "ID_SET_SLOW", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 31, -1, -1 },
    { "ID_SET_DECEL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 32, -1, -1 },
    { "ID_SET_BRK_FAK", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 33, -1, -1 },
    { "ID_SET_DELAY", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 34, -1, -1 },
    { "ID_SET_ACCEL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 35, -1, -1 },
    { "ID_MOTOR_MODE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 36, -1, -1 },
    { "ID_CURRENT_KP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 37, -1, -1 },
    { "ID_CURRENT_KI", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 38, -1, -1 },
    { "ID_CURRENT_KD", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 39, -1, -1 },
    { "ID_EDIT_KP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 40, -1, -1 },
    { "ID_EDIT_KI", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 41, -1, -1 },
    { "ID_EDIT_KD", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 42, -1, -1 },
    { "ID_EDIT_MIN", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 43, -1, -1 },
    { "ID_EDIT_MAX", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 44, -1, -1 },
    { "ID_EDIT_SLOW", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 45, -1, -1 },
    { "ID_EDIT_DECEL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 46, -1, -1 },
    { "ID_EDIT_BRK_FAK", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 47, -1, -1 },
    { "ID_EDIT_DELAY", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 48, -1, -1 },
    { "ID_EDIT_ACCEL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 49, -1, -1 },
    { "SENSOR_PREFIX", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 50, -1, -1 },
    { "SENSOR_RAW_PREFIX", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 51, -1, -1 },
    { "SOLL", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 52, -1, -1 },
    { "STELLWERT", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 53, -1, -1 },
    { "MAX_DATA_COUNT", "I", .constantValue.asInt = PIRCM_MotorSetupDelegate_MAX_DATA_COUNT, 0x19, -1, -1, -1, -1 },
    { "chartController_", "LPIGH_ChartController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "locomotive_", "LPIRR_Locomotive;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "dlgCtrl_", "LPIGC_DialogController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "startX_", "J", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "xStr_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "dataCount_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "motorIDs_", "LJavaUtilList;", .constantValue.asLong = 0, 0x2, -1, -1, 54, -1 },
    { "csvOut_", "LJavaIoOutputStream;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "csvPrint_", "LJavaIoPrintWriter;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "motorModeValue_", "C", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "initializeDialog", "LPIGC_DialogController;LPIRCE_EditCfgParamList;", "LJavaLangException;", "dialogCreated", "LPIGC_DialogController;", "isSlaveMotor", "LPIRM_MotorAction;LPIQ_QName;", "dialogShowing", "dialogBindingsInitialized", "backPressed", "dialogClosing", "LPIGC_DialogController;Z", "dialogClosed", "LPIGC_DialogController;ZLPISB_ParameterList;", "popupClosed", "LPIGC_ModuleGroup;", "setMotorModeValue", "C", "updatePID", "LNSString;LNSString;LNSString;", "processData", "LPIRM_Csv;", "processState", "LPIRM_State;", "processWifiState", "LPIRO_WifiState;", &PIRCM_MotorSetupDelegate_ID_SET_KP, &PIRCM_MotorSetupDelegate_ID_SET_KI, &PIRCM_MotorSetupDelegate_ID_SET_KD, &PIRCM_MotorSetupDelegate_ID_SET_MIN, &PIRCM_MotorSetupDelegate_ID_SET_MAX, &PIRCM_MotorSetupDelegate_ID_SET_SLOW, &PIRCM_MotorSetupDelegate_ID_SET_DECEL, &PIRCM_MotorSetupDelegate_ID_SET_BRK_FAK, &PIRCM_MotorSetupDelegate_ID_SET_DELAY, &PIRCM_MotorSetupDelegate_ID_SET_ACCEL, &PIRCM_MotorSetupDelegate_ID_MOTOR_MODE, &PIRCM_MotorSetupDelegate_ID_CURRENT_KP, &PIRCM_MotorSetupDelegate_ID_CURRENT_KI, &PIRCM_MotorSetupDelegate_ID_CURRENT_KD, &PIRCM_MotorSetupDelegate_ID_EDIT_KP, &PIRCM_MotorSetupDelegate_ID_EDIT_KI, &PIRCM_MotorSetupDelegate_ID_EDIT_KD, &PIRCM_MotorSetupDelegate_ID_EDIT_MIN, &PIRCM_MotorSetupDelegate_ID_EDIT_MAX, &PIRCM_MotorSetupDelegate_ID_EDIT_SLOW, &PIRCM_MotorSetupDelegate_ID_EDIT_DECEL, &PIRCM_MotorSetupDelegate_ID_EDIT_BRK_FAK, &PIRCM_MotorSetupDelegate_ID_EDIT_DELAY, &PIRCM_MotorSetupDelegate_ID_EDIT_ACCEL, &PIRCM_MotorSetupDelegate_SENSOR_PREFIX, &PIRCM_MotorSetupDelegate_SENSOR_RAW_PREFIX, &PIRCM_MotorSetupDelegate_SOLL, &PIRCM_MotorSetupDelegate_STELLWERT, "Ljava/util/List<Lde/pidata/qnames/QName;>;", "Ljava/lang/Object;Lde/pidata/gui/controller/base/DialogControllerDelegate<Lde/pidata/rail/client/editcfg/EditCfgParamList;Lde/pidata/service/base/ParameterList;>;Lde/pidata/rail/railway/DataListener;" };
  static const J2ObjcClassInfo _PIRCM_MotorSetupDelegate = { "MotorSetupDelegate", "de.pidata.rail.client.motor", ptrTable, methods, fields, 7, 0x1, 17, 39, -1, -1, -1, 55, -1 };
  return &_PIRCM_MotorSetupDelegate;
}

+ (void)initialize {
  if (self == [PIRCM_MotorSetupDelegate class]) {
    PIRCM_MotorSetupDelegate_ID_SET_KP = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"setKP"];
    PIRCM_MotorSetupDelegate_ID_SET_KI = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setKI"];
    PIRCM_MotorSetupDelegate_ID_SET_KD = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setKD"];
    PIRCM_MotorSetupDelegate_ID_SET_MIN = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setMin"];
    PIRCM_MotorSetupDelegate_ID_SET_MAX = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setMax"];
    PIRCM_MotorSetupDelegate_ID_SET_SLOW = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setSlow"];
    PIRCM_MotorSetupDelegate_ID_SET_DECEL = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setDecel"];
    PIRCM_MotorSetupDelegate_ID_SET_BRK_FAK = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setBrkFak"];
    PIRCM_MotorSetupDelegate_ID_SET_DELAY = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setDelay"];
    PIRCM_MotorSetupDelegate_ID_SET_ACCEL = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"setAccel"];
    PIRCM_MotorSetupDelegate_ID_MOTOR_MODE = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"motorMode"];
    PIRCM_MotorSetupDelegate_ID_CURRENT_KP = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"currentKP"];
    PIRCM_MotorSetupDelegate_ID_CURRENT_KI = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"currentKI"];
    PIRCM_MotorSetupDelegate_ID_CURRENT_KD = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"currentKD"];
    PIRCM_MotorSetupDelegate_ID_EDIT_KP = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editKP"];
    PIRCM_MotorSetupDelegate_ID_EDIT_KI = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editKI"];
    PIRCM_MotorSetupDelegate_ID_EDIT_KD = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editKD"];
    PIRCM_MotorSetupDelegate_ID_EDIT_MIN = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editMin"];
    PIRCM_MotorSetupDelegate_ID_EDIT_MAX = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editMax"];
    PIRCM_MotorSetupDelegate_ID_EDIT_SLOW = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editSlow"];
    PIRCM_MotorSetupDelegate_ID_EDIT_DECEL = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editDecel"];
    PIRCM_MotorSetupDelegate_ID_EDIT_BRK_FAK = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editBrkFak"];
    PIRCM_MotorSetupDelegate_ID_EDIT_DELAY = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editDelay"];
    PIRCM_MotorSetupDelegate_ID_EDIT_ACCEL = [JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"editAccel"];
    J2OBJC_SET_INITIALIZED(PIRCM_MotorSetupDelegate)
  }
}

@end

void PIRCM_MotorSetupDelegate_init(PIRCM_MotorSetupDelegate *self) {
  NSObject_init(self);
  self->startX_ = -1;
  self->dataCount_ = 0;
  self->motorIDs_ = new_JavaUtilArrayList_init();
}

PIRCM_MotorSetupDelegate *new_PIRCM_MotorSetupDelegate_init() {
  J2OBJC_NEW_IMPL(PIRCM_MotorSetupDelegate, init)
}

PIRCM_MotorSetupDelegate *create_PIRCM_MotorSetupDelegate_init() {
  J2OBJC_CREATE_IMPL(PIRCM_MotorSetupDelegate, init)
}

jboolean PIRCM_MotorSetupDelegate_isSlaveMotorWithPIRM_MotorAction_withPIQ_QName_(PIRCM_MotorSetupDelegate *self, PIRM_MotorAction *motorAction, PIQ_QName *mainMotorActionID) {
  PIRM_ItemConn *itemConn = [((PIRM_MotorAction *) nil_chk(motorAction)) getItemConn];
  for (id<PIMR_ModelIterator> iter = [((PIRM_ItemConn *) nil_chk(itemConn)) itemActionIter]; [((id<PIMR_ModelIterator>) nil_chk(iter)) hasNext]; ) {
    id<PIMR_Model> itemAction = [iter next];
    if ([itemAction isKindOfClass:[PIRM_TriggerAction class]]) {
      if (JreObjectEqualsEquals([((PIRM_TriggerAction *) nil_chk(((PIRM_TriggerAction *) itemAction))) getOnAction], mainMotorActionID)) {
        return true;
      }
    }
  }
  return false;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCM_MotorSetupDelegate)

J2OBJC_NAME_MAPPING(PIRCM_MotorSetupDelegate, "de.pidata.rail.client.motor", "PIRCM_")
