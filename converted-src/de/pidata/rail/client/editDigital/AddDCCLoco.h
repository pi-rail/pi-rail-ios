//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editDigital/AddDCCLoco.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditDigitalAddDCCLoco")
#ifdef RESTRICT_DePidataRailClientEditDigitalAddDCCLoco
#define INCLUDE_ALL_DePidataRailClientEditDigitalAddDCCLoco 0
#else
#define INCLUDE_ALL_DePidataRailClientEditDigitalAddDCCLoco 1
#endif
#undef RESTRICT_DePidataRailClientEditDigitalAddDCCLoco

#if !defined (DePidataRailClientEditDigitalAddDCCLoco_) && (INCLUDE_ALL_DePidataRailClientEditDigitalAddDCCLoco || defined(INCLUDE_DePidataRailClientEditDigitalAddDCCLoco))
#define DePidataRailClientEditDigitalAddDCCLoco_

#define RESTRICT_DePidataGuiControllerBaseGuiDelegateOperation 1
#define INCLUDE_PIGC_GuiDelegateOperation 1
#include "de/pidata/gui/controller/base/GuiDelegateOperation.h"

@class DePidataRailClientEditDigitalEditDigitalDelegate;
@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface DePidataRailClientEditDigitalAddDCCLoco : PIGC_GuiDelegateOperation

#pragma mark Public

- (instancetype)init;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)parentDlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

#pragma mark Protected

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(DePidataRailClientEditDigitalEditDigitalDelegate *)delegate
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(DePidataRailClientEditDigitalAddDCCLoco)

FOUNDATION_EXPORT void DePidataRailClientEditDigitalAddDCCLoco_init(DePidataRailClientEditDigitalAddDCCLoco *self);

FOUNDATION_EXPORT DePidataRailClientEditDigitalAddDCCLoco *new_DePidataRailClientEditDigitalAddDCCLoco_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailClientEditDigitalAddDCCLoco *create_DePidataRailClientEditDigitalAddDCCLoco_init(void);

J2OBJC_TYPE_LITERAL_HEADER(DePidataRailClientEditDigitalAddDCCLoco)

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditDigitalAddDCCLoco")
