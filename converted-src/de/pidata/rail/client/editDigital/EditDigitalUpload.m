//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editDigital/EditDigitalUpload.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/event/Dialog.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/editDigital/EditDigitalDelegate.h"
#include "de/pidata/rail/client/editDigital/EditDigitalUpload.h"
#include "de/pidata/rail/client/editcfg/SaveCfgOperation.h"
#include "de/pidata/rail/client/uiModels/EditCfgUI.h"
#include "de/pidata/rail/track/TrackCfg.h"
#include "java/lang/Runnable.h"
#include "java/lang/Thread.h"
#include "java/net/InetAddress.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/editDigital/EditDigitalUpload must be compiled with ARC (-fobjc-arc)"
#endif

@interface DePidataRailClientEditDigitalEditDigitalUpload_1 : NSObject < JavaLangRunnable > {
 @public
  PIRCU_EditCfgUI *val$uiModel_;
  DePidataRailTrackTrackCfg *val$trackCfg_;
  id<PIGC_Controller> val$source_;
}

- (instancetype)initWithPIRCU_EditCfgUI:(PIRCU_EditCfgUI *)capture$0
          withDePidataRailTrackTrackCfg:(DePidataRailTrackTrackCfg *)capture$1
                    withPIGC_Controller:(id<PIGC_Controller>)capture$2;

- (void)run;

@end

J2OBJC_EMPTY_STATIC_INIT(DePidataRailClientEditDigitalEditDigitalUpload_1)

__attribute__((unused)) static void DePidataRailClientEditDigitalEditDigitalUpload_1_initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_(DePidataRailClientEditDigitalEditDigitalUpload_1 *self, PIRCU_EditCfgUI *capture$0, DePidataRailTrackTrackCfg *capture$1, id<PIGC_Controller> capture$2);

__attribute__((unused)) static DePidataRailClientEditDigitalEditDigitalUpload_1 *new_DePidataRailClientEditDigitalEditDigitalUpload_1_initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_(PIRCU_EditCfgUI *capture$0, DePidataRailTrackTrackCfg *capture$1, id<PIGC_Controller> capture$2) NS_RETURNS_RETAINED;

__attribute__((unused)) static DePidataRailClientEditDigitalEditDigitalUpload_1 *create_DePidataRailClientEditDigitalEditDigitalUpload_1_initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_(PIRCU_EditCfgUI *capture$0, DePidataRailTrackTrackCfg *capture$1, id<PIGC_Controller> capture$2);

@implementation DePidataRailClientEditDigitalEditDigitalUpload

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  DePidataRailClientEditDigitalEditDigitalUpload_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(DePidataRailClientEditDigitalEditDigitalDelegate *)delegate
         withPIGC_Controller:(id<PIGC_Controller>)source
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  PIRCU_EditCfgUI *uiModel = (PIRCU_EditCfgUI *) cast_chk(dataContext, [PIRCU_EditCfgUI class]);
  if (uiModel == nil) {
    PICL_Logger_infoWithNSString_(@"Could not upload SwitchBox configuration, UI Model is null");
    return;
  }
  DePidataRailTrackTrackCfg *trackCfg = [uiModel getTrackCfg];
  if (trackCfg == nil) {
    PICL_Logger_infoWithNSString_(@"Could not upload SwitchBox configuration, TrackCfg is null");
    return;
  }
  [((DePidataRailClientEditDigitalEditDigitalDelegate *) nil_chk(delegate)) updateDigitalCfgWithPIRCU_EditCfgUI:uiModel withDePidataRailTrackTrackCfg:trackCfg];
  [((id<PIGE_Dialog>) nil_chk([((id<PIGC_DialogController>) nil_chk([((id<PIGC_Controller>) nil_chk(source)) getDialogController])) getDialogComp])) showBusyWithBoolean:true];
  [new_JavaLangThread_initWithJavaLangRunnable_(new_DePidataRailClientEditDigitalEditDigitalUpload_1_initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_(uiModel, trackCfg, source)) start];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(executeWithPIQ_QName:withPIGC_DialogControllerDelegate:withPIGC_Controller:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "execute", "LPIQ_QName;LDePidataRailClientEditDigitalEditDigitalDelegate;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;", "Lde/pidata/rail/client/editcfg/SaveCfgOperation<Lde/pidata/rail/client/editDigital/EditDigitalDelegate;>;" };
  static const J2ObjcClassInfo _DePidataRailClientEditDigitalEditDigitalUpload = { "EditDigitalUpload", "de.pidata.rail.client.editDigital", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, 3, -1 };
  return &_DePidataRailClientEditDigitalEditDigitalUpload;
}

@end

void DePidataRailClientEditDigitalEditDigitalUpload_init(DePidataRailClientEditDigitalEditDigitalUpload *self) {
  PIRCE_SaveCfgOperation_init(self);
}

DePidataRailClientEditDigitalEditDigitalUpload *new_DePidataRailClientEditDigitalEditDigitalUpload_init() {
  J2OBJC_NEW_IMPL(DePidataRailClientEditDigitalEditDigitalUpload, init)
}

DePidataRailClientEditDigitalEditDigitalUpload *create_DePidataRailClientEditDigitalEditDigitalUpload_init() {
  J2OBJC_CREATE_IMPL(DePidataRailClientEditDigitalEditDigitalUpload, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataRailClientEditDigitalEditDigitalUpload)

@implementation DePidataRailClientEditDigitalEditDigitalUpload_1

- (instancetype)initWithPIRCU_EditCfgUI:(PIRCU_EditCfgUI *)capture$0
          withDePidataRailTrackTrackCfg:(DePidataRailTrackTrackCfg *)capture$1
                    withPIGC_Controller:(id<PIGC_Controller>)capture$2 {
  DePidataRailClientEditDigitalEditDigitalUpload_1_initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_(self, capture$0, capture$1, capture$2);
  return self;
}

- (void)run {
  if (PIRCE_SaveCfgOperation_uploadTrackCfgWithJavaNetInetAddress_withDePidataRailTrackTrackCfg_withPIGC_DialogController_([((PIRCU_EditCfgUI *) nil_chk(val$uiModel_)) getInetAddress], val$trackCfg_, [((id<PIGC_Controller>) nil_chk(val$source_)) getDialogController])) {
    [((id<PIGC_DialogController>) nil_chk([val$source_ getDialogController])) closeWithBoolean:true];
  }
  [((id<PIGE_Dialog>) nil_chk([((id<PIGC_DialogController>) nil_chk([val$source_ getDialogController])) getDialogComp])) showBusyWithBoolean:false];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x0, -1, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIRCU_EditCfgUI:withDePidataRailTrackTrackCfg:withPIGC_Controller:);
  methods[1].selector = @selector(run);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "val$uiModel_", "LPIRCU_EditCfgUI;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
    { "val$trackCfg_", "LDePidataRailTrackTrackCfg;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
    { "val$source_", "LPIGC_Controller;", .constantValue.asLong = 0, 0x1012, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIRCU_EditCfgUI;LDePidataRailTrackTrackCfg;LPIGC_Controller;", "LDePidataRailClientEditDigitalEditDigitalUpload;", "executeWithPIQ_QName:withPIGC_DialogControllerDelegate:withPIGC_Controller:withPIMR_Model:" };
  static const J2ObjcClassInfo _DePidataRailClientEditDigitalEditDigitalUpload_1 = { "", "de.pidata.rail.client.editDigital", ptrTable, methods, fields, 7, 0x8010, 2, 3, 1, -1, 2, -1, -1 };
  return &_DePidataRailClientEditDigitalEditDigitalUpload_1;
}

@end

void DePidataRailClientEditDigitalEditDigitalUpload_1_initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_(DePidataRailClientEditDigitalEditDigitalUpload_1 *self, PIRCU_EditCfgUI *capture$0, DePidataRailTrackTrackCfg *capture$1, id<PIGC_Controller> capture$2) {
  self->val$uiModel_ = capture$0;
  self->val$trackCfg_ = capture$1;
  self->val$source_ = capture$2;
  NSObject_init(self);
}

DePidataRailClientEditDigitalEditDigitalUpload_1 *new_DePidataRailClientEditDigitalEditDigitalUpload_1_initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_(PIRCU_EditCfgUI *capture$0, DePidataRailTrackTrackCfg *capture$1, id<PIGC_Controller> capture$2) {
  J2OBJC_NEW_IMPL(DePidataRailClientEditDigitalEditDigitalUpload_1, initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_, capture$0, capture$1, capture$2)
}

DePidataRailClientEditDigitalEditDigitalUpload_1 *create_DePidataRailClientEditDigitalEditDigitalUpload_1_initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_(PIRCU_EditCfgUI *capture$0, DePidataRailTrackTrackCfg *capture$1, id<PIGC_Controller> capture$2) {
  J2OBJC_CREATE_IMPL(DePidataRailClientEditDigitalEditDigitalUpload_1, initWithPIRCU_EditCfgUI_withDePidataRailTrackTrackCfg_withPIGC_Controller_, capture$0, capture$1, capture$2)
}
