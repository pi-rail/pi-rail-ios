//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/timetable/PlayPauseTime.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/GuiBuilder.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/GuiOperation.h"
#include "de/pidata/gui/controller/base/ListController.h"
#include "de/pidata/gui/controller/base/MutableControllerGroup.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/timetable/PlayPauseTime.h"
#include "de/pidata/rail/railway/ModelRailway.h"
#include "de/pidata/rail/track/Timetable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/timetable/PlayPauseTime must be compiled with ARC (-fobjc-arc)"
#endif

@implementation DePidataRailClientTimetablePlayPauseTime

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  DePidataRailClientTimetablePlayPauseTime_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  PIRR_ModelRailway *modelRailway = (PIRR_ModelRailway *) cast_chk(dataContext, [PIRR_ModelRailway class]);
  id<PIGC_ListController> timetableList = (id<PIGC_ListController>) cast_check([((id<PIGC_MutableControllerGroup>) nil_chk([((id<PIGC_Controller>) nil_chk(sourceCtrl)) getControllerGroup])) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"timetableList"]], PIGC_ListController_class_());
  DePidataRailTrackTimetable *selectedTimetable = nil;
  if (timetableList != nil) {
    selectedTimetable = (DePidataRailTrackTimetable *) cast_chk([timetableList getSelectedRowWithInt:0], [DePidataRailTrackTimetable class]);
  }
  if ([((PIRR_ModelRailway *) nil_chk(modelRailway)) stopStartModelTimeWithDePidataRailTrackTimetable:selectedTimetable]) {
    [sourceCtrl setValueWithId:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGC_GuiOperation, NAMESPACE))) getQNameWithNSString:@"icons/actions/Loco/button_play.png"]];
  }
  else {
    [sourceCtrl setValueWithId:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGC_GuiOperation, NAMESPACE))) getQNameWithNSString:@"icons/actions/Loco/button_stop.png"]];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(executeWithPIQ_QName:withPIGC_Controller:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "execute", "LPIQ_QName;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;" };
  static const J2ObjcClassInfo _DePidataRailClientTimetablePlayPauseTime = { "PlayPauseTime", "de.pidata.rail.client.timetable", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_DePidataRailClientTimetablePlayPauseTime;
}

@end

void DePidataRailClientTimetablePlayPauseTime_init(DePidataRailClientTimetablePlayPauseTime *self) {
  PIGC_GuiOperation_init(self);
}

DePidataRailClientTimetablePlayPauseTime *new_DePidataRailClientTimetablePlayPauseTime_init() {
  J2OBJC_NEW_IMPL(DePidataRailClientTimetablePlayPauseTime, init)
}

DePidataRailClientTimetablePlayPauseTime *create_DePidataRailClientTimetablePlayPauseTime_init() {
  J2OBJC_CREATE_IMPL(DePidataRailClientTimetablePlayPauseTime, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataRailClientTimetablePlayPauseTime)
