//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/depot/DepotModule.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/GuiBuilder.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/ListController.h"
#include "de/pidata/gui/controller/base/ModuleGroup.h"
#include "de/pidata/gui/ui/base/UIContainer.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/depot/DepotModule.h"
#include "de/pidata/rail/comm/PiRail.h"
#include "de/pidata/rail/railway/ModelRailway.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/depot/DepotModule must be compiled with ARC (-fobjc-arc)"
#endif

@implementation DePidataRailClientDepotDepotModule

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  DePidataRailClientDepotDepotModule_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)activateModuleWithPIGU_UIContainer:(id<PIGU_UIContainer>)moduleContainer {
  @synchronized(self) {
    PIRR_ModelRailway *modelRailway = [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway];
    [self setModelWithPIMR_Model:modelRailway];
    id<PIGC_ListController> depotSelection = (id<PIGC_ListController>) cast_check([self getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"depotSelection"]], PIGC_ListController_class_());
    [super activateModuleWithPIGU_UIContainer:moduleContainer];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x21, 0, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(activateModuleWithPIGU_UIContainer:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "activateModule", "LPIGU_UIContainer;" };
  static const J2ObjcClassInfo _DePidataRailClientDepotDepotModule = { "DepotModule", "de.pidata.rail.client.depot", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, -1, -1 };
  return &_DePidataRailClientDepotDepotModule;
}

@end

void DePidataRailClientDepotDepotModule_init(DePidataRailClientDepotDepotModule *self) {
  PIGC_ModuleGroup_init(self);
}

DePidataRailClientDepotDepotModule *new_DePidataRailClientDepotDepotModule_init() {
  J2OBJC_NEW_IMPL(DePidataRailClientDepotDepotModule, init)
}

DePidataRailClientDepotDepotModule *create_DePidataRailClientDepotDepotModule_init() {
  J2OBJC_CREATE_IMPL(DePidataRailClientDepotDepotModule, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataRailClientDepotDepotModule)
