//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/depot/EditWagonIconUpload.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientDepotEditWagonIconUpload")
#ifdef RESTRICT_DePidataRailClientDepotEditWagonIconUpload
#define INCLUDE_ALL_DePidataRailClientDepotEditWagonIconUpload 0
#else
#define INCLUDE_ALL_DePidataRailClientDepotEditWagonIconUpload 1
#endif
#undef RESTRICT_DePidataRailClientDepotEditWagonIconUpload

#if !defined (DePidataRailClientDepotEditWagonIconUpload_) && (INCLUDE_ALL_DePidataRailClientDepotEditWagonIconUpload || defined(INCLUDE_DePidataRailClientDepotEditWagonIconUpload))
#define DePidataRailClientDepotEditWagonIconUpload_

#define RESTRICT_DePidataGuiControllerBaseGuiDelegateOperation 1
#define INCLUDE_PIGC_GuiDelegateOperation 1
#include "de/pidata/gui/controller/base/GuiDelegateOperation.h"

@class DePidataRailClientDepotEditWagonDelegate;
@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIMR_Model;

@interface DePidataRailClientDepotEditWagonIconUpload : PIGC_GuiDelegateOperation

#pragma mark Public

- (instancetype)init;

#pragma mark Protected

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(DePidataRailClientDepotEditWagonDelegate *)delegate
         withPIGC_Controller:(id<PIGC_Controller>)source
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(DePidataRailClientDepotEditWagonIconUpload)

FOUNDATION_EXPORT void DePidataRailClientDepotEditWagonIconUpload_init(DePidataRailClientDepotEditWagonIconUpload *self);

FOUNDATION_EXPORT DePidataRailClientDepotEditWagonIconUpload *new_DePidataRailClientDepotEditWagonIconUpload_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailClientDepotEditWagonIconUpload *create_DePidataRailClientDepotEditWagonIconUpload_init(void);

J2OBJC_TYPE_LITERAL_HEADER(DePidataRailClientDepotEditWagonIconUpload)

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientDepotEditWagonIconUpload")
