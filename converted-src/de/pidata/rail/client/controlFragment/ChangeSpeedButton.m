//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/controlFragment/ChangeSpeedButton.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/GuiOperation.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/types/simple/DecimalObject.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/controlFragment/ChangeSpeedButton.h"
#include "de/pidata/rail/railway/Locomotive.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/controlFragment/ChangeSpeedButton must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIRCT_ChangeSpeedButton)

PIQ_QName *PIRCT_ChangeSpeedButton_ID_SPEED_DEC_LARGE;
PIQ_QName *PIRCT_ChangeSpeedButton_ID_SPEED_DEC_SMALL;
PIQ_QName *PIRCT_ChangeSpeedButton_ID_SPEED_INC_SMALL;
PIQ_QName *PIRCT_ChangeSpeedButton_ID_SPEED_INC_LARGE;

@implementation PIRCT_ChangeSpeedButton

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCT_ChangeSpeedButton_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  PIRR_Locomotive *loco = (PIRR_Locomotive *) cast_chk(dataContext, [PIRR_Locomotive class]);
  jint delta = 0;
  if (JreObjectEqualsEquals(eventID, PIRCT_ChangeSpeedButton_ID_SPEED_DEC_LARGE)) {
    delta = -10;
  }
  else if (JreObjectEqualsEquals(eventID, PIRCT_ChangeSpeedButton_ID_SPEED_DEC_SMALL)) {
    delta = -1;
  }
  else if (JreObjectEqualsEquals(eventID, PIRCT_ChangeSpeedButton_ID_SPEED_INC_SMALL)) {
    delta = 1;
  }
  else if (JreObjectEqualsEquals(eventID, PIRCT_ChangeSpeedButton_ID_SPEED_INC_LARGE)) {
    delta = 10;
  }
  if ((delta != 0) && (loco != nil)) {
    PIME_DecimalObject *decValue = [((PIRR_Locomotive *) nil_chk(loco)) getTargetSpeedPercent];
    jint value;
    if (decValue == nil) {
      value = 0;
    }
    else {
      value = [decValue intValue];
    }
    jint newValue = value + delta;
    if (newValue < 0) {
      newValue = 0;
    }
    else if (newValue > 100) {
      newValue = 100;
    }
    [loco setTargetSpeedPercentWithPIME_DecimalObject:new_PIME_DecimalObject_initWithLong_withInt_(newValue, 0)];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(executeWithPIQ_QName:withPIGC_Controller:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "ID_SPEED_DEC_LARGE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 3, -1, -1 },
    { "ID_SPEED_DEC_SMALL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 4, -1, -1 },
    { "ID_SPEED_INC_SMALL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 5, -1, -1 },
    { "ID_SPEED_INC_LARGE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 6, -1, -1 },
  };
  static const void *ptrTable[] = { "execute", "LPIQ_QName;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;", &PIRCT_ChangeSpeedButton_ID_SPEED_DEC_LARGE, &PIRCT_ChangeSpeedButton_ID_SPEED_DEC_SMALL, &PIRCT_ChangeSpeedButton_ID_SPEED_INC_SMALL, &PIRCT_ChangeSpeedButton_ID_SPEED_INC_LARGE };
  static const J2ObjcClassInfo _PIRCT_ChangeSpeedButton = { "ChangeSpeedButton", "de.pidata.rail.client.controlFragment", ptrTable, methods, fields, 7, 0x1, 2, 4, -1, -1, -1, -1, -1 };
  return &_PIRCT_ChangeSpeedButton;
}

+ (void)initialize {
  if (self == [PIRCT_ChangeSpeedButton class]) {
    PIRCT_ChangeSpeedButton_ID_SPEED_DEC_LARGE = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGC_GuiOperation, NAMESPACE))) getQNameWithNSString:@"speedDecLarge"];
    PIRCT_ChangeSpeedButton_ID_SPEED_DEC_SMALL = [JreLoadStatic(PIGC_GuiOperation, NAMESPACE) getQNameWithNSString:@"speedDecSmall"];
    PIRCT_ChangeSpeedButton_ID_SPEED_INC_SMALL = [JreLoadStatic(PIGC_GuiOperation, NAMESPACE) getQNameWithNSString:@"speedIncSmall"];
    PIRCT_ChangeSpeedButton_ID_SPEED_INC_LARGE = [JreLoadStatic(PIGC_GuiOperation, NAMESPACE) getQNameWithNSString:@"speedIncLarge"];
    J2OBJC_SET_INITIALIZED(PIRCT_ChangeSpeedButton)
  }
}

@end

void PIRCT_ChangeSpeedButton_init(PIRCT_ChangeSpeedButton *self) {
  PIGC_GuiOperation_init(self);
}

PIRCT_ChangeSpeedButton *new_PIRCT_ChangeSpeedButton_init() {
  J2OBJC_NEW_IMPL(PIRCT_ChangeSpeedButton, init)
}

PIRCT_ChangeSpeedButton *create_PIRCT_ChangeSpeedButton_init() {
  J2OBJC_CREATE_IMPL(PIRCT_ChangeSpeedButton, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCT_ChangeSpeedButton)

J2OBJC_NAME_MAPPING(PIRCT_ChangeSpeedButton, "de.pidata.rail.client.controlFragment", "PIRCT_")
