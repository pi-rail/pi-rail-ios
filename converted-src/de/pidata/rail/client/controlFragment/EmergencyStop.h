//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/controlFragment/EmergencyStop.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientControlFragmentEmergencyStop")
#ifdef RESTRICT_DePidataRailClientControlFragmentEmergencyStop
#define INCLUDE_ALL_DePidataRailClientControlFragmentEmergencyStop 0
#else
#define INCLUDE_ALL_DePidataRailClientControlFragmentEmergencyStop 1
#endif
#undef RESTRICT_DePidataRailClientControlFragmentEmergencyStop

#if !defined (PIRCT_EmergencyStop_) && (INCLUDE_ALL_DePidataRailClientControlFragmentEmergencyStop || defined(INCLUDE_PIRCT_EmergencyStop))
#define PIRCT_EmergencyStop_

#define RESTRICT_DePidataGuiControllerBaseGuiOperation 1
#define INCLUDE_PIGC_GuiOperation 1
#include "de/pidata/gui/controller/base/GuiOperation.h"

@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIMR_Model;

@interface PIRCT_EmergencyStop : PIGC_GuiOperation

#pragma mark Public

- (instancetype)init;

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCT_EmergencyStop)

FOUNDATION_EXPORT void PIRCT_EmergencyStop_init(PIRCT_EmergencyStop *self);

FOUNDATION_EXPORT PIRCT_EmergencyStop *new_PIRCT_EmergencyStop_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCT_EmergencyStop *create_PIRCT_EmergencyStop_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCT_EmergencyStop)

@compatibility_alias DePidataRailClientControlFragmentEmergencyStop PIRCT_EmergencyStop;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientControlFragmentEmergencyStop")
