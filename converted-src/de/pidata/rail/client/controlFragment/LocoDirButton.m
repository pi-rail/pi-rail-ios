//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/controlFragment/LocoDirButton.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/GuiOperation.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/controlFragment/LocoDirButton.h"
#include "de/pidata/rail/model/MotorState.h"
#include "de/pidata/rail/railway/Locomotive.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/controlFragment/LocoDirButton must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIRCT_LocoDirButton)

PIQ_QName *PIRCT_LocoDirButton_ID_PAUSE;
PIQ_QName *PIRCT_LocoDirButton_ID_FORWARD;
PIQ_QName *PIRCT_LocoDirButton_ID_BACKWARD;

@implementation PIRCT_LocoDirButton

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCT_LocoDirButton_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  PIRR_Locomotive *selectedLoco = (PIRR_Locomotive *) cast_chk(dataContext, [PIRR_Locomotive class]);
  if (JreObjectEqualsEquals(eventID, PIRCT_LocoDirButton_ID_PAUSE)) {
    jboolean stopped = [((PIRR_Locomotive *) nil_chk(selectedLoco)) getStopped];
    [selectedLoco setStoppedWithBoolean:!stopped];
  }
  else if (JreObjectEqualsEquals(eventID, PIRCT_LocoDirButton_ID_FORWARD)) {
    [((PIRR_Locomotive *) nil_chk(selectedLoco)) setStoppedWithBoolean:false];
    [selectedLoco setTargetDirWithNSString:PIRM_MotorState_DIR_FORWARD];
  }
  else if (JreObjectEqualsEquals(eventID, PIRCT_LocoDirButton_ID_BACKWARD)) {
    [((PIRR_Locomotive *) nil_chk(selectedLoco)) setStoppedWithBoolean:false];
    [selectedLoco setTargetDirWithNSString:PIRM_MotorState_DIR_BACK];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(executeWithPIQ_QName:withPIGC_Controller:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "ID_PAUSE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 3, -1, -1 },
    { "ID_FORWARD", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 4, -1, -1 },
    { "ID_BACKWARD", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 5, -1, -1 },
  };
  static const void *ptrTable[] = { "execute", "LPIQ_QName;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;", &PIRCT_LocoDirButton_ID_PAUSE, &PIRCT_LocoDirButton_ID_FORWARD, &PIRCT_LocoDirButton_ID_BACKWARD };
  static const J2ObjcClassInfo _PIRCT_LocoDirButton = { "LocoDirButton", "de.pidata.rail.client.controlFragment", ptrTable, methods, fields, 7, 0x1, 2, 3, -1, -1, -1, -1, -1 };
  return &_PIRCT_LocoDirButton;
}

+ (void)initialize {
  if (self == [PIRCT_LocoDirButton class]) {
    PIRCT_LocoDirButton_ID_PAUSE = [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGC_GuiOperation, NAMESPACE))) getQNameWithNSString:@"LocoPlayPauseController"];
    PIRCT_LocoDirButton_ID_FORWARD = [JreLoadStatic(PIGC_GuiOperation, NAMESPACE) getQNameWithNSString:@"LocoForward"];
    PIRCT_LocoDirButton_ID_BACKWARD = [JreLoadStatic(PIGC_GuiOperation, NAMESPACE) getQNameWithNSString:@"LocoBackward"];
    J2OBJC_SET_INITIALIZED(PIRCT_LocoDirButton)
  }
}

@end

void PIRCT_LocoDirButton_init(PIRCT_LocoDirButton *self) {
  PIGC_GuiOperation_init(self);
}

PIRCT_LocoDirButton *new_PIRCT_LocoDirButton_init() {
  J2OBJC_NEW_IMPL(PIRCT_LocoDirButton, init)
}

PIRCT_LocoDirButton *create_PIRCT_LocoDirButton_init() {
  J2OBJC_CREATE_IMPL(PIRCT_LocoDirButton, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCT_LocoDirButton)

J2OBJC_NAME_MAPPING(PIRCT_LocoDirButton, "de.pidata.rail.client.controlFragment", "PIRCT_")
