//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editcfg/AbstractEditConfig.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditcfgAbstractEditConfig")
#ifdef RESTRICT_DePidataRailClientEditcfgAbstractEditConfig
#define INCLUDE_ALL_DePidataRailClientEditcfgAbstractEditConfig 0
#else
#define INCLUDE_ALL_DePidataRailClientEditcfgAbstractEditConfig 1
#endif
#undef RESTRICT_DePidataRailClientEditcfgAbstractEditConfig

#if !defined (PIRCE_AbstractEditConfig_) && (INCLUDE_ALL_DePidataRailClientEditcfgAbstractEditConfig || defined(INCLUDE_PIRCE_AbstractEditConfig))
#define PIRCE_AbstractEditConfig_

#define RESTRICT_DePidataGuiControllerBaseDialogControllerDelegate 1
#define INCLUDE_PIGC_DialogControllerDelegate 1
#include "de/pidata/gui/controller/base/DialogControllerDelegate.h"

#define RESTRICT_DePidataRailRailwayDataListener 1
#define INCLUDE_PIRR_DataListener 1
#include "de/pidata/rail/railway/DataListener.h"

@class JavaLangStringBuilder;
@class JavaNetInetAddress;
@class PIGC_TextEditorController;
@class PIQ_QName;
@class PIRM_Csv;
@class PIRM_State;
@class PIRO_WifiState;

@interface PIRCE_AbstractEditConfig : NSObject < PIGC_DialogControllerDelegate, PIRR_DataListener > {
 @public
  PIQ_QName *deviceId_;
  JavaNetInetAddress *deviceAddress_;
  PIGC_TextEditorController *updaterOutput_;
  JavaLangStringBuilder *messageCollector_;
}

#pragma mark Public

- (instancetype)init;

- (JavaNetInetAddress *)getDeviceAddress;

- (PIQ_QName *)getDeviceId;

- (JavaLangStringBuilder *)getMessageCollector;

- (PIGC_TextEditorController *)getUpdaterOutput;

- (void)processDataWithPIRM_Csv:(PIRM_Csv *)csvData;

- (void)processStateWithPIRM_State:(PIRM_State *)state;

- (void)processWifiStateWithPIRO_WifiState:(PIRO_WifiState *)newState;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCE_AbstractEditConfig)

J2OBJC_FIELD_SETTER(PIRCE_AbstractEditConfig, deviceId_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIRCE_AbstractEditConfig, deviceAddress_, JavaNetInetAddress *)
J2OBJC_FIELD_SETTER(PIRCE_AbstractEditConfig, updaterOutput_, PIGC_TextEditorController *)
J2OBJC_FIELD_SETTER(PIRCE_AbstractEditConfig, messageCollector_, JavaLangStringBuilder *)

FOUNDATION_EXPORT void PIRCE_AbstractEditConfig_init(PIRCE_AbstractEditConfig *self);

J2OBJC_TYPE_LITERAL_HEADER(PIRCE_AbstractEditConfig)

@compatibility_alias DePidataRailClientEditcfgAbstractEditConfig PIRCE_AbstractEditConfig;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditcfgAbstractEditConfig")
