//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editcfg/EditNameDelegate.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditcfgEditNameDelegate")
#ifdef RESTRICT_DePidataRailClientEditcfgEditNameDelegate
#define INCLUDE_ALL_DePidataRailClientEditcfgEditNameDelegate 0
#else
#define INCLUDE_ALL_DePidataRailClientEditcfgEditNameDelegate 1
#endif
#undef RESTRICT_DePidataRailClientEditcfgEditNameDelegate

#if !defined (PIRCE_EditNameDelegate_) && (INCLUDE_ALL_DePidataRailClientEditcfgEditNameDelegate || defined(INCLUDE_PIRCE_EditNameDelegate))
#define PIRCE_EditNameDelegate_

#define RESTRICT_DePidataGuiControllerBaseDialogControllerDelegate 1
#define INCLUDE_PIGC_DialogControllerDelegate 1
#include "de/pidata/gui/controller/base/DialogControllerDelegate.h"

#define RESTRICT_DePidataRailCommConfigLoaderListener 1
#define INCLUDE_PIRO_ConfigLoaderListener 1
#include "de/pidata/rail/comm/ConfigLoaderListener.h"

@class JavaNetInetAddress;
@class PIGC_ModuleGroup;
@class PIRCE_EditCfgParamList;
@class PIRO_ConfigLoader;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface PIRCE_EditNameDelegate : NSObject < PIGC_DialogControllerDelegate, PIRO_ConfigLoaderListener >

#pragma mark Public

- (instancetype)init;

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

- (id<PISB_ParameterList>)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                     withBoolean:(jboolean)ok;

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)finishedLoadingWithPIRO_ConfigLoader:(PIRO_ConfigLoader *)configLoader
                                 withBoolean:(jboolean)success;

- (JavaNetInetAddress *)getDeviceAddress;

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(PIRCE_EditCfgParamList *)parameterList OBJC_METHOD_FAMILY_NONE;

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCE_EditNameDelegate)

FOUNDATION_EXPORT void PIRCE_EditNameDelegate_init(PIRCE_EditNameDelegate *self);

FOUNDATION_EXPORT PIRCE_EditNameDelegate *new_PIRCE_EditNameDelegate_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCE_EditNameDelegate *create_PIRCE_EditNameDelegate_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCE_EditNameDelegate)

@compatibility_alias DePidataRailClientEditcfgEditNameDelegate PIRCE_EditNameDelegate;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditcfgEditNameDelegate")
