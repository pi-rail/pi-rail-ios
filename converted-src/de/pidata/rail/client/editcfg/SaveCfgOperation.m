//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editcfg/SaveCfgOperation.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/GuiDelegateOperation.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/xml/binder/XmlWriter.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/editcfg/SaveCfgOperation.h"
#include "de/pidata/rail/comm/ConfigLoader.h"
#include "de/pidata/rail/model/Cfg.h"
#include "de/pidata/rail/model/IoCfg.h"
#include "de/pidata/rail/model/PiRailFactory.h"
#include "de/pidata/rail/railway/RailDevice.h"
#include "de/pidata/rail/railway/RailDeviceAddress.h"
#include "de/pidata/rail/track/Depot.h"
#include "de/pidata/rail/track/PiRailTrackFactory.h"
#include "de/pidata/rail/track/TrackCfg.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/io/IOException.h"
#include "java/lang/Exception.h"
#include "java/lang/Integer.h"
#include "java/lang/StringBuilder.h"
#include "java/net/InetAddress.h"
#include "java/util/Properties.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/editcfg/SaveCfgOperation must be compiled with ARC (-fobjc-arc)"
#endif

@implementation PIRCE_SaveCfgOperation

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCE_SaveCfgOperation_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

+ (jboolean)uploadCfgWithJavaNetInetAddress:(JavaNetInetAddress *)deviceAddress
                               withPIRM_Cfg:(PIRM_Cfg *)cfg
                  withPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  return PIRCE_SaveCfgOperation_uploadCfgWithJavaNetInetAddress_withPIRM_Cfg_withPIGC_DialogController_(deviceAddress, cfg, dlgCtrl);
}

+ (jboolean)uploadIoCfgWithPIRR_RailDevice:(PIRR_RailDevice *)railDevice
                            withPIRM_IoCfg:(PIRM_IoCfg *)ioCfg
                 withPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  return PIRCE_SaveCfgOperation_uploadIoCfgWithPIRR_RailDevice_withPIRM_IoCfg_withPIGC_DialogController_(railDevice, ioCfg, dlgCtrl);
}

+ (jboolean)uploadTrackCfgWithJavaNetInetAddress:(JavaNetInetAddress *)deviceAddress
                   withDePidataRailTrackTrackCfg:(DePidataRailTrackTrackCfg *)trackCfg
                       withPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  return PIRCE_SaveCfgOperation_uploadTrackCfgWithJavaNetInetAddress_withDePidataRailTrackTrackCfg_withPIGC_DialogController_(deviceAddress, trackCfg, dlgCtrl);
}

+ (jboolean)uploadDepotWithJavaNetInetAddress:(JavaNetInetAddress *)deviceAddress
                   withDePidataRailTrackDepot:(DePidataRailTrackDepot *)depot
                    withPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  return PIRCE_SaveCfgOperation_uploadDepotWithJavaNetInetAddress_withDePidataRailTrackDepot_withPIGC_DialogController_(deviceAddress, depot, dlgCtrl);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 2, 3, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 4, 5, -1, -1, -1, -1 },
    { NULL, "Z", 0x9, 6, 7, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(uploadCfgWithJavaNetInetAddress:withPIRM_Cfg:withPIGC_DialogController:);
  methods[2].selector = @selector(uploadIoCfgWithPIRR_RailDevice:withPIRM_IoCfg:withPIGC_DialogController:);
  methods[3].selector = @selector(uploadTrackCfgWithJavaNetInetAddress:withDePidataRailTrackTrackCfg:withPIGC_DialogController:);
  methods[4].selector = @selector(uploadDepotWithJavaNetInetAddress:withDePidataRailTrackDepot:withPIGC_DialogController:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "uploadCfg", "LJavaNetInetAddress;LPIRM_Cfg;LPIGC_DialogController;", "uploadIoCfg", "LPIRR_RailDevice;LPIRM_IoCfg;LPIGC_DialogController;", "uploadTrackCfg", "LJavaNetInetAddress;LDePidataRailTrackTrackCfg;LPIGC_DialogController;", "uploadDepot", "LJavaNetInetAddress;LDePidataRailTrackDepot;LPIGC_DialogController;", "<DCD::Lde/pidata/gui/controller/base/DialogControllerDelegate;>Lde/pidata/gui/controller/base/GuiDelegateOperation<TDCD;>;" };
  static const J2ObjcClassInfo _PIRCE_SaveCfgOperation = { "SaveCfgOperation", "de.pidata.rail.client.editcfg", ptrTable, methods, NULL, 7, 0x401, 5, 0, -1, -1, -1, 8, -1 };
  return &_PIRCE_SaveCfgOperation;
}

@end

void PIRCE_SaveCfgOperation_init(PIRCE_SaveCfgOperation *self) {
  PIGC_GuiDelegateOperation_init(self);
}

jboolean PIRCE_SaveCfgOperation_uploadCfgWithJavaNetInetAddress_withPIRM_Cfg_withPIGC_DialogController_(JavaNetInetAddress *deviceAddress, PIRM_Cfg *cfg, id<PIGC_DialogController> dlgCtrl) {
  PIRCE_SaveCfgOperation_initialize();
  JavaLangInteger *version_ = [((PIRM_Cfg *) nil_chk(cfg)) getVersion];
  if (version_ == nil) {
    [cfg setVersionWithJavaLangInteger:JavaLangInteger_valueOfWithInt_(1)];
  }
  else {
    [cfg setVersionWithJavaLangInteger:JavaLangInteger_valueOfWithInt_([version_ intValue] + 1)];
  }
  PIMX_XmlWriter *xmlWriter = new_PIMX_XmlWriter_initWithPISYSB_Storage_(nil);
  JavaLangStringBuilder *stringBuffer = [xmlWriter writeXMLWithPIMR_Model:cfg withPIQ_QName:JreLoadStatic(PIRM_PiRailFactory, ID_CFG) withBoolean:true];
  JavaUtilProperties *params = new_JavaUtilProperties_init();
  (void) [params putWithId:@"filename" withId:PIRO_ConfigLoader_CFG_XML];
  (void) [params putWithId:@"addr" withId:deviceAddress];
  @try {
    jint rc = PIRO_ConfigLoader_doUploadWithJavaNetInetAddress_withJavaLangCharSequence_withNSString_(deviceAddress, stringBuffer, PIRO_ConfigLoader_CFG_XML);
    if ((rc >= 200) && (rc < 300)) {
      PICL_Logger_infoWithNSString_(JreStrcat("$$$@", @"Successfully updated ", PIRO_ConfigLoader_CFG_XML, @" on ", deviceAddress));
      return true;
    }
    else {
      PICL_Logger_errorWithNSString_(JreStrcat("$$$@$I", @"Error updating ", PIRO_ConfigLoader_CFG_XML, @" on ", deviceAddress, @", responseCode=", rc));
      NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_H" withNSString:nil withJavaUtilProperties:nil];
      NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_TXT" withNSString:nil withJavaUtilProperties:params];
      [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:JreStrcat("$$I", text, @"\nHTTP rc=", rc)];
    }
  }
  @catch (JavaLangException *e) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(@"Error while upload", e);
    NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_H" withNSString:nil withJavaUtilProperties:nil];
    NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_TXT" withNSString:nil withJavaUtilProperties:params];
    [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:JreStrcat("$$$", text, @"\nmsg=", [e getLocalizedMessage])];
  }
  return false;
}

jboolean PIRCE_SaveCfgOperation_uploadIoCfgWithPIRR_RailDevice_withPIRM_IoCfg_withPIGC_DialogController_(PIRR_RailDevice *railDevice, PIRM_IoCfg *ioCfg, id<PIGC_DialogController> dlgCtrl) {
  PIRCE_SaveCfgOperation_initialize();
  PIMX_XmlWriter *xmlWriter = new_PIMX_XmlWriter_initWithPISYSB_Storage_(nil);
  JavaLangStringBuilder *stringBuffer = [xmlWriter writeXMLWithPIMR_Model:ioCfg withPIQ_QName:JreLoadStatic(PIRM_PiRailFactory, ID_IOCFG) withBoolean:true];
  JavaNetInetAddress *deviceAddress = [((PIRR_RailDeviceAddress *) nil_chk([((PIRR_RailDevice *) nil_chk(railDevice)) getAddress])) getInetAddress];
  JavaUtilProperties *params = new_JavaUtilProperties_init();
  (void) [params putWithId:@"filename" withId:PIRO_ConfigLoader_IO_CFG_XML];
  (void) [params putWithId:@"addr" withId:deviceAddress];
  @try {
    jint rc = PIRO_ConfigLoader_doUploadWithJavaNetInetAddress_withJavaLangCharSequence_withNSString_(deviceAddress, stringBuffer, PIRO_ConfigLoader_IO_CFG_XML);
    if ((rc >= 200) && (rc < 300)) {
      PICL_Logger_infoWithNSString_(JreStrcat("$$$@", @"Successfully updated ", PIRO_ConfigLoader_IO_CFG_XML, @" on ", deviceAddress));
      [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) closeWithBoolean:true];
      return true;
    }
    else {
      PICL_Logger_errorWithNSString_(JreStrcat("$$$@$I", @"Error updating ", PIRO_ConfigLoader_IO_CFG_XML, @" on ", deviceAddress, @", responseCode=", rc));
      NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_H" withNSString:nil withJavaUtilProperties:nil];
      NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_TXT" withNSString:nil withJavaUtilProperties:params];
      [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:JreStrcat("$$I", text, @"\nHTTP rc=", rc)];
      return false;
    }
  }
  @catch (JavaLangException *e) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$$$@", @"Error updating ", PIRO_ConfigLoader_IO_CFG_XML, @" on ", railDevice), e);
    NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_H" withNSString:nil withJavaUtilProperties:nil];
    NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_TXT" withNSString:nil withJavaUtilProperties:params];
    [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:JreStrcat("$$$", text, @"\nmsg=", [e getLocalizedMessage])];
    return false;
  }
}

jboolean PIRCE_SaveCfgOperation_uploadTrackCfgWithJavaNetInetAddress_withDePidataRailTrackTrackCfg_withPIGC_DialogController_(JavaNetInetAddress *deviceAddress, DePidataRailTrackTrackCfg *trackCfg, id<PIGC_DialogController> dlgCtrl) {
  PIRCE_SaveCfgOperation_initialize();
  JavaLangInteger *version_ = [((DePidataRailTrackTrackCfg *) nil_chk(trackCfg)) getVersion];
  if (version_ == nil) {
    [trackCfg setVersionWithJavaLangInteger:JavaLangInteger_valueOfWithInt_(1)];
  }
  else {
    [trackCfg setVersionWithJavaLangInteger:JavaLangInteger_valueOfWithInt_([version_ intValue] + 1)];
  }
  PIMX_XmlWriter *xmlWriter = new_PIMX_XmlWriter_initWithPISYSB_Storage_(nil);
  JavaLangStringBuilder *stringBuffer = [xmlWriter writeXMLWithPIMR_Model:trackCfg withPIQ_QName:JreLoadStatic(DePidataRailTrackPiRailTrackFactory, ID_TRACKCFG) withBoolean:true];
  JavaUtilProperties *params = new_JavaUtilProperties_init();
  (void) [params putWithId:@"filename" withId:PIRO_ConfigLoader_TRACK_CFG_XML];
  (void) [params putWithId:@"addr" withId:deviceAddress];
  @try {
    jint rc = PIRO_ConfigLoader_doUploadWithJavaNetInetAddress_withJavaLangCharSequence_withNSString_(deviceAddress, stringBuffer, PIRO_ConfigLoader_TRACK_CFG_XML);
    if ((rc >= 200) && (rc < 300)) {
      PICL_Logger_infoWithNSString_(JreStrcat("$$$@", @"Successfully updated ", PIRO_ConfigLoader_TRACK_CFG_XML, @" on ", deviceAddress));
      return true;
    }
    else {
      PICL_Logger_errorWithNSString_(JreStrcat("$$$@$I", @"Error updating ", PIRO_ConfigLoader_TRACK_CFG_XML, @" on ", deviceAddress, @", responseCode=", rc));
      NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_H" withNSString:nil withJavaUtilProperties:nil];
      NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_TXT" withNSString:nil withJavaUtilProperties:params];
      [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:JreStrcat("$$I", text, @"\nHTTP rc=", rc)];
      return false;
    }
  }
  @catch (JavaLangException *e) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$$$@", @"Error updating ", PIRO_ConfigLoader_TRACK_CFG_XML, @" on ", deviceAddress), e);
    NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_H" withNSString:nil withJavaUtilProperties:nil];
    NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_TXT" withNSString:nil withJavaUtilProperties:params];
    [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:JreStrcat("$$$", text, @"\nmsg=", [e getLocalizedMessage])];
    return false;
  }
}

jboolean PIRCE_SaveCfgOperation_uploadDepotWithJavaNetInetAddress_withDePidataRailTrackDepot_withPIGC_DialogController_(JavaNetInetAddress *deviceAddress, DePidataRailTrackDepot *depot, id<PIGC_DialogController> dlgCtrl) {
  PIRCE_SaveCfgOperation_initialize();
  PIMX_XmlWriter *xmlWriter = new_PIMX_XmlWriter_initWithPISYSB_Storage_(nil);
  JavaLangStringBuilder *stringBuffer = [xmlWriter writeXMLWithPIMR_Model:depot withPIQ_QName:JreLoadStatic(DePidataRailTrackPiRailTrackFactory, ID_DEPOT) withBoolean:true];
  JavaUtilProperties *params = new_JavaUtilProperties_init();
  (void) [params putWithId:@"filename" withId:PIRO_ConfigLoader_DEPOT_XML];
  (void) [params putWithId:@"addr" withId:deviceAddress];
  @try {
    jint rc = PIRO_ConfigLoader_doUploadWithJavaNetInetAddress_withJavaLangCharSequence_withNSString_(deviceAddress, stringBuffer, PIRO_ConfigLoader_DEPOT_XML);
    if ((rc >= 200) && (rc < 300)) {
      PICL_Logger_infoWithNSString_(JreStrcat("$$$@", @"Successfully updated ", PIRO_ConfigLoader_DEPOT_XML, @" on ", deviceAddress));
      return true;
    }
    else {
      PICL_Logger_errorWithNSString_(JreStrcat("$$$@$I", @"Error updating ", PIRO_ConfigLoader_DEPOT_XML, @" on ", deviceAddress, @", responseCode=", rc));
      NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_H" withNSString:nil withJavaUtilProperties:nil];
      NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_TXT" withNSString:nil withJavaUtilProperties:params];
      [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:JreStrcat("$$I", text, @"\nHTTP rc=", rc)];
      return false;
    }
  }
  @catch (JavaIoIOException *e) {
    PICL_Logger_errorWithNSString_withJavaLangThrowable_(JreStrcat("$$$@", @"Error updating ", PIRO_ConfigLoader_DEPOT_XML, @" on ", deviceAddress), e);
    NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_H" withNSString:nil withJavaUtilProperties:nil];
    NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"saveCfgOperation_Error_TXT" withNSString:nil withJavaUtilProperties:params];
    [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:JreStrcat("$$$", text, @"\nmsg=", [e getLocalizedMessage])];
    return false;
  }
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCE_SaveCfgOperation)

J2OBJC_NAME_MAPPING(PIRCE_SaveCfgOperation, "de.pidata.rail.client.editcfg", "PIRCE_")
