//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editcfg/ReplaceIoCfg.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditcfgReplaceIoCfg")
#ifdef RESTRICT_DePidataRailClientEditcfgReplaceIoCfg
#define INCLUDE_ALL_DePidataRailClientEditcfgReplaceIoCfg 0
#else
#define INCLUDE_ALL_DePidataRailClientEditcfgReplaceIoCfg 1
#endif
#undef RESTRICT_DePidataRailClientEditcfgReplaceIoCfg

#if !defined (PIRCE_ReplaceIoCfg_) && (INCLUDE_ALL_DePidataRailClientEditcfgReplaceIoCfg || defined(INCLUDE_PIRCE_ReplaceIoCfg))
#define PIRCE_ReplaceIoCfg_

#define RESTRICT_DePidataRailClientEditcfgSaveCfgOperation 1
#define INCLUDE_PIRCE_SaveCfgOperation 1
#include "de/pidata/rail/client/editcfg/SaveCfgOperation.h"

@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIGC_DialogController;
@protocol PIGC_DialogControllerDelegate;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface PIRCE_ReplaceIoCfg : PIRCE_SaveCfgOperation

#pragma mark Public

- (instancetype)init;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)parentDlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

#pragma mark Protected

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(id<PIGC_DialogControllerDelegate>)delegate
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCE_ReplaceIoCfg)

inline NSString *PIRCE_ReplaceIoCfg_get_TITLE_IO_CFG_HOCHLADEN(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT NSString *PIRCE_ReplaceIoCfg_TITLE_IO_CFG_HOCHLADEN;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRCE_ReplaceIoCfg, TITLE_IO_CFG_HOCHLADEN, NSString *)

FOUNDATION_EXPORT void PIRCE_ReplaceIoCfg_init(PIRCE_ReplaceIoCfg *self);

FOUNDATION_EXPORT PIRCE_ReplaceIoCfg *new_PIRCE_ReplaceIoCfg_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCE_ReplaceIoCfg *create_PIRCE_ReplaceIoCfg_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCE_ReplaceIoCfg)

@compatibility_alias DePidataRailClientEditcfgReplaceIoCfg PIRCE_ReplaceIoCfg;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditcfgReplaceIoCfg")
