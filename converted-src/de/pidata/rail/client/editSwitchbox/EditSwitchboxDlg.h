//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editSwitchbox/EditSwitchboxDlg.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditSwitchboxEditSwitchboxDlg")
#ifdef RESTRICT_DePidataRailClientEditSwitchboxEditSwitchboxDlg
#define INCLUDE_ALL_DePidataRailClientEditSwitchboxEditSwitchboxDlg 0
#else
#define INCLUDE_ALL_DePidataRailClientEditSwitchboxEditSwitchboxDlg 1
#endif
#undef RESTRICT_DePidataRailClientEditSwitchboxEditSwitchboxDlg

#if !defined (PIRCB_EditSwitchboxDlg_) && (INCLUDE_ALL_DePidataRailClientEditSwitchboxEditSwitchboxDlg || defined(INCLUDE_PIRCB_EditSwitchboxDlg))
#define PIRCB_EditSwitchboxDlg_

#define RESTRICT_DePidataGuiControllerBaseGuiOperation 1
#define INCLUDE_PIGC_GuiOperation 1
#include "de/pidata/gui/controller/base/GuiOperation.h"

@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIMR_Model;

@interface PIRCB_EditSwitchboxDlg : PIGC_GuiOperation

#pragma mark Public

- (instancetype)init;

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCB_EditSwitchboxDlg)

FOUNDATION_EXPORT void PIRCB_EditSwitchboxDlg_init(PIRCB_EditSwitchboxDlg *self);

FOUNDATION_EXPORT PIRCB_EditSwitchboxDlg *new_PIRCB_EditSwitchboxDlg_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCB_EditSwitchboxDlg *create_PIRCB_EditSwitchboxDlg_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCB_EditSwitchboxDlg)

@compatibility_alias DePidataRailClientEditSwitchboxEditSwitchboxDlg PIRCB_EditSwitchboxDlg;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditSwitchboxEditSwitchboxDlg")
