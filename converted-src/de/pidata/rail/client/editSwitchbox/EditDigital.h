//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editSwitchbox/EditDigital.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditSwitchboxEditDigital")
#ifdef RESTRICT_DePidataRailClientEditSwitchboxEditDigital
#define INCLUDE_ALL_DePidataRailClientEditSwitchboxEditDigital 0
#else
#define INCLUDE_ALL_DePidataRailClientEditSwitchboxEditDigital 1
#endif
#undef RESTRICT_DePidataRailClientEditSwitchboxEditDigital

#if !defined (PIRCB_EditDigital_) && (INCLUDE_ALL_DePidataRailClientEditSwitchboxEditDigital || defined(INCLUDE_PIRCB_EditDigital))
#define PIRCB_EditDigital_

#define RESTRICT_DePidataGuiControllerBaseGuiDelegateOperation 1
#define INCLUDE_PIGC_GuiDelegateOperation 1
#include "de/pidata/gui/controller/base/GuiDelegateOperation.h"

@class PIQ_QName;
@class PIRCB_EditSwitchboxDelegate;
@protocol PIGC_Controller;
@protocol PIMR_Model;

@interface PIRCB_EditDigital : PIGC_GuiDelegateOperation

#pragma mark Public

- (instancetype)init;

#pragma mark Protected

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(PIRCB_EditSwitchboxDelegate *)delegate
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCB_EditDigital)

FOUNDATION_EXPORT void PIRCB_EditDigital_init(PIRCB_EditDigital *self);

FOUNDATION_EXPORT PIRCB_EditDigital *new_PIRCB_EditDigital_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCB_EditDigital *create_PIRCB_EditDigital_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCB_EditDigital)

@compatibility_alias DePidataRailClientEditSwitchboxEditDigital PIRCB_EditDigital;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditSwitchboxEditDigital")
