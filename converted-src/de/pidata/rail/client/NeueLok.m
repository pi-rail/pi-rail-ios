//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/NeueLok.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/GuiOperation.h"
#include "de/pidata/gui/controller/base/QuestionBoxResult.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/NeueLok.h"
#include "de/pidata/service/base/ParameterList.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/NeueLok must be compiled with ARC (-fobjc-arc)"
#endif

inline NSString *PIRC_NeueLok_get_AKTUALISIEREN(void);
static NSString *PIRC_NeueLok_AKTUALISIEREN = @"Aktualisieren";
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRC_NeueLok, AKTUALISIEREN, NSString *)

@implementation PIRC_NeueLok

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRC_NeueLok_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  [((id<PIGC_DialogController>) nil_chk([((id<PIGC_Controller>) nil_chk(sourceCtrl)) getDialogController])) showQuestionWithNSString:PIRC_NeueLok_AKTUALISIEREN withNSString:@"Wollen Sie die Liste aktualisieren oder einen neuen Wireless Access Point ausw\u00e4hlen?" withNSString:@"Access Point" withNSString:PIRC_NeueLok_AKTUALISIEREN withNSString:@"Abbruch"];
}

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)parentDlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList {
  if ([((NSString *) nil_chk([((PIGC_QuestionBoxResult *) nil_chk(((PIGC_QuestionBoxResult *) cast_chk(resultList, [PIGC_QuestionBoxResult class])))) getTitle])) isEqual:PIRC_NeueLok_AKTUALISIEREN]) {
    PIQ_QName *selectedButton = [((PIGC_QuestionBoxResult *) nil_chk(((PIGC_QuestionBoxResult *) cast_chk(resultList, [PIGC_QuestionBoxResult class])))) getSelectedButton];
    if ((JreObjectEqualsEquals(selectedButton, JreLoadStatic(PIGC_QuestionBoxResult, YES_OK)))) {
      PICL_Logger_infoWithNSString_(@"YES: Neuer Access Point!");
    }
    if ((JreObjectEqualsEquals(selectedButton, JreLoadStatic(PIGC_QuestionBoxResult, NO)))) {
      PICL_Logger_infoWithNSString_(@"No: Aktualisieren!");
    }
    if ((JreObjectEqualsEquals(selectedButton, JreLoadStatic(PIGC_QuestionBoxResult, CANCEL)))) {
      PICL_Logger_infoWithNSString_(@"Cancel: Abbruch!");
    }
    else {
      [super dialogClosedWithPIGC_DialogController:parentDlgCtrl withBoolean:resultOK withPISB_ParameterList:resultList];
    }
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 0, 1, 2, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, 5, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(executeWithPIQ_QName:withPIGC_Controller:withPIMR_Model:);
  methods[2].selector = @selector(dialogClosedWithPIGC_DialogController:withBoolean:withPISB_ParameterList:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "AKTUALISIEREN", "LNSString;", .constantValue.asLong = 0, 0x1a, -1, 6, -1, -1 },
  };
  static const void *ptrTable[] = { "execute", "LPIQ_QName;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;", "dialogClosed", "LPIGC_DialogController;ZLPISB_ParameterList;", "LJavaLangException;", &PIRC_NeueLok_AKTUALISIEREN };
  static const J2ObjcClassInfo _PIRC_NeueLok = { "NeueLok", "de.pidata.rail.client", ptrTable, methods, fields, 7, 0x1, 3, 1, -1, -1, -1, -1, -1 };
  return &_PIRC_NeueLok;
}

@end

void PIRC_NeueLok_init(PIRC_NeueLok *self) {
  PIGC_GuiOperation_init(self);
}

PIRC_NeueLok *new_PIRC_NeueLok_init() {
  J2OBJC_NEW_IMPL(PIRC_NeueLok, init)
}

PIRC_NeueLok *create_PIRC_NeueLok_init() {
  J2OBJC_CREATE_IMPL(PIRC_NeueLok, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRC_NeueLok)

J2OBJC_NAME_MAPPING(PIRC_NeueLok, "de.pidata.rail.client", "PIRC_")
