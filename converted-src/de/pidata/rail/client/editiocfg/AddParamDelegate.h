//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editiocfg/AddParamDelegate.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditiocfgAddParamDelegate")
#ifdef RESTRICT_DePidataRailClientEditiocfgAddParamDelegate
#define INCLUDE_ALL_DePidataRailClientEditiocfgAddParamDelegate 0
#else
#define INCLUDE_ALL_DePidataRailClientEditiocfgAddParamDelegate 1
#endif
#undef RESTRICT_DePidataRailClientEditiocfgAddParamDelegate

#if !defined (PIRCO_AddParamDelegate_) && (INCLUDE_ALL_DePidataRailClientEditiocfgAddParamDelegate || defined(INCLUDE_PIRCO_AddParamDelegate))
#define PIRCO_AddParamDelegate_

#define RESTRICT_DePidataGuiControllerBaseDialogControllerDelegate 1
#define INCLUDE_PIGC_DialogControllerDelegate 1
#include "de/pidata/gui/controller/base/DialogControllerDelegate.h"

#define RESTRICT_DePidataModelsTreeEventListener 1
#define INCLUDE_PIMR_EventListener 1
#include "de/pidata/models/tree/EventListener.h"

#define RESTRICT_DePidataGuiControllerBaseFlagListener 1
#define INCLUDE_PIGC_FlagListener 1
#include "de/pidata/gui/controller/base/FlagListener.h"

#define RESTRICT_DePidataGuiControllerBaseDialogValidator 1
#define INCLUDE_PIGC_DialogValidator 1
#include "de/pidata/gui/controller/base/DialogValidator.h"

#define RESTRICT_DePidataGuiControllerBaseTextFieldListener 1
#define INCLUDE_PIGC_TextFieldListener 1
#include "de/pidata/gui/controller/base/TextFieldListener.h"

@class JavaLangBoolean;
@class PIGC_FlagController;
@class PIGC_ModuleGroup;
@class PIGC_TextController;
@class PIMR_EventSender;
@class PIQ_QName;
@class PIRCO_ParameterPickerParamList;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface PIRCO_AddParamDelegate : NSObject < PIGC_DialogControllerDelegate, PIMR_EventListener, PIGC_FlagListener, PIGC_DialogValidator, PIGC_TextFieldListener >

#pragma mark Public

- (instancetype)init;

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

- (PIRCO_ParameterPickerParamList *)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                               withBoolean:(jboolean)ok;

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)eventOccuredWithPIMR_EventSender:(PIMR_EventSender *)eventSender
                                 withInt:(jint)eventID
                                  withId:(id)source
                           withPIQ_QName:(PIQ_QName *)modelID
                                  withId:(id)oldValue
                                  withId:(id)newValue;

- (void)flagChangedWithPIGC_FlagController:(PIGC_FlagController *)source
                       withJavaLangBoolean:(JavaLangBoolean *)newValue;

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(id<PISB_ParameterList>)parameterList OBJC_METHOD_FAMILY_NONE;

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup;

- (void)textChangedWithPIGC_TextController:(PIGC_TextController *)source
                              withNSString:(NSString *)oldValue
                              withNSString:(NSString *)newValue;

- (jboolean)validateWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCO_AddParamDelegate)

inline NSString *PIRCO_AddParamDelegate_get_SIFA_TIME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT NSString *PIRCO_AddParamDelegate_SIFA_TIME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRCO_AddParamDelegate, SIFA_TIME, NSString *)

inline NSString *PIRCO_AddParamDelegate_get_DCC_ADDR(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT NSString *PIRCO_AddParamDelegate_DCC_ADDR;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRCO_AddParamDelegate, DCC_ADDR, NSString *)

inline NSString *PIRCO_AddParamDelegate_get_DCC_F_PLUS(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT NSString *PIRCO_AddParamDelegate_DCC_F_PLUS;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRCO_AddParamDelegate, DCC_F_PLUS, NSString *)

inline NSString *PIRCO_AddParamDelegate_get_DCC_F_MINUS(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT NSString *PIRCO_AddParamDelegate_DCC_F_MINUS;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRCO_AddParamDelegate, DCC_F_MINUS, NSString *)

inline NSString *PIRCO_AddParamDelegate_get_DCC_REGEX(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT NSString *PIRCO_AddParamDelegate_DCC_REGEX;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRCO_AddParamDelegate, DCC_REGEX, NSString *)

FOUNDATION_EXPORT void PIRCO_AddParamDelegate_init(PIRCO_AddParamDelegate *self);

FOUNDATION_EXPORT PIRCO_AddParamDelegate *new_PIRCO_AddParamDelegate_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCO_AddParamDelegate *create_PIRCO_AddParamDelegate_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCO_AddParamDelegate)

@compatibility_alias DePidataRailClientEditiocfgAddParamDelegate PIRCO_AddParamDelegate;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditiocfgAddParamDelegate")
