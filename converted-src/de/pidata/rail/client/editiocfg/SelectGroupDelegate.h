//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editiocfg/SelectGroupDelegate.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditiocfgSelectGroupDelegate")
#ifdef RESTRICT_DePidataRailClientEditiocfgSelectGroupDelegate
#define INCLUDE_ALL_DePidataRailClientEditiocfgSelectGroupDelegate 0
#else
#define INCLUDE_ALL_DePidataRailClientEditiocfgSelectGroupDelegate 1
#endif
#undef RESTRICT_DePidataRailClientEditiocfgSelectGroupDelegate

#if !defined (PIRCO_SelectGroupDelegate_) && (INCLUDE_ALL_DePidataRailClientEditiocfgSelectGroupDelegate || defined(INCLUDE_PIRCO_SelectGroupDelegate))
#define PIRCO_SelectGroupDelegate_

#define RESTRICT_DePidataGuiControllerBaseDialogControllerDelegate 1
#define INCLUDE_PIGC_DialogControllerDelegate 1
#include "de/pidata/gui/controller/base/DialogControllerDelegate.h"

@class PIGC_ModuleGroup;
@class PIQ_QName;
@class PIRCO_SelectGroupParams;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface PIRCO_SelectGroupDelegate : NSObject < PIGC_DialogControllerDelegate >

#pragma mark Public

- (instancetype)init;

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

- (PIRCO_SelectGroupParams *)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                        withBoolean:(jboolean)ok;

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(PIRCO_SelectGroupParams *)parameterList OBJC_METHOD_FAMILY_NONE;

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup;

@end

J2OBJC_STATIC_INIT(PIRCO_SelectGroupDelegate)

inline PIQ_QName *PIRCO_SelectGroupDelegate_get_GROUPS_LIST(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRCO_SelectGroupDelegate_GROUPS_LIST;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRCO_SelectGroupDelegate, GROUPS_LIST, PIQ_QName *)

FOUNDATION_EXPORT void PIRCO_SelectGroupDelegate_init(PIRCO_SelectGroupDelegate *self);

FOUNDATION_EXPORT PIRCO_SelectGroupDelegate *new_PIRCO_SelectGroupDelegate_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCO_SelectGroupDelegate *create_PIRCO_SelectGroupDelegate_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCO_SelectGroupDelegate)

@compatibility_alias DePidataRailClientEditiocfgSelectGroupDelegate PIRCO_SelectGroupDelegate;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditiocfgSelectGroupDelegate")
