//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editiocfg/DeleteItemCfg.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditiocfgDeleteItemCfg")
#ifdef RESTRICT_DePidataRailClientEditiocfgDeleteItemCfg
#define INCLUDE_ALL_DePidataRailClientEditiocfgDeleteItemCfg 0
#else
#define INCLUDE_ALL_DePidataRailClientEditiocfgDeleteItemCfg 1
#endif
#undef RESTRICT_DePidataRailClientEditiocfgDeleteItemCfg

#if !defined (PIRCO_DeleteItemCfg_) && (INCLUDE_ALL_DePidataRailClientEditiocfgDeleteItemCfg || defined(INCLUDE_PIRCO_DeleteItemCfg))
#define PIRCO_DeleteItemCfg_

#define RESTRICT_DePidataGuiControllerBaseGuiDelegateOperation 1
#define INCLUDE_PIGC_GuiDelegateOperation 1
#include "de/pidata/gui/controller/base/GuiDelegateOperation.h"

@class PIQ_QName;
@class PIRCO_EditCfgDelegate;
@protocol PIGC_Controller;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface PIRCO_DeleteItemCfg : PIGC_GuiDelegateOperation

#pragma mark Public

- (instancetype)init;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)parentDlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

#pragma mark Protected

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(PIRCO_EditCfgDelegate *)delegate
         withPIGC_Controller:(id<PIGC_Controller>)source
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCO_DeleteItemCfg)

FOUNDATION_EXPORT void PIRCO_DeleteItemCfg_init(PIRCO_DeleteItemCfg *self);

FOUNDATION_EXPORT PIRCO_DeleteItemCfg *new_PIRCO_DeleteItemCfg_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCO_DeleteItemCfg *create_PIRCO_DeleteItemCfg_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCO_DeleteItemCfg)

@compatibility_alias DePidataRailClientEditiocfgDeleteItemCfg PIRCO_DeleteItemCfg;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditiocfgDeleteItemCfg")
