//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editiocfg/AddParamDelegate.java
//

#include "IOSClass.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/GuiBuilder.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/DialogController.h"
#include "de/pidata/gui/controller/base/FlagController.h"
#include "de/pidata/gui/controller/base/ListController.h"
#include "de/pidata/gui/controller/base/ModuleGroup.h"
#include "de/pidata/gui/controller/base/TextController.h"
#include "de/pidata/models/tree/EventSender.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/editiocfg/AddParamDelegate.h"
#include "de/pidata/rail/client/editiocfg/ParameterPickerParamList.h"
#include "de/pidata/rail/model/Param.h"
#include "de/pidata/rail/railway/DefaultParameter.h"
#include "de/pidata/rail/railway/ParameterSelection.h"
#include "de/pidata/service/base/ParameterList.h"
#include "de/pidata/string/Helper.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/lang/Boolean.h"
#include "java/lang/Integer.h"
#include "java/lang/NumberFormatException.h"
#include "java/lang/StringBuilder.h"
#include "java/lang/System.h"
#include "java/util/Properties.h"
#include "java/util/regex/Matcher.h"
#include "java/util/regex/Pattern.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/editiocfg/AddParamDelegate must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRCO_AddParamDelegate () {
 @public
  NSString *customParamName_;
  id<PIGC_DialogController> dlgCtrl_;
  id<PIGC_ListController> paramListCtrl_;
  PIRR_DefaultParameter *customParameter_;
  PIRR_ParameterSelection *parameterSelection_;
  PIGC_FlagController *textFlagCtrl_;
  PIGC_FlagController *numberFlagCtrl_;
  PIGC_TextController *paramNameCtrl_;
  PIGC_TextController *paramValueCtrl_;
}

@end

J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, customParamName_, NSString *)
J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, dlgCtrl_, id<PIGC_DialogController>)
J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, paramListCtrl_, id<PIGC_ListController>)
J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, customParameter_, PIRR_DefaultParameter *)
J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, parameterSelection_, PIRR_ParameterSelection *)
J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, textFlagCtrl_, PIGC_FlagController *)
J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, numberFlagCtrl_, PIGC_FlagController *)
J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, paramNameCtrl_, PIGC_TextController *)
J2OBJC_FIELD_SETTER(PIRCO_AddParamDelegate, paramValueCtrl_, PIGC_TextController *)

NSString *PIRCO_AddParamDelegate_SIFA_TIME = @"sifaTime";
NSString *PIRCO_AddParamDelegate_DCC_ADDR = @"dccAddr";
NSString *PIRCO_AddParamDelegate_DCC_F_PLUS = @"dccF#+";
NSString *PIRCO_AddParamDelegate_DCC_F_MINUS = @"dccF#-";
NSString *PIRCO_AddParamDelegate_DCC_REGEX = @"dccF([0-5]?[0-9]|6[0-3])(\\+|-)";

@implementation PIRCO_AddParamDelegate

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCO_AddParamDelegate_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(id<PISB_ParameterList>)parameterList {
  self->dlgCtrl_ = dlgCtrl;
  [((id<PIGC_DialogController>) nil_chk(self->dlgCtrl_)) setValidatorWithPIGC_DialogValidator:self];
  paramListCtrl_ = (id<PIGC_ListController>) cast_check([((id<PIGC_DialogController>) nil_chk(dlgCtrl)) getControllerWithPIQ_QName:[((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"param_list"]], PIGC_ListController_class_());
  [((id<PIGC_ListController>) nil_chk(paramListCtrl_)) addListenerWithPIMR_EventListener:self];
  textFlagCtrl_ = (PIGC_FlagController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:[JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"text_check"]], [PIGC_FlagController class]);
  [((PIGC_FlagController *) nil_chk(textFlagCtrl_)) addListenerWithPIGC_FlagListener:self];
  numberFlagCtrl_ = (PIGC_FlagController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:[JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"number_check"]], [PIGC_FlagController class]);
  [((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) addListenerWithPIGC_FlagListener:self];
  paramNameCtrl_ = (PIGC_TextController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:[JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"param_name_txt"]], [PIGC_TextController class]);
  [((PIGC_TextController *) nil_chk(paramNameCtrl_)) addListenerWithPIGC_TextFieldListener:self];
  paramValueCtrl_ = (PIGC_TextController *) cast_chk([dlgCtrl getControllerWithPIQ_QName:[JreLoadStatic(PIGB_GuiBuilder, NAMESPACE) getQNameWithNSString:@"param_value_txt"]], [PIGC_TextController class]);
  [((PIGC_TextController *) nil_chk(paramValueCtrl_)) addListenerWithPIGC_TextFieldListener:self];
  parameterSelection_ = new_PIRR_ParameterSelection_init();
  [parameterSelection_ addParameterWithPIRR_DefaultParameter:new_PIRR_DefaultParameter_initWithNSString_(PIRCO_AddParamDelegate_SIFA_TIME)];
  [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) addParameterWithPIRR_DefaultParameter:new_PIRR_DefaultParameter_initWithNSString_(PIRCO_AddParamDelegate_DCC_ADDR)];
  [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) addParameterWithPIRR_DefaultParameter:new_PIRR_DefaultParameter_initWithNSString_(PIRCO_AddParamDelegate_DCC_F_PLUS)];
  [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) addParameterWithPIRR_DefaultParameter:new_PIRR_DefaultParameter_initWithNSString_(PIRCO_AddParamDelegate_DCC_F_MINUS)];
  customParamName_ = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getGlossaryStringWithNSString:@"customParameter"];
  customParameter_ = new_PIRR_DefaultParameter_initWithNSString_(customParamName_);
  [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) addParameterWithPIRR_DefaultParameter:customParameter_];
  return parameterSelection_;
}

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
}

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  [((id<PIGC_ListController>) nil_chk(paramListCtrl_)) selectRowWithPIMR_Model:customParameter_];
}

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) closeWithBoolean:false];
}

- (PIRCO_ParameterPickerParamList *)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                               withBoolean:(jboolean)ok {
  if (ok) {
    return new_PIRCO_ParameterPickerParamList_initWithNSString_withNSString_withJavaLangBoolean_([((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) getParamName], [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) getParamValue], [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) getIsNumber]);
  }
  return nil;
}

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList {
}

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup {
}

- (void)eventOccuredWithPIMR_EventSender:(PIMR_EventSender *)eventSender
                                 withInt:(jint)eventID
                                  withId:(id)source
                           withPIQ_QName:(PIQ_QName *)modelID
                                  withId:(id)oldValue
                                  withId:(id)newValue {
  if (JreObjectEqualsEquals([((PIMR_EventSender *) nil_chk(eventSender)) getOwner], paramListCtrl_) && [newValue isKindOfClass:[PIRR_DefaultParameter class]]) {
    NSString *selectedParamName = [((PIRR_DefaultParameter *) nil_chk(((PIRR_DefaultParameter *) cast_chk(newValue, [PIRR_DefaultParameter class])))) getName];
    [((PIGC_TextController *) nil_chk(paramNameCtrl_)) setValueWithId:selectedParamName];
    if ([((NSString *) nil_chk(selectedParamName)) isEqual:PIRCO_AddParamDelegate_DCC_F_PLUS] || [selectedParamName isEqual:PIRCO_AddParamDelegate_DCC_F_MINUS]) {
      [((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) setValueWithId:JreLoadStatic(JavaLangBoolean, FALSE)];
      [((PIGC_FlagController *) nil_chk(textFlagCtrl_)) setValueWithId:JreLoadStatic(JavaLangBoolean, TRUE)];
    }
    else if ([selectedParamName isEqual:PIRCO_AddParamDelegate_DCC_ADDR]) {
      [((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) setValueWithId:JreLoadStatic(JavaLangBoolean, TRUE)];
      [((PIGC_FlagController *) nil_chk(textFlagCtrl_)) setValueWithId:JreLoadStatic(JavaLangBoolean, FALSE)];
    }
    else if ([selectedParamName isEqual:PIRCO_AddParamDelegate_SIFA_TIME]) {
      [((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) setValueWithId:JreLoadStatic(JavaLangBoolean, TRUE)];
      [((PIGC_FlagController *) nil_chk(textFlagCtrl_)) setValueWithId:JreLoadStatic(JavaLangBoolean, FALSE)];
    }
    else if ([selectedParamName isEqual:customParamName_]) {
      [((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) setValueWithId:JreLoadStatic(JavaLangBoolean, FALSE)];
      [((PIGC_FlagController *) nil_chk(textFlagCtrl_)) setValueWithId:JreLoadStatic(JavaLangBoolean, FALSE)];
    }
  }
}

- (void)flagChangedWithPIGC_FlagController:(PIGC_FlagController *)source
                       withJavaLangBoolean:(JavaLangBoolean *)newValue {
  [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) setIsNumberWithJavaLangBoolean:[((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) getBooleanValue]];
}

- (jboolean)validateWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl {
  JavaLangStringBuilder *messages = new_JavaLangStringBuilder_init();
  NSString *paramName = [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) getParamName];
  if (PICS_Helper_isNullOrEmptyWithId_(paramName)) {
    (void) [messages appendWithNSString:[((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamNoName_MSG" withNSString:nil withJavaUtilProperties:nil]];
    (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
  }
  else {
    NSString *msg = PIRM_Param_checkIDWithNSString_(paramName);
    if (msg == nil) {
      if ([((NSString *) nil_chk(paramName)) java_hasPrefix:@"dccF"]) {
        JavaUtilRegexPattern *pattern = JavaUtilRegexPattern_compileWithNSString_withInt_(PIRCO_AddParamDelegate_DCC_REGEX, JavaUtilRegexPattern_CASE_INSENSITIVE);
        JavaUtilRegexMatcher *matcher = [((JavaUtilRegexPattern *) nil_chk(pattern)) matcherWithJavaLangCharSequence:paramName];
        if (![((JavaUtilRegexMatcher *) nil_chk(matcher)) matches]) {
          (void) [messages appendWithNSString:[((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamDCCRange_MSG" withNSString:nil withJavaUtilProperties:nil]];
          (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
        }
        else {
          if (![((PIGC_FlagController *) nil_chk(textFlagCtrl_)) getBoolValue]) {
            JavaUtilProperties *params = new_JavaUtilProperties_init();
            (void) [params putWithId:@"name" withId:paramName];
            (void) [messages appendWithNSString:[((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamTextReq_MSG" withNSString:nil withJavaUtilProperties:params]];
            (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
          }
        }
      }
    }
    else {
      JavaUtilProperties *params = new_JavaUtilProperties_init();
      (void) [params putWithId:@"name" withId:paramName];
      NSString *text = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamInvalidName_TXT" withNSString:nil withJavaUtilProperties:params];
      (void) [messages appendWithNSString:JreStrcat("$$C$", @"- ", text, 0x000a, msg)];
      (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
    }
    if ([((NSString *) nil_chk(paramName)) isEqual:PIRCO_AddParamDelegate_DCC_ADDR] && ![((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) getBoolValue]) {
      JavaUtilProperties *params = new_JavaUtilProperties_init();
      (void) [params putWithId:@"name" withId:PIRCO_AddParamDelegate_DCC_ADDR];
      (void) [messages appendWithNSString:[((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamNumberReq_MSG" withNSString:nil withJavaUtilProperties:params]];
      (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
    }
    if ([paramName isEqual:PIRCO_AddParamDelegate_SIFA_TIME] && ![((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) getBoolValue]) {
      JavaUtilProperties *params = new_JavaUtilProperties_init();
      (void) [params putWithId:@"name" withId:PIRCO_AddParamDelegate_SIFA_TIME];
      (void) [messages appendWithNSString:[((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamNumberReq_MSG" withNSString:nil withJavaUtilProperties:params]];
      (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
    }
  }
  if (![((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) getBoolValue] && ![((PIGC_FlagController *) nil_chk(textFlagCtrl_)) getBoolValue]) {
    (void) [messages appendWithNSString:[((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamTextOrNum_MSG" withNSString:nil withJavaUtilProperties:nil]];
    (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
  }
  NSString *paramValue = [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) getParamValue];
  if (!([((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) getBoolValue] && [((PIGC_FlagController *) nil_chk(textFlagCtrl_)) getBoolValue]) && PICS_Helper_isNotNullAndNotEmptyWithId_(paramValue)) {
    if ([((PIGC_FlagController *) nil_chk(numberFlagCtrl_)) getBoolValue]) {
      @try {
        JavaLangInteger_parseIntWithNSString_(paramValue);
      }
      @catch (JavaLangNumberFormatException *nfe) {
        JavaUtilProperties *params = new_JavaUtilProperties_init();
        (void) [params putWithId:@"value" withId:paramValue];
        (void) [messages appendWithNSString:[((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamCheckNumberReq_MSG" withNSString:nil withJavaUtilProperties:params]];
        (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
      }
    }
    if ([((PIGC_FlagController *) nil_chk(textFlagCtrl_)) getBoolValue]) {
      if ([((NSString *) nil_chk(paramValue)) java_length] > 1) {
        JavaUtilProperties *params = new_JavaUtilProperties_init();
        (void) [params putWithId:@"value" withId:paramValue];
        (void) [messages appendWithNSString:[((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamCheckCharReq" withNSString:nil withJavaUtilProperties:params]];
        (void) [((JavaLangStringBuilder *) nil_chk([messages appendWithNSString:JavaLangSystem_lineSeparator()])) appendWithNSString:JavaLangSystem_lineSeparator()];
      }
    }
  }
  if ([messages java_length] > 0) {
    NSString *title = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"addParamDoComplete_H" withNSString:nil withJavaUtilProperties:nil];
    [((id<PIGC_DialogController>) nil_chk(dlgCtrl)) showMessageWithNSString:title withNSString:[messages description]];
    return false;
  }
  return true;
}

- (void)textChangedWithPIGC_TextController:(PIGC_TextController *)source
                              withNSString:(NSString *)oldValue
                              withNSString:(NSString *)newValue {
  if (JreObjectEqualsEquals(source, paramNameCtrl_)) {
    [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) setParamNameWithNSString:newValue];
  }
  if (JreObjectEqualsEquals(source, paramValueCtrl_)) {
    [((PIRR_ParameterSelection *) nil_chk(parameterSelection_)) setParamValueWithNSString:newValue];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_Model;", 0x1, 0, 1, 2, -1, -1, -1 },
    { NULL, "V", 0x1, 3, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 4, -1, -1, -1, -1 },
    { NULL, "LPIRCO_ParameterPickerParamList;", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 14, 15, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 16, 17, -1, -1, -1, -1 },
    { NULL, "Z", 0x1, 18, 4, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 19, 20, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initializeDialogWithPIGC_DialogController:withPISB_ParameterList:);
  methods[2].selector = @selector(dialogCreatedWithPIGC_DialogController:);
  methods[3].selector = @selector(dialogShowingWithPIGC_DialogController:);
  methods[4].selector = @selector(dialogBindingsInitializedWithPIGC_DialogController:);
  methods[5].selector = @selector(backPressedWithPIGC_DialogController:);
  methods[6].selector = @selector(dialogClosingWithPIGC_DialogController:withBoolean:);
  methods[7].selector = @selector(dialogClosedWithPIGC_DialogController:withBoolean:withPISB_ParameterList:);
  methods[8].selector = @selector(popupClosedWithPIGC_ModuleGroup:);
  methods[9].selector = @selector(eventOccuredWithPIMR_EventSender:withInt:withId:withPIQ_QName:withId:withId:);
  methods[10].selector = @selector(flagChangedWithPIGC_FlagController:withJavaLangBoolean:);
  methods[11].selector = @selector(validateWithPIGC_DialogController:);
  methods[12].selector = @selector(textChangedWithPIGC_TextController:withNSString:withNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "SIFA_TIME", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 21, -1, -1 },
    { "DCC_ADDR", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 22, -1, -1 },
    { "DCC_F_PLUS", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 23, -1, -1 },
    { "DCC_F_MINUS", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 24, -1, -1 },
    { "DCC_REGEX", "LNSString;", .constantValue.asLong = 0, 0x19, -1, 25, -1, -1 },
    { "customParamName_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "dlgCtrl_", "LPIGC_DialogController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "paramListCtrl_", "LPIGC_ListController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "customParameter_", "LPIRR_DefaultParameter;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "parameterSelection_", "LPIRR_ParameterSelection;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "textFlagCtrl_", "LPIGC_FlagController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "numberFlagCtrl_", "LPIGC_FlagController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "paramNameCtrl_", "LPIGC_TextController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "paramValueCtrl_", "LPIGC_TextController;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "initializeDialog", "LPIGC_DialogController;LPISB_ParameterList;", "LJavaLangException;", "dialogCreated", "LPIGC_DialogController;", "dialogShowing", "dialogBindingsInitialized", "backPressed", "dialogClosing", "LPIGC_DialogController;Z", "dialogClosed", "LPIGC_DialogController;ZLPISB_ParameterList;", "popupClosed", "LPIGC_ModuleGroup;", "eventOccured", "LPIMR_EventSender;ILNSObject;LPIQ_QName;LNSObject;LNSObject;", "flagChanged", "LPIGC_FlagController;LJavaLangBoolean;", "validate", "textChanged", "LPIGC_TextController;LNSString;LNSString;", &PIRCO_AddParamDelegate_SIFA_TIME, &PIRCO_AddParamDelegate_DCC_ADDR, &PIRCO_AddParamDelegate_DCC_F_PLUS, &PIRCO_AddParamDelegate_DCC_F_MINUS, &PIRCO_AddParamDelegate_DCC_REGEX, "Ljava/lang/Object;Lde/pidata/gui/controller/base/DialogControllerDelegate<Lde/pidata/service/base/ParameterList;Lde/pidata/rail/client/editiocfg/ParameterPickerParamList;>;Lde/pidata/models/tree/EventListener;Lde/pidata/gui/controller/base/FlagListener;Lde/pidata/gui/controller/base/DialogValidator;Lde/pidata/gui/controller/base/TextFieldListener;" };
  static const J2ObjcClassInfo _PIRCO_AddParamDelegate = { "AddParamDelegate", "de.pidata.rail.client.editiocfg", ptrTable, methods, fields, 7, 0x1, 13, 14, -1, -1, -1, 26, -1 };
  return &_PIRCO_AddParamDelegate;
}

@end

void PIRCO_AddParamDelegate_init(PIRCO_AddParamDelegate *self) {
  NSObject_init(self);
}

PIRCO_AddParamDelegate *new_PIRCO_AddParamDelegate_init() {
  J2OBJC_NEW_IMPL(PIRCO_AddParamDelegate, init)
}

PIRCO_AddParamDelegate *create_PIRCO_AddParamDelegate_init() {
  J2OBJC_CREATE_IMPL(PIRCO_AddParamDelegate, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCO_AddParamDelegate)

J2OBJC_NAME_MAPPING(PIRCO_AddParamDelegate, "de.pidata.rail.client.editiocfg", "PIRCO_")
