//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editiocfg/EditIoCfgUpload.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditiocfgEditIoCfgUpload")
#ifdef RESTRICT_DePidataRailClientEditiocfgEditIoCfgUpload
#define INCLUDE_ALL_DePidataRailClientEditiocfgEditIoCfgUpload 0
#else
#define INCLUDE_ALL_DePidataRailClientEditiocfgEditIoCfgUpload 1
#endif
#undef RESTRICT_DePidataRailClientEditiocfgEditIoCfgUpload

#if !defined (PIRCO_EditIoCfgUpload_) && (INCLUDE_ALL_DePidataRailClientEditiocfgEditIoCfgUpload || defined(INCLUDE_PIRCO_EditIoCfgUpload))
#define PIRCO_EditIoCfgUpload_

#define RESTRICT_DePidataRailClientEditcfgSaveCfgOperation 1
#define INCLUDE_PIRCE_SaveCfgOperation 1
#include "de/pidata/rail/client/editcfg/SaveCfgOperation.h"

@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIGC_DialogControllerDelegate;
@protocol PIMR_Model;

@interface PIRCO_EditIoCfgUpload : PIRCE_SaveCfgOperation

#pragma mark Public

- (instancetype)init;

#pragma mark Protected

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(id<PIGC_DialogControllerDelegate>)delegate
         withPIGC_Controller:(id<PIGC_Controller>)source
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRCO_EditIoCfgUpload)

FOUNDATION_EXPORT void PIRCO_EditIoCfgUpload_init(PIRCO_EditIoCfgUpload *self);

FOUNDATION_EXPORT PIRCO_EditIoCfgUpload *new_PIRCO_EditIoCfgUpload_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCO_EditIoCfgUpload *create_PIRCO_EditIoCfgUpload_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCO_EditIoCfgUpload)

@compatibility_alias DePidataRailClientEditiocfgEditIoCfgUpload PIRCO_EditIoCfgUpload;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditiocfgEditIoCfgUpload")
