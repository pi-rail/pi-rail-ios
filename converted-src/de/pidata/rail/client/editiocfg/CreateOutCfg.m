//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editiocfg/CreateOutCfg.java
//

#include "J2ObjC_source.h"
#include "de/pidata/gui/controller/base/Controller.h"
#include "de/pidata/gui/controller/base/GuiDelegateOperation.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/client/editiocfg/CreateOutCfg.h"
#include "de/pidata/rail/client/editiocfg/EditCfgDelegate.h"
#include "java/lang/RuntimeException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/client/editiocfg/CreateOutCfg must be compiled with ARC (-fobjc-arc)"
#endif

@implementation PIRCO_CreateOutCfg

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRCO_CreateOutCfg_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
withPIGC_DialogControllerDelegate:(PIRCO_EditCfgDelegate *)delegate
         withPIGC_Controller:(id<PIGC_Controller>)source
              withPIMR_Model:(id<PIMR_Model>)dataContext {
  @throw new_JavaLangRuntimeException_initWithNSString_(@"TODO");
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 0, 1, 2, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(executeWithPIQ_QName:withPIGC_DialogControllerDelegate:withPIGC_Controller:withPIMR_Model:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "execute", "LPIQ_QName;LPIRCO_EditCfgDelegate;LPIGC_Controller;LPIMR_Model;", "LPISB_ServiceException;", "Lde/pidata/gui/controller/base/GuiDelegateOperation<Lde/pidata/rail/client/editiocfg/EditCfgDelegate;>;" };
  static const J2ObjcClassInfo _PIRCO_CreateOutCfg = { "CreateOutCfg", "de.pidata.rail.client.editiocfg", ptrTable, methods, NULL, 7, 0x1, 2, 0, -1, -1, -1, 3, -1 };
  return &_PIRCO_CreateOutCfg;
}

@end

void PIRCO_CreateOutCfg_init(PIRCO_CreateOutCfg *self) {
  PIGC_GuiDelegateOperation_init(self);
}

PIRCO_CreateOutCfg *new_PIRCO_CreateOutCfg_init() {
  J2OBJC_NEW_IMPL(PIRCO_CreateOutCfg, init)
}

PIRCO_CreateOutCfg *create_PIRCO_CreateOutCfg_init() {
  J2OBJC_CREATE_IMPL(PIRCO_CreateOutCfg, init)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRCO_CreateOutCfg)

J2OBJC_NAME_MAPPING(PIRCO_CreateOutCfg, "de.pidata.rail.client.editiocfg", "PIRCO_")
