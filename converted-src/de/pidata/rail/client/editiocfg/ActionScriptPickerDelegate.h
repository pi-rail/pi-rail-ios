//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/rail/client/editiocfg/ActionScriptPickerDelegate.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailClientEditiocfgActionScriptPickerDelegate")
#ifdef RESTRICT_DePidataRailClientEditiocfgActionScriptPickerDelegate
#define INCLUDE_ALL_DePidataRailClientEditiocfgActionScriptPickerDelegate 0
#else
#define INCLUDE_ALL_DePidataRailClientEditiocfgActionScriptPickerDelegate 1
#endif
#undef RESTRICT_DePidataRailClientEditiocfgActionScriptPickerDelegate

#if !defined (PIRCO_ActionScriptPickerDelegate_) && (INCLUDE_ALL_DePidataRailClientEditiocfgActionScriptPickerDelegate || defined(INCLUDE_PIRCO_ActionScriptPickerDelegate))
#define PIRCO_ActionScriptPickerDelegate_

#define RESTRICT_DePidataGuiControllerBaseDialogControllerDelegate 1
#define INCLUDE_PIGC_DialogControllerDelegate 1
#include "de/pidata/gui/controller/base/DialogControllerDelegate.h"

@class PIGC_ModuleGroup;
@class PIRCO_ActionPickerParamList;
@protocol PIGC_DialogController;
@protocol PIMR_Model;
@protocol PISB_ParameterList;

@interface PIRCO_ActionScriptPickerDelegate : NSObject < PIGC_DialogControllerDelegate >

#pragma mark Public

- (instancetype)init;

- (void)backPressedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogBindingsInitializedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogClosedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                  withBoolean:(jboolean)resultOK
                       withPISB_ParameterList:(id<PISB_ParameterList>)resultList;

- (PIRCO_ActionPickerParamList *)dialogClosingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                                            withBoolean:(jboolean)ok;

- (void)dialogCreatedWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (void)dialogShowingWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl;

- (id<PIMR_Model>)initializeDialogWithPIGC_DialogController:(id<PIGC_DialogController>)dlgCtrl
                                     withPISB_ParameterList:(PIRCO_ActionPickerParamList *)parameterList OBJC_METHOD_FAMILY_NONE;

- (void)popupClosedWithPIGC_ModuleGroup:(PIGC_ModuleGroup *)oldModuleGroup;

@end

J2OBJC_STATIC_INIT(PIRCO_ActionScriptPickerDelegate)

FOUNDATION_EXPORT void PIRCO_ActionScriptPickerDelegate_init(PIRCO_ActionScriptPickerDelegate *self);

FOUNDATION_EXPORT PIRCO_ActionScriptPickerDelegate *new_PIRCO_ActionScriptPickerDelegate_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRCO_ActionScriptPickerDelegate *create_PIRCO_ActionScriptPickerDelegate_init(void);

J2OBJC_TYPE_LITERAL_HEADER(PIRCO_ActionScriptPickerDelegate)

@compatibility_alias DePidataRailClientEditiocfgActionScriptPickerDelegate PIRCO_ActionScriptPickerDelegate;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailClientEditiocfgActionScriptPickerDelegate")
