//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-trackcfg/src/de/pidata/rail/track/MeasureCfg.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/model/PiRailFactory.h"
#include "de/pidata/rail/track/MeasureCfg.h"
#include "de/pidata/rail/track/PiRailTrackFactory.h"
#include "de/pidata/string/Helper.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/track/MeasureCfg must be compiled with ARC (-fobjc-arc)"
#endif

@interface DePidataRailTrackMeasureCfg () {
 @public
  jint distance_;
}

@end

J2OBJC_INITIALIZED_DEFN(DePidataRailTrackMeasureCfg)

PIQ_Namespace *DePidataRailTrackMeasureCfg_NAMESPACE;
PIQ_QName *DePidataRailTrackMeasureCfg_ID_ENDMSGID;
PIQ_QName *DePidataRailTrackMeasureCfg_ID_NAME;
PIQ_QName *DePidataRailTrackMeasureCfg_ID_STARTMSGID;

@implementation DePidataRailTrackMeasureCfg

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  DePidataRailTrackMeasureCfg_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  DePidataRailTrackMeasureCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  DePidataRailTrackMeasureCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (NSString *)getName {
  return (NSString *) cast_chk([self getWithPIQ_QName:DePidataRailTrackMeasureCfg_ID_NAME], [NSString class]);
}

- (void)setNameWithNSString:(NSString *)name {
  [self setWithPIQ_QName:DePidataRailTrackMeasureCfg_ID_NAME withId:name];
}

- (PIQ_QName *)getStartMsgID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:DePidataRailTrackMeasureCfg_ID_STARTMSGID], [PIQ_QName class]);
}

- (void)setStartMsgIDWithPIQ_QName:(PIQ_QName *)startMsgID {
  [self setWithPIQ_QName:DePidataRailTrackMeasureCfg_ID_STARTMSGID withId:startMsgID];
}

- (PIQ_QName *)getEndMsgID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:DePidataRailTrackMeasureCfg_ID_ENDMSGID], [PIQ_QName class]);
}

- (void)setEndMsgIDWithPIQ_QName:(PIQ_QName *)endMsgID {
  [self setWithPIQ_QName:DePidataRailTrackMeasureCfg_ID_ENDMSGID withId:endMsgID];
}

- (void)setDistanceWithInt:(jint)distance {
  self->distance_ = distance;
}

- (jint)getDistance {
  return self->distance_;
}

- (jint)getStartMsgIndex {
  PIQ_QName *msg = [self getStartMsgID];
  if (msg != nil) {
    NSString *name = [msg getName];
    if ([((NSString *) nil_chk(name)) java_length] >= 3) {
      return [name charAtWithInt:2];
    }
  }
  return 0;
}

- (jint)getEndMsgIndex {
  PIQ_QName *msg = [self getEndMsgID];
  if (msg != nil) {
    NSString *name = [msg getName];
    if ([((NSString *) nil_chk(name)) java_length] >= 3) {
      return [name charAtWithInt:2];
    }
  }
  return 0;
}

+ (NSString *)checkIDWithNSString:(NSString *)value {
  return DePidataRailTrackMeasureCfg_checkIDWithNSString_(value);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, 3, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 11, 5, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getName);
  methods[4].selector = @selector(setNameWithNSString:);
  methods[5].selector = @selector(getStartMsgID);
  methods[6].selector = @selector(setStartMsgIDWithPIQ_QName:);
  methods[7].selector = @selector(getEndMsgID);
  methods[8].selector = @selector(setEndMsgIDWithPIQ_QName:);
  methods[9].selector = @selector(setDistanceWithInt:);
  methods[10].selector = @selector(getDistance);
  methods[11].selector = @selector(getStartMsgIndex);
  methods[12].selector = @selector(getEndMsgIndex);
  methods[13].selector = @selector(checkIDWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 12, -1, -1 },
    { "ID_ENDMSGID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 13, -1, -1 },
    { "ID_NAME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 14, -1, -1 },
    { "ID_STARTMSGID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 15, -1, -1 },
    { "distance_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setName", "LNSString;", "setStartMsgID", "LPIQ_QName;", "setEndMsgID", "setDistance", "I", "checkID", &DePidataRailTrackMeasureCfg_NAMESPACE, &DePidataRailTrackMeasureCfg_ID_ENDMSGID, &DePidataRailTrackMeasureCfg_ID_NAME, &DePidataRailTrackMeasureCfg_ID_STARTMSGID };
  static const J2ObjcClassInfo _DePidataRailTrackMeasureCfg = { "MeasureCfg", "de.pidata.rail.track", ptrTable, methods, fields, 7, 0x1, 14, 5, -1, -1, -1, -1, -1 };
  return &_DePidataRailTrackMeasureCfg;
}

+ (void)initialize {
  if (self == [DePidataRailTrackMeasureCfg class]) {
    DePidataRailTrackMeasureCfg_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/pi-rail-track.xsd");
    DePidataRailTrackMeasureCfg_ID_ENDMSGID = [((PIQ_Namespace *) nil_chk(DePidataRailTrackMeasureCfg_NAMESPACE)) getQNameWithNSString:@"endMsgID"];
    DePidataRailTrackMeasureCfg_ID_NAME = [DePidataRailTrackMeasureCfg_NAMESPACE getQNameWithNSString:@"name"];
    DePidataRailTrackMeasureCfg_ID_STARTMSGID = [DePidataRailTrackMeasureCfg_NAMESPACE getQNameWithNSString:@"startMsgID"];
    J2OBJC_SET_INITIALIZED(DePidataRailTrackMeasureCfg)
  }
}

@end

void DePidataRailTrackMeasureCfg_init(DePidataRailTrackMeasureCfg *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(DePidataRailTrackPiRailTrackFactory, MEASURECFG_TYPE), nil, nil, nil);
}

DePidataRailTrackMeasureCfg *new_DePidataRailTrackMeasureCfg_init() {
  J2OBJC_NEW_IMPL(DePidataRailTrackMeasureCfg, init)
}

DePidataRailTrackMeasureCfg *create_DePidataRailTrackMeasureCfg_init() {
  J2OBJC_CREATE_IMPL(DePidataRailTrackMeasureCfg, init)
}

void DePidataRailTrackMeasureCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(DePidataRailTrackMeasureCfg *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(DePidataRailTrackPiRailTrackFactory, MEASURECFG_TYPE), attributeNames, anyAttribs, childNames);
}

DePidataRailTrackMeasureCfg *new_DePidataRailTrackMeasureCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(DePidataRailTrackMeasureCfg, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

DePidataRailTrackMeasureCfg *create_DePidataRailTrackMeasureCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(DePidataRailTrackMeasureCfg, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void DePidataRailTrackMeasureCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(DePidataRailTrackMeasureCfg *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

DePidataRailTrackMeasureCfg *new_DePidataRailTrackMeasureCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(DePidataRailTrackMeasureCfg, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

DePidataRailTrackMeasureCfg *create_DePidataRailTrackMeasureCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(DePidataRailTrackMeasureCfg, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

NSString *DePidataRailTrackMeasureCfg_checkIDWithNSString_(NSString *value) {
  DePidataRailTrackMeasureCfg_initialize();
  if (PICS_Helper_isNullOrEmptyWithId_(value)) {
    return @"Ein Messstreckenname muss mindestens 1 Zeichen lang sein!";
  }
  else if ([((NSString *) nil_chk(value)) java_length] > PIRM_PiRailFactory_ID_MAX_LEN) {
    return JreStrcat("$I$", @"Ein Messstreckenname darf maximal ", PIRM_PiRailFactory_ID_MAX_LEN, @" Zeichen lang sein");
  }
  else {
    for (jint i = 0; i < [value java_length]; i++) {
      jchar ch = [value charAtWithInt:i];
      jboolean ok = ((ch >= '0' && ch <= '9')) || ((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z')) || (ch == '_') || (ch == '-');
      if (!ok) {
        return [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"invalidCharName" withNSString:nil withJavaUtilProperties:nil];
      }
    }
  }
  return nil;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(DePidataRailTrackMeasureCfg)
