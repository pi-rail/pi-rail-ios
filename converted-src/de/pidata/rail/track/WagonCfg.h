//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-trackcfg/src/de/pidata/rail/track/WagonCfg.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailTrackWagonCfg")
#ifdef RESTRICT_DePidataRailTrackWagonCfg
#define INCLUDE_ALL_DePidataRailTrackWagonCfg 0
#else
#define INCLUDE_ALL_DePidataRailTrackWagonCfg 1
#endif
#undef RESTRICT_DePidataRailTrackWagonCfg

#if !defined (DePidataRailTrackWagonCfg_) && (INCLUDE_ALL_DePidataRailTrackWagonCfg || defined(INCLUDE_DePidataRailTrackWagonCfg))
#define DePidataRailTrackWagonCfg_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class DePidataRailTrackDepot;
@class IOSObjectArray;
@class JavaLangInteger;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface DePidataRailTrackWagonCfg : PIMR_SequenceModel

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

+ (NSString *)checkIconNameWithNSString:(NSString *)value;

- (DePidataRailTrackDepot *)getDepot;

- (PIQ_QName *)getIcon;

- (PIQ_QName *)getId;

- (JavaLangInteger *)getLen;

- (NSString *)getName;

- (JavaLangInteger *)getTagPos;

- (void)setIconWithPIQ_QName:(PIQ_QName *)icon;

- (void)setLenWithJavaLangInteger:(JavaLangInteger *)len;

- (void)setNameWithNSString:(NSString *)name;

- (void)setTagPosWithJavaLangInteger:(JavaLangInteger *)tagPos;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(DePidataRailTrackWagonCfg)

inline PIQ_Namespace *DePidataRailTrackWagonCfg_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *DePidataRailTrackWagonCfg_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailTrackWagonCfg, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *DePidataRailTrackWagonCfg_get_ID_ICON(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailTrackWagonCfg_ID_ICON;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailTrackWagonCfg, ID_ICON, PIQ_QName *)

inline PIQ_QName *DePidataRailTrackWagonCfg_get_ID_ID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailTrackWagonCfg_ID_ID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailTrackWagonCfg, ID_ID, PIQ_QName *)

inline PIQ_QName *DePidataRailTrackWagonCfg_get_ID_LEN(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailTrackWagonCfg_ID_LEN;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailTrackWagonCfg, ID_LEN, PIQ_QName *)

inline PIQ_QName *DePidataRailTrackWagonCfg_get_ID_NAME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailTrackWagonCfg_ID_NAME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailTrackWagonCfg, ID_NAME, PIQ_QName *)

inline PIQ_QName *DePidataRailTrackWagonCfg_get_ID_TAGPOS(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *DePidataRailTrackWagonCfg_ID_TAGPOS;
J2OBJC_STATIC_FIELD_OBJ_FINAL(DePidataRailTrackWagonCfg, ID_TAGPOS, PIQ_QName *)

inline jint DePidataRailTrackWagonCfg_get_ICON_NAME_MAX_LEN(void);
#define DePidataRailTrackWagonCfg_ICON_NAME_MAX_LEN 30
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailTrackWagonCfg, ICON_NAME_MAX_LEN, jint)

FOUNDATION_EXPORT void DePidataRailTrackWagonCfg_initWithPIQ_Key_(DePidataRailTrackWagonCfg *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT DePidataRailTrackWagonCfg *new_DePidataRailTrackWagonCfg_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailTrackWagonCfg *create_DePidataRailTrackWagonCfg_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void DePidataRailTrackWagonCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(DePidataRailTrackWagonCfg *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT DePidataRailTrackWagonCfg *new_DePidataRailTrackWagonCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailTrackWagonCfg *create_DePidataRailTrackWagonCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void DePidataRailTrackWagonCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(DePidataRailTrackWagonCfg *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT DePidataRailTrackWagonCfg *new_DePidataRailTrackWagonCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailTrackWagonCfg *create_DePidataRailTrackWagonCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT NSString *DePidataRailTrackWagonCfg_checkIconNameWithNSString_(NSString *value);

J2OBJC_TYPE_LITERAL_HEADER(DePidataRailTrackWagonCfg)

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailTrackWagonCfg")
