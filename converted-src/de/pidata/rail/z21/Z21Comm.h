//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/z21/Z21Comm.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailZ21Z21Comm")
#ifdef RESTRICT_DePidataRailZ21Z21Comm
#define INCLUDE_ALL_DePidataRailZ21Z21Comm 0
#else
#define INCLUDE_ALL_DePidataRailZ21Z21Comm 1
#endif
#undef RESTRICT_DePidataRailZ21Z21Comm

#if !defined (DePidataRailZ21Z21Comm_) && (INCLUDE_ALL_DePidataRailZ21Z21Comm || defined(INCLUDE_DePidataRailZ21Z21Comm))
#define DePidataRailZ21Z21Comm_

#define RESTRICT_JavaLangRunnable 1
#define INCLUDE_JavaLangRunnable 1
#include "java/lang/Runnable.h"

#define RESTRICT_DePidataConnectUdpInetReceiveListener 1
#define INCLUDE_PINU_InetReceiveListener 1
#include "de/pidata/connect/udp/InetReceiveListener.h"

@class DePidataRailZ21TypeActIP;
@class DePidataRailZ21Z21LocoDir;
@class IOSByteArray;
@class IOSIntArray;
@class JavaNetInetAddress;
@class JavaNetInetSocketAddress;
@protocol DePidataRailZ21Z21Interface;

@interface DePidataRailZ21Z21Comm : NSObject < JavaLangRunnable, PINU_InetReceiveListener > {
 @public
  IOSByteArray *last_rcv_packet_;
  IOSIntArray *last_snd_packet_;
}

#pragma mark Public

- (instancetype)initWithDePidataRailZ21Z21Interface:(id<DePidataRailZ21Z21Interface>)z21Interface;

- (jint)findEEPROMBCFlagWithInt:(jint)IPHash;

- (jint)getPower;

- (jint)getz21BcFlagWithInt:(jint)flag;

- (void)receivedDataWithJavaNetInetAddress:(JavaNetInetAddress *)client
                             withByteArray:(IOSByteArray *)packet
              withJavaNetInetSocketAddress:(JavaNetInetSocketAddress *)myAddress
                                   withInt:(jint)replyPort;

- (void)returnAccessoryStateChangedWithInt:(jint)addr;

- (void)returnLocoStateFullWithJavaNetInetAddress:(JavaNetInetAddress *)client
                                          withInt:(jint)sendPort
                                          withInt:(jint)addr
                                      withBoolean:(jboolean)bc;

- (void)returnLocoStateFullWithInt:(jint)addr;

- (void)returnSensorStateChangedWithJavaNetInetAddress:(JavaNetInetAddress *)client
                                               withInt:(jint)sendPort
                                               withInt:(jint)feedbackAddr
                                               withInt:(jint)locoAddr
                         withDePidataRailZ21Z21LocoDir:(DePidataRailZ21Z21LocoDir *)locoDir
                                           withBoolean:(jboolean)bc;

- (void)returnSensorStateChangedWithInt:(jint)feedbackAddr
                                withInt:(jint)locoAddr
          withDePidataRailZ21Z21LocoDir:(DePidataRailZ21Z21LocoDir *)locoDir;

- (void)run;

- (void)sendWithJavaNetInetAddress:(JavaNetInetAddress *)client
                           withInt:(jint)z21Port
                     withByteArray:(IOSByteArray *)msgBytes;

- (void)setCVPOMBYTEWithInt:(jint)cvAddr
                    withInt:(jint)value;

- (void)setLocoStateExtWithInt:(jint)Adr;

- (void)setPowerWithInt:(jint)state;

- (void)setS88DataWithIntArray:(IOSIntArray *)data;

- (jboolean)start;

- (void)stop;

#pragma mark Package-Private

- (void)addBusySlotWithJavaNetInetAddress:(JavaNetInetAddress *)client
                                  withInt:(jint)adr;

- (jint)addIPToSlotWithJavaNetInetAddress:(JavaNetInetAddress *)client
                                  withInt:(jint)replyPort
                                  withInt:(jint)bcFlag;

- (void)clearIPSlotWithJavaNetInetAddress:(JavaNetInetAddress *)client;

- (void)clearIPSlots;

- (void)ethSendWithJavaNetInetAddress:(JavaNetInetAddress *)client
                              withInt:(jint)sendPort
                              withInt:(jint)dataLen
                              withInt:(jint)header
                         withIntArray:(IOSIntArray *)dataString
                          withBoolean:(jboolean)withXOR
                              withInt:(jint)bc;

- (jint)getLocalBcFlagWithInt:(jint)flag;

- (void)reqLocoBusyWithInt:(jint)adr;

- (void)returnSensorStateChangedWithInt:(jint)nid
                                withInt:(jint)addr
                                withInt:(jint)port
                                withInt:(jint)locoAddr
          withDePidataRailZ21Z21LocoDir:(DePidataRailZ21Z21LocoDir *)clockDir;

- (void)sendSystemInfoWithJavaNetInetAddress:(JavaNetInetAddress *)client
                                     withInt:(jint)sendPort
                                     withInt:(jint)maincurrent
                                     withInt:(jint)mainvoltage
                                     withInt:(jint)temp;

- (void)setCANDetectorWithInt:(jint)nid
                      withInt:(jint)addr
                      withInt:(jint)port
                      withInt:(jint)typ
                      withInt:(jint)v1
                      withInt:(jint)v2;

- (void)setCVNack;

- (void)setCVNackSC;

- (void)setCVReturnWithInt:(jint)CV
                   withInt:(jint)value;

- (void)setEEPROMBCFlagWithInt:(jint)IPHash
                       withInt:(jint)bcFlag;

- (void)setExtACCInfoWithInt:(jint)Adr
                     withInt:(jint)State
                     withInt:(jint)Status;

- (jboolean)setLNMessageWithIntArray:(IOSIntArray *)data
                             withInt:(jint)dataLen
                             withInt:(jint)bcType
                         withBoolean:(jboolean)TX;

- (void)setOtherSlotBusyWithDePidataRailZ21TypeActIP:(DePidataRailZ21TypeActIP *)currentActIP;

// Disallowed inherited constructors, do not use.

- (instancetype)init NS_UNAVAILABLE;

@end

J2OBJC_EMPTY_STATIC_INIT(DePidataRailZ21Z21Comm)

J2OBJC_FIELD_SETTER(DePidataRailZ21Z21Comm, last_rcv_packet_, IOSByteArray *)
J2OBJC_FIELD_SETTER(DePidataRailZ21Z21Comm, last_snd_packet_, IOSIntArray *)

inline jboolean DePidataRailZ21Z21Comm_get_LOG_RCV(void);
#define DePidataRailZ21Z21Comm_LOG_RCV false
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LOG_RCV, jboolean)

inline jboolean DePidataRailZ21Z21Comm_get_LOG_SND(void);
#define DePidataRailZ21Z21Comm_LOG_SND false
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LOG_SND, jboolean)

inline jboolean DePidataRailZ21Z21Comm_get_LOG_SND_REPEAT(void);
#define DePidataRailZ21Z21Comm_LOG_SND_REPEAT false
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LOG_SND_REPEAT, jboolean)

inline jboolean DePidataRailZ21Z21Comm_get_LOG_OTHER(void);
#define DePidataRailZ21Z21Comm_LOG_OTHER false
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LOG_OTHER, jboolean)

inline jint DePidataRailZ21Z21Comm_get_Z21_PORT(void);
#define DePidataRailZ21Z21Comm_Z21_PORT 21105
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21_PORT, jint)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_Header(void);
#define DePidataRailZ21Z21Comm_LAN_X_Header 64
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_Header, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_GET_SERIAL_NUMBER(void);
#define DePidataRailZ21Z21Comm_LAN_GET_SERIAL_NUMBER 16
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_GET_SERIAL_NUMBER, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_GET_CODE(void);
#define DePidataRailZ21Z21Comm_LAN_GET_CODE 24
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_GET_CODE, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_LOGOFF(void);
#define DePidataRailZ21Z21Comm_LAN_LOGOFF 48
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_LOGOFF, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_GET_SETTING(void);
#define DePidataRailZ21Z21Comm_LAN_X_GET_SETTING 33
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_GET_SETTING, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_BC_TRACK_POWER(void);
#define DePidataRailZ21Z21Comm_LAN_X_BC_TRACK_POWER 97
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_BC_TRACK_POWER, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_UNKNOWN_COMMAND(void);
#define DePidataRailZ21Z21Comm_LAN_X_UNKNOWN_COMMAND 97
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_UNKNOWN_COMMAND, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_STATUS_CHANGED(void);
#define DePidataRailZ21Z21Comm_LAN_X_STATUS_CHANGED 98
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_STATUS_CHANGED, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_GET_VERSION(void);
#define DePidataRailZ21Z21Comm_LAN_X_GET_VERSION 99
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_GET_VERSION, jbyte)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_SET_STOP(void);
#define DePidataRailZ21Z21Comm_LAN_X_SET_STOP 128
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_SET_STOP, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_BC_STOPPED(void);
#define DePidataRailZ21Z21Comm_LAN_X_BC_STOPPED 129
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_BC_STOPPED, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_GET_FIRMWARE_VERSION(void);
#define DePidataRailZ21Z21Comm_LAN_X_GET_FIRMWARE_VERSION 241
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_GET_FIRMWARE_VERSION, jint)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_SET_BROADCASTFLAGS(void);
#define DePidataRailZ21Z21Comm_LAN_SET_BROADCASTFLAGS 80
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_SET_BROADCASTFLAGS, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_GET_BROADCASTFLAGS(void);
#define DePidataRailZ21Z21Comm_LAN_GET_BROADCASTFLAGS 81
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_GET_BROADCASTFLAGS, jbyte)

inline jint DePidataRailZ21Z21Comm_get_LAN_SYSTEMSTATE_DATACHANGED(void);
#define DePidataRailZ21Z21Comm_LAN_SYSTEMSTATE_DATACHANGED 132
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_SYSTEMSTATE_DATACHANGED, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_SYSTEMSTATE_GETDATA(void);
#define DePidataRailZ21Z21Comm_LAN_SYSTEMSTATE_GETDATA 133
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_SYSTEMSTATE_GETDATA, jint)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_GET_HWINFO(void);
#define DePidataRailZ21Z21Comm_LAN_GET_HWINFO 26
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_GET_HWINFO, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_GET_LOCOMODE(void);
#define DePidataRailZ21Z21Comm_LAN_GET_LOCOMODE 96
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_GET_LOCOMODE, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_SET_LOCOMODE(void);
#define DePidataRailZ21Z21Comm_LAN_SET_LOCOMODE 97
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_SET_LOCOMODE, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_GET_TURNOUTMODE(void);
#define DePidataRailZ21Z21Comm_LAN_GET_TURNOUTMODE 112
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_GET_TURNOUTMODE, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_SET_TURNOUTMODE(void);
#define DePidataRailZ21Z21Comm_LAN_SET_TURNOUTMODE 113
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_SET_TURNOUTMODE, jbyte)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_GET_LOCO_INFO(void);
#define DePidataRailZ21Z21Comm_LAN_X_GET_LOCO_INFO 227
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_GET_LOCO_INFO, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_SET_LOCO(void);
#define DePidataRailZ21Z21Comm_LAN_X_SET_LOCO 228
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_SET_LOCO, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_SET_LOCO_FUNCTION(void);
#define DePidataRailZ21Z21Comm_LAN_X_SET_LOCO_FUNCTION 248
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_SET_LOCO_FUNCTION, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_SET_LOCO_BINARY_STATE(void);
#define DePidataRailZ21Z21Comm_LAN_X_SET_LOCO_BINARY_STATE 229
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_SET_LOCO_BINARY_STATE, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_LOCO_INFO(void);
#define DePidataRailZ21Z21Comm_LAN_X_LOCO_INFO 239
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_LOCO_INFO, jint)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_GET_TURNOUT_INFO(void);
#define DePidataRailZ21Z21Comm_LAN_X_GET_TURNOUT_INFO 67
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_GET_TURNOUT_INFO, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_SET_TURNOUT(void);
#define DePidataRailZ21Z21Comm_LAN_X_SET_TURNOUT 83
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_SET_TURNOUT, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_TURNOUT_INFO(void);
#define DePidataRailZ21Z21Comm_LAN_X_TURNOUT_INFO 67
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_TURNOUT_INFO, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_SET_EXT_ACCESSORY(void);
#define DePidataRailZ21Z21Comm_LAN_X_SET_EXT_ACCESSORY 84
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_SET_EXT_ACCESSORY, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_GET_EXT_ACCESSORY_INFO(void);
#define DePidataRailZ21Z21Comm_LAN_X_GET_EXT_ACCESSORY_INFO 68
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_GET_EXT_ACCESSORY_INFO, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_CV_READ(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_READ 35
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_READ, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_CV_WRITE(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_WRITE 36
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_WRITE, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_CV_NACK_SC(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_NACK_SC 97
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_NACK_SC, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_CV_NACK(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_NACK 97
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_NACK, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_CV_RESULT(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_RESULT 100
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_RESULT, jbyte)

inline jint DePidataRailZ21Z21Comm_get_LAN_RMBUS_DATACHANGED(void);
#define DePidataRailZ21Z21Comm_LAN_RMBUS_DATACHANGED 128
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_RMBUS_DATACHANGED, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_RMBUS_GETDATA(void);
#define DePidataRailZ21Z21Comm_LAN_RMBUS_GETDATA 129
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_RMBUS_GETDATA, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_RMBUS_PROGRAMMODULE(void);
#define DePidataRailZ21Z21Comm_LAN_RMBUS_PROGRAMMODULE 130
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_RMBUS_PROGRAMMODULE, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_RAILCOM_DATACHANGED(void);
#define DePidataRailZ21Z21Comm_LAN_RAILCOM_DATACHANGED 136
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_RAILCOM_DATACHANGED, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_RAILCOM_GETDATA(void);
#define DePidataRailZ21Z21Comm_LAN_RAILCOM_GETDATA 137
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_RAILCOM_GETDATA, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_LOCONET_Z21_RX(void);
#define DePidataRailZ21Z21Comm_LAN_LOCONET_Z21_RX 160
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_LOCONET_Z21_RX, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_LOCONET_Z21_TX(void);
#define DePidataRailZ21Z21Comm_LAN_LOCONET_Z21_TX 161
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_LOCONET_Z21_TX, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_LOCONET_FROM_LAN(void);
#define DePidataRailZ21Z21Comm_LAN_LOCONET_FROM_LAN 162
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_LOCONET_FROM_LAN, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_LOCONET_DISPATCH_ADDR(void);
#define DePidataRailZ21Z21Comm_LAN_LOCONET_DISPATCH_ADDR 163
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_LOCONET_DISPATCH_ADDR, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_LOCONET_DETECTOR(void);
#define DePidataRailZ21Z21Comm_LAN_LOCONET_DETECTOR 164
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_LOCONET_DETECTOR, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_CAN_DETECTOR(void);
#define DePidataRailZ21Z21Comm_LAN_CAN_DETECTOR 196
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_CAN_DETECTOR, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_CV_POM(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_POM 230
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_POM, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_CV_POM_WRITE_BYTE(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_POM_WRITE_BYTE 236
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_POM_WRITE_BYTE, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_CV_POM_WRITE_BIT(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_POM_WRITE_BIT 232
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_POM_WRITE_BIT, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_CV_POM_READ_BYTE(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_POM_READ_BYTE 228
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_POM_READ_BYTE, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_CV_POM_ACCESSORY_WRITE_BYTE(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_POM_ACCESSORY_WRITE_BYTE 236
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_POM_ACCESSORY_WRITE_BYTE, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_CV_POM_ACCESSORY_WRITE_BIT(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_POM_ACCESSORY_WRITE_BIT 232
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_POM_ACCESSORY_WRITE_BIT, jint)

inline jint DePidataRailZ21Z21Comm_get_LAN_X_CV_POM_ACCESSORY_READ_BYTE(void);
#define DePidataRailZ21Z21Comm_LAN_X_CV_POM_ACCESSORY_READ_BYTE 228
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_CV_POM_ACCESSORY_READ_BYTE, jint)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_MM_WRITE_BYTE(void);
#define DePidataRailZ21Z21Comm_LAN_X_MM_WRITE_BYTE 36
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_MM_WRITE_BYTE, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_DCC_READ_REGISTER(void);
#define DePidataRailZ21Z21Comm_LAN_X_DCC_READ_REGISTER 34
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_DCC_READ_REGISTER, jbyte)

inline jbyte DePidataRailZ21Z21Comm_get_LAN_X_DCC_WRITE_REGISTER(void);
#define DePidataRailZ21Z21Comm_LAN_X_DCC_WRITE_REGISTER 35
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, LAN_X_DCC_WRITE_REGISTER, jbyte)

inline jint DePidataRailZ21Z21Comm_get_Z21bcNone(void);
#define DePidataRailZ21Z21Comm_Z21bcNone 0
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcNone, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcAll(void);
#define DePidataRailZ21Z21Comm_Z21bcAll 1
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcAll, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcAll_s(void);
#define DePidataRailZ21Z21Comm_Z21bcAll_s 1
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcAll_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcRBus(void);
#define DePidataRailZ21Z21Comm_Z21bcRBus 2
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcRBus, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcRBus_s(void);
#define DePidataRailZ21Z21Comm_Z21bcRBus_s 2
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcRBus_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcRailcom(void);
#define DePidataRailZ21Z21Comm_Z21bcRailcom 4
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcRailcom, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcRailcom_s(void);
#define DePidataRailZ21Z21Comm_Z21bcRailcom_s 256
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcRailcom_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcSystemInfo(void);
#define DePidataRailZ21Z21Comm_Z21bcSystemInfo 256
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcSystemInfo, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcSystemInfo_s(void);
#define DePidataRailZ21Z21Comm_Z21bcSystemInfo_s 4
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcSystemInfo_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcNetAll(void);
#define DePidataRailZ21Z21Comm_Z21bcNetAll 65536
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcNetAll, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcNetAll_s(void);
#define DePidataRailZ21Z21Comm_Z21bcNetAll_s 8
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcNetAll_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcLocoNet(void);
#define DePidataRailZ21Z21Comm_Z21bcLocoNet 16777216
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcLocoNet, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcLocoNet_s(void);
#define DePidataRailZ21Z21Comm_Z21bcLocoNet_s 16
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcLocoNet_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcLocoNetLocos(void);
#define DePidataRailZ21Z21Comm_Z21bcLocoNetLocos 33554432
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcLocoNetLocos, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcLocoNetLocos_s(void);
#define DePidataRailZ21Z21Comm_Z21bcLocoNetLocos_s 48
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcLocoNetLocos_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcLocoNetSwitches(void);
#define DePidataRailZ21Z21Comm_Z21bcLocoNetSwitches 67108864
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcLocoNetSwitches, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcLocoNetSwitches_s(void);
#define DePidataRailZ21Z21Comm_Z21bcLocoNetSwitches_s 80
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcLocoNetSwitches_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcLocoNetGBM(void);
#define DePidataRailZ21Z21Comm_Z21bcLocoNetGBM 134217728
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcLocoNetGBM, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcLocoNetGBM_s(void);
#define DePidataRailZ21Z21Comm_Z21bcLocoNetGBM_s 144
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcLocoNetGBM_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcRailComAll(void);
#define DePidataRailZ21Z21Comm_Z21bcRailComAll 262144
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcRailComAll, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcRailComAll_s(void);
#define DePidataRailZ21Z21Comm_Z21bcRailComAll_s 128
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcRailComAll_s, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcCANDetector(void);
#define DePidataRailZ21Z21Comm_Z21bcCANDetector 524288
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcCANDetector, jint)

inline jint DePidataRailZ21Z21Comm_get_Z21bcCANDetector_s(void);
#define DePidataRailZ21Z21Comm_Z21bcCANDetector_s 192
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, Z21bcCANDetector_s, jint)

inline jint DePidataRailZ21Z21Comm_get_CONF1STORE(void);
#define DePidataRailZ21Z21Comm_CONF1STORE 50
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, CONF1STORE, jint)

inline jint DePidataRailZ21Z21Comm_get_CONF2STORE(void);
#define DePidataRailZ21Z21Comm_CONF2STORE 60
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, CONF2STORE, jint)

inline jint DePidataRailZ21Z21Comm_get_CLIENTHASHSTORE(void);
#define DePidataRailZ21Z21Comm_CLIENTHASHSTORE 512
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, CLIENTHASHSTORE, jint)

inline jint DePidataRailZ21Z21Comm_get_csNormal(void);
#define DePidataRailZ21Z21Comm_csNormal 0
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, csNormal, jint)

inline jint DePidataRailZ21Z21Comm_get_csEmergencyStop(void);
#define DePidataRailZ21Z21Comm_csEmergencyStop 1
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, csEmergencyStop, jint)

inline jint DePidataRailZ21Z21Comm_get_csTrackVoltageOff(void);
#define DePidataRailZ21Z21Comm_csTrackVoltageOff 2
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, csTrackVoltageOff, jint)

inline jint DePidataRailZ21Z21Comm_get_csShortCircuit(void);
#define DePidataRailZ21Z21Comm_csShortCircuit 4
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, csShortCircuit, jint)

inline jint DePidataRailZ21Z21Comm_get_csServiceMode(void);
#define DePidataRailZ21Z21Comm_csServiceMode 8
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, csServiceMode, jint)

inline jint DePidataRailZ21Z21Comm_get_cseHighTemperature(void);
#define DePidataRailZ21Z21Comm_cseHighTemperature 1
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, cseHighTemperature, jint)

inline jint DePidataRailZ21Z21Comm_get_csePowerLost(void);
#define DePidataRailZ21Z21Comm_csePowerLost 2
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, csePowerLost, jint)

inline jint DePidataRailZ21Z21Comm_get_cseShortCircuitExternal(void);
#define DePidataRailZ21Z21Comm_cseShortCircuitExternal 4
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, cseShortCircuitExternal, jint)

inline jint DePidataRailZ21Z21Comm_get_cseShortCircuitInternal(void);
#define DePidataRailZ21Z21Comm_cseShortCircuitInternal 8
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, cseShortCircuitInternal, jint)

inline jint DePidataRailZ21Z21Comm_get_z21ActTimeIP(void);
#define DePidataRailZ21Z21Comm_z21ActTimeIP 20
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, z21ActTimeIP, jint)

inline jint DePidataRailZ21Z21Comm_get_z21IPinterval(void);
#define DePidataRailZ21Z21Comm_z21IPinterval 2000
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, z21IPinterval, jint)

inline jint DePidataRailZ21Z21Comm_get_DCCSTEP14(void);
#define DePidataRailZ21Z21Comm_DCCSTEP14 1
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, DCCSTEP14, jint)

inline jint DePidataRailZ21Z21Comm_get_DCCSTEP28(void);
#define DePidataRailZ21Z21Comm_DCCSTEP28 2
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, DCCSTEP28, jint)

inline jint DePidataRailZ21Z21Comm_get_DCCSTEP128(void);
#define DePidataRailZ21Z21Comm_DCCSTEP128 3
J2OBJC_STATIC_FIELD_CONSTANT(DePidataRailZ21Z21Comm, DCCSTEP128, jint)

FOUNDATION_EXPORT void DePidataRailZ21Z21Comm_initWithDePidataRailZ21Z21Interface_(DePidataRailZ21Z21Comm *self, id<DePidataRailZ21Z21Interface> z21Interface);

FOUNDATION_EXPORT DePidataRailZ21Z21Comm *new_DePidataRailZ21Z21Comm_initWithDePidataRailZ21Z21Interface_(id<DePidataRailZ21Z21Interface> z21Interface) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataRailZ21Z21Comm *create_DePidataRailZ21Z21Comm_initWithDePidataRailZ21Z21Interface_(id<DePidataRailZ21Z21Interface> z21Interface);

J2OBJC_TYPE_LITERAL_HEADER(DePidataRailZ21Z21Comm)

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailZ21Z21Comm")
