//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/z21/Z21LocoDir.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailZ21Z21LocoDir")
#ifdef RESTRICT_DePidataRailZ21Z21LocoDir
#define INCLUDE_ALL_DePidataRailZ21Z21LocoDir 0
#else
#define INCLUDE_ALL_DePidataRailZ21Z21LocoDir 1
#endif
#undef RESTRICT_DePidataRailZ21Z21LocoDir

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability"
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (DePidataRailZ21Z21LocoDir_) && (INCLUDE_ALL_DePidataRailZ21Z21LocoDir || defined(INCLUDE_DePidataRailZ21Z21LocoDir))
#define DePidataRailZ21Z21LocoDir_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;

typedef NS_ENUM(NSUInteger, DePidataRailZ21Z21LocoDir_Enum) {
  DePidataRailZ21Z21LocoDir_Enum_forward = 0,
  DePidataRailZ21Z21LocoDir_Enum_backward = 1,
  DePidataRailZ21Z21LocoDir_Enum_unknown = 2,
};

@interface DePidataRailZ21Z21LocoDir : JavaLangEnum

#pragma mark Public

+ (DePidataRailZ21Z21LocoDir *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (DePidataRailZ21Z21LocoDir_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(DePidataRailZ21Z21LocoDir)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT DePidataRailZ21Z21LocoDir *DePidataRailZ21Z21LocoDir_values_[];

inline DePidataRailZ21Z21LocoDir *DePidataRailZ21Z21LocoDir_get_forward(void);
J2OBJC_ENUM_CONSTANT(DePidataRailZ21Z21LocoDir, forward)

inline DePidataRailZ21Z21LocoDir *DePidataRailZ21Z21LocoDir_get_backward(void);
J2OBJC_ENUM_CONSTANT(DePidataRailZ21Z21LocoDir, backward)

inline DePidataRailZ21Z21LocoDir *DePidataRailZ21Z21LocoDir_get_unknown(void);
J2OBJC_ENUM_CONSTANT(DePidataRailZ21Z21LocoDir, unknown)

FOUNDATION_EXPORT IOSObjectArray *DePidataRailZ21Z21LocoDir_values(void);

FOUNDATION_EXPORT DePidataRailZ21Z21LocoDir *DePidataRailZ21Z21LocoDir_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT DePidataRailZ21Z21LocoDir *DePidataRailZ21Z21LocoDir_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(DePidataRailZ21Z21LocoDir)

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_DePidataRailZ21Z21LocoDir")
