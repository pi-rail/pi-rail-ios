//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/SyncCmd.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/model/PiRailFactory.h"
#include "de/pidata/rail/model/SyncCmd.h"
#include "java/lang/Integer.h"
#include "java/lang/Long.h"
#include "java/lang/System.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/model/SyncCmd must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIRM_SyncCmd)

PIQ_Namespace *PIRM_SyncCmd_NAMESPACE;
PIQ_QName *PIRM_SyncCmd_ID_ENDIP;
PIQ_QName *PIRM_SyncCmd_ID_MSINTERVAL;
PIQ_QName *PIRM_SyncCmd_ID_MSSLOTSIZE;
PIQ_QName *PIRM_SyncCmd_ID_STARTIP;
PIQ_QName *PIRM_SyncCmd_ID_TIME;

@implementation PIRM_SyncCmd

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRM_SyncCmd_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRM_SyncCmd_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRM_SyncCmd_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (JavaLangLong *)getTime {
  return (JavaLangLong *) cast_chk([self getWithPIQ_QName:PIRM_SyncCmd_ID_TIME], [JavaLangLong class]);
}

- (void)setTimeWithJavaLangLong:(JavaLangLong *)time {
  [self setWithPIQ_QName:PIRM_SyncCmd_ID_TIME withId:time];
}

- (JavaLangInteger *)getStartIP {
  return (JavaLangInteger *) cast_chk([self getWithPIQ_QName:PIRM_SyncCmd_ID_STARTIP], [JavaLangInteger class]);
}

- (void)setStartIPWithJavaLangInteger:(JavaLangInteger *)startIP {
  [self setWithPIQ_QName:PIRM_SyncCmd_ID_STARTIP withId:startIP];
}

- (JavaLangInteger *)getEndIP {
  return (JavaLangInteger *) cast_chk([self getWithPIQ_QName:PIRM_SyncCmd_ID_ENDIP], [JavaLangInteger class]);
}

- (void)setEndIPWithJavaLangInteger:(JavaLangInteger *)endIP {
  [self setWithPIQ_QName:PIRM_SyncCmd_ID_ENDIP withId:endIP];
}

- (JavaLangInteger *)getMsSlotSize {
  return (JavaLangInteger *) cast_chk([self getWithPIQ_QName:PIRM_SyncCmd_ID_MSSLOTSIZE], [JavaLangInteger class]);
}

- (void)setMsSlotSizeWithJavaLangInteger:(JavaLangInteger *)msSlotSize {
  [self setWithPIQ_QName:PIRM_SyncCmd_ID_MSSLOTSIZE withId:msSlotSize];
}

- (JavaLangInteger *)getMsInterval {
  return (JavaLangInteger *) cast_chk([self getWithPIQ_QName:PIRM_SyncCmd_ID_MSINTERVAL], [JavaLangInteger class]);
}

- (void)setMsIntervalWithJavaLangInteger:(JavaLangInteger *)msInterval {
  [self setWithPIQ_QName:PIRM_SyncCmd_ID_MSINTERVAL withId:msInterval];
}

- (instancetype)initWithInt:(jint)msSlotSize
                    withInt:(jint)msInterval
                    withInt:(jint)startIP
                    withInt:(jint)endIP {
  PIRM_SyncCmd_initWithInt_withInt_withInt_withInt_(self, msSlotSize, msInterval, startIP, endIP);
  return self;
}

- (jint)getMsSlotSizeInt {
  JavaLangInteger *value = [self getMsSlotSize];
  if (value == nil) {
    return 0;
  }
  else {
    return [value intValue];
  }
}

- (jint)getEndIPInt {
  JavaLangInteger *value = [self getEndIP];
  if (value == nil) {
    return 0;
  }
  else {
    return [value intValue];
  }
}

- (jint)getgetStartIPInt {
  JavaLangInteger *value = [self getStartIP];
  if (value == nil) {
    return 0;
  }
  else {
    return [value intValue];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, 3, -1, -1 },
    { NULL, "LJavaLangLong;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 7, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 7, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 7, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 11, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getTime);
  methods[4].selector = @selector(setTimeWithJavaLangLong:);
  methods[5].selector = @selector(getStartIP);
  methods[6].selector = @selector(setStartIPWithJavaLangInteger:);
  methods[7].selector = @selector(getEndIP);
  methods[8].selector = @selector(setEndIPWithJavaLangInteger:);
  methods[9].selector = @selector(getMsSlotSize);
  methods[10].selector = @selector(setMsSlotSizeWithJavaLangInteger:);
  methods[11].selector = @selector(getMsInterval);
  methods[12].selector = @selector(setMsIntervalWithJavaLangInteger:);
  methods[13].selector = @selector(initWithInt:withInt:withInt:withInt:);
  methods[14].selector = @selector(getMsSlotSizeInt);
  methods[15].selector = @selector(getEndIPInt);
  methods[16].selector = @selector(getgetStartIPInt);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 12, -1, -1 },
    { "ID_ENDIP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 13, -1, -1 },
    { "ID_MSINTERVAL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 14, -1, -1 },
    { "ID_MSSLOTSIZE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 15, -1, -1 },
    { "ID_STARTIP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 16, -1, -1 },
    { "ID_TIME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 17, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setTime", "LJavaLangLong;", "setStartIP", "LJavaLangInteger;", "setEndIP", "setMsSlotSize", "setMsInterval", "IIII", &PIRM_SyncCmd_NAMESPACE, &PIRM_SyncCmd_ID_ENDIP, &PIRM_SyncCmd_ID_MSINTERVAL, &PIRM_SyncCmd_ID_MSSLOTSIZE, &PIRM_SyncCmd_ID_STARTIP, &PIRM_SyncCmd_ID_TIME };
  static const J2ObjcClassInfo _PIRM_SyncCmd = { "SyncCmd", "de.pidata.rail.model", ptrTable, methods, fields, 7, 0x1, 17, 6, -1, -1, -1, -1, -1 };
  return &_PIRM_SyncCmd;
}

+ (void)initialize {
  if (self == [PIRM_SyncCmd class]) {
    PIRM_SyncCmd_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/pi-rail.xsd");
    PIRM_SyncCmd_ID_ENDIP = [((PIQ_Namespace *) nil_chk(PIRM_SyncCmd_NAMESPACE)) getQNameWithNSString:@"endIP"];
    PIRM_SyncCmd_ID_MSINTERVAL = [PIRM_SyncCmd_NAMESPACE getQNameWithNSString:@"msInterval"];
    PIRM_SyncCmd_ID_MSSLOTSIZE = [PIRM_SyncCmd_NAMESPACE getQNameWithNSString:@"msSlotSize"];
    PIRM_SyncCmd_ID_STARTIP = [PIRM_SyncCmd_NAMESPACE getQNameWithNSString:@"startIP"];
    PIRM_SyncCmd_ID_TIME = [PIRM_SyncCmd_NAMESPACE getQNameWithNSString:@"time"];
    J2OBJC_SET_INITIALIZED(PIRM_SyncCmd)
  }
}

@end

void PIRM_SyncCmd_init(PIRM_SyncCmd *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(PIRM_PiRailFactory, SYNCCMD_TYPE), nil, nil, nil);
}

PIRM_SyncCmd *new_PIRM_SyncCmd_init() {
  J2OBJC_NEW_IMPL(PIRM_SyncCmd, init)
}

PIRM_SyncCmd *create_PIRM_SyncCmd_init() {
  J2OBJC_CREATE_IMPL(PIRM_SyncCmd, init)
}

void PIRM_SyncCmd_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_SyncCmd *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRM_PiRailFactory, SYNCCMD_TYPE), attributeNames, anyAttribs, childNames);
}

PIRM_SyncCmd *new_PIRM_SyncCmd_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRM_SyncCmd, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIRM_SyncCmd *create_PIRM_SyncCmd_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRM_SyncCmd, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIRM_SyncCmd_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_SyncCmd *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIRM_SyncCmd *new_PIRM_SyncCmd_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRM_SyncCmd, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIRM_SyncCmd *create_PIRM_SyncCmd_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRM_SyncCmd, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

void PIRM_SyncCmd_initWithInt_withInt_withInt_withInt_(PIRM_SyncCmd *self, jint msSlotSize, jint msInterval, jint startIP, jint endIP) {
  PIRM_SyncCmd_init(self);
  [self setMsSlotSizeWithJavaLangInteger:JavaLangInteger_valueOfWithInt_(msSlotSize)];
  [self setMsIntervalWithJavaLangInteger:JavaLangInteger_valueOfWithInt_(msInterval)];
  [self setStartIPWithJavaLangInteger:JavaLangInteger_valueOfWithInt_(startIP)];
  [self setEndIPWithJavaLangInteger:JavaLangInteger_valueOfWithInt_(endIP)];
  [self setTimeWithJavaLangLong:JavaLangLong_valueOfWithLong_(JavaLangSystem_currentTimeMillis())];
}

PIRM_SyncCmd *new_PIRM_SyncCmd_initWithInt_withInt_withInt_withInt_(jint msSlotSize, jint msInterval, jint startIP, jint endIP) {
  J2OBJC_NEW_IMPL(PIRM_SyncCmd, initWithInt_withInt_withInt_withInt_, msSlotSize, msInterval, startIP, endIP)
}

PIRM_SyncCmd *create_PIRM_SyncCmd_initWithInt_withInt_withInt_withInt_(jint msSlotSize, jint msInterval, jint startIP, jint endIP) {
  J2OBJC_CREATE_IMPL(PIRM_SyncCmd, initWithInt_withInt_withInt_withInt_, msSlotSize, msInterval, startIP, endIP)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRM_SyncCmd)

J2OBJC_NAME_MAPPING(PIRM_SyncCmd, "de.pidata.rail.model", "PIRM_")
