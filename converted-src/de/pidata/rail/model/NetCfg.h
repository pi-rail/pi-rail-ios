//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/NetCfg.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailModelNetCfg")
#ifdef RESTRICT_DePidataRailModelNetCfg
#define INCLUDE_ALL_DePidataRailModelNetCfg 0
#else
#define INCLUDE_ALL_DePidataRailModelNetCfg 1
#endif
#undef RESTRICT_DePidataRailModelNetCfg

#if !defined (PIRM_NetCfg_) && (INCLUDE_ALL_DePidataRailModelNetCfg || defined(INCLUDE_PIRM_NetCfg))
#define PIRM_NetCfg_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@class PIRM_WifiCfg;
@protocol JavaUtilCollection;
@protocol PIMR_ModelIterator;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRM_NetCfg : PIMR_SequenceModel

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (instancetype)initWithNSString:(NSString *)ssid
                    withNSString:(NSString *)password
                    withNSString:(NSString *)id_;

- (void)addWifiCfgWithPIRM_WifiCfg:(PIRM_WifiCfg *)wifiCfg;

- (NSString *)getId;

- (PIRM_WifiCfg *)getWifiCfgWithPIQ_Key:(id<PIQ_Key>)wifiCfgID;

- (id<JavaUtilCollection>)getWifiCfgs;

- (void)removeWifiCfgWithPIRM_WifiCfg:(PIRM_WifiCfg *)wifiCfg;

- (void)setIdWithNSString:(NSString *)id_;

- (jint)wifiCfgCount;

- (id<PIMR_ModelIterator>)wifiCfgIter;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIRM_NetCfg)

inline PIQ_Namespace *PIRM_NetCfg_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRM_NetCfg_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_NetCfg, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRM_NetCfg_get_ID_ID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_NetCfg_ID_ID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_NetCfg, ID_ID, PIQ_QName *)

inline PIQ_QName *PIRM_NetCfg_get_ID_WIFICFG(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_NetCfg_ID_WIFICFG;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_NetCfg, ID_WIFICFG, PIQ_QName *)

FOUNDATION_EXPORT void PIRM_NetCfg_init(PIRM_NetCfg *self);

FOUNDATION_EXPORT PIRM_NetCfg *new_PIRM_NetCfg_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_NetCfg *create_PIRM_NetCfg_init(void);

FOUNDATION_EXPORT void PIRM_NetCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_NetCfg *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRM_NetCfg *new_PIRM_NetCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_NetCfg *create_PIRM_NetCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRM_NetCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_NetCfg *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRM_NetCfg *new_PIRM_NetCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_NetCfg *create_PIRM_NetCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRM_NetCfg_initWithNSString_withNSString_withNSString_(PIRM_NetCfg *self, NSString *ssid, NSString *password, NSString *id_);

FOUNDATION_EXPORT PIRM_NetCfg *new_PIRM_NetCfg_initWithNSString_withNSString_withNSString_(NSString *ssid, NSString *password, NSString *id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_NetCfg *create_PIRM_NetCfg_initWithNSString_withNSString_withNSString_(NSString *ssid, NSString *password, NSString *id_);

J2OBJC_TYPE_LITERAL_HEADER(PIRM_NetCfg)

@compatibility_alias DePidataRailModelNetCfg PIRM_NetCfg;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailModelNetCfg")
