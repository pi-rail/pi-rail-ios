//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/ExtCfg.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelCollection.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/complex/DefaultComplexType.h"
#include "de/pidata/models/types/simple/StringType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/model/ExtCfg.h"
#include "de/pidata/rail/model/InCfg.h"
#include "de/pidata/rail/model/OutCfg.h"
#include "de/pidata/rail/model/PiRailFactory.h"
#include "de/pidata/rail/model/PortCfg.h"
#include "de/pidata/rail/model/PortMode.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/lang/Integer.h"
#include "java/util/Collection.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/model/ExtCfg must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRM_ExtCfg () {
 @public
  id<JavaUtilCollection> portCfgs_;
  id<JavaUtilCollection> outCfgs_;
  id<JavaUtilCollection> inCfgs_;
}

@end

J2OBJC_FIELD_SETTER(PIRM_ExtCfg, portCfgs_, id<JavaUtilCollection>)
J2OBJC_FIELD_SETTER(PIRM_ExtCfg, outCfgs_, id<JavaUtilCollection>)
J2OBJC_FIELD_SETTER(PIRM_ExtCfg, inCfgs_, id<JavaUtilCollection>)

J2OBJC_INITIALIZED_DEFN(PIRM_ExtCfg)

PIQ_Namespace *PIRM_ExtCfg_NAMESPACE;
PIQ_QName *PIRM_ExtCfg_ID_BUSADDR;
PIQ_QName *PIRM_ExtCfg_ID_ID;
PIQ_QName *PIRM_ExtCfg_ID_INCFG;
PIQ_QName *PIRM_ExtCfg_ID_ITEMID;
PIQ_QName *PIRM_ExtCfg_ID_OUTCFG;
PIQ_QName *PIRM_ExtCfg_ID_PORT;
PIQ_QName *PIRM_ExtCfg_ID_PORTCFG;
PIQ_QName *PIRM_ExtCfg_ID_TYPE;
id<PIMT_ComplexType> PIRM_ExtCfg_TRANSIENT_TYPE;
PIQ_QName *PIRM_ExtCfg_ID_KIND;
PIQ_QName *PIRM_ExtCfg_ID_LABEL;

@implementation PIRM_ExtCfg

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_ {
  PIRM_ExtCfg_initWithPIQ_Key_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRM_ExtCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRM_ExtCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getId {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRM_ExtCfg_ID_ID], [PIQ_QName class]);
}

- (PIRM_PortMode *)getType {
  return (PIRM_PortMode *) cast_chk([self getWithPIQ_QName:PIRM_ExtCfg_ID_TYPE], [PIRM_PortMode class]);
}

- (void)setTypeWithPIRM_PortMode:(PIRM_PortMode *)type {
  [self setWithPIQ_QName:PIRM_ExtCfg_ID_TYPE withId:type];
}

- (PIQ_QName *)getPort {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRM_ExtCfg_ID_PORT], [PIQ_QName class]);
}

- (void)setPortWithPIQ_QName:(PIQ_QName *)port {
  [self setWithPIQ_QName:PIRM_ExtCfg_ID_PORT withId:port];
}

- (JavaLangInteger *)getBusAddr {
  return (JavaLangInteger *) cast_chk([self getWithPIQ_QName:PIRM_ExtCfg_ID_BUSADDR], [JavaLangInteger class]);
}

- (void)setBusAddrWithJavaLangInteger:(JavaLangInteger *)busAddr {
  [self setWithPIQ_QName:PIRM_ExtCfg_ID_BUSADDR withId:busAddr];
}

- (PIQ_QName *)getItemID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRM_ExtCfg_ID_ITEMID], [PIQ_QName class]);
}

- (void)setItemIDWithPIQ_QName:(PIQ_QName *)itemID {
  [self setWithPIQ_QName:PIRM_ExtCfg_ID_ITEMID withId:itemID];
}

- (PIRM_PortCfg *)getPortCfgWithPIQ_Key:(id<PIQ_Key>)portCfgID {
  return (PIRM_PortCfg *) cast_chk([self getWithPIQ_QName:PIRM_ExtCfg_ID_PORTCFG withPIQ_Key:portCfgID], [PIRM_PortCfg class]);
}

- (void)addPortCfgWithPIRM_PortCfg:(PIRM_PortCfg *)portCfg {
  [self addWithPIQ_QName:PIRM_ExtCfg_ID_PORTCFG withPIMR_Model:portCfg];
}

- (void)removePortCfgWithPIRM_PortCfg:(PIRM_PortCfg *)portCfg {
  [self removeWithPIQ_QName:PIRM_ExtCfg_ID_PORTCFG withPIMR_Model:portCfg];
}

- (id<PIMR_ModelIterator>)portCfgIter {
  return [self iteratorWithPIQ_QName:PIRM_ExtCfg_ID_PORTCFG withPIMR_Filter:nil];
}

- (jint)portCfgCount {
  return [self childCountWithPIQ_QName:PIRM_ExtCfg_ID_PORTCFG];
}

- (id<JavaUtilCollection>)getPortCfgs {
  return portCfgs_;
}

- (PIRM_OutCfg *)getOutCfgWithPIQ_Key:(id<PIQ_Key>)outCfgID {
  return (PIRM_OutCfg *) cast_chk([self getWithPIQ_QName:PIRM_ExtCfg_ID_OUTCFG withPIQ_Key:outCfgID], [PIRM_OutCfg class]);
}

- (void)addOutCfgWithPIRM_OutCfg:(PIRM_OutCfg *)outCfg {
  [self addWithPIQ_QName:PIRM_ExtCfg_ID_OUTCFG withPIMR_Model:outCfg];
}

- (void)removeOutCfgWithPIRM_OutCfg:(PIRM_OutCfg *)outCfg {
  [self removeWithPIQ_QName:PIRM_ExtCfg_ID_OUTCFG withPIMR_Model:outCfg];
}

- (id<PIMR_ModelIterator>)outCfgIter {
  return [self iteratorWithPIQ_QName:PIRM_ExtCfg_ID_OUTCFG withPIMR_Filter:nil];
}

- (jint)outCfgCount {
  return [self childCountWithPIQ_QName:PIRM_ExtCfg_ID_OUTCFG];
}

- (id<JavaUtilCollection>)getOutCfgs {
  return outCfgs_;
}

- (PIRM_InCfg *)getInCfgWithPIQ_Key:(id<PIQ_Key>)inCfgID {
  return (PIRM_InCfg *) cast_chk([self getWithPIQ_QName:PIRM_ExtCfg_ID_INCFG withPIQ_Key:inCfgID], [PIRM_InCfg class]);
}

- (void)addInCfgWithPIRM_InCfg:(PIRM_InCfg *)inCfg {
  [self addWithPIQ_QName:PIRM_ExtCfg_ID_INCFG withPIMR_Model:inCfg];
}

- (void)removeInCfgWithPIRM_InCfg:(PIRM_InCfg *)inCfg {
  [self removeWithPIQ_QName:PIRM_ExtCfg_ID_INCFG withPIMR_Model:inCfg];
}

- (id<PIMR_ModelIterator>)inCfgIter {
  return [self iteratorWithPIQ_QName:PIRM_ExtCfg_ID_INCFG withPIMR_Filter:nil];
}

- (jint)inCfgCount {
  return [self childCountWithPIQ_QName:PIRM_ExtCfg_ID_INCFG];
}

- (id<JavaUtilCollection>)getInCfgs {
  return inCfgs_;
}

- (id<PIMT_ComplexType>)transientType {
  if (PIRM_ExtCfg_TRANSIENT_TYPE == nil) {
    PIMO_DefaultComplexType *type = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_([((PIQ_Namespace *) nil_chk(PIRM_ExtCfg_NAMESPACE)) getQNameWithNSString:@"ChipCfg_Transient"], [PIRM_ExtCfg_class_() getName], 0);
    PIRM_ExtCfg_TRANSIENT_TYPE = type;
    [type addAttributeTypeWithPIQ_QName:PIRM_ExtCfg_ID_KIND withPIMT_SimpleType:PIME_StringType_getDefString()];
    [type addAttributeTypeWithPIQ_QName:PIRM_ExtCfg_ID_LABEL withPIMT_SimpleType:PIME_StringType_getDefString()];
  }
  return PIRM_ExtCfg_TRANSIENT_TYPE;
}

- (id)transientGetWithInt:(jint)transientIndex {
  PIQ_QName *attributeName = [((id<PIMT_ComplexType>) nil_chk([self transientType])) getAttributeNameWithInt:transientIndex];
  if (JreObjectEqualsEquals(attributeName, PIRM_ExtCfg_ID_KIND)) {
    return @"Chip";
  }
  else if (JreObjectEqualsEquals(attributeName, PIRM_ExtCfg_ID_LABEL)) {
    return [self getLabel];
  }
  else {
    return [super transientGetWithInt:transientIndex];
  }
}

- (void)fireEventWithInt:(jint)eventID
                  withId:(id)source
           withPIQ_QName:(PIQ_QName *)elementID
                  withId:(id)oldValue
                  withId:(id)newValue {
  [super fireEventWithInt:eventID withId:source withPIQ_QName:elementID withId:oldValue withId:newValue];
  if ((JreObjectEqualsEquals(elementID, PIRM_ExtCfg_ID_BUSADDR)) || (JreObjectEqualsEquals(elementID, PIRM_ExtCfg_ID_TYPE))) {
    [self fireDataChangedWithPIQ_QName:PIRM_ExtCfg_ID_LABEL withId:nil withId:[self getLabel]];
  }
}

- (NSString *)getLabel {
  NSString *addrStr = [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getGlossaryStringWithNSString:@"address"];
  return JreStrcat("$$$C@$@C", [((PIQ_QName *) nil_chk([self getId])) getName], @": ", addrStr, ' ', [self getBusAddr], @" (", [self getType], ')');
}

- (jint)getBusAddrInt {
  JavaLangInteger *value = [self getBusAddr];
  if (value == nil) {
    return 0;
  }
  else {
    return [value intValue];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, 2, -1, -1 },
    { NULL, NULL, 0x4, -1, 3, -1, 4, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIRM_PortMode;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 8, -1, -1, -1, -1 },
    { NULL, "LPIRM_PortCfg;", 0x1, 12, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 13, 14, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 15, 14, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, -1, -1, -1, 16, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilCollection;", 0x1, -1, -1, -1, 17, -1, -1 },
    { NULL, "LPIRM_OutCfg;", 0x1, 18, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 19, 20, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 21, 20, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, -1, -1, -1, 22, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilCollection;", 0x1, -1, -1, -1, 23, -1, -1 },
    { NULL, "LPIRM_InCfg;", 0x1, 24, 0, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 25, 26, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 27, 26, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, -1, -1, -1, 28, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LJavaUtilCollection;", 0x1, -1, -1, -1, 29, -1, -1 },
    { NULL, "LPIMT_ComplexType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 30, 31, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 32, 33, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_Key:);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getId);
  methods[4].selector = @selector(getType);
  methods[5].selector = @selector(setTypeWithPIRM_PortMode:);
  methods[6].selector = @selector(getPort);
  methods[7].selector = @selector(setPortWithPIQ_QName:);
  methods[8].selector = @selector(getBusAddr);
  methods[9].selector = @selector(setBusAddrWithJavaLangInteger:);
  methods[10].selector = @selector(getItemID);
  methods[11].selector = @selector(setItemIDWithPIQ_QName:);
  methods[12].selector = @selector(getPortCfgWithPIQ_Key:);
  methods[13].selector = @selector(addPortCfgWithPIRM_PortCfg:);
  methods[14].selector = @selector(removePortCfgWithPIRM_PortCfg:);
  methods[15].selector = @selector(portCfgIter);
  methods[16].selector = @selector(portCfgCount);
  methods[17].selector = @selector(getPortCfgs);
  methods[18].selector = @selector(getOutCfgWithPIQ_Key:);
  methods[19].selector = @selector(addOutCfgWithPIRM_OutCfg:);
  methods[20].selector = @selector(removeOutCfgWithPIRM_OutCfg:);
  methods[21].selector = @selector(outCfgIter);
  methods[22].selector = @selector(outCfgCount);
  methods[23].selector = @selector(getOutCfgs);
  methods[24].selector = @selector(getInCfgWithPIQ_Key:);
  methods[25].selector = @selector(addInCfgWithPIRM_InCfg:);
  methods[26].selector = @selector(removeInCfgWithPIRM_InCfg:);
  methods[27].selector = @selector(inCfgIter);
  methods[28].selector = @selector(inCfgCount);
  methods[29].selector = @selector(getInCfgs);
  methods[30].selector = @selector(transientType);
  methods[31].selector = @selector(transientGetWithInt:);
  methods[32].selector = @selector(fireEventWithInt:withId:withPIQ_QName:withId:withId:);
  methods[33].selector = @selector(getLabel);
  methods[34].selector = @selector(getBusAddrInt);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 34, -1, -1 },
    { "ID_BUSADDR", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 35, -1, -1 },
    { "ID_ID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 36, -1, -1 },
    { "ID_INCFG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 37, -1, -1 },
    { "ID_ITEMID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 38, -1, -1 },
    { "ID_OUTCFG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 39, -1, -1 },
    { "ID_PORT", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 40, -1, -1 },
    { "ID_PORTCFG", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 41, -1, -1 },
    { "ID_TYPE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 42, -1, -1 },
    { "portCfgs_", "LJavaUtilCollection;", .constantValue.asLong = 0, 0x12, -1, -1, 43, -1 },
    { "outCfgs_", "LJavaUtilCollection;", .constantValue.asLong = 0, 0x12, -1, -1, 44, -1 },
    { "inCfgs_", "LJavaUtilCollection;", .constantValue.asLong = 0, 0x12, -1, -1, 45, -1 },
    { "TRANSIENT_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x9, -1, 46, -1, -1 },
    { "ID_KIND", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 47, -1, -1 },
    { "ID_LABEL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 48, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;", "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setType", "LPIRM_PortMode;", "setPort", "LPIQ_QName;", "setBusAddr", "LJavaLangInteger;", "setItemID", "getPortCfg", "addPortCfg", "LPIRM_PortCfg;", "removePortCfg", "()Lde/pidata/models/tree/ModelIterator<Lde/pidata/rail/model/PortCfg;>;", "()Ljava/util/Collection<Lde/pidata/rail/model/PortCfg;>;", "getOutCfg", "addOutCfg", "LPIRM_OutCfg;", "removeOutCfg", "()Lde/pidata/models/tree/ModelIterator<Lde/pidata/rail/model/OutCfg;>;", "()Ljava/util/Collection<Lde/pidata/rail/model/OutCfg;>;", "getInCfg", "addInCfg", "LPIRM_InCfg;", "removeInCfg", "()Lde/pidata/models/tree/ModelIterator<Lde/pidata/rail/model/InCfg;>;", "()Ljava/util/Collection<Lde/pidata/rail/model/InCfg;>;", "transientGet", "I", "fireEvent", "ILNSObject;LPIQ_QName;LNSObject;LNSObject;", &PIRM_ExtCfg_NAMESPACE, &PIRM_ExtCfg_ID_BUSADDR, &PIRM_ExtCfg_ID_ID, &PIRM_ExtCfg_ID_INCFG, &PIRM_ExtCfg_ID_ITEMID, &PIRM_ExtCfg_ID_OUTCFG, &PIRM_ExtCfg_ID_PORT, &PIRM_ExtCfg_ID_PORTCFG, &PIRM_ExtCfg_ID_TYPE, "Ljava/util/Collection<Lde/pidata/rail/model/PortCfg;>;", "Ljava/util/Collection<Lde/pidata/rail/model/OutCfg;>;", "Ljava/util/Collection<Lde/pidata/rail/model/InCfg;>;", &PIRM_ExtCfg_TRANSIENT_TYPE, &PIRM_ExtCfg_ID_KIND, &PIRM_ExtCfg_ID_LABEL };
  static const J2ObjcClassInfo _PIRM_ExtCfg = { "ExtCfg", "de.pidata.rail.model", ptrTable, methods, fields, 7, 0x1, 35, 15, -1, -1, -1, -1, -1 };
  return &_PIRM_ExtCfg;
}

+ (void)initialize {
  if (self == [PIRM_ExtCfg class]) {
    PIRM_ExtCfg_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/pi-rail.xsd");
    PIRM_ExtCfg_ID_BUSADDR = [((PIQ_Namespace *) nil_chk(PIRM_ExtCfg_NAMESPACE)) getQNameWithNSString:@"busAddr"];
    PIRM_ExtCfg_ID_ID = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"id"];
    PIRM_ExtCfg_ID_INCFG = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"inCfg"];
    PIRM_ExtCfg_ID_ITEMID = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"itemID"];
    PIRM_ExtCfg_ID_OUTCFG = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"outCfg"];
    PIRM_ExtCfg_ID_PORT = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"port"];
    PIRM_ExtCfg_ID_PORTCFG = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"portCfg"];
    PIRM_ExtCfg_ID_TYPE = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"type"];
    PIRM_ExtCfg_ID_KIND = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"kind"];
    PIRM_ExtCfg_ID_LABEL = [PIRM_ExtCfg_NAMESPACE getQNameWithNSString:@"label"];
    J2OBJC_SET_INITIALIZED(PIRM_ExtCfg)
  }
}

@end

void PIRM_ExtCfg_initWithPIQ_Key_(PIRM_ExtCfg *self, id<PIQ_Key> id_) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, JreLoadStatic(PIRM_PiRailFactory, EXTCFG_TYPE), nil, nil, nil);
  self->portCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_PORTCFG, self);
  self->outCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_OUTCFG, self);
  self->inCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_INCFG, self);
}

PIRM_ExtCfg *new_PIRM_ExtCfg_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_NEW_IMPL(PIRM_ExtCfg, initWithPIQ_Key_, id_)
}

PIRM_ExtCfg *create_PIRM_ExtCfg_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_CREATE_IMPL(PIRM_ExtCfg, initWithPIQ_Key_, id_)
}

void PIRM_ExtCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_ExtCfg *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRM_PiRailFactory, EXTCFG_TYPE), attributeNames, anyAttribs, childNames);
  self->portCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_PORTCFG, self);
  self->outCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_OUTCFG, self);
  self->inCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_INCFG, self);
}

PIRM_ExtCfg *new_PIRM_ExtCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRM_ExtCfg, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIRM_ExtCfg *create_PIRM_ExtCfg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRM_ExtCfg, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIRM_ExtCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_ExtCfg *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  self->portCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_PORTCFG, self);
  self->outCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_OUTCFG, self);
  self->inCfgs_ = new_PIMR_ModelCollection_initWithPIQ_QName_withPIMR_SequenceModel_(PIRM_ExtCfg_ID_INCFG, self);
}

PIRM_ExtCfg *new_PIRM_ExtCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRM_ExtCfg, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIRM_ExtCfg *create_PIRM_ExtCfg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRM_ExtCfg, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRM_ExtCfg)

J2OBJC_NAME_MAPPING(PIRM_ExtCfg, "de.pidata.rail.model", "PIRM_")
