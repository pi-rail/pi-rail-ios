//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/OutType.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/rail/model/OutType.h"
#include "java/lang/Enum.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/model/OutType must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRM_OutType () {
 @public
  NSString *value_;
}

@end

J2OBJC_FIELD_SETTER(PIRM_OutType, value_, NSString *)

__attribute__((unused)) static void PIRM_OutType_initWithNSString_withNSString_withInt_(PIRM_OutType *self, NSString *value, NSString *__name, jint __ordinal);

__attribute__((unused)) static PIRM_OutType *new_PIRM_OutType_initWithNSString_withNSString_withInt_(NSString *value, NSString *__name, jint __ordinal) NS_RETURNS_RETAINED;

J2OBJC_INITIALIZED_DEFN(PIRM_OutType)

PIRM_OutType *PIRM_OutType_values_[5];

@implementation PIRM_OutType

- (NSString *)getValue {
  return self->value_;
}

+ (PIRM_OutType *)fromStringWithNSString:(NSString *)stringValue {
  return PIRM_OutType_fromStringWithNSString_(stringValue);
}

- (NSString *)description {
  return value_;
}

+ (IOSObjectArray *)values {
  return PIRM_OutType_values();
}

+ (PIRM_OutType *)valueOfWithNSString:(NSString *)name {
  return PIRM_OutType_valueOfWithNSString_(name);
}

- (PIRM_OutType_Enum)toNSEnum {
  return (PIRM_OutType_Enum)[self ordinal];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIRM_OutType;", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 2, -1, -1, -1, -1, -1 },
    { NULL, "[LPIRM_OutType;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIRM_OutType;", 0x9, 3, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getValue);
  methods[1].selector = @selector(fromStringWithNSString:);
  methods[2].selector = @selector(description);
  methods[3].selector = @selector(values);
  methods[4].selector = @selector(valueOfWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "Logic", "LPIRM_OutType;", .constantValue.asLong = 0, 0x4019, -1, 4, -1, -1 },
    { "LowSide", "LPIRM_OutType;", .constantValue.asLong = 0, 0x4019, -1, 5, -1, -1 },
    { "HighSide", "LPIRM_OutType;", .constantValue.asLong = 0, 0x4019, -1, 6, -1, -1 },
    { "Servo", "LPIRM_OutType;", .constantValue.asLong = 0, 0x4019, -1, 7, -1, -1 },
    { "HalfBridge", "LPIRM_OutType;", .constantValue.asLong = 0, 0x4019, -1, 8, -1, -1 },
    { "value_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "fromString", "LNSString;", "toString", "valueOf", &JreEnum(PIRM_OutType, Logic), &JreEnum(PIRM_OutType, LowSide), &JreEnum(PIRM_OutType, HighSide), &JreEnum(PIRM_OutType, Servo), &JreEnum(PIRM_OutType, HalfBridge), "Ljava/lang/Enum<Lde/pidata/rail/model/OutType;>;" };
  static const J2ObjcClassInfo _PIRM_OutType = { "OutType", "de.pidata.rail.model", ptrTable, methods, fields, 7, 0x4011, 5, 6, -1, -1, -1, 9, -1 };
  return &_PIRM_OutType;
}

+ (void)initialize {
  if (self == [PIRM_OutType class]) {
    JreEnum(PIRM_OutType, Logic) = new_PIRM_OutType_initWithNSString_withNSString_withInt_(@"Logic", JreEnumConstantName(PIRM_OutType_class_(), 0), 0);
    JreEnum(PIRM_OutType, LowSide) = new_PIRM_OutType_initWithNSString_withNSString_withInt_(@"LowSide", JreEnumConstantName(PIRM_OutType_class_(), 1), 1);
    JreEnum(PIRM_OutType, HighSide) = new_PIRM_OutType_initWithNSString_withNSString_withInt_(@"HighSide", JreEnumConstantName(PIRM_OutType_class_(), 2), 2);
    JreEnum(PIRM_OutType, Servo) = new_PIRM_OutType_initWithNSString_withNSString_withInt_(@"Servo", JreEnumConstantName(PIRM_OutType_class_(), 3), 3);
    JreEnum(PIRM_OutType, HalfBridge) = new_PIRM_OutType_initWithNSString_withNSString_withInt_(@"HalfBridge", JreEnumConstantName(PIRM_OutType_class_(), 4), 4);
    J2OBJC_SET_INITIALIZED(PIRM_OutType)
  }
}

@end

void PIRM_OutType_initWithNSString_withNSString_withInt_(PIRM_OutType *self, NSString *value, NSString *__name, jint __ordinal) {
  JavaLangEnum_initWithNSString_withInt_(self, __name, __ordinal);
  self->value_ = value;
}

PIRM_OutType *new_PIRM_OutType_initWithNSString_withNSString_withInt_(NSString *value, NSString *__name, jint __ordinal) {
  J2OBJC_NEW_IMPL(PIRM_OutType, initWithNSString_withNSString_withInt_, value, __name, __ordinal)
}

PIRM_OutType *PIRM_OutType_fromStringWithNSString_(NSString *stringValue) {
  PIRM_OutType_initialize();
  if (stringValue == nil) {
    return nil;
  }
  {
    IOSObjectArray *a__ = PIRM_OutType_values();
    PIRM_OutType * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
    PIRM_OutType * const *e__ = b__ + a__->size_;
    while (b__ < e__) {
      PIRM_OutType *entry_ = *b__++;
      if ([((NSString *) nil_chk(((PIRM_OutType *) nil_chk(entry_))->value_)) isEqual:stringValue]) {
        return entry_;
      }
    }
  }
  return nil;
}

IOSObjectArray *PIRM_OutType_values() {
  PIRM_OutType_initialize();
  return [IOSObjectArray arrayWithObjects:PIRM_OutType_values_ count:5 type:PIRM_OutType_class_()];
}

PIRM_OutType *PIRM_OutType_valueOfWithNSString_(NSString *name) {
  PIRM_OutType_initialize();
  for (int i = 0; i < 5; i++) {
    PIRM_OutType *e = PIRM_OutType_values_[i];
    if ([name isEqual:[e name]]) {
      return e;
    }
  }
  @throw create_JavaLangIllegalArgumentException_initWithNSString_(name);
  return nil;
}

PIRM_OutType *PIRM_OutType_fromOrdinal(NSUInteger ordinal) {
  PIRM_OutType_initialize();
  if (ordinal >= 5) {
    return nil;
  }
  return PIRM_OutType_values_[ordinal];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRM_OutType)

J2OBJC_NAME_MAPPING(PIRM_OutType, "de.pidata.rail.model", "PIRM_")
