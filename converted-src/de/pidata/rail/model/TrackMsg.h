//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/TrackMsg.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailModelTrackMsg")
#ifdef RESTRICT_DePidataRailModelTrackMsg
#define INCLUDE_ALL_DePidataRailModelTrackMsg 0
#else
#define INCLUDE_ALL_DePidataRailModelTrackMsg 1
#endif
#undef RESTRICT_DePidataRailModelTrackMsg

#if !defined (PIRM_TrackMsg_) && (INCLUDE_ALL_DePidataRailModelTrackMsg || defined(INCLUDE_PIRM_TrackMsg))
#define PIRM_TrackMsg_

#define RESTRICT_DePidataRailModelAction 1
#define INCLUDE_PIRM_Action 1
#include "de/pidata/rail/model/Action.h"

@class IOSObjectArray;
@class JavaLangInteger;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRM_TrackMsg : PIRM_Action

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (NSString *)getCmd;

- (jchar)getCmdChar;

- (JavaLangInteger *)getCmdDist;

- (jint)getCmdDistInt;

- (void)setCmdWithNSString:(NSString *)cmd;

- (void)setCmdDistWithJavaLangInteger:(JavaLangInteger *)cmdDist;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (NSString *)getLabel;

- (PIQ_QName *)getTypeImage;

- (NSString *)getValuesString;

@end

J2OBJC_STATIC_INIT(PIRM_TrackMsg)

inline PIQ_Namespace *PIRM_TrackMsg_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRM_TrackMsg_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_TrackMsg, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRM_TrackMsg_get_ID_CMD(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_TrackMsg_ID_CMD;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_TrackMsg, ID_CMD, PIQ_QName *)

inline PIQ_QName *PIRM_TrackMsg_get_ID_CMDDIST(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_TrackMsg_ID_CMDDIST;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_TrackMsg, ID_CMDDIST, PIQ_QName *)

FOUNDATION_EXPORT void PIRM_TrackMsg_initWithPIQ_Key_(PIRM_TrackMsg *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIRM_TrackMsg *new_PIRM_TrackMsg_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_TrackMsg *create_PIRM_TrackMsg_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIRM_TrackMsg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_TrackMsg *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRM_TrackMsg *new_PIRM_TrackMsg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_TrackMsg *create_PIRM_TrackMsg_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRM_TrackMsg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_TrackMsg *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRM_TrackMsg *new_PIRM_TrackMsg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_TrackMsg *create_PIRM_TrackMsg_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIRM_TrackMsg)

@compatibility_alias DePidataRailModelTrackMsg PIRM_TrackMsg;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailModelTrackMsg")
