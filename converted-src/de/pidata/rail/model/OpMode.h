//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/OpMode.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailModelOpMode")
#ifdef RESTRICT_DePidataRailModelOpMode
#define INCLUDE_ALL_DePidataRailModelOpMode 0
#else
#define INCLUDE_ALL_DePidataRailModelOpMode 1
#endif
#undef RESTRICT_DePidataRailModelOpMode

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability"
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (PIRM_OpMode_) && (INCLUDE_ALL_DePidataRailModelOpMode || defined(INCLUDE_PIRM_OpMode))
#define PIRM_OpMode_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;

typedef NS_ENUM(NSUInteger, PIRM_OpMode_Enum) {
  PIRM_OpMode_Enum_Manual = 0,
  PIRM_OpMode_Enum_Halt = 1,
  PIRM_OpMode_Enum_Waiting = 2,
  PIRM_OpMode_Enum_Stop = 3,
  PIRM_OpMode_Enum_Reverse = 4,
  PIRM_OpMode_Enum_Pause = 5,
  PIRM_OpMode_Enum_PauseRev = 6,
  PIRM_OpMode_Enum_Continue = 7,
  PIRM_OpMode_Enum_Drive = 8,
  PIRM_OpMode_Enum_Limit = 9,
  PIRM_OpMode_Enum_Emergency = 10,
};

@interface PIRM_OpMode : JavaLangEnum

#pragma mark Public

+ (PIRM_OpMode *)fromStringWithNSString:(NSString *)stringValue;

- (NSString *)getValue;

- (NSString *)description;

+ (PIRM_OpMode *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (PIRM_OpMode_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(PIRM_OpMode)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT PIRM_OpMode *PIRM_OpMode_values_[];

inline PIRM_OpMode *PIRM_OpMode_get_Manual(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Manual)

inline PIRM_OpMode *PIRM_OpMode_get_Halt(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Halt)

inline PIRM_OpMode *PIRM_OpMode_get_Waiting(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Waiting)

inline PIRM_OpMode *PIRM_OpMode_get_Stop(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Stop)

inline PIRM_OpMode *PIRM_OpMode_get_Reverse(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Reverse)

inline PIRM_OpMode *PIRM_OpMode_get_Pause(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Pause)

inline PIRM_OpMode *PIRM_OpMode_get_PauseRev(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, PauseRev)

inline PIRM_OpMode *PIRM_OpMode_get_Continue(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Continue)

inline PIRM_OpMode *PIRM_OpMode_get_Drive(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Drive)

inline PIRM_OpMode *PIRM_OpMode_get_Limit(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Limit)

inline PIRM_OpMode *PIRM_OpMode_get_Emergency(void);
J2OBJC_ENUM_CONSTANT(PIRM_OpMode, Emergency)

FOUNDATION_EXPORT PIRM_OpMode *PIRM_OpMode_fromStringWithNSString_(NSString *stringValue);

FOUNDATION_EXPORT IOSObjectArray *PIRM_OpMode_values(void);

FOUNDATION_EXPORT PIRM_OpMode *PIRM_OpMode_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT PIRM_OpMode *PIRM_OpMode_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(PIRM_OpMode)

@compatibility_alias DePidataRailModelOpMode PIRM_OpMode;

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_DePidataRailModelOpMode")
