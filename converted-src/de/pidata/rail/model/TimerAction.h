//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/TimerAction.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailModelTimerAction")
#ifdef RESTRICT_DePidataRailModelTimerAction
#define INCLUDE_ALL_DePidataRailModelTimerAction 0
#else
#define INCLUDE_ALL_DePidataRailModelTimerAction 1
#endif
#undef RESTRICT_DePidataRailModelTimerAction

#if !defined (PIRM_TimerAction_) && (INCLUDE_ALL_DePidataRailModelTimerAction || defined(INCLUDE_PIRM_TimerAction))
#define PIRM_TimerAction_

#define RESTRICT_DePidataRailModelAction 1
#define INCLUDE_PIRM_Action 1
#include "de/pidata/rail/model/Action.h"

@class IOSObjectArray;
@class JavaLangInteger;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@class PIRM_StateScript;
@class PIRM_TimerType;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRM_TimerAction : PIRM_Action

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (JavaLangInteger *)getDist;

- (jint)getDistInt;

- (JavaLangInteger *)getMsInterval;

- (jint)getMsIntervalInt;

- (PIRM_StateScript *)getOnState;

- (PIRM_TimerType *)getType;

- (NSString *)getValuesString;

- (void)setMsIntervalWithJavaLangInteger:(JavaLangInteger *)msInterval;

- (void)setOnStateWithPIRM_StateScript:(PIRM_StateScript *)onState;

- (void)setTypeWithPIRM_TimerType:(PIRM_TimerType *)type;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (NSString *)getLabel;

- (PIQ_QName *)getTypeImage;

@end

J2OBJC_STATIC_INIT(PIRM_TimerAction)

inline PIQ_Namespace *PIRM_TimerAction_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRM_TimerAction_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_TimerAction, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRM_TimerAction_get_ID_MSINTERVAL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_TimerAction_ID_MSINTERVAL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_TimerAction, ID_MSINTERVAL, PIQ_QName *)

inline PIQ_QName *PIRM_TimerAction_get_ID_ONSTATE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_TimerAction_ID_ONSTATE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_TimerAction, ID_ONSTATE, PIQ_QName *)

inline PIQ_QName *PIRM_TimerAction_get_ID_TYPE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_TimerAction_ID_TYPE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_TimerAction, ID_TYPE, PIQ_QName *)

FOUNDATION_EXPORT void PIRM_TimerAction_initWithPIQ_Key_(PIRM_TimerAction *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIRM_TimerAction *new_PIRM_TimerAction_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_TimerAction *create_PIRM_TimerAction_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIRM_TimerAction_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_TimerAction *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRM_TimerAction *new_PIRM_TimerAction_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_TimerAction *create_PIRM_TimerAction_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRM_TimerAction_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_TimerAction *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRM_TimerAction *new_PIRM_TimerAction_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_TimerAction *create_PIRM_TimerAction_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIRM_TimerAction)

@compatibility_alias DePidataRailModelTimerAction PIRM_TimerAction;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailModelTimerAction")
