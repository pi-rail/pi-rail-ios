//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/Action.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailModelAction")
#ifdef RESTRICT_DePidataRailModelAction
#define INCLUDE_ALL_DePidataRailModelAction 0
#else
#define INCLUDE_ALL_DePidataRailModelAction 1
#endif
#undef RESTRICT_DePidataRailModelAction

#if !defined (PIRM_Action_) && (INCLUDE_ALL_DePidataRailModelAction || defined(INCLUDE_PIRM_Action))
#define PIRM_Action_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class IOSObjectArray;
@class JavaLangInteger;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@class PIRM_Cfg;
@class PIRM_FireType;
@class PIRM_ItemConn;
@class PIRM_TrackPos;
@protocol PIMR_Filter;
@protocol PIMR_ModelIterator;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRM_Action : PIMR_SequenceModel

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

+ (NSString *)checkIDWithNSString:(NSString *)value;

- (PIRM_Cfg *)getCfg;

- (PIRM_FireType *)getFireOn;

- (PIQ_QName *)getGroup;

- (PIQ_QName *)getId;

- (PIRM_ItemConn *)getItemConn;

- (PIQ_QName *)getItemID;

- (NSString *)getName;

- (JavaLangInteger *)getPollInterval;

- (PIRM_TrackPos *)getTrackPos;

- (void)setGroupWithPIQ_QName:(PIQ_QName *)group;

- (void)setItemIDWithPIQ_QName:(PIQ_QName *)itemID;

- (void)setNameWithNSString:(NSString *)name;

- (void)setTrackPosWithPIRM_TrackPos:(PIRM_TrackPos *)trackPos;

- (NSString *)description;

- (id<PIMR_ModelIterator>)transientChildIterWithPIQ_QName:(PIQ_QName *)relationName
                                          withPIMR_Filter:(id<PIMR_Filter>)filter;

- (id)transientGetWithInt:(jint)transientIndex;

- (void)transientSetWithInt:(jint)transientIndex
                     withId:(id)value;

- (id<PIMT_ComplexType>)transientType;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (NSString *)getLabel;

- (id<PIMR_ModelIterator>)getOutPinListIter;

- (id<PIMR_ModelIterator>)getParamListIter;

- (PIQ_QName *)getTypeImage;

- (NSString *)getValuesString;

- (void)initTransient OBJC_METHOD_FAMILY_NONE;

- (void)setFireOnWithPIRM_FireType:(PIRM_FireType *)value;

- (void)setPollIntervalWithJavaLangInteger:(JavaLangInteger *)value;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIRM_Action)

inline PIQ_Namespace *PIRM_Action_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRM_Action_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRM_Action_get_ID_GROUP(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_GROUP;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_GROUP, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_ID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_ID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_ID, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_ITEMID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_ITEMID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_ITEMID, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_TRACKPOS(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_TRACKPOS;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_TRACKPOS, PIQ_QName *)

inline PIQ_Namespace *PIRM_Action_get_NAMESPACE_GUI(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRM_Action_NAMESPACE_GUI;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, NAMESPACE_GUI, PIQ_Namespace *)

inline id<PIMT_ComplexType> PIRM_Action_get_TRANSIENT_TYPE(void);
inline id<PIMT_ComplexType> PIRM_Action_set_TRANSIENT_TYPE(id<PIMT_ComplexType> value);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT id<PIMT_ComplexType> PIRM_Action_TRANSIENT_TYPE;
J2OBJC_STATIC_FIELD_OBJ(PIRM_Action, TRANSIENT_TYPE, id<PIMT_ComplexType>)

inline PIQ_QName *PIRM_Action_get_ID_LABEL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_LABEL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_LABEL, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_VALUES_STRING(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_VALUES_STRING;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_VALUES_STRING, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_NAME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_NAME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_NAME, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_TYPE_IMAGE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_TYPE_IMAGE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_TYPE_IMAGE, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_OUT_PIN_LIST(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_OUT_PIN_LIST;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_OUT_PIN_LIST, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_PARAM_LIST(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_PARAM_LIST;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_PARAM_LIST, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_POLL_INTERVAL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_POLL_INTERVAL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_POLL_INTERVAL, PIQ_QName *)

inline PIQ_QName *PIRM_Action_get_ID_FIRE_ON(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_Action_ID_FIRE_ON;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_Action, ID_FIRE_ON, PIQ_QName *)

FOUNDATION_EXPORT void PIRM_Action_initWithPIQ_Key_(PIRM_Action *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIRM_Action_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_Action *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRM_Action_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_Action *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT NSString *PIRM_Action_checkIDWithNSString_(NSString *value);

J2OBJC_TYPE_LITERAL_HEADER(PIRM_Action)

@compatibility_alias DePidataRailModelAction PIRM_Action;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailModelAction")
