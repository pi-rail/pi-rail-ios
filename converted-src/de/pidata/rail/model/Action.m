//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/Action.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Filter.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/tree/ModelIteratorChildList.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/complex/DefaultComplexType.h"
#include "de/pidata/models/types/simple/IntegerType.h"
#include "de/pidata/models/types/simple/QNameType.h"
#include "de/pidata/models/types/simple/StringType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/model/Action.h"
#include "de/pidata/rail/model/Cfg.h"
#include "de/pidata/rail/model/EnumAction.h"
#include "de/pidata/rail/model/FireType.h"
#include "de/pidata/rail/model/ItemConn.h"
#include "de/pidata/rail/model/PiRailFactory.h"
#include "de/pidata/rail/model/TrackPos.h"
#include "de/pidata/string/Helper.h"
#include "de/pidata/system/base/SystemManager.h"
#include "java/lang/Integer.h"
#include "java/lang/RuntimeException.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/model/Action must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRM_Action () {
 @public
  NSString *name_;
}

@end

J2OBJC_FIELD_SETTER(PIRM_Action, name_, NSString *)

J2OBJC_INITIALIZED_DEFN(PIRM_Action)

PIQ_Namespace *PIRM_Action_NAMESPACE;
PIQ_QName *PIRM_Action_ID_GROUP;
PIQ_QName *PIRM_Action_ID_ID;
PIQ_QName *PIRM_Action_ID_ITEMID;
PIQ_QName *PIRM_Action_ID_TRACKPOS;
PIQ_Namespace *PIRM_Action_NAMESPACE_GUI;
id<PIMT_ComplexType> PIRM_Action_TRANSIENT_TYPE;
PIQ_QName *PIRM_Action_ID_LABEL;
PIQ_QName *PIRM_Action_ID_VALUES_STRING;
PIQ_QName *PIRM_Action_ID_NAME;
PIQ_QName *PIRM_Action_ID_TYPE_IMAGE;
PIQ_QName *PIRM_Action_ID_OUT_PIN_LIST;
PIQ_QName *PIRM_Action_ID_PARAM_LIST;
PIQ_QName *PIRM_Action_ID_POLL_INTERVAL;
PIQ_QName *PIRM_Action_ID_FIRE_ON;

@implementation PIRM_Action

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_ {
  PIRM_Action_initWithPIQ_Key_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRM_Action_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRM_Action_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getId {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRM_Action_ID_ID], [PIQ_QName class]);
}

- (PIQ_QName *)getItemID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRM_Action_ID_ITEMID], [PIQ_QName class]);
}

- (void)setItemIDWithPIQ_QName:(PIQ_QName *)itemID {
  [self setWithPIQ_QName:PIRM_Action_ID_ITEMID withId:itemID];
}

- (PIQ_QName *)getGroup {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRM_Action_ID_GROUP], [PIQ_QName class]);
}

- (void)setGroupWithPIQ_QName:(PIQ_QName *)group {
  [self setWithPIQ_QName:PIRM_Action_ID_GROUP withId:group];
}

- (PIRM_TrackPos *)getTrackPos {
  return (PIRM_TrackPos *) cast_chk([self getWithPIQ_QName:PIRM_Action_ID_TRACKPOS withPIQ_Key:nil], [PIRM_TrackPos class]);
}

- (void)setTrackPosWithPIRM_TrackPos:(PIRM_TrackPos *)trackPos {
  [self setChildWithPIQ_QName:PIRM_Action_ID_TRACKPOS withPIMR_Model:trackPos];
}

- (void)initTransient {
  [super initTransient];
}

- (id<PIMT_ComplexType>)transientType {
  if (PIRM_Action_TRANSIENT_TYPE == nil) {
    PIMO_DefaultComplexType *type = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_([((PIQ_Namespace *) nil_chk(PIRM_Action_NAMESPACE)) getQNameWithNSString:@"EnumCfg_Transient"], [PIRM_EnumAction_class_() getName], 0, [super transientType]);
    PIRM_Action_TRANSIENT_TYPE = type;
    [type addAttributeTypeWithPIQ_QName:PIRM_Action_ID_LABEL withPIMT_SimpleType:PIME_StringType_getDefString()];
    [type addAttributeTypeWithPIQ_QName:PIRM_Action_ID_VALUES_STRING withPIMT_SimpleType:PIME_StringType_getDefString()];
    [type addAttributeTypeWithPIQ_QName:PIRM_Action_ID_NAME withPIMT_SimpleType:PIME_StringType_getDefString()];
    [type addAttributeTypeWithPIQ_QName:PIRM_Action_ID_TYPE_IMAGE withPIMT_SimpleType:PIME_QNameType_getQName()];
    [type addAttributeTypeWithPIQ_QName:PIRM_Action_ID_POLL_INTERVAL withPIMT_SimpleType:PIME_IntegerType_getDefInt()];
    [type addAttributeTypeWithPIQ_QName:PIRM_Action_ID_FIRE_ON withPIMT_SimpleType:JreLoadStatic(PIRM_PiRailFactory, FIRETYPE)];
    [type addRelationWithPIQ_QName:PIRM_Action_ID_OUT_PIN_LIST withPIMT_Type:JreLoadStatic(PIRM_PiRailFactory, OUTPIN_TYPE) withInt:0 withInt:JavaLangInteger_MAX_VALUE];
    [type addRelationWithPIQ_QName:PIRM_Action_ID_PARAM_LIST withPIMT_Type:JreLoadStatic(PIRM_PiRailFactory, PARAM_TYPE) withInt:0 withInt:JavaLangInteger_MAX_VALUE];
  }
  return PIRM_Action_TRANSIENT_TYPE;
}

- (id)transientGetWithInt:(jint)transientIndex {
  PIQ_QName *attributeName = [((id<PIMT_ComplexType>) nil_chk([self transientType])) getAttributeNameWithInt:transientIndex];
  if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_VALUES_STRING)) {
    return [self getValuesString];
  }
  else if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_LABEL)) {
    return [self getLabel];
  }
  else if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_NAME)) {
    return name_;
  }
  else if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_TYPE_IMAGE)) {
    return [self getTypeImage];
  }
  else if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_POLL_INTERVAL)) {
    return [self getPollInterval];
  }
  else if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_FIRE_ON)) {
    return [self getFireOn];
  }
  else {
    return [super transientGetWithInt:transientIndex];
  }
}

- (void)transientSetWithInt:(jint)transientIndex
                     withId:(id)value {
  PIQ_QName *attributeName = [((id<PIMT_ComplexType>) nil_chk([self transientType])) getAttributeNameWithInt:transientIndex];
  if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_NAME)) {
    self->name_ = (NSString *) cast_chk(value, [NSString class]);
  }
  else if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_POLL_INTERVAL)) {
    [self setPollIntervalWithJavaLangInteger:(JavaLangInteger *) cast_chk(value, [JavaLangInteger class])];
  }
  else if (JreObjectEqualsEquals(attributeName, PIRM_Action_ID_FIRE_ON)) {
    [self setFireOnWithPIRM_FireType:(PIRM_FireType *) cast_chk(value, [PIRM_FireType class])];
  }
  else {
    @throw new_JavaLangRuntimeException_initWithNSString_(JreStrcat("$@$", @"Transient attribute ", attributeName, @" is readonly"));
  }
}

- (void)setPollIntervalWithJavaLangInteger:(JavaLangInteger *)value {
}

- (JavaLangInteger *)getPollInterval {
  return nil;
}

- (void)setFireOnWithPIRM_FireType:(PIRM_FireType *)value {
}

- (PIRM_FireType *)getFireOn {
  return nil;
}

- (id<PIMR_ModelIterator>)transientChildIterWithPIQ_QName:(PIQ_QName *)relationName
                                          withPIMR_Filter:(id<PIMR_Filter>)filter {
  if (JreObjectEqualsEquals(relationName, PIRM_Action_ID_OUT_PIN_LIST)) {
    return [self getOutPinListIter];
  }
  else if (JreObjectEqualsEquals(relationName, PIRM_Action_ID_PARAM_LIST)) {
    return [self getParamListIter];
  }
  else {
    return [super transientChildIterWithPIQ_QName:relationName withPIMR_Filter:filter];
  }
}

- (NSString *)getName {
  return name_;
}

- (void)setNameWithNSString:(NSString *)name {
  self->name_ = name;
}

- (NSString *)getLabel {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (NSString *)description {
  return JreStrcat("$C$", [[self java_getClass] getSimpleName], ' ', [self getLabel]);
}

- (NSString *)getValuesString {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (PIQ_QName *)getTypeImage {
  // can't call an abstract method
  [self doesNotRecognizeSelector:_cmd];
  return 0;
}

- (PIRM_Cfg *)getCfg {
  id<PIMR_Model> parent = [self getParentWithBoolean:false];
  if ([parent isKindOfClass:[PIRM_Cfg class]]) {
    return (PIRM_Cfg *) parent;
  }
  else {
    return nil;
  }
}

- (PIRM_ItemConn *)getItemConn {
  PIRM_Cfg *cfg = [self getCfg];
  if (cfg == nil) {
    return nil;
  }
  return [cfg getItemConnWithPIQ_Key:[self getItemID]];
}

- (id<PIMR_ModelIterator>)getOutPinListIter {
  PIRM_ItemConn *itemConn = [self getItemConn];
  if (itemConn != nil) {
    jint x = 5;
    return [itemConn outPinIter];
  }
  return JreLoadStatic(PIMR_ModelIteratorChildList, EMPTY_ITER);
}

- (id<PIMR_ModelIterator>)getParamListIter {
  PIRM_ItemConn *itemConn = [self getItemConn];
  if (itemConn != nil) {
    return [itemConn paramIter];
  }
  return JreLoadStatic(PIMR_ModelIteratorChildList, EMPTY_ITER);
}

+ (NSString *)checkIDWithNSString:(NSString *)value {
  return PIRM_Action_checkIDWithNSString_(value);
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, 2, -1, -1 },
    { NULL, NULL, 0x4, -1, 3, -1, 4, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 6, -1, -1, -1, -1 },
    { NULL, "LPIRM_TrackPos;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMT_ComplexType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 10, 11, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 14, 15, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x4, 16, 17, -1, -1, -1, -1 },
    { NULL, "LPIRM_FireType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x1, 18, 19, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 20, 21, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x404, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 22, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x404, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x404, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIRM_Cfg;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIRM_ItemConn;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMR_ModelIterator;", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x9, 23, 21, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_Key:);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getId);
  methods[4].selector = @selector(getItemID);
  methods[5].selector = @selector(setItemIDWithPIQ_QName:);
  methods[6].selector = @selector(getGroup);
  methods[7].selector = @selector(setGroupWithPIQ_QName:);
  methods[8].selector = @selector(getTrackPos);
  methods[9].selector = @selector(setTrackPosWithPIRM_TrackPos:);
  methods[10].selector = @selector(initTransient);
  methods[11].selector = @selector(transientType);
  methods[12].selector = @selector(transientGetWithInt:);
  methods[13].selector = @selector(transientSetWithInt:withId:);
  methods[14].selector = @selector(setPollIntervalWithJavaLangInteger:);
  methods[15].selector = @selector(getPollInterval);
  methods[16].selector = @selector(setFireOnWithPIRM_FireType:);
  methods[17].selector = @selector(getFireOn);
  methods[18].selector = @selector(transientChildIterWithPIQ_QName:withPIMR_Filter:);
  methods[19].selector = @selector(getName);
  methods[20].selector = @selector(setNameWithNSString:);
  methods[21].selector = @selector(getLabel);
  methods[22].selector = @selector(description);
  methods[23].selector = @selector(getValuesString);
  methods[24].selector = @selector(getTypeImage);
  methods[25].selector = @selector(getCfg);
  methods[26].selector = @selector(getItemConn);
  methods[27].selector = @selector(getOutPinListIter);
  methods[28].selector = @selector(getParamListIter);
  methods[29].selector = @selector(checkIDWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 24, -1, -1 },
    { "ID_GROUP", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 25, -1, -1 },
    { "ID_ID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 26, -1, -1 },
    { "ID_ITEMID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 27, -1, -1 },
    { "ID_TRACKPOS", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 28, -1, -1 },
    { "NAMESPACE_GUI", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 29, -1, -1 },
    { "TRANSIENT_TYPE", "LPIMT_ComplexType;", .constantValue.asLong = 0, 0x9, -1, 30, -1, -1 },
    { "ID_LABEL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 31, -1, -1 },
    { "ID_VALUES_STRING", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 32, -1, -1 },
    { "ID_NAME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 33, -1, -1 },
    { "ID_TYPE_IMAGE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 34, -1, -1 },
    { "ID_OUT_PIN_LIST", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 35, -1, -1 },
    { "ID_PARAM_LIST", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 36, -1, -1 },
    { "ID_POLL_INTERVAL", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 37, -1, -1 },
    { "ID_FIRE_ON", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 38, -1, -1 },
    { "name_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;", "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setItemID", "LPIQ_QName;", "setGroup", "setTrackPos", "LPIRM_TrackPos;", "transientGet", "I", "transientSet", "ILNSObject;", "setPollInterval", "LJavaLangInteger;", "setFireOn", "LPIRM_FireType;", "transientChildIter", "LPIQ_QName;LPIMR_Filter;", "setName", "LNSString;", "toString", "checkID", &PIRM_Action_NAMESPACE, &PIRM_Action_ID_GROUP, &PIRM_Action_ID_ID, &PIRM_Action_ID_ITEMID, &PIRM_Action_ID_TRACKPOS, &PIRM_Action_NAMESPACE_GUI, &PIRM_Action_TRANSIENT_TYPE, &PIRM_Action_ID_LABEL, &PIRM_Action_ID_VALUES_STRING, &PIRM_Action_ID_NAME, &PIRM_Action_ID_TYPE_IMAGE, &PIRM_Action_ID_OUT_PIN_LIST, &PIRM_Action_ID_PARAM_LIST, &PIRM_Action_ID_POLL_INTERVAL, &PIRM_Action_ID_FIRE_ON };
  static const J2ObjcClassInfo _PIRM_Action = { "Action", "de.pidata.rail.model", ptrTable, methods, fields, 7, 0x401, 30, 16, -1, -1, -1, -1, -1 };
  return &_PIRM_Action;
}

+ (void)initialize {
  if (self == [PIRM_Action class]) {
    PIRM_Action_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/pi-rail.xsd");
    PIRM_Action_ID_GROUP = [((PIQ_Namespace *) nil_chk(PIRM_Action_NAMESPACE)) getQNameWithNSString:@"group"];
    PIRM_Action_ID_ID = [PIRM_Action_NAMESPACE getQNameWithNSString:@"id"];
    PIRM_Action_ID_ITEMID = [PIRM_Action_NAMESPACE getQNameWithNSString:@"itemID"];
    PIRM_Action_ID_TRACKPOS = [PIRM_Action_NAMESPACE getQNameWithNSString:@"trackPos"];
    PIRM_Action_NAMESPACE_GUI = PIQ_Namespace_getInstanceWithNSString_(@"de.pidata.gui");
    PIRM_Action_ID_LABEL = [PIRM_Action_NAMESPACE getQNameWithNSString:@"label"];
    PIRM_Action_ID_VALUES_STRING = [PIRM_Action_NAMESPACE getQNameWithNSString:@"valuesString"];
    PIRM_Action_ID_NAME = [PIRM_Action_NAMESPACE getQNameWithNSString:@"name"];
    PIRM_Action_ID_TYPE_IMAGE = [PIRM_Action_NAMESPACE getQNameWithNSString:@"typeImage"];
    PIRM_Action_ID_OUT_PIN_LIST = [PIRM_Action_NAMESPACE getQNameWithNSString:@"outPinList"];
    PIRM_Action_ID_PARAM_LIST = [PIRM_Action_NAMESPACE getQNameWithNSString:@"paramList"];
    PIRM_Action_ID_POLL_INTERVAL = [PIRM_Action_NAMESPACE getQNameWithNSString:@"pollInterval"];
    PIRM_Action_ID_FIRE_ON = [PIRM_Action_NAMESPACE getQNameWithNSString:@"fireOn"];
    J2OBJC_SET_INITIALIZED(PIRM_Action)
  }
}

@end

void PIRM_Action_initWithPIQ_Key_(PIRM_Action *self, id<PIQ_Key> id_) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, JreLoadStatic(PIRM_PiRailFactory, ACTION_TYPE), nil, nil, nil);
}

void PIRM_Action_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_Action *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRM_PiRailFactory, ACTION_TYPE), attributeNames, anyAttribs, childNames);
}

void PIRM_Action_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_Action *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

NSString *PIRM_Action_checkIDWithNSString_(NSString *value) {
  PIRM_Action_initialize();
  if (PICS_Helper_isNullOrEmptyWithId_(value)) {
    return @"Eine ID (Name) muss mindestens 1 Zeichen lang sein!";
  }
  else if ([((NSString *) nil_chk(value)) java_length] > PIRM_PiRailFactory_ID_MAX_LEN) {
    return JreStrcat("$I$", @"Eine ID (Name) darf maximal ", PIRM_PiRailFactory_ID_MAX_LEN, @" Zeichen lang sein");
  }
  else {
    for (jint i = 0; i < [value java_length]; i++) {
      jchar ch = [value charAtWithInt:i];
      jboolean ok = ((ch >= '0' && ch <= '9')) || ((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z')) || (ch == '_') || (ch == '-');
      if (!ok) {
        return [((PISYSB_SystemManager *) nil_chk(PISYSB_SystemManager_getInstance())) getLocalizedMessageWithNSString:@"invalidCharName" withNSString:nil withJavaUtilProperties:nil];
      }
    }
  }
  return nil;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRM_Action)

J2OBJC_NAME_MAPPING(PIRM_Action, "de.pidata.rail.model", "PIRM_")
