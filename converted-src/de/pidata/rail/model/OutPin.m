//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/OutPin.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/model/IoConn.h"
#include "de/pidata/rail/model/OutMode.h"
#include "de/pidata/rail/model/OutPin.h"
#include "de/pidata/rail/model/OutType.h"
#include "de/pidata/rail/model/PiRailFactory.h"
#include "java/lang/Integer.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/model/OutPin must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIRM_OutPin)

PIQ_Namespace *PIRM_OutPin_NAMESPACE;
PIQ_QName *PIRM_OutPin_ID_MODE;
PIQ_QName *PIRM_OutPin_ID_PULSE;
PIQ_QName *PIRM_OutPin_ID_TYPE;

@implementation PIRM_OutPin

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_ {
  PIRM_OutPin_initWithPIQ_Key_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRM_OutPin_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRM_OutPin_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIRM_OutType *)getType {
  return (PIRM_OutType *) cast_chk([self getWithPIQ_QName:PIRM_OutPin_ID_TYPE], [PIRM_OutType class]);
}

- (void)setTypeWithPIRM_OutType:(PIRM_OutType *)type {
  [self setWithPIQ_QName:PIRM_OutPin_ID_TYPE withId:type];
}

- (PIRM_OutMode *)getMode {
  return (PIRM_OutMode *) cast_chk([self getWithPIQ_QName:PIRM_OutPin_ID_MODE], [PIRM_OutMode class]);
}

- (void)setModeWithPIRM_OutMode:(PIRM_OutMode *)mode {
  [self setWithPIQ_QName:PIRM_OutPin_ID_MODE withId:mode];
}

- (JavaLangInteger *)getPulse {
  return (JavaLangInteger *) cast_chk([self getWithPIQ_QName:PIRM_OutPin_ID_PULSE], [JavaLangInteger class]);
}

- (void)setPulseWithJavaLangInteger:(JavaLangInteger *)pulse {
  [self setWithPIQ_QName:PIRM_OutPin_ID_PULSE withId:pulse];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, 2, -1, -1 },
    { NULL, NULL, 0x4, -1, 3, -1, 4, -1, -1 },
    { NULL, "LPIRM_OutType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 5, 6, -1, -1, -1, -1 },
    { NULL, "LPIRM_OutMode;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 7, 8, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 9, 10, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_Key:);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getType);
  methods[4].selector = @selector(setTypeWithPIRM_OutType:);
  methods[5].selector = @selector(getMode);
  methods[6].selector = @selector(setModeWithPIRM_OutMode:);
  methods[7].selector = @selector(getPulse);
  methods[8].selector = @selector(setPulseWithJavaLangInteger:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 11, -1, -1 },
    { "ID_MODE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 12, -1, -1 },
    { "ID_PULSE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 13, -1, -1 },
    { "ID_TYPE", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 14, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;", "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setType", "LPIRM_OutType;", "setMode", "LPIRM_OutMode;", "setPulse", "LJavaLangInteger;", &PIRM_OutPin_NAMESPACE, &PIRM_OutPin_ID_MODE, &PIRM_OutPin_ID_PULSE, &PIRM_OutPin_ID_TYPE };
  static const J2ObjcClassInfo _PIRM_OutPin = { "OutPin", "de.pidata.rail.model", ptrTable, methods, fields, 7, 0x1, 9, 4, -1, -1, -1, -1, -1 };
  return &_PIRM_OutPin;
}

+ (void)initialize {
  if (self == [PIRM_OutPin class]) {
    PIRM_OutPin_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/pi-rail.xsd");
    PIRM_OutPin_ID_MODE = [((PIQ_Namespace *) nil_chk(PIRM_OutPin_NAMESPACE)) getQNameWithNSString:@"mode"];
    PIRM_OutPin_ID_PULSE = [PIRM_OutPin_NAMESPACE getQNameWithNSString:@"pulse"];
    PIRM_OutPin_ID_TYPE = [PIRM_OutPin_NAMESPACE getQNameWithNSString:@"type"];
    J2OBJC_SET_INITIALIZED(PIRM_OutPin)
  }
}

@end

void PIRM_OutPin_initWithPIQ_Key_(PIRM_OutPin *self, id<PIQ_Key> id_) {
  PIRM_IoConn_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, JreLoadStatic(PIRM_PiRailFactory, OUTPIN_TYPE), nil, nil, nil);
}

PIRM_OutPin *new_PIRM_OutPin_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_NEW_IMPL(PIRM_OutPin, initWithPIQ_Key_, id_)
}

PIRM_OutPin *create_PIRM_OutPin_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_CREATE_IMPL(PIRM_OutPin, initWithPIQ_Key_, id_)
}

void PIRM_OutPin_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_OutPin *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIRM_IoConn_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRM_PiRailFactory, OUTPIN_TYPE), attributeNames, anyAttribs, childNames);
}

PIRM_OutPin *new_PIRM_OutPin_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRM_OutPin, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIRM_OutPin *create_PIRM_OutPin_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRM_OutPin, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIRM_OutPin_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_OutPin *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIRM_IoConn_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIRM_OutPin *new_PIRM_OutPin_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRM_OutPin, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIRM_OutPin *create_PIRM_OutPin_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRM_OutPin, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRM_OutPin)

J2OBJC_NAME_MAPPING(PIRM_OutPin, "de.pidata.rail.model", "PIRM_")
