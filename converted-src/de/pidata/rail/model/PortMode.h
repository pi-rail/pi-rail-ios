//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/PortMode.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailModelPortMode")
#ifdef RESTRICT_DePidataRailModelPortMode
#define INCLUDE_ALL_DePidataRailModelPortMode 0
#else
#define INCLUDE_ALL_DePidataRailModelPortMode 1
#endif
#undef RESTRICT_DePidataRailModelPortMode

#if __has_feature(nullability)
#pragma clang diagnostic push
#pragma GCC diagnostic ignored "-Wnullability"
#pragma GCC diagnostic ignored "-Wnullability-completeness"
#endif

#if !defined (PIRM_PortMode_) && (INCLUDE_ALL_DePidataRailModelPortMode || defined(INCLUDE_PIRM_PortMode))
#define PIRM_PortMode_

#define RESTRICT_JavaLangEnum 1
#define INCLUDE_JavaLangEnum 1
#include "java/lang/Enum.h"

@class IOSObjectArray;

typedef NS_ENUM(NSUInteger, PIRM_PortMode_Enum) {
  PIRM_PortMode_Enum_none = 0,
  PIRM_PortMode_Enum_MotorDriver = 1,
  PIRM_PortMode_Enum_IOExtender = 2,
  PIRM_PortMode_Enum_IDReader = 3,
  PIRM_PortMode_Enum_IR_Sender = 4,
  PIRM_PortMode_Enum_IR_Reader = 5,
  PIRM_PortMode_Enum_NFC_Reader = 6,
  PIRM_PortMode_Enum_DCCDecoder = 7,
  PIRM_PortMode_Enum_SUSI = 8,
};

@interface PIRM_PortMode : JavaLangEnum

#pragma mark Public

+ (PIRM_PortMode *)fromStringWithNSString:(NSString *)stringValue;

- (NSString *)getValue;

- (NSString *)description;

+ (PIRM_PortMode *)valueOfWithNSString:(NSString *)name;

+ (IOSObjectArray *)values;

#pragma mark Package-Private

- (PIRM_PortMode_Enum)toNSEnum;

@end

J2OBJC_STATIC_INIT(PIRM_PortMode)

/*! INTERNAL ONLY - Use enum accessors declared below. */
FOUNDATION_EXPORT PIRM_PortMode *PIRM_PortMode_values_[];

inline PIRM_PortMode *PIRM_PortMode_get_none(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, none)

inline PIRM_PortMode *PIRM_PortMode_get_MotorDriver(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, MotorDriver)

inline PIRM_PortMode *PIRM_PortMode_get_IOExtender(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, IOExtender)

inline PIRM_PortMode *PIRM_PortMode_get_IDReader(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, IDReader)

inline PIRM_PortMode *PIRM_PortMode_get_IR_Sender(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, IR_Sender)

inline PIRM_PortMode *PIRM_PortMode_get_IR_Reader(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, IR_Reader)

inline PIRM_PortMode *PIRM_PortMode_get_NFC_Reader(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, NFC_Reader)

inline PIRM_PortMode *PIRM_PortMode_get_DCCDecoder(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, DCCDecoder)

inline PIRM_PortMode *PIRM_PortMode_get_SUSI(void);
J2OBJC_ENUM_CONSTANT(PIRM_PortMode, SUSI)

FOUNDATION_EXPORT PIRM_PortMode *PIRM_PortMode_fromStringWithNSString_(NSString *stringValue);

FOUNDATION_EXPORT IOSObjectArray *PIRM_PortMode_values(void);

FOUNDATION_EXPORT PIRM_PortMode *PIRM_PortMode_valueOfWithNSString_(NSString *name);

FOUNDATION_EXPORT PIRM_PortMode *PIRM_PortMode_fromOrdinal(NSUInteger ordinal);

J2OBJC_TYPE_LITERAL_HEADER(PIRM_PortMode)

@compatibility_alias DePidataRailModelPortMode PIRM_PortMode;

#endif


#if __has_feature(nullability)
#pragma clang diagnostic pop
#endif
#pragma pop_macro("INCLUDE_ALL_DePidataRailModelPortMode")
