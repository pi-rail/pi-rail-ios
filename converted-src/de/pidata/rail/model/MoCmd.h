//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/MoCmd.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailModelMoCmd")
#ifdef RESTRICT_DePidataRailModelMoCmd
#define INCLUDE_ALL_DePidataRailModelMoCmd 0
#else
#define INCLUDE_ALL_DePidataRailModelMoCmd 1
#endif
#undef RESTRICT_DePidataRailModelMoCmd

#if !defined (PIRM_MoCmd_) && (INCLUDE_ALL_DePidataRailModelMoCmd || defined(INCLUDE_PIRM_MoCmd))
#define PIRM_MoCmd_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class IOSObjectArray;
@class JavaLangBoolean;
@class JavaLangInteger;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRM_MoCmd : PIMR_SequenceModel

#pragma mark Public

- (instancetype)init;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)id_
                         withChar:(jchar)dir
                          withInt:(jint)speed
                      withBoolean:(jboolean)immediate;

- (NSString *)getDir;

- (PIQ_QName *)getId;

- (JavaLangBoolean *)getImmediate;

- (JavaLangInteger *)getSpeed;

- (jint)getSpeedInt;

- (void)setDirWithNSString:(NSString *)dir;

- (void)setIdWithPIQ_QName:(PIQ_QName *)id_;

- (void)setImmediateWithJavaLangBoolean:(JavaLangBoolean *)immediate;

- (void)setSpeedWithJavaLangInteger:(JavaLangInteger *)speed;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIRM_MoCmd)

inline PIQ_Namespace *PIRM_MoCmd_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRM_MoCmd_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_MoCmd, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRM_MoCmd_get_ID_DIR(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_MoCmd_ID_DIR;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_MoCmd, ID_DIR, PIQ_QName *)

inline PIQ_QName *PIRM_MoCmd_get_ID_ID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_MoCmd_ID_ID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_MoCmd, ID_ID, PIQ_QName *)

inline PIQ_QName *PIRM_MoCmd_get_ID_IMMEDIATE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_MoCmd_ID_IMMEDIATE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_MoCmd, ID_IMMEDIATE, PIQ_QName *)

inline PIQ_QName *PIRM_MoCmd_get_ID_SPEED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRM_MoCmd_ID_SPEED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRM_MoCmd, ID_SPEED, PIQ_QName *)

FOUNDATION_EXPORT void PIRM_MoCmd_init(PIRM_MoCmd *self);

FOUNDATION_EXPORT PIRM_MoCmd *new_PIRM_MoCmd_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_MoCmd *create_PIRM_MoCmd_init(void);

FOUNDATION_EXPORT void PIRM_MoCmd_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_MoCmd *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRM_MoCmd *new_PIRM_MoCmd_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_MoCmd *create_PIRM_MoCmd_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRM_MoCmd_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRM_MoCmd *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRM_MoCmd *new_PIRM_MoCmd_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_MoCmd *create_PIRM_MoCmd_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRM_MoCmd_initWithPIQ_QName_withChar_withInt_withBoolean_(PIRM_MoCmd *self, PIQ_QName *id_, jchar dir, jint speed, jboolean immediate);

FOUNDATION_EXPORT PIRM_MoCmd *new_PIRM_MoCmd_initWithPIQ_QName_withChar_withInt_withBoolean_(PIQ_QName *id_, jchar dir, jint speed, jboolean immediate) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRM_MoCmd *create_PIRM_MoCmd_initWithPIQ_QName_withChar_withInt_withBoolean_(PIQ_QName *id_, jchar dir, jint speed, jboolean immediate);

J2OBJC_TYPE_LITERAL_HEADER(PIRM_MoCmd)

@compatibility_alias DePidataRailModelMoCmd PIRM_MoCmd;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailModelMoCmd")
