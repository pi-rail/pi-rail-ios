//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-models/src/de/pidata/rail/model/FunctionType.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/rail/model/FunctionType.h"
#include "java/lang/Enum.h"
#include "java/lang/IllegalArgumentException.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/model/FunctionType must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRM_FunctionType () {
 @public
  NSString *value_;
}

@end

J2OBJC_FIELD_SETTER(PIRM_FunctionType, value_, NSString *)

__attribute__((unused)) static void PIRM_FunctionType_initWithNSString_withNSString_withInt_(PIRM_FunctionType *self, NSString *value, NSString *__name, jint __ordinal);

__attribute__((unused)) static PIRM_FunctionType *new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(NSString *value, NSString *__name, jint __ordinal) NS_RETURNS_RETAINED;

J2OBJC_INITIALIZED_DEFN(PIRM_FunctionType)

PIRM_FunctionType *PIRM_FunctionType_values_[19];

@implementation PIRM_FunctionType

- (NSString *)getValue {
  return self->value_;
}

+ (PIRM_FunctionType *)fromStringWithNSString:(NSString *)stringValue {
  return PIRM_FunctionType_fromStringWithNSString_(stringValue);
}

- (NSString *)description {
  return value_;
}

+ (IOSObjectArray *)values {
  return PIRM_FunctionType_values();
}

+ (PIRM_FunctionType *)valueOfWithNSString:(NSString *)name {
  return PIRM_FunctionType_valueOfWithNSString_(name);
}

- (PIRM_FunctionType_Enum)toNSEnum {
  return (PIRM_FunctionType_Enum)[self ordinal];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIRM_FunctionType;", 0x9, 0, 1, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 2, -1, -1, -1, -1, -1 },
    { NULL, "[LPIRM_FunctionType;", 0x9, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIRM_FunctionType;", 0x9, 3, 1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(getValue);
  methods[1].selector = @selector(fromStringWithNSString:);
  methods[2].selector = @selector(description);
  methods[3].selector = @selector(values);
  methods[4].selector = @selector(valueOfWithNSString:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "Pulse", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 4, -1, -1 },
    { "Switch", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 5, -1, -1 },
    { "Toggle", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 6, -1, -1 },
    { "Light", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 7, -1, -1 },
    { "Direction", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 8, -1, -1 },
    { "Mode", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 9, -1, -1 },
    { "Auto", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 10, -1, -1 },
    { "Disconnect", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 11, -1, -1 },
    { "Sound", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 12, -1, -1 },
    { "Path", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 13, -1, -1 },
    { "Job", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 14, -1, -1 },
    { "Left", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 15, -1, -1 },
    { "Right", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 16, -1, -1 },
    { "Three", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 17, -1, -1 },
    { "Cross", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 18, -1, -1 },
    { "Yturn", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 19, -1, -1 },
    { "Stop", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 20, -1, -1 },
    { "Warn", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 21, -1, -1 },
    { "Sign", "LPIRM_FunctionType;", .constantValue.asLong = 0, 0x4019, -1, 22, -1, -1 },
    { "value_", "LNSString;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "fromString", "LNSString;", "toString", "valueOf", &JreEnum(PIRM_FunctionType, Pulse), &JreEnum(PIRM_FunctionType, Switch), &JreEnum(PIRM_FunctionType, Toggle), &JreEnum(PIRM_FunctionType, Light), &JreEnum(PIRM_FunctionType, Direction), &JreEnum(PIRM_FunctionType, Mode), &JreEnum(PIRM_FunctionType, Auto), &JreEnum(PIRM_FunctionType, Disconnect), &JreEnum(PIRM_FunctionType, Sound), &JreEnum(PIRM_FunctionType, Path), &JreEnum(PIRM_FunctionType, Job), &JreEnum(PIRM_FunctionType, Left), &JreEnum(PIRM_FunctionType, Right), &JreEnum(PIRM_FunctionType, Three), &JreEnum(PIRM_FunctionType, Cross), &JreEnum(PIRM_FunctionType, Yturn), &JreEnum(PIRM_FunctionType, Stop), &JreEnum(PIRM_FunctionType, Warn), &JreEnum(PIRM_FunctionType, Sign), "Ljava/lang/Enum<Lde/pidata/rail/model/FunctionType;>;" };
  static const J2ObjcClassInfo _PIRM_FunctionType = { "FunctionType", "de.pidata.rail.model", ptrTable, methods, fields, 7, 0x4011, 5, 20, -1, -1, -1, 23, -1 };
  return &_PIRM_FunctionType;
}

+ (void)initialize {
  if (self == [PIRM_FunctionType class]) {
    JreEnum(PIRM_FunctionType, Pulse) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Pulse", JreEnumConstantName(PIRM_FunctionType_class_(), 0), 0);
    JreEnum(PIRM_FunctionType, Switch) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Switch", JreEnumConstantName(PIRM_FunctionType_class_(), 1), 1);
    JreEnum(PIRM_FunctionType, Toggle) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Toggle", JreEnumConstantName(PIRM_FunctionType_class_(), 2), 2);
    JreEnum(PIRM_FunctionType, Light) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Light", JreEnumConstantName(PIRM_FunctionType_class_(), 3), 3);
    JreEnum(PIRM_FunctionType, Direction) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Direction", JreEnumConstantName(PIRM_FunctionType_class_(), 4), 4);
    JreEnum(PIRM_FunctionType, Mode) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Mode", JreEnumConstantName(PIRM_FunctionType_class_(), 5), 5);
    JreEnum(PIRM_FunctionType, Auto) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Auto", JreEnumConstantName(PIRM_FunctionType_class_(), 6), 6);
    JreEnum(PIRM_FunctionType, Disconnect) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Disconnect", JreEnumConstantName(PIRM_FunctionType_class_(), 7), 7);
    JreEnum(PIRM_FunctionType, Sound) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Sound", JreEnumConstantName(PIRM_FunctionType_class_(), 8), 8);
    JreEnum(PIRM_FunctionType, Path) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Path", JreEnumConstantName(PIRM_FunctionType_class_(), 9), 9);
    JreEnum(PIRM_FunctionType, Job) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Job", JreEnumConstantName(PIRM_FunctionType_class_(), 10), 10);
    JreEnum(PIRM_FunctionType, Left) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Left", JreEnumConstantName(PIRM_FunctionType_class_(), 11), 11);
    JreEnum(PIRM_FunctionType, Right) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Right", JreEnumConstantName(PIRM_FunctionType_class_(), 12), 12);
    JreEnum(PIRM_FunctionType, Three) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Three", JreEnumConstantName(PIRM_FunctionType_class_(), 13), 13);
    JreEnum(PIRM_FunctionType, Cross) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Cross", JreEnumConstantName(PIRM_FunctionType_class_(), 14), 14);
    JreEnum(PIRM_FunctionType, Yturn) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Yturn", JreEnumConstantName(PIRM_FunctionType_class_(), 15), 15);
    JreEnum(PIRM_FunctionType, Stop) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Stop", JreEnumConstantName(PIRM_FunctionType_class_(), 16), 16);
    JreEnum(PIRM_FunctionType, Warn) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Warn", JreEnumConstantName(PIRM_FunctionType_class_(), 17), 17);
    JreEnum(PIRM_FunctionType, Sign) = new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(@"Sign", JreEnumConstantName(PIRM_FunctionType_class_(), 18), 18);
    J2OBJC_SET_INITIALIZED(PIRM_FunctionType)
  }
}

@end

void PIRM_FunctionType_initWithNSString_withNSString_withInt_(PIRM_FunctionType *self, NSString *value, NSString *__name, jint __ordinal) {
  JavaLangEnum_initWithNSString_withInt_(self, __name, __ordinal);
  self->value_ = value;
}

PIRM_FunctionType *new_PIRM_FunctionType_initWithNSString_withNSString_withInt_(NSString *value, NSString *__name, jint __ordinal) {
  J2OBJC_NEW_IMPL(PIRM_FunctionType, initWithNSString_withNSString_withInt_, value, __name, __ordinal)
}

PIRM_FunctionType *PIRM_FunctionType_fromStringWithNSString_(NSString *stringValue) {
  PIRM_FunctionType_initialize();
  if (stringValue == nil) {
    return nil;
  }
  {
    IOSObjectArray *a__ = PIRM_FunctionType_values();
    PIRM_FunctionType * const *b__ = ((IOSObjectArray *) nil_chk(a__))->buffer_;
    PIRM_FunctionType * const *e__ = b__ + a__->size_;
    while (b__ < e__) {
      PIRM_FunctionType *entry_ = *b__++;
      if ([((NSString *) nil_chk(((PIRM_FunctionType *) nil_chk(entry_))->value_)) isEqual:stringValue]) {
        return entry_;
      }
    }
  }
  return nil;
}

IOSObjectArray *PIRM_FunctionType_values() {
  PIRM_FunctionType_initialize();
  return [IOSObjectArray arrayWithObjects:PIRM_FunctionType_values_ count:19 type:PIRM_FunctionType_class_()];
}

PIRM_FunctionType *PIRM_FunctionType_valueOfWithNSString_(NSString *name) {
  PIRM_FunctionType_initialize();
  for (int i = 0; i < 19; i++) {
    PIRM_FunctionType *e = PIRM_FunctionType_values_[i];
    if ([name isEqual:[e name]]) {
      return e;
    }
  }
  @throw create_JavaLangIllegalArgumentException_initWithNSString_(name);
  return nil;
}

PIRM_FunctionType *PIRM_FunctionType_fromOrdinal(NSUInteger ordinal) {
  PIRM_FunctionType_initialize();
  if (ordinal >= 19) {
    return nil;
  }
  return PIRM_FunctionType_values_[ordinal];
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRM_FunctionType)

J2OBJC_NAME_MAPPING(PIRM_FunctionType, "de.pidata.rail.model", "PIRM_")
