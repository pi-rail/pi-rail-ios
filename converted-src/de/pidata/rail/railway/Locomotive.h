//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/Locomotive.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailRailwayLocomotive")
#ifdef RESTRICT_DePidataRailRailwayLocomotive
#define INCLUDE_ALL_DePidataRailRailwayLocomotive 0
#else
#define INCLUDE_ALL_DePidataRailRailwayLocomotive 1
#endif
#undef RESTRICT_DePidataRailRailwayLocomotive

#if !defined (PIRR_Locomotive_) && (INCLUDE_ALL_DePidataRailRailwayLocomotive || defined(INCLUDE_PIRR_Locomotive))
#define PIRR_Locomotive_

#define RESTRICT_DePidataRailRailwayRailDevice 1
#define INCLUDE_PIRR_RailDevice 1
#include "de/pidata/rail/railway/RailDevice.h"

@class IOSObjectArray;
@class JavaLangInteger;
@class JavaNetInetAddress;
@class JavaUtilHashtable;
@class PIME_DecimalObject;
@class PIMO_DefaultComplexType;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@class PIRM_ActionState;
@class PIRM_Cfg;
@class PIRM_Cmd;
@class PIRM_FunctionType;
@class PIRM_MotorState;
@class PIRM_TrackPos;
@class PIRR_PositionTime;
@class PIRR_RailAction;
@class PIRR_RailBlock;
@class PIRR_RailFunction;
@class PIRR_RailRange;
@class PIRR_RfidEvent;
@class PIRR_Wagon;
@protocol JavaUtilCollection;
@protocol JavaUtilList;
@protocol PIMR_ModelIterator;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRR_Locomotive : PIRR_RailDevice

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (instancetype)initWithPIQ_QName:(PIQ_QName *)id_
           withJavaNetInetAddress:(JavaNetInetAddress *)address
                     withNSString:(NSString *)deviceType;

- (jint)actionCount;

- (id<PIMR_ModelIterator>)actionIter;

- (void)addActionWithPIRR_RailAction:(PIRR_RailAction *)action;

- (void)addRegisteredPositionWithPIRR_PositionTime:(PIRR_PositionTime *)registeredPosition;

- (void)addWagonWithPIRR_Wagon:(PIRR_Wagon *)wagon;

- (jshort)calcTargetSpeed;

- (void)emergencyStop;

- (JavaLangInteger *)getAccel;

- (PIRR_RailAction *)getActionWithPIQ_Key:(id<PIQ_Key>)actionID;

- (id<JavaUtilList>)getActionList;

- (jchar)getClockDir;

- (PIRR_RailBlock *)getCurrentBlock;

- (PIQ_QName *)getCurrentPosID;

- (jint)getCurrentSpeed;

- (PIME_DecimalObject *)getCurrentSpeedPercent;

- (jint)getDCCAddr;

- (jboolean)getDCCFunctionStateWithInt:(jint)fkt;

- (JavaLangInteger *)getDigitalID;

- (jboolean)getDrivingBwd;

- (jboolean)getDrivingFwd;

- (PIRR_RailFunction *)getFunctionWithPIRM_FunctionType:(PIRM_FunctionType *)functionType;

- (PIRR_RailFunction *)getFunctionWithPIQ_QName:(PIQ_QName *)id_;

- (jchar)getLastCmd;

- (PIRM_TrackPos *)getLastEventPos;

- (PIRR_RfidEvent *)getLastRfidEvent;

- (NSString *)getLineSpeed;

- (PIRR_RailRange *)getMainMotor;

- (PIRM_MotorState *)getMainMotorState;

- (PIQ_QName *)getManufacturer;

- (PIRR_RailFunction *)getModeFunction;

- (NSString *)getPartnumber;

- (PIRR_PositionTime *)getRegisteredPositionWithPIQ_Key:(id<PIQ_Key>)registeredPositionID;

- (id<JavaUtilCollection>)getRegisteredPositions;

- (JavaLangInteger *)getSpeedLimit;

- (jint)getSpeedLimitInt;

- (jboolean)getStopped;

- (NSString *)getTargetDir;

- (jchar)getTargetDirChar;

- (JavaLangInteger *)getTargetSpeed;

- (PIME_DecimalObject *)getTargetSpeedPercent;

- (PIRR_Wagon *)getWagonWithPIQ_Key:(id<PIQ_Key>)wagonID;

- (id<JavaUtilList>)getWagonList;

- (jboolean)isAutoDrive;

- (jboolean)isLockerWithPIQ_QName:(PIQ_QName *)lockID;

- (jboolean)needsSifaPingWithPIQ_QName:(PIQ_QName *)myLockID;

- (void)processStateMsg;

- (jint)registeredPositionCount;

- (id<PIMR_ModelIterator>)registeredPositionIter;

- (void)removeActionWithPIRR_RailAction:(PIRR_RailAction *)action;

- (void)removeRegisteredPositionWithPIRR_PositionTime:(PIRR_PositionTime *)registeredPosition;

- (void)removeWagonWithPIRR_Wagon:(PIRR_Wagon *)wagon;

- (void)setAutoDriveWithBoolean:(jboolean)autoDrive;

- (void)setClockDirWithChar:(jchar)clockDir;

- (void)setCmdSentWithPIRM_Cmd:(PIRM_Cmd *)cmdSent;

- (void)setDCCFunctionStateWithInt:(jint)fkt
                       withBoolean:(jboolean)on;

- (void)setDigitalIDWithJavaLangInteger:(JavaLangInteger *)digitalID;

- (void)setLastRfidEventWithPIRR_RfidEvent:(PIRR_RfidEvent *)lastRfidEvent;

- (void)setLockWithBoolean:(jboolean)locked;

- (void)setManufacturerWithPIQ_QName:(PIQ_QName *)manufacturer;

- (void)setPartnumberWithNSString:(NSString *)partnumber;

- (void)setStoppedWithBoolean:(jboolean)stopped;

- (void)setTargetDirWithNSString:(NSString *)targetDir;

- (void)setTargetSpeedPercentWithPIME_DecimalObject:(PIME_DecimalObject *)targetSpeedPercent;

- (NSString *)description;

- (id)transientGetWithInt:(jint)transientIndex;

- (void)transientSetWithInt:(jint)transientIndex
                     withId:(id)value;

- (id<PIMT_ComplexType>)transientType;

- (void)updateConfigWithPIRM_Cfg:(PIRM_Cfg *)cfg;

- (void)updateWagonImageData;

- (jint)wagonCount;

- (id<PIMR_ModelIterator>)wagonIter;

- (void)wagonReadWithPIRR_RailBlock:(PIRR_RailBlock *)railBlock
                     withPIRR_Wagon:(PIRR_Wagon *)wagon;

- (void)wagonReadWithPIRR_Wagon:(PIRR_Wagon *)wagon;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (void)fireEventWithInt:(jint)eventID
                  withId:(id)source
           withPIQ_QName:(PIQ_QName *)elementID
                  withId:(id)oldValue
                  withId:(id)newValue;

- (jint)getLastMsgIndex;

- (void)initTransient OBJC_METHOD_FAMILY_NONE;

- (void)setLastEventWithPIRM_ActionState:(PIRM_ActionState *)lastEvent
                                withLong:(jlong)receiveTime;

- (void)setLineSpeedWithInt:(jint)lineSp
                   withChar:(jchar)clockDir
                    withInt:(jint)dist;

@end

J2OBJC_STATIC_INIT(PIRR_Locomotive)

inline PIQ_Namespace *PIRR_Locomotive_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRR_Locomotive_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRR_Locomotive_get_ID_ACTION(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_ACTION;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_ACTION, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_DIGITALID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_DIGITALID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_DIGITALID, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_LASTRFIDEVENT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_LASTRFIDEVENT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_LASTRFIDEVENT, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_MANUFACTURER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_MANUFACTURER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_MANUFACTURER, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_PARTNUMBER(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_PARTNUMBER;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_PARTNUMBER, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_REGISTEREDPOSITION(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_REGISTEREDPOSITION;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_REGISTEREDPOSITION, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_WAGON(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_WAGON;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_WAGON, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_CURRENT_SPEED_PERCENT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_CURRENT_SPEED_PERCENT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_CURRENT_SPEED_PERCENT, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_TARGET_SPEED_PERCENT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_TARGET_SPEED_PERCENT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_TARGET_SPEED_PERCENT, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_TARGET_SPEED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_TARGET_SPEED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_TARGET_SPEED, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_LINE_SPEED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_LINE_SPEED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_LINE_SPEED, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_SPEED_LIMIT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_SPEED_LIMIT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_SPEED_LIMIT, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_ACCEL(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_ACCEL;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_ACCEL, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_LAST_EVENT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_LAST_EVENT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_LAST_EVENT, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_EVENT_DIST(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_EVENT_DIST;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_EVENT_DIST, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_STOPPED(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_STOPPED;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, STOPPED, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_DRIVING_FWD(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_DRIVING_FWD;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_DRIVING_FWD, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_DRIVING_BWD(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_DRIVING_BWD;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_DRIVING_BWD, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_ICON_LOCK(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_ICON_LOCK;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_ICON_LOCK, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_ICON_PAUSE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_ICON_PAUSE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_ICON_PAUSE, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_ICON_FWD(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_ICON_FWD;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_ICON_FWD, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_ICON_BACK(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_ICON_BACK;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_ICON_BACK, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_ICON_CLOCK_DIR(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_ICON_CLOCK_DIR;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_ICON_CLOCK_DIR, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_ICON_AUTO_DRIVE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_ICON_AUTO_DRIVE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_ICON_AUTO_DRIVE, PIQ_QName *)

inline PIQ_QName *PIRR_Locomotive_get_ID_WAGON_IMAGE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ID_WAGON_IMAGE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ID_WAGON_IMAGE, PIQ_QName *)

inline PIMO_DefaultComplexType *PIRR_Locomotive_get_TRANSIENT_TYPE(void);
inline PIMO_DefaultComplexType *PIRR_Locomotive_set_TRANSIENT_TYPE(PIMO_DefaultComplexType *value);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIMO_DefaultComplexType *PIRR_Locomotive_TRANSIENT_TYPE;
J2OBJC_STATIC_FIELD_OBJ(PIRR_Locomotive, TRANSIENT_TYPE, PIMO_DefaultComplexType *)

inline PIQ_QName *PIRR_Locomotive_get_ACTION_ID_AUTODRIVE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Locomotive_ACTION_ID_AUTODRIVE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Locomotive, ACTION_ID_AUTODRIVE, PIQ_QName *)

FOUNDATION_EXPORT void PIRR_Locomotive_initWithPIQ_Key_(PIRR_Locomotive *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIRR_Locomotive *new_PIRR_Locomotive_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Locomotive *create_PIRR_Locomotive_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIRR_Locomotive_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_Locomotive *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_Locomotive *new_PIRR_Locomotive_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Locomotive *create_PIRR_Locomotive_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRR_Locomotive_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_Locomotive *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_Locomotive *new_PIRR_Locomotive_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Locomotive *create_PIRR_Locomotive_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRR_Locomotive_initWithPIQ_QName_withJavaNetInetAddress_withNSString_(PIRR_Locomotive *self, PIQ_QName *id_, JavaNetInetAddress *address, NSString *deviceType);

FOUNDATION_EXPORT PIRR_Locomotive *new_PIRR_Locomotive_initWithPIQ_QName_withJavaNetInetAddress_withNSString_(PIQ_QName *id_, JavaNetInetAddress *address, NSString *deviceType) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Locomotive *create_PIRR_Locomotive_initWithPIQ_QName_withJavaNetInetAddress_withNSString_(PIQ_QName *id_, JavaNetInetAddress *address, NSString *deviceType);

J2OBJC_TYPE_LITERAL_HEADER(PIRR_Locomotive)

@compatibility_alias DePidataRailRailwayLocomotive PIRR_Locomotive;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailRailwayLocomotive")
