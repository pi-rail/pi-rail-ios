//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/Product.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailRailwayProduct")
#ifdef RESTRICT_DePidataRailRailwayProduct
#define INCLUDE_ALL_DePidataRailRailwayProduct 0
#else
#define INCLUDE_ALL_DePidataRailRailwayProduct 1
#endif
#undef RESTRICT_DePidataRailRailwayProduct

#if !defined (PIRR_Product_) && (INCLUDE_ALL_DePidataRailRailwayProduct || defined(INCLUDE_PIRR_Product))
#define PIRR_Product_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@class PIRM_ItemConn;
@class PIRR_LocalName;
@class PIRR_ProductCfg;
@class PIRR_ProductType;
@protocol JavaUtilCollection;
@protocol PIMR_ModelIterator;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRR_Product : PIMR_SequenceModel

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (void)addLocalNameWithPIRR_LocalName:(PIRR_LocalName *)localName;

- (void)addProductCfgWithPIRR_ProductCfg:(PIRR_ProductCfg *)productCfg;

- (PIQ_QName *)getId;

- (PIRM_ItemConn *)getItemConn;

- (PIRR_LocalName *)getLocalNameWithPIQ_Key:(id<PIQ_Key>)localNameID;

- (id<JavaUtilCollection>)getLocalNames;

- (NSString *)getName;

- (PIRR_ProductCfg *)getProductCfgWithPIQ_Key:(id<PIQ_Key>)productCfgID;

- (id<JavaUtilCollection>)getProductCfgs;

- (PIRR_ProductType *)getType;

- (jint)localNameCount;

- (id<PIMR_ModelIterator>)localNameIter;

- (jint)productCfgCount;

- (id<PIMR_ModelIterator>)productCfgIter;

- (void)removeLocalNameWithPIRR_LocalName:(PIRR_LocalName *)localName;

- (void)removeProductCfgWithPIRR_ProductCfg:(PIRR_ProductCfg *)productCfg;

- (void)setItemConnWithPIRM_ItemConn:(PIRM_ItemConn *)itemConn;

- (void)setNameWithNSString:(NSString *)name;

- (void)setTypeWithPIRR_ProductType:(PIRR_ProductType *)type;

- (id)transientGetWithInt:(jint)transientIndex;

- (id<PIMT_ComplexType>)transientType;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIRR_Product)

inline PIQ_Namespace *PIRR_Product_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRR_Product_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRR_Product_get_ID_ID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Product_ID_ID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, ID_ID, PIQ_QName *)

inline PIQ_QName *PIRR_Product_get_ID_ITEMCONN(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Product_ID_ITEMCONN;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, ID_ITEMCONN, PIQ_QName *)

inline PIQ_QName *PIRR_Product_get_ID_LOCALNAME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Product_ID_LOCALNAME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, ID_LOCALNAME, PIQ_QName *)

inline PIQ_QName *PIRR_Product_get_ID_NAME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Product_ID_NAME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, ID_NAME, PIQ_QName *)

inline PIQ_QName *PIRR_Product_get_ID_PRODUCTCFG(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Product_ID_PRODUCTCFG;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, ID_PRODUCTCFG, PIQ_QName *)

inline PIQ_QName *PIRR_Product_get_ID_TYPE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Product_ID_TYPE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, ID_TYPE, PIQ_QName *)

inline PIQ_QName *PIRR_Product_get_PRODUCT_BLOCK(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Product_PRODUCT_BLOCK;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, PRODUCT_BLOCK, PIQ_QName *)

inline id<PIMT_ComplexType> PIRR_Product_get_TRANSIENT_TYPE(void);
inline id<PIMT_ComplexType> PIRR_Product_set_TRANSIENT_TYPE(id<PIMT_ComplexType> value);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT id<PIMT_ComplexType> PIRR_Product_TRANSIENT_TYPE;
J2OBJC_STATIC_FIELD_OBJ(PIRR_Product, TRANSIENT_TYPE, id<PIMT_ComplexType>)

inline PIQ_QName *PIRR_Product_get_ID_DISPLAYNAME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Product_ID_DISPLAYNAME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Product, ID_DISPLAYNAME, PIQ_QName *)

FOUNDATION_EXPORT void PIRR_Product_initWithPIQ_Key_(PIRR_Product *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIRR_Product *new_PIRR_Product_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Product *create_PIRR_Product_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIRR_Product_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_Product *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_Product *new_PIRR_Product_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Product *create_PIRR_Product_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRR_Product_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_Product *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_Product *new_PIRR_Product_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Product *create_PIRR_Product_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIRR_Product)

@compatibility_alias DePidataRailRailwayProduct PIRR_Product;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailRailwayProduct")
