//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/DataListener.java
//

#include "J2ObjC_source.h"
#include "de/pidata/rail/railway/DataListener.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/railway/DataListener must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRR_DataListener : NSObject

@end

@implementation PIRR_DataListener

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, "V", 0x401, 0, 1, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 2, 3, -1, -1, -1, -1 },
    { NULL, "V", 0x401, 4, 5, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(processDataWithPIRM_Csv:);
  methods[1].selector = @selector(processStateWithPIRM_State:);
  methods[2].selector = @selector(processWifiStateWithPIRO_WifiState:);
  #pragma clang diagnostic pop
  static const void *ptrTable[] = { "processData", "LPIRM_Csv;", "processState", "LPIRM_State;", "processWifiState", "LPIRO_WifiState;" };
  static const J2ObjcClassInfo _PIRR_DataListener = { "DataListener", "de.pidata.rail.railway", ptrTable, methods, NULL, 7, 0x609, 3, 0, -1, -1, -1, -1, -1 };
  return &_PIRR_DataListener;
}

@end

J2OBJC_INTERFACE_TYPE_LITERAL_SOURCE(PIRR_DataListener)

J2OBJC_NAME_MAPPING(PIRR_DataListener, "de.pidata.rail.railway", "PIRR_")
