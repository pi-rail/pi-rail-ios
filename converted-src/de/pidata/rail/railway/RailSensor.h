//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/RailSensor.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailRailwayRailSensor")
#ifdef RESTRICT_DePidataRailRailwayRailSensor
#define INCLUDE_ALL_DePidataRailRailwayRailSensor 0
#else
#define INCLUDE_ALL_DePidataRailRailwayRailSensor 1
#endif
#undef RESTRICT_DePidataRailRailwayRailSensor

#if !defined (PIRR_RailSensor_) && (INCLUDE_ALL_DePidataRailRailwayRailSensor || defined(INCLUDE_PIRR_RailSensor))
#define PIRR_RailSensor_

#define RESTRICT_DePidataRailRailwayRailAction 1
#define INCLUDE_PIRR_RailAction 1
#include "de/pidata/rail/railway/RailAction.h"

@class IOSObjectArray;
@class JavaLangInteger;
@class JavaUtilHashtable;
@class PIME_DateObject;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@class PIRM_Action;
@class PIRM_ActionState;
@class PIRM_Data;
@class PIRM_SensorAction;
@class PIRR_RailDevice;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRR_RailSensor : PIRR_RailAction

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (instancetype)initWithPIRM_SensorAction:(PIRM_SensorAction *)config
                      withPIRR_RailDevice:(PIRR_RailDevice *)railDevice;

- (JavaLangInteger *)getAgo;

- (PIQ_QName *)getCurrentImageIDWithBoolean:(jboolean)diag;

- (PIQ_QName *)getIconStateWithInt:(jint)index
                       withBoolean:(jboolean)diag;

- (JavaLangInteger *)getNum;

- (NSString *)getRead;

- (PIME_DateObject *)getReceiveTime;

- (NSString *)getType;

- (void)invokeActionWithInt:(jint)index;

- (void)processDataWithPIRM_Data:(PIRM_Data *)data;

- (void)processStateWithPIRM_ActionState:(PIRM_ActionState *)actionState
                                withLong:(jlong)receiveTime;

- (void)setAgoWithJavaLangInteger:(JavaLangInteger *)ago;

- (void)setNumWithJavaLangInteger:(JavaLangInteger *)num;

- (void)setReadWithNSString:(NSString *)read;

- (void)setReceiveTimeWithPIME_DateObject:(PIME_DateObject *)receiveTime;

- (void)setTypeWithNSString:(NSString *)type;

- (void)setValueWithChar:(jchar)newValue
                 withInt:(jint)newIntValue
           withPIQ_QName:(PIQ_QName *)lockID;

- (jint)stateCount;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIRM_Action:(PIRM_Action *)arg0
                withPIRR_RailDevice:(PIRR_RailDevice *)arg1 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIRR_RailSensor)

inline PIQ_Namespace *PIRR_RailSensor_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRR_RailSensor_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailSensor, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRR_RailSensor_get_ID_AGO(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_RailSensor_ID_AGO;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailSensor, ID_AGO, PIQ_QName *)

inline PIQ_QName *PIRR_RailSensor_get_ID_NUM(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_RailSensor_ID_NUM;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailSensor, ID_NUM, PIQ_QName *)

inline PIQ_QName *PIRR_RailSensor_get_ID_READ(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_RailSensor_ID_READ;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailSensor, ID_READ, PIQ_QName *)

inline PIQ_QName *PIRR_RailSensor_get_ID_RECEIVETIME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_RailSensor_ID_RECEIVETIME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailSensor, ID_RECEIVETIME, PIQ_QName *)

inline PIQ_QName *PIRR_RailSensor_get_ID_TYPE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_RailSensor_ID_TYPE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailSensor, ID_TYPE, PIQ_QName *)

FOUNDATION_EXPORT void PIRR_RailSensor_initWithPIQ_Key_(PIRR_RailSensor *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIRR_RailSensor *new_PIRR_RailSensor_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_RailSensor *create_PIRR_RailSensor_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIRR_RailSensor_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_RailSensor *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_RailSensor *new_PIRR_RailSensor_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_RailSensor *create_PIRR_RailSensor_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRR_RailSensor_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_RailSensor *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_RailSensor *new_PIRR_RailSensor_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_RailSensor *create_PIRR_RailSensor_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRR_RailSensor_initWithPIRM_SensorAction_withPIRR_RailDevice_(PIRR_RailSensor *self, PIRM_SensorAction *config, PIRR_RailDevice *railDevice);

FOUNDATION_EXPORT PIRR_RailSensor *new_PIRR_RailSensor_initWithPIRM_SensorAction_withPIRR_RailDevice_(PIRM_SensorAction *config, PIRR_RailDevice *railDevice) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_RailSensor *create_PIRR_RailSensor_initWithPIRM_SensorAction_withPIRR_RailDevice_(PIRM_SensorAction *config, PIRR_RailDevice *railDevice);

J2OBJC_TYPE_LITERAL_HEADER(PIRR_RailSensor)

@compatibility_alias DePidataRailRailwayRailSensor PIRR_RailSensor;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailRailwayRailSensor")
