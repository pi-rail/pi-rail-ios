//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/ConnectorPosition.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/railway/ConnectorPosition.h"
#include "de/pidata/rail/railway/Position.h"
#include "de/pidata/rail/railway/RailwayFactory.h"
#include "de/pidata/rail/railway/TrackConnector.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/railway/ConnectorPosition must be compiled with ARC (-fobjc-arc)"
#endif

J2OBJC_INITIALIZED_DEFN(PIRR_ConnectorPosition)

PIQ_Namespace *PIRR_ConnectorPosition_NAMESPACE;
PIQ_QName *PIRR_ConnectorPosition_ID_POSITION;
PIQ_QName *PIRR_ConnectorPosition_ID_TRACKCONNECTOR;

@implementation PIRR_ConnectorPosition

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRR_ConnectorPosition_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRR_ConnectorPosition_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRR_ConnectorPosition_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIRR_Position *)getPosition {
  return (PIRR_Position *) cast_chk([self getWithPIQ_QName:PIRR_ConnectorPosition_ID_POSITION withPIQ_Key:nil], [PIRR_Position class]);
}

- (void)setPositionWithPIRR_Position:(PIRR_Position *)position {
  [self setChildWithPIQ_QName:PIRR_ConnectorPosition_ID_POSITION withPIMR_Model:position];
}

- (PIRR_TrackConnector *)getTrackConnector {
  return (PIRR_TrackConnector *) cast_chk([self getWithPIQ_QName:PIRR_ConnectorPosition_ID_TRACKCONNECTOR withPIQ_Key:nil], [PIRR_TrackConnector class]);
}

- (void)setTrackConnectorWithPIRR_TrackConnector:(PIRR_TrackConnector *)trackConnector {
  [self setChildWithPIQ_QName:PIRR_ConnectorPosition_ID_TRACKCONNECTOR withPIMR_Model:trackConnector];
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, 3, -1, -1 },
    { NULL, "LPIRR_Position;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LPIRR_TrackConnector;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getPosition);
  methods[4].selector = @selector(setPositionWithPIRR_Position:);
  methods[5].selector = @selector(getTrackConnector);
  methods[6].selector = @selector(setTrackConnectorWithPIRR_TrackConnector:);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 8, -1, -1 },
    { "ID_POSITION", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 9, -1, -1 },
    { "ID_TRACKCONNECTOR", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 10, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setPosition", "LPIRR_Position;", "setTrackConnector", "LPIRR_TrackConnector;", &PIRR_ConnectorPosition_NAMESPACE, &PIRR_ConnectorPosition_ID_POSITION, &PIRR_ConnectorPosition_ID_TRACKCONNECTOR };
  static const J2ObjcClassInfo _PIRR_ConnectorPosition = { "ConnectorPosition", "de.pidata.rail.railway", ptrTable, methods, fields, 7, 0x1, 7, 3, -1, -1, -1, -1, -1 };
  return &_PIRR_ConnectorPosition;
}

+ (void)initialize {
  if (self == [PIRR_ConnectorPosition class]) {
    PIRR_ConnectorPosition_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/railway.xsd");
    PIRR_ConnectorPosition_ID_POSITION = [((PIQ_Namespace *) nil_chk(PIRR_ConnectorPosition_NAMESPACE)) getQNameWithNSString:@"position"];
    PIRR_ConnectorPosition_ID_TRACKCONNECTOR = [PIRR_ConnectorPosition_NAMESPACE getQNameWithNSString:@"trackConnector"];
    J2OBJC_SET_INITIALIZED(PIRR_ConnectorPosition)
  }
}

@end

void PIRR_ConnectorPosition_init(PIRR_ConnectorPosition *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(PIRR_RailwayFactory, CONNECTORPOSITION_TYPE), nil, nil, nil);
}

PIRR_ConnectorPosition *new_PIRR_ConnectorPosition_init() {
  J2OBJC_NEW_IMPL(PIRR_ConnectorPosition, init)
}

PIRR_ConnectorPosition *create_PIRR_ConnectorPosition_init() {
  J2OBJC_CREATE_IMPL(PIRR_ConnectorPosition, init)
}

void PIRR_ConnectorPosition_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_ConnectorPosition *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRR_RailwayFactory, CONNECTORPOSITION_TYPE), attributeNames, anyAttribs, childNames);
}

PIRR_ConnectorPosition *new_PIRR_ConnectorPosition_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRR_ConnectorPosition, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIRR_ConnectorPosition *create_PIRR_ConnectorPosition_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRR_ConnectorPosition, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIRR_ConnectorPosition_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_ConnectorPosition *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
}

PIRR_ConnectorPosition *new_PIRR_ConnectorPosition_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRR_ConnectorPosition, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIRR_ConnectorPosition *create_PIRR_ConnectorPosition_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRR_ConnectorPosition, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRR_ConnectorPosition)

J2OBJC_NAME_MAPPING(PIRR_ConnectorPosition, "de.pidata.rail.railway", "PIRR_")
