//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/SwitchBox.java
//

#include "IOSClass.h"
#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/log/Logger.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/Model.h"
#include "de/pidata/models/tree/ModelIterator.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/models/types/complex/DefaultComplexType.h"
#include "de/pidata/models/types/simple/Binary.h"
#include "de/pidata/models/types/simple/DecimalType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/comm/PiRail.h"
#include "de/pidata/rail/model/Cfg.h"
#include "de/pidata/rail/model/EnumAction.h"
#include "de/pidata/rail/model/RangeAction.h"
#include "de/pidata/rail/model/SensorAction.h"
#include "de/pidata/rail/model/State.h"
#include "de/pidata/rail/model/TimerAction.h"
#include "de/pidata/rail/model/TriggerAction.h"
#include "de/pidata/rail/railway/ModelRailway.h"
#include "de/pidata/rail/railway/RailAction.h"
#include "de/pidata/rail/railway/RailDevice.h"
#include "de/pidata/rail/railway/RailDeviceAddress.h"
#include "de/pidata/rail/railway/RailRange.h"
#include "de/pidata/rail/railway/RailwayFactory.h"
#include "de/pidata/rail/railway/SwitchBox.h"
#include "java/lang/Double.h"
#include "java/lang/Integer.h"
#include "java/net/InetAddress.h"
#include "java/util/ArrayList.h"
#include "java/util/Hashtable.h"
#include "java/util/List.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/railway/SwitchBox must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRR_SwitchBox () {
 @public
  id<PIME_Binary> icon_;
  jint servoPos1_;
  jint servoPos2_;
  PIRM_Cfg *cloneConfig_;
}

@end

J2OBJC_FIELD_SETTER(PIRR_SwitchBox, icon_, id<PIME_Binary>)
J2OBJC_FIELD_SETTER(PIRR_SwitchBox, cloneConfig_, PIRM_Cfg *)

J2OBJC_INITIALIZED_DEFN(PIRR_SwitchBox)

PIQ_Namespace *PIRR_SwitchBox_NAMESPACE;
PIQ_QName *PIRR_SwitchBox_ID_SERVO_POS_1;
PIQ_QName *PIRR_SwitchBox_ID_SERVO_POS_2;
PIQ_QName *PIRR_SwitchBox_ID_IO_DESC;
PIMO_DefaultComplexType *PIRR_SwitchBox_TRANSIENT_TYPE;

@implementation PIRR_SwitchBox

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_ {
  PIRR_SwitchBox_initWithPIQ_Key_(self, id_);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRR_SwitchBox_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRR_SwitchBox_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)id_
           withJavaNetInetAddress:(JavaNetInetAddress *)address
                     withNSString:(NSString *)deviceType {
  PIRR_SwitchBox_initWithPIQ_QName_withJavaNetInetAddress_withNSString_(self, id_, address, deviceType);
  return self;
}

- (void)initTransient {
  [super initTransient];
}

- (id<PIMT_ComplexType>)transientType {
  if (PIRR_SwitchBox_TRANSIENT_TYPE == nil) {
    PIMO_DefaultComplexType *type = new_PIMO_DefaultComplexType_initWithPIQ_QName_withNSString_withInt_withPIMT_Type_([((PIQ_Namespace *) nil_chk(PIRR_SwitchBox_NAMESPACE)) getQNameWithNSString:@"Loco_Transient"], [PIRR_SwitchBox_class_() getName], 0, [super transientType]);
    PIRR_SwitchBox_TRANSIENT_TYPE = type;
    [type addAttributeTypeWithPIQ_QName:PIRR_SwitchBox_ID_SERVO_POS_1 withPIMT_SimpleType:PIME_DecimalType_getDefault()];
    [type addAttributeTypeWithPIQ_QName:PIRR_SwitchBox_ID_SERVO_POS_2 withPIMT_SimpleType:PIME_DecimalType_getDefault()];
  }
  return PIRR_SwitchBox_TRANSIENT_TYPE;
}

- (id)transientGetWithInt:(jint)transientIndex {
  PIQ_QName *attributeName = [((id<PIMT_ComplexType>) nil_chk([self transientType])) getAttributeNameWithInt:transientIndex];
  if (JreObjectEqualsEquals(attributeName, PIRR_SwitchBox_ID_SERVO_POS_1)) {
    return JavaLangInteger_valueOfWithInt_(servoPos1_);
  }
  else if (JreObjectEqualsEquals(attributeName, PIRR_SwitchBox_ID_SERVO_POS_2)) {
    return JavaLangInteger_valueOfWithInt_(servoPos2_);
  }
  else {
    return [super transientGetWithInt:transientIndex];
  }
}

- (void)transientSetWithInt:(jint)transientIndex
                     withId:(id)value {
  PIQ_QName *attributeName = [((id<PIMT_ComplexType>) nil_chk([self transientType])) getAttributeNameWithInt:transientIndex];
  if (JreObjectEqualsEquals(attributeName, PIRR_SwitchBox_ID_SERVO_POS_1)) {
    jint newValue = [((JavaLangDouble *) nil_chk(((JavaLangDouble *) cast_chk(value, [JavaLangDouble class])))) intValue];
    jint oldValue = servoPos1_;
    if (newValue != oldValue) {
      servoPos1_ = newValue;
      [self fireDataChangedWithPIQ_QName:PIRR_SwitchBox_ID_SERVO_POS_1 withId:JavaLangInteger_valueOfWithInt_(oldValue) withId:JavaLangInteger_valueOfWithInt_(servoPos1_)];
      PIRM_Cfg *cfg = [self getConfig];
      if (cfg != nil) {
        PIRM_RangeAction *rangeCfg = [cfg getRangeAtWithInt:0];
        if (rangeCfg != nil) {
          jint pos = JreFpToInt(((servoPos1_ * ([rangeCfg getMaxValInt] - [rangeCfg getMinValInt]) / 100.0) + [rangeCfg getMinValInt]));
          PIRR_RailAction *rangeAction = [((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) getRailActionWithPIQ_QName:[self getId] withPIQ_QName:[rangeCfg getId]];
          if ([rangeAction isKindOfClass:[PIRR_RailRange class]]) {
            [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) sendRangeCommandWithPIRR_RailRange:(PIRR_RailRange *) rangeAction withJavaLangInteger:JavaLangInteger_valueOfWithInt_(pos)];
          }
        }
      }
    }
  }
  else if (JreObjectEqualsEquals(attributeName, PIRR_SwitchBox_ID_SERVO_POS_2)) {
    jint newValue = [((JavaLangDouble *) nil_chk(((JavaLangDouble *) cast_chk(value, [JavaLangDouble class])))) intValue];
    jint oldValue = servoPos2_;
    if (newValue != oldValue) {
      servoPos2_ = newValue;
      [self fireDataChangedWithPIQ_QName:PIRR_SwitchBox_ID_SERVO_POS_2 withId:JavaLangInteger_valueOfWithInt_(oldValue) withId:JavaLangInteger_valueOfWithInt_(servoPos2_)];
      PIRM_Cfg *cfg = [self getConfig];
      if (cfg != nil) {
        PIRM_RangeAction *rangeCfg = [cfg getRangeAtWithInt:1];
        if (rangeCfg != nil) {
          jint pos = JreFpToInt(((servoPos2_ * ([rangeCfg getMaxValInt] - [rangeCfg getMinValInt]) / 100.0) + [rangeCfg getMinValInt]));
          PIRR_RailAction *rangeAction = [((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) getRailActionWithPIQ_QName:[self getId] withPIQ_QName:[rangeCfg getId]];
          if ([rangeAction isKindOfClass:[PIRR_RailRange class]]) {
            [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) sendRangeCommandWithPIRR_RailRange:(PIRR_RailRange *) rangeAction withJavaLangInteger:JavaLangInteger_valueOfWithInt_(pos)];
          }
        }
      }
    }
  }
  else {
    [super transientSetWithInt:transientIndex withId:value];
  }
}

- (NSString *)description {
  return JreStrcat("$@", @"SwitchBox@", [self getAddress]);
}

+ (PIRR_SwitchBox *)createSignalTowerWithPIQ_QName:(PIQ_QName *)deviceId {
  return PIRR_SwitchBox_createSignalTowerWithPIQ_QName_(deviceId);
}

- (void)updateConfigWithPIRM_Cfg:(PIRM_Cfg *)cfg {
  configValid_ = true;
  [self setConfigWithPIRM_Cfg:cfg];
  PICL_Logger_debugWithNSString_(JreStrcat("$$$@", @"Update Config for SwitchBox name=", [self getDisplayName], @", version=", [((PIRM_Cfg *) nil_chk(cfg)) getVersion]));
  id<JavaUtilList> updatedIDs = new_JavaUtilArrayList_init();
  PIRR_ModelRailway *modelRailway = [((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway];
  for (PIRM_EnumAction * __strong enumAction in nil_chk([cfg funcIter])) {
    (void) [((PIRR_ModelRailway *) nil_chk(modelRailway)) addOrGetActionWithPIRM_Action:enumAction withPIRR_RailDevice:self];
    [updatedIDs addWithId:[((PIRM_EnumAction *) nil_chk(enumAction)) getId]];
  }
  for (PIRM_RangeAction * __strong rangeAction in nil_chk([cfg rangeIter])) {
    (void) [((PIRR_ModelRailway *) nil_chk(modelRailway)) addOrGetActionWithPIRM_Action:rangeAction withPIRR_RailDevice:self];
    [updatedIDs addWithId:[((PIRM_RangeAction *) nil_chk(rangeAction)) getId]];
  }
  for (PIRM_SensorAction * __strong sensorAction in nil_chk([cfg sensorIter])) {
    (void) [((PIRR_ModelRailway *) nil_chk(modelRailway)) addOrGetActionWithPIRM_Action:sensorAction withPIRR_RailDevice:self];
    [updatedIDs addWithId:[((PIRM_SensorAction *) nil_chk(sensorAction)) getId]];
  }
  for (PIRM_TimerAction * __strong timerAction in nil_chk([cfg timerIter])) {
    (void) [((PIRR_ModelRailway *) nil_chk(modelRailway)) addOrGetActionWithPIRM_Action:timerAction withPIRR_RailDevice:self];
    [updatedIDs addWithId:[((PIRM_TimerAction *) nil_chk(timerAction)) getId]];
  }
  for (PIRM_TriggerAction * __strong triggerAction in nil_chk([cfg triggerIter])) {
    (void) [((PIRR_ModelRailway *) nil_chk(modelRailway)) addOrGetActionWithPIRM_Action:triggerAction withPIRR_RailDevice:self];
    [updatedIDs addWithId:[((PIRM_TriggerAction *) nil_chk(triggerAction)) getId]];
  }
  [((PIRR_ModelRailway *) nil_chk(modelRailway)) updateActionsWithPIRR_RailDevice:self withJavaUtilList:updatedIDs];
}

- (PIRM_Cfg *)getCloneConfig {
  if (self->cloneConfig_ == nil) {
    PIRM_Cfg *originConfig = [self getConfig];
    if (originConfig != nil) {
      cloneConfig_ = (PIRM_Cfg *) cast_chk([originConfig cloneWithPIQ_Key:nil withBoolean:true withBoolean:true], [PIRM_Cfg class]);
    }
  }
  return cloneConfig_;
}

- (void)resetCloneConfig {
  self->cloneConfig_ = nil;
}

- (jint)getLastMsgIndex {
  PIRM_State *state = [self getState];
  if (state == nil) {
    return -1;
  }
  else {
    return [state getNumInt];
  }
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, 0, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 1, -1, 2, -1, -1 },
    { NULL, NULL, 0x4, -1, 3, -1, 4, -1, -1 },
    { NULL, NULL, 0x1, -1, 5, -1, -1, -1, -1 },
    { NULL, "V", 0x4, -1, -1, -1, -1, -1, -1 },
    { NULL, "LPIMT_ComplexType;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LNSObject;", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, 10, -1, -1, -1, -1, -1 },
    { NULL, "LPIRR_SwitchBox;", 0x9, 11, 12, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 13, 14, -1, -1, -1, -1 },
    { NULL, "LPIRM_Cfg;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "I", 0x4, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(initWithPIQ_Key:);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(initWithPIQ_QName:withJavaNetInetAddress:withNSString:);
  methods[4].selector = @selector(initTransient);
  methods[5].selector = @selector(transientType);
  methods[6].selector = @selector(transientGetWithInt:);
  methods[7].selector = @selector(transientSetWithInt:withId:);
  methods[8].selector = @selector(description);
  methods[9].selector = @selector(createSignalTowerWithPIQ_QName:);
  methods[10].selector = @selector(updateConfigWithPIRM_Cfg:);
  methods[11].selector = @selector(getCloneConfig);
  methods[12].selector = @selector(resetCloneConfig);
  methods[13].selector = @selector(getLastMsgIndex);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 15, -1, -1 },
    { "icon_", "LPIME_Binary;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "ID_SERVO_POS_1", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 16, -1, -1 },
    { "ID_SERVO_POS_2", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 17, -1, -1 },
    { "ID_IO_DESC", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 18, -1, -1 },
    { "TRANSIENT_TYPE", "LPIMO_DefaultComplexType;", .constantValue.asLong = 0, 0x9, -1, 19, -1, -1 },
    { "servoPos1_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "servoPos2_", "I", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "cloneConfig_", "LPIRM_Cfg;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;", "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_QName;LJavaNetInetAddress;LNSString;", "transientGet", "I", "transientSet", "ILNSObject;", "toString", "createSignalTower", "LPIQ_QName;", "updateConfig", "LPIRM_Cfg;", &PIRR_SwitchBox_NAMESPACE, &PIRR_SwitchBox_ID_SERVO_POS_1, &PIRR_SwitchBox_ID_SERVO_POS_2, &PIRR_SwitchBox_ID_IO_DESC, &PIRR_SwitchBox_TRANSIENT_TYPE };
  static const J2ObjcClassInfo _PIRR_SwitchBox = { "SwitchBox", "de.pidata.rail.railway", ptrTable, methods, fields, 7, 0x1, 14, 9, -1, -1, -1, -1, -1 };
  return &_PIRR_SwitchBox;
}

+ (void)initialize {
  if (self == [PIRR_SwitchBox class]) {
    PIRR_SwitchBox_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/railway.xsd");
    PIRR_SwitchBox_ID_SERVO_POS_1 = [((PIQ_Namespace *) nil_chk(PIRR_SwitchBox_NAMESPACE)) getQNameWithNSString:@"servoPos1"];
    PIRR_SwitchBox_ID_SERVO_POS_2 = [PIRR_SwitchBox_NAMESPACE getQNameWithNSString:@"servoPos2"];
    PIRR_SwitchBox_ID_IO_DESC = [PIRR_SwitchBox_NAMESPACE getQNameWithNSString:@"ioDesc"];
    J2OBJC_SET_INITIALIZED(PIRR_SwitchBox)
  }
}

@end

void PIRR_SwitchBox_initWithPIQ_Key_(PIRR_SwitchBox *self, id<PIQ_Key> id_) {
  PIRR_RailDevice_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, id_, JreLoadStatic(PIRR_RailwayFactory, SWITCHBOX_TYPE), nil, nil, nil);
  self->servoPos1_ = 0;
  self->servoPos2_ = 0;
  self->cloneConfig_ = nil;
}

PIRR_SwitchBox *new_PIRR_SwitchBox_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_NEW_IMPL(PIRR_SwitchBox, initWithPIQ_Key_, id_)
}

PIRR_SwitchBox *create_PIRR_SwitchBox_initWithPIQ_Key_(id<PIQ_Key> id_) {
  J2OBJC_CREATE_IMPL(PIRR_SwitchBox, initWithPIQ_Key_, id_)
}

void PIRR_SwitchBox_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_SwitchBox *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIRR_RailDevice_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRR_RailwayFactory, SWITCHBOX_TYPE), attributeNames, anyAttribs, childNames);
  self->servoPos1_ = 0;
  self->servoPos2_ = 0;
  self->cloneConfig_ = nil;
}

PIRR_SwitchBox *new_PIRR_SwitchBox_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRR_SwitchBox, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIRR_SwitchBox *create_PIRR_SwitchBox_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRR_SwitchBox, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIRR_SwitchBox_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_SwitchBox *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIRR_RailDevice_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  self->servoPos1_ = 0;
  self->servoPos2_ = 0;
  self->cloneConfig_ = nil;
}

PIRR_SwitchBox *new_PIRR_SwitchBox_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRR_SwitchBox, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIRR_SwitchBox *create_PIRR_SwitchBox_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRR_SwitchBox, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

void PIRR_SwitchBox_initWithPIQ_QName_withJavaNetInetAddress_withNSString_(PIRR_SwitchBox *self, PIQ_QName *id_, JavaNetInetAddress *address, NSString *deviceType) {
  PIRR_SwitchBox_initWithPIQ_Key_(self, id_);
  [self setAddressWithPIRR_RailDeviceAddress:new_PIRR_RailDeviceAddress_initWithJavaNetInetAddress_(address)];
  [self setDeviceTypeWithNSString:deviceType];
}

PIRR_SwitchBox *new_PIRR_SwitchBox_initWithPIQ_QName_withJavaNetInetAddress_withNSString_(PIQ_QName *id_, JavaNetInetAddress *address, NSString *deviceType) {
  J2OBJC_NEW_IMPL(PIRR_SwitchBox, initWithPIQ_QName_withJavaNetInetAddress_withNSString_, id_, address, deviceType)
}

PIRR_SwitchBox *create_PIRR_SwitchBox_initWithPIQ_QName_withJavaNetInetAddress_withNSString_(PIQ_QName *id_, JavaNetInetAddress *address, NSString *deviceType) {
  J2OBJC_CREATE_IMPL(PIRR_SwitchBox, initWithPIQ_QName_withJavaNetInetAddress_withNSString_, id_, address, deviceType)
}

PIRR_SwitchBox *PIRR_SwitchBox_createSignalTowerWithPIQ_QName_(PIQ_QName *deviceId) {
  PIRR_SwitchBox_initialize();
  PIRR_SwitchBox *tower = new_PIRR_SwitchBox_initWithPIQ_Key_(deviceId);
  return tower;
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRR_SwitchBox)

J2OBJC_NAME_MAPPING(PIRR_SwitchBox, "de.pidata.rail.railway", "PIRR_")
