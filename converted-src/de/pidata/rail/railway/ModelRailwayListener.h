//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/ModelRailwayListener.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailRailwayModelRailwayListener")
#ifdef RESTRICT_DePidataRailRailwayModelRailwayListener
#define INCLUDE_ALL_DePidataRailRailwayModelRailwayListener 0
#else
#define INCLUDE_ALL_DePidataRailRailwayModelRailwayListener 1
#endif
#undef RESTRICT_DePidataRailRailwayModelRailwayListener

#if !defined (PIRR_ModelRailwayListener_) && (INCLUDE_ALL_DePidataRailRailwayModelRailwayListener || defined(INCLUDE_PIRR_ModelRailwayListener))
#define PIRR_ModelRailwayListener_

@class PIRM_ActionState;
@class PIRR_RailAction;

@protocol PIRR_ModelRailwayListener < JavaObject >

- (void)stateChangedWithPIRR_RailAction:(PIRR_RailAction *)changedAction
                   withPIRM_ActionState:(PIRM_ActionState *)newState;

@end

J2OBJC_EMPTY_STATIC_INIT(PIRR_ModelRailwayListener)

J2OBJC_TYPE_LITERAL_HEADER(PIRR_ModelRailwayListener)

#define DePidataRailRailwayModelRailwayListener PIRR_ModelRailwayListener

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailRailwayModelRailwayListener")
