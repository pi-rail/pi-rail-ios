//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/Extension.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailRailwayExtension")
#ifdef RESTRICT_DePidataRailRailwayExtension
#define INCLUDE_ALL_DePidataRailRailwayExtension 0
#else
#define INCLUDE_ALL_DePidataRailRailwayExtension 1
#endif
#undef RESTRICT_DePidataRailRailwayExtension

#if !defined (PIRR_Extension_) && (INCLUDE_ALL_DePidataRailRailwayExtension || defined(INCLUDE_PIRR_Extension))
#define PIRR_Extension_

#define RESTRICT_DePidataModelsTreeSequenceModel 1
#define INCLUDE_PIMR_SequenceModel 1
#include "de/pidata/models/tree/SequenceModel.h"

@class IOSObjectArray;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@class PIRM_ExtCfg;
@class PIRM_PortMode;
@class PIRR_LocalName;
@class PIRR_Product;
@protocol JavaUtilCollection;
@protocol PIMR_ModelIterator;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRR_Extension : PIMR_SequenceModel

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (void)addLocalNameWithPIRR_LocalName:(PIRR_LocalName *)localName;

- (void)addProductWithPIRR_Product:(PIRR_Product *)product;

- (PIRM_ExtCfg *)getExtCfg;

- (PIQ_QName *)getId;

- (PIRR_LocalName *)getLocalNameWithPIQ_Key:(id<PIQ_Key>)localNameID;

- (id<JavaUtilCollection>)getLocalNames;

- (NSString *)getName;

- (PIRR_Product *)getProductWithPIQ_Key:(id<PIQ_Key>)productID;

- (id<JavaUtilCollection>)getProducts;

- (PIRM_PortMode *)getType;

- (jint)localNameCount;

- (id<PIMR_ModelIterator>)localNameIter;

- (jint)productCount;

- (id<PIMR_ModelIterator>)productIter;

- (void)removeLocalNameWithPIRR_LocalName:(PIRR_LocalName *)localName;

- (void)removeProductWithPIRR_Product:(PIRR_Product *)product;

- (void)setExtCfgWithPIRM_ExtCfg:(PIRM_ExtCfg *)extCfg;

- (void)setNameWithNSString:(NSString *)name;

- (void)setTypeWithPIRM_PortMode:(PIRM_PortMode *)type;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)arg0
           withPIMT_ComplexType:(id<PIMT_ComplexType>)arg1
              withNSObjectArray:(IOSObjectArray *)arg2
             withPIMR_ChildList:(PIMR_ChildList *)arg3 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIRR_Extension)

inline PIQ_Namespace *PIRR_Extension_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRR_Extension_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Extension, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRR_Extension_get_ID_EXTCFG(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Extension_ID_EXTCFG;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Extension, ID_EXTCFG, PIQ_QName *)

inline PIQ_QName *PIRR_Extension_get_ID_ID(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Extension_ID_ID;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Extension, ID_ID, PIQ_QName *)

inline PIQ_QName *PIRR_Extension_get_ID_LOCALNAME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Extension_ID_LOCALNAME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Extension, ID_LOCALNAME, PIQ_QName *)

inline PIQ_QName *PIRR_Extension_get_ID_NAME(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Extension_ID_NAME;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Extension, ID_NAME, PIQ_QName *)

inline PIQ_QName *PIRR_Extension_get_ID_PRODUCT(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Extension_ID_PRODUCT;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Extension, ID_PRODUCT, PIQ_QName *)

inline PIQ_QName *PIRR_Extension_get_ID_TYPE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_Extension_ID_TYPE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_Extension, ID_TYPE, PIQ_QName *)

FOUNDATION_EXPORT void PIRR_Extension_initWithPIQ_Key_(PIRR_Extension *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIRR_Extension *new_PIRR_Extension_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Extension *create_PIRR_Extension_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIRR_Extension_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_Extension *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_Extension *new_PIRR_Extension_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Extension *create_PIRR_Extension_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRR_Extension_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_Extension *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_Extension *new_PIRR_Extension_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_Extension *create_PIRR_Extension_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

J2OBJC_TYPE_LITERAL_HEADER(PIRR_Extension)

@compatibility_alias DePidataRailRailwayExtension PIRR_Extension;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailRailwayExtension")
