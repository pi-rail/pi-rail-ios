//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/RailRange.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataRailRailwayRailRange")
#ifdef RESTRICT_DePidataRailRailwayRailRange
#define INCLUDE_ALL_DePidataRailRailwayRailRange 0
#else
#define INCLUDE_ALL_DePidataRailRailwayRailRange 1
#endif
#undef RESTRICT_DePidataRailRailwayRailRange

#if !defined (PIRR_RailRange_) && (INCLUDE_ALL_DePidataRailRailwayRailRange || defined(INCLUDE_PIRR_RailRange))
#define PIRR_RailRange_

#define RESTRICT_DePidataRailRailwayRailAction 1
#define INCLUDE_PIRR_RailAction 1
#include "de/pidata/rail/railway/RailAction.h"

@class IOSObjectArray;
@class JavaLangInteger;
@class JavaUtilHashtable;
@class PIMR_ChildList;
@class PIQ_Namespace;
@class PIQ_QName;
@class PIRM_Action;
@class PIRM_ActionState;
@class PIRM_RangeAction;
@class PIRR_RailDevice;
@protocol PIMT_ComplexType;
@protocol PIQ_Key;

@interface PIRR_RailRange : PIRR_RailAction

#pragma mark Public

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)id_;

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

- (instancetype)initWithPIRM_RangeAction:(PIRM_RangeAction *)config
                     withPIRR_RailDevice:(PIRR_RailDevice *)railDevice;

- (NSString *)getCurrentValue;

- (PIQ_QName *)getIconStateWithInt:(jint)index
                       withBoolean:(jboolean)diag;

- (JavaLangInteger *)getState;

- (JavaLangInteger *)getTarget;

- (jint)getTargetInt;

- (void)invokeActionWithInt:(jint)index;

- (void)processStateWithPIRM_ActionState:(PIRM_ActionState *)actionState
                                withLong:(jlong)receiveTime;

- (void)setStateWithJavaLangInteger:(JavaLangInteger *)state;

- (void)setTargetWithJavaLangInteger:(JavaLangInteger *)target;

- (void)setValueWithChar:(jchar)newValue
                 withInt:(jint)newIntValue
           withPIQ_QName:(PIQ_QName *)lockID;

- (jint)stateCount;

#pragma mark Protected

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames;

// Disallowed inherited constructors, do not use.

- (instancetype)initWithPIRM_Action:(PIRM_Action *)arg0
                withPIRR_RailDevice:(PIRR_RailDevice *)arg1 NS_UNAVAILABLE;

@end

J2OBJC_STATIC_INIT(PIRR_RailRange)

inline PIQ_Namespace *PIRR_RailRange_get_NAMESPACE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_Namespace *PIRR_RailRange_NAMESPACE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailRange, NAMESPACE, PIQ_Namespace *)

inline PIQ_QName *PIRR_RailRange_get_ID_STATE(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_RailRange_ID_STATE;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailRange, ID_STATE, PIQ_QName *)

inline PIQ_QName *PIRR_RailRange_get_ID_TARGET(void);
/*! INTERNAL ONLY - Use accessor function from above. */
FOUNDATION_EXPORT PIQ_QName *PIRR_RailRange_ID_TARGET;
J2OBJC_STATIC_FIELD_OBJ_FINAL(PIRR_RailRange, ID_TARGET, PIQ_QName *)

FOUNDATION_EXPORT void PIRR_RailRange_initWithPIQ_Key_(PIRR_RailRange *self, id<PIQ_Key> id_);

FOUNDATION_EXPORT PIRR_RailRange *new_PIRR_RailRange_initWithPIQ_Key_(id<PIQ_Key> id_) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_RailRange *create_PIRR_RailRange_initWithPIQ_Key_(id<PIQ_Key> id_);

FOUNDATION_EXPORT void PIRR_RailRange_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_RailRange *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_RailRange *new_PIRR_RailRange_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_RailRange *create_PIRR_RailRange_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRR_RailRange_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_RailRange *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT PIRR_RailRange *new_PIRR_RailRange_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_RailRange *create_PIRR_RailRange_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames);

FOUNDATION_EXPORT void PIRR_RailRange_initWithPIRM_RangeAction_withPIRR_RailDevice_(PIRR_RailRange *self, PIRM_RangeAction *config, PIRR_RailDevice *railDevice);

FOUNDATION_EXPORT PIRR_RailRange *new_PIRR_RailRange_initWithPIRM_RangeAction_withPIRR_RailDevice_(PIRM_RangeAction *config, PIRR_RailDevice *railDevice) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT PIRR_RailRange *create_PIRR_RailRange_initWithPIRM_RangeAction_withPIRR_RailDevice_(PIRM_RangeAction *config, PIRR_RailDevice *railDevice);

J2OBJC_TYPE_LITERAL_HEADER(PIRR_RailRange)

@compatibility_alias DePidataRailRailwayRailRange PIRR_RailRange;

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataRailRailwayRailRange")
