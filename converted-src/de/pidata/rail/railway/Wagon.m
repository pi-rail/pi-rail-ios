//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-base/pi-rail-railway/src/de/pidata/rail/railway/Wagon.java
//

#include "IOSObjectArray.h"
#include "J2ObjC_source.h"
#include "de/pidata/gui/component/base/ComponentBitmap.h"
#include "de/pidata/gui/component/base/GuiBuilder.h"
#include "de/pidata/models/tree/ChildList.h"
#include "de/pidata/models/tree/SequenceModel.h"
#include "de/pidata/models/types/ComplexType.h"
#include "de/pidata/qnames/Key.h"
#include "de/pidata/qnames/Namespace.h"
#include "de/pidata/qnames/QName.h"
#include "de/pidata/rail/comm/PiRail.h"
#include "de/pidata/rail/railway/ModelRailway.h"
#include "de/pidata/rail/railway/RailBlock.h"
#include "de/pidata/rail/railway/RailwayFactory.h"
#include "de/pidata/rail/railway/Wagon.h"
#include "de/pidata/rail/track/WagonCfg.h"
#include "java/lang/Integer.h"
#include "java/lang/Long.h"
#include "java/util/Hashtable.h"

#if !__has_feature(objc_arc)
#error "de/pidata/rail/railway/Wagon must be compiled with ARC (-fobjc-arc)"
#endif

@interface PIRR_Wagon () {
 @public
  PIQ_QName *imageID_;
  DePidataRailTrackWagonCfg *wagonCfg_;
}

@end

J2OBJC_FIELD_SETTER(PIRR_Wagon, imageID_, PIQ_QName *)
J2OBJC_FIELD_SETTER(PIRR_Wagon, wagonCfg_, DePidataRailTrackWagonCfg *)

J2OBJC_INITIALIZED_DEFN(PIRR_Wagon)

PIQ_Namespace *PIRR_Wagon_NAMESPACE;
PIQ_QName *PIRR_Wagon_ID_ID;
PIQ_QName *PIRR_Wagon_ID_LASTBLOCKID;
PIQ_QName *PIRR_Wagon_ID_LASTBLOCKMS;
PIQ_QName *PIRR_Wagon_ID_LEN;
PIQ_QName *PIRR_Wagon_ID_NAME;
PIQ_QName *PIRR_Wagon_ID_TAGPOS;

@implementation PIRR_Wagon

J2OBJC_IGNORE_DESIGNATED_BEGIN
- (instancetype)init {
  PIRR_Wagon_init(self);
  return self;
}
J2OBJC_IGNORE_DESIGNATED_END

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRR_Wagon_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, attributeNames, anyAttribs, childNames);
  return self;
}

- (instancetype)initWithPIQ_Key:(id<PIQ_Key>)key
           withPIMT_ComplexType:(id<PIMT_ComplexType>)type
              withNSObjectArray:(IOSObjectArray *)attributeNames
          withJavaUtilHashtable:(JavaUtilHashtable *)anyAttribs
             withPIMR_ChildList:(PIMR_ChildList *)childNames {
  PIRR_Wagon_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  return self;
}

- (PIQ_QName *)getId {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRR_Wagon_ID_ID], [PIQ_QName class]);
}

- (void)setIdWithPIQ_QName:(PIQ_QName *)id_ {
  [self setWithPIQ_QName:PIRR_Wagon_ID_ID withId:id_];
}

- (NSString *)getName {
  return (NSString *) cast_chk([self getWithPIQ_QName:PIRR_Wagon_ID_NAME], [NSString class]);
}

- (void)setNameWithNSString:(NSString *)name {
  [self setWithPIQ_QName:PIRR_Wagon_ID_NAME withId:name];
}

- (JavaLangInteger *)getLen {
  return (JavaLangInteger *) cast_chk([self getWithPIQ_QName:PIRR_Wagon_ID_LEN], [JavaLangInteger class]);
}

- (void)setLenWithJavaLangInteger:(JavaLangInteger *)len {
  [self setWithPIQ_QName:PIRR_Wagon_ID_LEN withId:len];
}

- (JavaLangInteger *)getTagPos {
  return (JavaLangInteger *) cast_chk([self getWithPIQ_QName:PIRR_Wagon_ID_TAGPOS], [JavaLangInteger class]);
}

- (void)setTagPosWithJavaLangInteger:(JavaLangInteger *)tagPos {
  [self setWithPIQ_QName:PIRR_Wagon_ID_TAGPOS withId:tagPos];
}

- (PIQ_QName *)getLastBlockID {
  return (PIQ_QName *) cast_chk([self getWithPIQ_QName:PIRR_Wagon_ID_LASTBLOCKID], [PIQ_QName class]);
}

- (void)setLastBlockIDWithPIQ_QName:(PIQ_QName *)lastBlockID {
  [self setWithPIQ_QName:PIRR_Wagon_ID_LASTBLOCKID withId:lastBlockID];
}

- (JavaLangLong *)getLastBlockMs {
  return (JavaLangLong *) cast_chk([self getWithPIQ_QName:PIRR_Wagon_ID_LASTBLOCKMS], [JavaLangLong class]);
}

- (void)setLastBlockMsWithJavaLangLong:(JavaLangLong *)lastBlockMs {
  [self setWithPIQ_QName:PIRR_Wagon_ID_LASTBLOCKMS withId:lastBlockMs];
}

- (instancetype)initWithPIQ_QName:(PIQ_QName *)wagonID
                    withPIQ_QName:(PIQ_QName *)wagonImageID {
  PIRR_Wagon_initWithPIQ_QName_withPIQ_QName_(self, wagonID, wagonImageID);
  return self;
}

- (void)init__WithDePidataRailTrackWagonCfg:(DePidataRailTrackWagonCfg *)wagonCfg {
  [self setNameWithNSString:[((DePidataRailTrackWagonCfg *) nil_chk(wagonCfg)) getName]];
  [self setLenWithJavaLangInteger:[wagonCfg getLen]];
  [self setTagPosWithJavaLangInteger:[wagonCfg getTagPos]];
  self->wagonCfg_ = wagonCfg;
}

- (void)setLastBlockWithPIRR_RailBlock:(PIRR_RailBlock *)railBlock
                              withLong:(jlong)eventTime {
  PIQ_QName *lastBlockID = [self getLastBlockID];
  if ((lastBlockID != nil) && (!JreObjectEqualsEquals(lastBlockID, [((PIRR_RailBlock *) nil_chk(railBlock)) getId]))) {
    PIRR_RailBlock *lastBlock = [((PIRR_ModelRailway *) nil_chk([((PIRO_PiRail *) nil_chk(PIRO_PiRail_getInstance())) getModelRailway])) getOrCreateBlockWithPIQ_QName:lastBlockID];
    [((PIRR_RailBlock *) nil_chk(lastBlock)) leftWithPIRR_Wagon:self];
  }
  [self setLastBlockIDWithPIQ_QName:[((PIRR_RailBlock *) nil_chk(railBlock)) getId]];
  [self setLastBlockMsWithJavaLangLong:JavaLangLong_valueOfWithLong_(eventTime)];
}

- (id<PIGB_ComponentBitmap>)getWagonImage {
  return PIRR_ModelRailway_getBitmapOrMissingWithPIQ_QName_withPIQ_QName_(imageID_, [((PIQ_Namespace *) nil_chk(JreLoadStatic(PIGB_GuiBuilder, NAMESPACE))) getQNameWithNSString:@"icons/missing.png"]);
}

- (DePidataRailTrackWagonCfg *)getWagonCfg {
  return wagonCfg_;
}

+ (const J2ObjcClassInfo *)__metadata {
  static J2ObjcMethodInfo methods[] = {
    { NULL, NULL, 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 0, -1, 1, -1, -1 },
    { NULL, NULL, 0x4, -1, 2, -1, 3, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 4, 5, -1, -1, -1, -1 },
    { NULL, "LNSString;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 6, 7, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 8, 9, -1, -1, -1, -1 },
    { NULL, "LJavaLangInteger;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 10, 9, -1, -1, -1, -1 },
    { NULL, "LPIQ_QName;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 11, 5, -1, -1, -1, -1 },
    { NULL, "LJavaLangLong;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 12, 13, -1, -1, -1, -1 },
    { NULL, NULL, 0x1, -1, 14, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 15, 16, -1, -1, -1, -1 },
    { NULL, "V", 0x1, 17, 18, -1, -1, -1, -1 },
    { NULL, "LPIGB_ComponentBitmap;", 0x1, -1, -1, -1, -1, -1, -1 },
    { NULL, "LDePidataRailTrackWagonCfg;", 0x1, -1, -1, -1, -1, -1, -1 },
  };
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wobjc-multiple-method-names"
  #pragma clang diagnostic ignored "-Wundeclared-selector"
  methods[0].selector = @selector(init);
  methods[1].selector = @selector(initWithPIQ_Key:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[2].selector = @selector(initWithPIQ_Key:withPIMT_ComplexType:withNSObjectArray:withJavaUtilHashtable:withPIMR_ChildList:);
  methods[3].selector = @selector(getId);
  methods[4].selector = @selector(setIdWithPIQ_QName:);
  methods[5].selector = @selector(getName);
  methods[6].selector = @selector(setNameWithNSString:);
  methods[7].selector = @selector(getLen);
  methods[8].selector = @selector(setLenWithJavaLangInteger:);
  methods[9].selector = @selector(getTagPos);
  methods[10].selector = @selector(setTagPosWithJavaLangInteger:);
  methods[11].selector = @selector(getLastBlockID);
  methods[12].selector = @selector(setLastBlockIDWithPIQ_QName:);
  methods[13].selector = @selector(getLastBlockMs);
  methods[14].selector = @selector(setLastBlockMsWithJavaLangLong:);
  methods[15].selector = @selector(initWithPIQ_QName:withPIQ_QName:);
  methods[16].selector = @selector(init__WithDePidataRailTrackWagonCfg:);
  methods[17].selector = @selector(setLastBlockWithPIRR_RailBlock:withLong:);
  methods[18].selector = @selector(getWagonImage);
  methods[19].selector = @selector(getWagonCfg);
  #pragma clang diagnostic pop
  static const J2ObjcFieldInfo fields[] = {
    { "NAMESPACE", "LPIQ_Namespace;", .constantValue.asLong = 0, 0x19, -1, 19, -1, -1 },
    { "ID_ID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 20, -1, -1 },
    { "ID_LASTBLOCKID", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 21, -1, -1 },
    { "ID_LASTBLOCKMS", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 22, -1, -1 },
    { "ID_LEN", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 23, -1, -1 },
    { "ID_NAME", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 24, -1, -1 },
    { "ID_TAGPOS", "LPIQ_QName;", .constantValue.asLong = 0, 0x19, -1, 25, -1, -1 },
    { "imageID_", "LPIQ_QName;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
    { "wagonCfg_", "LDePidataRailTrackWagonCfg;", .constantValue.asLong = 0, 0x2, -1, -1, -1, -1 },
  };
  static const void *ptrTable[] = { "LPIQ_Key;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "LPIQ_Key;LPIMT_ComplexType;[LNSObject;LJavaUtilHashtable;LPIMR_ChildList;", "(Lde/pidata/qnames/Key;Lde/pidata/models/types/ComplexType;[Ljava/lang/Object;Ljava/util/Hashtable<Lde/pidata/qnames/QName;Ljava/lang/Object;>;Lde/pidata/models/tree/ChildList;)V", "setId", "LPIQ_QName;", "setName", "LNSString;", "setLen", "LJavaLangInteger;", "setTagPos", "setLastBlockID", "setLastBlockMs", "LJavaLangLong;", "LPIQ_QName;LPIQ_QName;", "init", "LDePidataRailTrackWagonCfg;", "setLastBlock", "LPIRR_RailBlock;J", &PIRR_Wagon_NAMESPACE, &PIRR_Wagon_ID_ID, &PIRR_Wagon_ID_LASTBLOCKID, &PIRR_Wagon_ID_LASTBLOCKMS, &PIRR_Wagon_ID_LEN, &PIRR_Wagon_ID_NAME, &PIRR_Wagon_ID_TAGPOS };
  static const J2ObjcClassInfo _PIRR_Wagon = { "Wagon", "de.pidata.rail.railway", ptrTable, methods, fields, 7, 0x1, 20, 9, -1, -1, -1, -1, -1 };
  return &_PIRR_Wagon;
}

+ (void)initialize {
  if (self == [PIRR_Wagon class]) {
    PIRR_Wagon_NAMESPACE = PIQ_Namespace_getInstanceWithNSString_(@"http://res.pirail.org/railway.xsd");
    PIRR_Wagon_ID_ID = [((PIQ_Namespace *) nil_chk(PIRR_Wagon_NAMESPACE)) getQNameWithNSString:@"id"];
    PIRR_Wagon_ID_LASTBLOCKID = [PIRR_Wagon_NAMESPACE getQNameWithNSString:@"lastBlockID"];
    PIRR_Wagon_ID_LASTBLOCKMS = [PIRR_Wagon_NAMESPACE getQNameWithNSString:@"lastBlockMs"];
    PIRR_Wagon_ID_LEN = [PIRR_Wagon_NAMESPACE getQNameWithNSString:@"len"];
    PIRR_Wagon_ID_NAME = [PIRR_Wagon_NAMESPACE getQNameWithNSString:@"name"];
    PIRR_Wagon_ID_TAGPOS = [PIRR_Wagon_NAMESPACE getQNameWithNSString:@"tagPos"];
    J2OBJC_SET_INITIALIZED(PIRR_Wagon)
  }
}

@end

void PIRR_Wagon_init(PIRR_Wagon *self) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, nil, JreLoadStatic(PIRR_RailwayFactory, WAGON_TYPE), nil, nil, nil);
  self->wagonCfg_ = nil;
}

PIRR_Wagon *new_PIRR_Wagon_init() {
  J2OBJC_NEW_IMPL(PIRR_Wagon, init)
}

PIRR_Wagon *create_PIRR_Wagon_init() {
  J2OBJC_CREATE_IMPL(PIRR_Wagon, init)
}

void PIRR_Wagon_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_Wagon *self, id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, JreLoadStatic(PIRR_RailwayFactory, WAGON_TYPE), attributeNames, anyAttribs, childNames);
  self->wagonCfg_ = nil;
}

PIRR_Wagon *new_PIRR_Wagon_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRR_Wagon, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

PIRR_Wagon *create_PIRR_Wagon_initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRR_Wagon, initWithPIQ_Key_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, attributeNames, anyAttribs, childNames)
}

void PIRR_Wagon_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(PIRR_Wagon *self, id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  PIMR_SequenceModel_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(self, key, type, attributeNames, anyAttribs, childNames);
  self->wagonCfg_ = nil;
}

PIRR_Wagon *new_PIRR_Wagon_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_NEW_IMPL(PIRR_Wagon, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

PIRR_Wagon *create_PIRR_Wagon_initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_(id<PIQ_Key> key, id<PIMT_ComplexType> type, IOSObjectArray *attributeNames, JavaUtilHashtable *anyAttribs, PIMR_ChildList *childNames) {
  J2OBJC_CREATE_IMPL(PIRR_Wagon, initWithPIQ_Key_withPIMT_ComplexType_withNSObjectArray_withJavaUtilHashtable_withPIMR_ChildList_, key, type, attributeNames, anyAttribs, childNames)
}

void PIRR_Wagon_initWithPIQ_QName_withPIQ_QName_(PIRR_Wagon *self, PIQ_QName *wagonID, PIQ_QName *wagonImageID) {
  PIRR_Wagon_init(self);
  [self setIdWithPIQ_QName:wagonID];
  self->imageID_ = wagonImageID;
}

PIRR_Wagon *new_PIRR_Wagon_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *wagonID, PIQ_QName *wagonImageID) {
  J2OBJC_NEW_IMPL(PIRR_Wagon, initWithPIQ_QName_withPIQ_QName_, wagonID, wagonImageID)
}

PIRR_Wagon *create_PIRR_Wagon_initWithPIQ_QName_withPIQ_QName_(PIQ_QName *wagonID, PIQ_QName *wagonImageID) {
  J2OBJC_CREATE_IMPL(PIRR_Wagon, initWithPIQ_QName_withPIQ_QName_, wagonID, wagonImageID)
}

J2OBJC_CLASS_TYPE_LITERAL_SOURCE(PIRR_Wagon)

J2OBJC_NAME_MAPPING(PIRR_Wagon, "de.pidata.rail.railway", "PIRR_")
