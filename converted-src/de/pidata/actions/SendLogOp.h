//
//  Generated by the J2ObjC translator.  DO NOT EDIT!
//  source: ../pi-rail-client/pi-rail-client-base/src/de/pidata/actions/SendLogOp.java
//

#include "J2ObjC_header.h"

#pragma push_macro("INCLUDE_ALL_DePidataActionsSendLogOp")
#ifdef RESTRICT_DePidataActionsSendLogOp
#define INCLUDE_ALL_DePidataActionsSendLogOp 0
#else
#define INCLUDE_ALL_DePidataActionsSendLogOp 1
#endif
#undef RESTRICT_DePidataActionsSendLogOp

#if !defined (DePidataActionsSendLogOp_) && (INCLUDE_ALL_DePidataActionsSendLogOp || defined(INCLUDE_DePidataActionsSendLogOp))
#define DePidataActionsSendLogOp_

#define RESTRICT_DePidataGuiControllerBaseGuiOperation 1
#define INCLUDE_PIGC_GuiOperation 1
#include "de/pidata/gui/controller/base/GuiOperation.h"

@class PIQ_QName;
@protocol PIGC_Controller;
@protocol PIMR_Model;

@interface DePidataActionsSendLogOp : PIGC_GuiOperation

#pragma mark Public

- (instancetype)init;

- (void)executeWithPIQ_QName:(PIQ_QName *)eventID
         withPIGC_Controller:(id<PIGC_Controller>)sourceCtrl
              withPIMR_Model:(id<PIMR_Model>)dataContext;

@end

J2OBJC_EMPTY_STATIC_INIT(DePidataActionsSendLogOp)

FOUNDATION_EXPORT void DePidataActionsSendLogOp_init(DePidataActionsSendLogOp *self);

FOUNDATION_EXPORT DePidataActionsSendLogOp *new_DePidataActionsSendLogOp_init(void) NS_RETURNS_RETAINED;

FOUNDATION_EXPORT DePidataActionsSendLogOp *create_DePidataActionsSendLogOp_init(void);

J2OBJC_TYPE_LITERAL_HEADER(DePidataActionsSendLogOp)

#endif

#pragma pop_macro("INCLUDE_ALL_DePidataActionsSendLogOp")
