# PI-Rail-iOS

PI-Rail client for iOS (iPhone an iPad), based on [PI-Rail-Client](https://gitlab.com/pi-rail/pi-rail-client) and [PI-Mobile iOS](https://gitlab.com/pi-mobile/pi-mobile-ios)

This is the java part of the PI-Rail client for iOS. It's main job is the cross platform code generation using [Goolgle j2objc](https://developers.google.com/j2objc)

Please be patient we have just started - documentation will be available soon. 

See [PI-Rail-FX](https://gitlab.com/pi-rail/pi-rail-fx) for documentation on desktop app and [PI-Rail-Arduino](https://gitlab.com/pi-rail/pi-rail-arduino) for documentation on firmware.
  